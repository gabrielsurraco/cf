Const OverwriteExisting = TRUE

Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objShell = CreateObject("Shell.Application")
Const FOF_CREATEPROGRESSDLG = &H0&

If (objFSO.FileExists("\\decfcg-bascula\EXPOTEC\Data\CG\Balanza.dat")) Then
   set f = objFSO.GetFile("\\decfcg-bascula\EXPOTEC\Data\CG\Balanza.dat")
   GetFileSizeCGOrigen = f.size 
   
   Set objFolder = objShell.NameSpace("D:\EXPOTEC\Data\CG\")
   objFolder.CopyHere "\\decfcg-bascula\EXPOTEC\Data\CG\Balanza.dat", FOF_CREATEPROGRESSDLG  

   set f2 = objFSO.GetFile("D:\EXPOTEC\Data\CG\Balanza.dat")
   GetFileSizeCGDestino = f2.size
	if GetFileSizeCGOrigen <> GetFileSizeCGDestino Then
	  WScript.echo "CG SE COPIO CON ERRORES!"
	End if
Else
   msg = "CG No se pudo copiar."
   WScript.Echo msg
End If

If (objFSO.FileExists("\\decfcg-bascula\EXPOTEC\Data\DCG\Balanza.dat")) Then
   set f = objFSO.GetFile("\\decfcg-bascula\EXPOTEC\Data\DCG\Balanza.dat")
   GetFileSizeDCGOrigen = f.size
   
   Set objFolder = objShell.NameSpace("D:\EXPOTEC\Data\DCG\")
   objFolder.CopyHere "\\decfcg-bascula\EXPOTEC\Data\DCG\Balanza.dat", FOF_CREATEPROGRESSDLG
   
   set f2 = objFSO.GetFile("D:\EXPOTEC\Data\DCG\Balanza.dat")
   GetFileSizeDCGDestino = f2.size
   if GetFileSizeDCGOrigen <> GetFileSizeDCGDestino Then
	  WScript.echo "DCG SE COPIO CON ERRORES!"
   End if
	
Else
   msg = "DCG No se pudo copiar."
   WScript.Echo msg
End If

If (objFSO.FileExists("\\decfta-bascula\EXPOTEC\Data\TA\Balanza.dat")) Then
   set f = objFSO.GetFile("\\decfta-bascula\EXPOTEC\Data\TA\Balanza.dat")
   GetFileSizeTAOrigen = f.size
   
   Set objFolder = objShell.NameSpace("D:\EXPOTEC\Data\TA\")
   objFolder.CopyHere "\\decfta-bascula\EXPOTEC\Data\TA\Balanza.dat", FOF_CREATEPROGRESSDLG
   
   
   set f2 = objFSO.GetFile("D:\EXPOTEC\Data\TA\Balanza.dat")
   GetFileSizeTADestino = f2.size
   if GetFileSizeTAOrigen <> GetFileSizeTADestino Then
	  WScript.echo "TA SE COPIO CON ERRORES!"
	End if
   
Else
   msg = "TA No se pudo copiar."
   WScript.Echo msg
End If

If (objFSO.FileExists("\\decfcv-bascula\EXPOTEC\Data\CV\Balanza.dat")) Then
	set f = objFSO.GetFile("\\decfcv-bascula\EXPOTEC\Data\CV\Balanza.dat")
   GetFileSizeCVOrigen = f.size
   
   Set objFolder = objShell.NameSpace("D:\EXPOTEC\Data\CV\")
   objFolder.CopyHere "\\decfcv-bascula\EXPOTEC\Data\CV\Balanza.dat", FOF_CREATEPROGRESSDLG

   
  set f2 = objFSO.GetFile("D:\EXPOTEC\Data\CV\Balanza.dat")
  GetFileSizeCVDestino = f2.size
   if GetFileSizeCVOrigen <> GetFileSizeCVDestino Then
	  WScript.echo "CV SE COPIO CON ERRORES!"
	End if
   
Else
   msg = "CV No se pudo copiar."
   WScript.Echo msg
End If

If (objFSO.FileExists("\\decfcv-bascula\EXPOTEC\Data\CVM\Balanza.dat")) Then
   set f = objFSO.GetFile("\\decfcv-bascula\EXPOTEC\Data\CVM\Balanza.dat")
   GetFileSizeCVMOrigen = f.size
   
   Set objFolder = objShell.NameSpace("D:\EXPOTEC\Data\CVM\")
   objFolder.CopyHere "\\decfcv-bascula\EXPOTEC\Data\CVM\Balanza.dat", FOF_CREATEPROGRESSDLG

   
   set f2 = objFSO.GetFile("D:\EXPOTEC\Data\CVM\Balanza.dat")
   GetFileSizeCVMDestino = f2.size
   if GetFileSizeCVMOrigen <> GetFileSizeCVMDestino Then
	  WScript.echo "CVM SE COPIO CON ERRORES!"
	End if
   
Else
   msg = "CVM No se pudo copiar."
   WScript.Echo msg
End If

If (objFSO.FileExists("\\decfaca-bascula\EXPOTEC\Data\CA\Balanza.dat")) Then
	set f = objFSO.GetFile("\\decfaca-bascula\EXPOTEC\Data\CA\Balanza.dat")
   GetFileSizeCAOrigen = f.size
   
   Set objFolder = objShell.NameSpace("D:\EXPOTEC\Data\CA\")
   objFolder.CopyHere "\\decfaca-bascula\EXPOTEC\Data\CA\Balanza.dat", FOF_CREATEPROGRESSDLG

   
   set f2 = objFSO.GetFile("D:\EXPOTEC\Data\CA\Balanza.dat")
   GetFileSizeCADestino = f2.size
   if GetFileSizeCAOrigen <> GetFileSizeCADestino Then
	  WScript.echo "CA SE COPIO CON ERRORES!"
	End if
   
Else
   msg = "CA No se pudo copiar."
   WScript.Echo msg
End If

