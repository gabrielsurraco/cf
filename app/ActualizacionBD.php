<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class ActualizacionBD extends Model
{
    protected $table = 'BALANZASTEMP';

    protected $guarded = ['ID'];

    public static $ID = 'ID';
    public static $DESCRIPCION = 'Descripcion';
    public static $UBICACION = 'Ubicacion';
    public static $NRO_PUNTO_DE_VENTA = 'Nro_Punto_De_Venta';
    public static $NRO_FDA = 'Nro_FDA';
    public static $NOMENCLATURA = 'Nomenclatura';
    public static $DOMICILIO = 'Domicilio';
    public static $MEMBRETE = 'Membrete';
    public static $OBSERVACIONES = '_Observaciones';
    public static $ESTADO = 'Estado';
    public static $FECHA_SINCRONIZACION = 'Fecha_Sincronizacion';
    public static $USUARIO_SINCRONIZACION = 'Usuario_Sincronizacion';
    public static $CREADO_POR = '_Creado_por';
    public static $FECHA_CREACION = '_Fecha_Creacion';
    public static $MODIFICADO_POR = '_Modificado_por';
    public static $FECHA_MODIFICACION = '_Fecha_Modificacion';

    public function getFechaActualizacionAttribute( $value ) {
        return (new Carbon($value))->format('d/m/Y');
    }

    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }

//    public function setFechaActualizacionAttribute( $value ) {
//        $fecha =  Carbon::createFromFormat('d/m/Y', $value); //->toDateTimeString()
//        $this->attributes['fecha_actualizacion'] = $fecha;
//    }

//    public function getUpdateAtAttribute( $value ) {
//        return (new Carbon($value))->format('d/m/Y');
//    }
//
//    public function setUpdatedAtAttribute( $value ) {
//        $fecha =  Carbon::createFromFormat('d/m/Y', $value); //->toDateTimeString()
//        $this->attributes['updated_at'] = $fecha;
//                //$fecha->format('Y-m-d');
//    }



}
