<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bolson extends Model
{
    protected $table = 'bolsones';

    protected $guarded = [];

    PUBLIC STATIC $ID                 = "id";
    PUBLIC STATIC $FECHA              = "Fecha";
    PUBLIC STATIC $ID_BOLSON          = "_IDBolson";
    PUBLIC STATIC $ID_BALANZA         = "_IDBalanza";
    PUBLIC STATIC $ID_PRODUCTO        = "_IDProducto";
    PUBLIC STATIC $CANTIDAD           = "Cantidad";
    PUBLIC STATIC $M3                 = "M3";
    PUBLIC STATIC $BOLSON             = "Bolson";
    PUBLIC STATIC $PLANTA_ORIGEN      = "Planta_origen";
    PUBLIC STATIC $DENSIDAD           = "Densidad";
    PUBLIC STATIC $PORC_FINO          = "Porc_fino";
    PUBLIC STATIC $CAPATAZ            = "Capataz";
    PUBLIC STATIC $ANIOZAFRA          = "AnioZafra";
    PUBLIC STATIC $IMPRESORA          = "Impresora";
    PUBLIC STATIC $MANUAL             = "Manual";
    PUBLIC STATIC $IDPESAJEORIGEN     = "_IDPesajeOrigen";
    PUBLIC STATIC $NROTICKETORIGEN    = "NroTicketOrigen";
    PUBLIC STATIC $SINCRONIZADO       = "Sincronizado";
    PUBLIC STATIC $NRO_LOTE           = "Nro_Lote";
    PUBLIC STATIC $CREADO_POR         = "_Creado_por";
    PUBLIC STATIC $FECHA_CREACION     = "_Fecha_Creacion";
    PUBLIC STATIC $MODIFICADO_POR     = "_Modificado_por";
    PUBLIC STATIC $FECHA_MODIFICACION = "_Fecha_Modificacion";
    PUBLIC STATIC $OBSERVACIONES      = "_Observaciones";

    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }
}
