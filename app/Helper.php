<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;
use App\Model\ACA\ACA_Rodado;
use Illuminate\Support\Facades\DB;
use App\Planta;

/**
 * Description of Helper
 *
 * @author gabriel.surraco
 */
class Helper {
    
    //CORREGIR YA QUE ESTO ES EL CASO IDEAL NO EL REAL
    public static function getRodadoName($id){
        return ACA_Rodado::findOrFail($id)->codigo;
    }
    
    public static function getProductoName($id){
        return DB::connection('EXPOTEC_STOCK')
            ->table('ARTICULOS')
            ->select(DB::raw("Codigo+' - '+Descripcion AS nombre"))
            ->where('ID', $id)
            ->first()->nombre;
    }
    
    public static function getProveedorName($id){
        if($id == "" || $id == null || empty($id) )
            return "";
        
//        if($id == 324){
//            return "ARGENTE";
//        }
        
        $contador = DB::connection('EXPOTEC_COMPRAS')
        ->table('PROVEEDORES')
        ->select(DB::raw("Codigo+' - '+Razon_Social AS nombre"))
        ->where('ID', $id)
        ->count();
        
        if($contador == 0)
            return "Codigo " . $id . " Inexistente";
        
        return DB::connection('EXPOTEC_COMPRAS')
        ->table('PROVEEDORES')
        ->select(DB::raw("Codigo+' - '+Razon_Social AS nombre"))
        ->where('ID', $id)
        ->first()->nombre;
        
    }
    
    public static function getPlantaName($id){
        if($id == "" || $id == null || empty($id))
            return "";
        return Planta::where('id_planta',$id)->first()["descripcion"];
    }
    
}
