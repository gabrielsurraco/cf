<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ActualizacionBD;
use App\Model\ACA\ACA_Rodado;
use Illuminate\Support\Facades\Response;
use App\Model\ACA\ACA_Embalaje;
use PDOException;
use App\Model\ACA\ACA_Tipo_Humedad;
use Illuminate\Support\Facades\Validator;
use App\Model\ACA\ACA_Producto;
use Illuminate\Support\Facades\DB;
use App\Model\ACA\ACA_Pesaje;
use App\Model\ACA\ACA_Chacra;
use App\Model\ACA\ACA_Lote;
use App\Model\ACA\ACA_Tipo_Comprobante;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use App\Model\ACA\ACA_Proveedor;
use App\Planta;
use App\Permission;
use App\Role;
use Illuminate\Support\Facades\Auth;
use App\Model\ACA\ACA_Mov_Stock;
use App\Mov_Stock;
use Illuminate\Support\Facades\View;
use App\Helper;

class BalanzaController extends Controller
{
    public function index(Request $request){
        $pesajes        = ACA_Pesaje::where("fecha","!=",NULL)->where("fecha_anulacion",NULL)->paginate(5);
        
        $pesajes_fix                     = array();
        $pesajes_fix["total"]            = $pesajes->total();
        $pesajes_fix["last_page"]        = $pesajes->lastPage();
//        $pesajes_fix["data"]             = $pesajes->items();
        
        $p = array();
        foreach($pesajes as $pesaje){
            $p[] = array(
                "id"                => $pesaje->id,
                "id_bd"             => $pesaje->id_bd,
                "id_pesaje"         => $pesaje->id_pesaje,
                "id_talonario"      => $pesaje->id_talonario,
                "nro_comprobante"   => $pesaje->nro_comprobante,
                "fecha"             => $pesaje->fecha,
                "id_rodado"         => array("id" => $pesaje->id_rodado, "codigo" => $pesaje->camion->codigo),
                "patente_acoplado"  => $pesaje->patente_acoplado,
                "conductor"         => $pesaje->conductor,
                "id_producto"       => array("id" => $pesaje->id_producto, "nombre" => Helper::getProductoName($pesaje->id_producto)),
                "id_planta_origen"  => array("id" => $pesaje->id_planta_origen, "nombre" => Helper::getPlantaName($pesaje->id_planta_origen)),
                "id_planta_destino" => array("id" => $pesaje->id_planta_destino, "nombre" => Helper::getPlantaName($pesaje->id_planta_destino)),
                "id_proveedor"      => array("id" => $pesaje->id_proveedor, "nombre" => Helper::getProveedorName($pesaje->id_proveedor)),
                "bruto"             => $pesaje->bruto,
                "tara"              => $pesaje->tara,
                "neto"              => $pesaje->neto,
                "estado"            => $pesaje->estado,
                "id_chacra"         => $pesaje->id_chacra,
                "te_certificado"    => $pesaje->te_certificado,
                "expo"              => $pesaje->expo
            );
        }
        
        $pesajes_fix["data"] = $p;
        
//        dd($p);
        $pesajes_fix["perPage"]          = $pesajes->perPage();
        $pesajes_fix["current_page"]     = $pesajes->currentPage();
        $pesajes_fix["next_page_url"]    = $pesajes->nextPageUrl();
        $pesajes_fix["prev_page_url"]    = $pesajes->previousPageUrl();
        
//        dd($pesajes_fix); 
//        die();
        
        if ($request->ajax()) {
            return response()->json($pesajes_fix, 200);
        }
        
        return view("balanza.index_balanza", compact("pesajes"));
    }
    
    public function nuevoPesaje(){
        
        if(Auth::user()->hasRole("ADMINISTRADOR"))
            $plantas = ActualizacionBD::where("estado",true)->get();
        
        if(Auth::user()->hasRole("PESADOR_CV"))
            $plantas = ActualizacionBD::whereIn("nombre_bd",array("CV","CVII","CVM"))->get();
        
        if(Auth::user()->hasRole("PESADOR_CG"))
            $plantas = ActualizacionBD::whereIn("nombre_bd",array("CG","DCG"))->get();
        
        if(Auth::user()->hasRole("PESADOR_OBERA"))
            $plantas = ActualizacionBD::whereIn("nombre_bd",array("DB","DCH","DCR","OBIT","OBMY","OBSO","OBTO"))->get();
        
        if(Auth::user()->hasRole("PESADOR_TABAY"))
            $plantas = ActualizacionBD::whereIn("nombre_bd",array("TA"))->get();
        
        if(Auth::user()->hasRole("PESADOR_ACARAGUA"))
            $plantas = ActualizacionBD::whereIn("nombre_bd",array("ACA"))->get();
        
            
        $factories = array();
        foreach($plantas as $planta => $value){
            $factories[$value->id] = $value->descripcion;
        }
        return view("balanza.nuevo_pesaje", compact('factories'));
    }
    
    public function getEmbalaje(Request $planta){
        $id_bascula = $planta["planta"];
        
        try{
            $embalajes = ACA_Embalaje::select("id","id_embalaje","descripcion","peso")->where('id_bd',$id_bascula)->get();
            $array_embalajes = array();
            foreach($embalajes as $key => $value){
                $array_embalajes[] = array("id" => $value->id ,"id_embalaje" =>$value->id_embalaje ,"descripcion" => $value->descripcion, "peso" => round($value->peso,2));
            }
        } catch (\PDOException $ex) {
            return Response::json(array('resultado' => $ex->getMessage()),500);
        }
        
        return Response::json(array('resultado' => $array_embalajes),200);
    }
    
    public function getHumedad(Request $planta){
        $id_bascula = $planta["planta"];
        
        try{
            $tipo_humedades = ACA_Tipo_Humedad::select("id","id_tipo_humedad","descripcion","porcentaje")->where('id_bd',$id_bascula)->orderBy('porcentaje',"asc")->get();
            $array_humedades = array();
            foreach($tipo_humedades as $key => $value){
                $array_humedades[] = array("id" => $value->id ,"id_tipo_humedad" => $value->id_tipo_humedad ,"descripcion" => $value->descripcion, "porcentaje" => $value->porcentaje);
            }
        } catch (\PDOException $ex) {
            return Response::json(array('resultado' => $ex->getMessage()),500);
        }
        
        return Response::json(array('resultado' => $array_humedades),200);
    }

    public function getPeso(Request $planta){
        $id_bascula = $planta["planta"];
        
        if(in_array($id_bascula, array(1))){
            $path = public_path() . "/balanzas/aca/Rx.txt";
        }
        
        if(in_array($id_bascula, array(2,9))){
            $path = public_path() . "/balanzas/cg/Rx.txt";
        }
        
        if(in_array($id_bascula, array(3,7,6))){
            $path = public_path() . "/balanzas/cv/Rx.txt";
        }
        
        if(in_array($id_bascula, array(5))){
            $path = public_path() . "/balanzas/ta/Rx.txt";
        }
        
        if(in_array($id_bascula, array(4,8,10,11,12,13,15))){
           $path = public_path() . "/balanzas/obso/Rx.txt";
        }
        
        
        
//        switch ($id_bascula){
//            case 1: //Acaragua
//                $path = public_path() . "/balanzas/aca/Rx.txt";
//                break;
//            case 2: //Campo Grande
//                $path = public_path() . "/balanzas/cg/Rx.txt";
//                break;
//            case 3: //Campo Viera
//                $path = public_path() . "/balanzas/cv/Rx.txt";
//                break;
//            case 4: //Obera Secadero
//                $path = public_path() . "/balanzas/obso/Rx.txt";
//                break;
//            case 5: //Tabay
//                $path = public_path() . "/balanzas/ta/Rx.txt";
//                break;
//            default:
//                $path = null;
//        }
        
        $fp = fopen($path, "r");
        $resultado = array();
        $cont = 0;
        
        while(!feof($fp)){
            $resultado[$cont] = fgets($fp);
            $cont++;
        }
        
        fclose($fp);
        $neto = explode(" ", $resultado[1]);
        $neto_int = (int) $neto[7];
        
        return Response::json(array('peso' => $neto_int),200);
    }
    
    public function backWithValues($request, $errores){
        if(isset($request->camion)){
                $rodados = ACA_Rodado::select('id',DB::raw("codigo+' - '+descripcion AS text"))
                ->where('id_bd', $request->planta)
                ->where('id', $request->camion)
                ->get();
            }
            
            if(isset($request->producto)){
                $productos =    DB::connection('EXPOTEC_STOCK')
                        ->table('ARTICULOS')
                        ->select('ID as id',DB::raw("Codigo+' - '+Descripcion AS text"))
                        ->where('Activo', true)
                        ->where('ID', $request->producto)
                        ->get();
            }
            
            if(isset($request->proveedor)){
                $proveedores =  DB::connection('EXPOTEC_COMPRAS')
                        ->table('PROVEEDORES')
                        ->select('ID as id',DB::raw("Codigo+' - '+Razon_Social AS text"))
                        ->where('Activo', true)
                        ->where('ID', $request->proveedor)
                        ->get();
            }
            
            if(isset($request->planta_origen)){
                $planta_origen = Planta::select("id","descripcion as text")
                        ->where('id',$request->planta_origen)
                        ->get();
            }
            
            if(isset($request->planta_destino)){
                $planta_destino = Planta::select("id","descripcion as text")
                        ->where('id',$request->planta_destino)
                        ->get();
            }
            
            if(isset($request->embalaje_tipo) and $request->embalaje_tipo != "default"){
                $embalaje_tipo = ACA_Embalaje::select("id","descripcion","peso")
                        ->where('id_bd',$request->planta)
                        ->where('id', $request->embalaje_tipo)
                        ->get();
            }
            
            return redirect()->back()->withInput($request->toArray())->with(compact("rodados","productos","proveedores","planta_origen","planta_destino","embalaje_tipo"))->withErrors($errores);
    }

    public function storePesaje(Request $request){
        
        $v =    Validator::make(Input::all(), [
                    'planta'        => 'required',
                    'producto'      => 'required',
                    'fecha'         => 'required',
                    'camion'        => 'required',
                    'planta_origen' => 'required_without:proveedor',
                    'planta_destino' => 'required',
                    'proveedor'     => 'required_without:planta_origen'
                ]);
        
        if ($v->fails()){
            return Response::json( array("error" => $v->errors()) , 400);
        }
        
        if(isset($request->proveedor) && isset($request->planta_origen)){
            return Response::json( array("error" => array('planta_origen' => 'Planta Origen y Proveedor no pueden estar seleccionados.')) , 400);
        }
        
        if(isset($request->estado)){
            if(isset($request->bruto) && isset($request->tara)){
                $bruto = $request->bruto;
                $tara = $request->tara;
                
                if(($bruto >= 1) && ($tara >= 1)){
                    $estado_pesaje = 2; // COMPLETO
                    $fecha_bruto = date("d/m/Y H:i");
                }else{
                    return Response::json( array("pesaje_completo" => "Para guardar como pesaje completo el BRUTO y la TARA debe ser mayor a 1.") , 400);
                }
            }
            
        }else{
            if(isset($request->bruto) && isset($request->tara)){
                $bruto = $request->bruto;
                $tara = $request->tara;
                if(($bruto >= 1) && ($tara >= 1)){
                    $estado_pesaje = 0; // UNIDAD CARGADA
                    $fecha_bruto = date("d/m/Y H:i");
                }
                
                if(($bruto >= 1) && ($tara == 0)){
                    $estado_pesaje = 0; // UNIDAD CARGADA
                    $fecha_bruto = date("d/m/Y H:i");
                }
                
                if(($bruto == 0) && ($tara >= 1)){
                    $estado_pesaje = 1; // UNIDAD DESCARGADA
                    $fecha_bruto = NULL;
                }
            }
            
        }
        $producto = DB::connection('EXPOTEC_STOCK')
                    ->table('ARTICULOS')
                    ->where('ID',$request->producto)
                    ->pluck("Codigo");
//        $producto = ACA_Producto::where('id_bd',$request->planta)->where('id',$request->producto)->pluck("codigo");
        
        //SI EL PRODUCTO ES TV USO EL TALONARIO 'TICKET TV' SINO 'OTROS TALONARIOS'
        if($producto[0] === 'TV01' || $producto[0] === 'TV02'){
            $id_talonario = 8;
            
            $v =    Validator::make(Input::all(), [
                    'tamanio'       => 'required',
                    'malezas'       => 'required',
                    'calidad'       => 'required',
                    'acaros'        => 'required',
                    'temperatura'   => 'required',
                    'humedad_hoja'  => 'required'
                ]);
        
            if ($v->fails()){
                return Response::json( array("error" => $v->errors()) , 400);
            }
            //Verificar si estan cargadas las variables de calidad y Humedad
            
            if($producto[0] === 'TV02'){
                $v =    Validator::make(Input::all(), [
                    'lote'          => 'required',
                    'chacra'        => 'required',
                ]);
        
                if ($v->fails()){
                    return Response::json( array("error" => $v->errors()) , 400);
                }
            }
            
        }else{
            $id_talonario = 2;
        }
        
        
        if(isset($request->expo)){
                $v =    Validator::make(Input::all(), [
                'buque'             => 'required',
                'reserva'           => 'required',
                'codigo_aduana'     => 'required',
                'nro_certificado'   => 'required',
                'nro_contenedor'    => 'required',
                'permiso_embarque'  => 'required'
                ]);
        
            if ($v->fails()){
                return Response::json( array("error" => $v->errors()) , 400);
            }
            $expo = 1;
        }else{
            $expo = 0;
        }
        
        if(isset($request->derivado)){
            $v =    Validator::make(Input::all(), [
                    'derivado_a'    => 'required',
                ]);
        
            if ($v->fails()){
                return Response::json( array("error" => $v->errors()) , 400);
            }
        }
        
        if(isset($request->planta_origen) && $request->planta_origen != ""){
                $planta_origen = Planta::findOrFail($request->planta_origen)->id_planta;
            }else{
                $planta_origen = 0;
            }
            
            if(isset($request->planta_destino)){
                $planta_destino = Planta::findOrFail($request->planta_destino)->id_planta;
            }else{
                $planta_destino = 0;
            }
            
            if(isset($request->proveedor)){
//                $proveedor = ACA_Proveedor::findOrFail($request->proveedor)->id_proveedor;
                $proveedor = $request->proveedor;
            }else{
                $proveedor = 0;
            }
            
            if(isset($request->embalaje_tipo) && $request->embalaje_tipo != 'default'){
                $embalaje = ACA_Embalaje::findOrFail($request->embalaje_tipo)->id_embalaje;
            }else{
                $embalaje = 0;
            }
            
            if(isset($request->humedad_hoja)){
                $id_humedad     = ACA_Tipo_Humedad::findOrFail($request->humedad_hoja)->id_tipo_humedad;
                $porc_humedad   = ACA_Tipo_Humedad::findOrFail($request->humedad_hoja)->porcentaje;
            }else{
                $id_humedad     = 0;
                $porc_humedad   = 0;
            }
            
            if(isset($request->bruto)){
                $bruto     = $request->bruto;
            }else{
                $bruto     = 0;
            }
            
            if(isset($request->tara)){
                $tara     = $request->tara;
            }else{
                $tara     = 0;
            }
            
            if(isset($request->humedad_peso)){
                $humedad_peso     = $request->humedad_peso;
            }else{
                $humedad_peso     = 0;
            }
            
            if(isset($request->embalaje_peso)){
                $embalaje_peso     = $request->embalaje_peso;
            }else{
                $embalaje_peso     = 0;
            }
            
            if(isset($request->neto)){
                $neto     = $request->neto;
            }else{
                $neto     = 0;
            }
            
            if(isset($request->chacra) && $request->chacra != ""){
                $chacra     = ACA_Chacra::findOrFail($request->chacra)->id_chacra;
            }else{
                $chacra     = 0;
            }
            
            if(isset($request->certificado)){
                $certificado     = 1;
            }else{
                $certificado     = 0;
            }
            
            if(isset($request->lote) && $request->lote != ""){
                //En este caso el LOTE es un array estático en el expotec, por lo tanto insertar
                //el valor ingresado directamente. La tabla Lotes hace referencia
                //a los lotes envasados en ITALIA o CG.
                $lote     = $request->lote;
            }else{
                $lote     = NULL;
            }
            
            if(isset($request->tamanio)){
                $tamanio     = $request->tamanio;
            }else{
                $tamanio     = NULL;
            }
            
            if(isset($request->malezas)){
                $malezas     = $request->malezas;
            }else{
                $malezas     = NULL;
            }
            
            if(isset($request->calidad)){
                $calidad     = $request->calidad;
            }else{
                $calidad     = NULL;
            }
            
            if(isset($request->acaros)){
                $acaros     = $request->acaros;
            }else{
                $acaros     = NULL;
            }
            
            if(isset($request->temperatura)){
                $temperatura     = $request->temperatura;
            }else{
                $temperatura     = NULL;
            }
            
            if(isset($request->embalaje_tipo) && $request->embalaje_tipo != "default"){
                $peso_emb_x_unidad     = ACA_Embalaje::findOrFail($request->embalaje_tipo)->peso;
            }else{
                $peso_emb_x_unidad     = NULL;
            }
            
            if(isset($request->porcentaje_palo) && $request->porcentaje_palo != ""){
                $porc_palo     = $request->porcentaje_palo;
            }else{
                $porc_palo     = NULL;
            }
            
            if(isset($request->buque) && $request->buque != ""){
                $buque     = $request->buque;
            }else{
                $buque     = NULL;
            }
//            
            
            if(isset($request->reserva) && $request->reserva != ""){
                $reserva     = $request->reserva;
            }else{
                $reserva     = NULL;
            }
            
            if(isset($request->codigo_aduana) && $request->codigo_aduana != ""){
                $codigo_aduana     = $request->codigo_aduana;
            }else{
                $codigo_aduana     = NULL;
            }
            
            if(isset($request->codigo_balanza) && $request->codigo_balanza != ""){
                $codigo_balanza     = $request->codigo_balanza;
            }else{
                $codigo_balanza     = NULL;
            }
            
            if(isset($request->nro_certificado) && $request->nro_certificado != ""){
                $nro_certificado     = $request->nro_certificado;
            }else{
                $nro_certificado     = NULL;
            }
            
            if(isset($request->nro_contenedor) && $request->nro_contenedor != ""){
                $nro_contenedor     = $request->nro_contenedor;
            }else{
                $nro_contenedor     = NULL;
            }
            
            if(isset($request->vencimiento) && $request->vencimiento != ""){
                $vencimiento     = $request->vencimiento;
            }else{
                $vencimiento     = NULL;
            }
            
            if(isset($request->precintos) && $request->precintos != ""){
                $precintos     = $request->precintos;
            }else{
                $precintos     = NULL;
            }
            
            if(isset($request->linea) && $request->linea != ""){
                $linea    = $request->linea;
            }else{
                $linea     = NULL;
            }
            
            if(isset($request->otros) && $request->otros != ""){
                $otros    = $request->otros;
            }else{
                $otros     = NULL;
            }
            
            if(isset($request->cuit) && $request->cuit != ""){
                $cuit   = $request->cuit;
            }else{
                $cuit     = NULL;
            }
            
            if(isset($request->apellido_nombre) && $request->apellido_nombre != ""){
                $apellido_nombre   = $request->apellido_nombre;
            }else{
                $apellido_nombre     = NULL;
            }
            
            if(isset($request->permiso_embarque) && $request->permiso_embarque != ""){
                $permiso_embarque   = $request->permiso_embarque;
            }else{
                $permiso_embarque   = NULL;
            }
        
        if($estado_pesaje == 2){
            
            try{
                DB::beginTransaction();
                //COMO ES PESAJE COMPLETO BUSCO EL COMPROBANTE
                $tipo_comprobante   =   ACA_Tipo_Comprobante::where(ACA_Tipo_Comprobante::$ID_BD , $request->planta)->where(ACA_Tipo_Comprobante::$ID_TIPO_COMPROBANTE, $id_talonario)->get(['id','nro_final','nro_actual']);
                $nro_actual         =   explode("-", $tipo_comprobante[0]->nro_actual);
                $nro_siguiente      =   "-" . str_pad(((int)$nro_actual[1] + 1), 8 , "0", STR_PAD_LEFT);

                $ult_id_pesaje      =   ACA_Pesaje::where('id_bd', $request->planta)->max('id_pesaje');
                
                //ACTUALIZO CON EL SGTE NRO DE COMPROBANTE
                ACA_Tipo_Comprobante::where(ACA_Tipo_Comprobante::$ID_BD , $request->planta)
                                    ->where(ACA_Tipo_Comprobante::$ID_TIPO_COMPROBANTE, $id_talonario)
                                    ->update(['nro_actual' => $nro_siguiente, 'updated_by' => Auth::user()->name]);
                
                $pesaje = ACA_Pesaje::create([
                    'id_bd'                 => $request->planta,
                    'id_pesaje'             => (int)$ult_id_pesaje + 1,
                    'id_talonario'          => $id_talonario,
                    'nro_comprobante'       => $tipo_comprobante[0]->nro_actual,
                    'fecha'                 => $request->fecha,
                    'id_rodado'             => ACA_Rodado::findOrFail($request->camion)->id_rodado,
                    'patente_acoplado'      => $request->pat_acoplado,
                    'conductor'             => $request->conductor,
                    'id_producto'           => $request->producto,
                    'id_tipo_precio'        => 0,
                    'precio'                => 0,
                    'id_planta_origen'      => $planta_origen,
                    'id_planta_destino'     => $planta_destino,
                    'id_proveedor'          => $proveedor,
                    'id_acopiador'          => 0,
                    'es_acopiador'          => 0,
                    'orden_acopio'          => 0,
                    'val_ref'               => 0,
                    'id_embalaje'           => $embalaje,
                    'cant_embalaje'         => (isset($request->cantidad)) ? $request->cantidad : 0,
                    'peso_emb_x_unidad'     => $peso_emb_x_unidad,
                    'id_tipo_humedad'       => $id_humedad,
                    'porcentaje_humedad'    => $porc_humedad,
                    'porcentaje_palo'       => 0,
                    'peso_palo'             => 0,
                    'm3'                    => 0,
                    'fecha_bruto'           => $fecha_bruto, //AL EDITAR NO MODIFICAR
                    'fecha_anulacion'       => NULL,
                    'usuario_anulacion'     => NULL,
                    'motivo_anulacion'      => NULL,
                    'bruto'                 => (float)$bruto,
                    'tara'                  => (float)$tara,
                    'total_embalaje'        => (float)$embalaje_peso,
                    'peso_humedad'          => (float)$humedad_peso,
                    'neto'                  => (float)$neto,
                    'estado'                => $estado_pesaje,
                    'id_chacra'             => $chacra,
                    'te_certificado'        => $certificado,
                    'id_pesaje_base_orig'   => 0,
                    'id_pesaje_dup'         => 0,
                    'id_mig_orig'           => 0,
                    'migrado'               => 0,
                    'modificado'            => 0,
                    'lote_chacra'           => $lote,
                    'tamanio'               => $tamanio,
                    'malezas'               => $malezas,
                    'calidad'               => $calidad,
                    'acaros'                => $acaros,
                    'temperatura'           => $temperatura,
                    
                    'expo'                  => $expo,
                    'porc_malla_30'         => $porc_palo,
                    'buque'                 => $buque,
                    'codigo_aduana'         => $codigo_aduana,
                    'reserva'               => $reserva,
                    'codigo_balanza'        => $codigo_balanza,
                    'nro_certif_hab'        => $nro_certificado,
                    'vencimiento'           => $vencimiento,
                    'nro_contenedor'        => $nro_contenedor,
                    'afip'                  => $precintos,
                    'linea'                 => $linea,
                    'otros'                 => $otros,
                    'cuit_ata'              => $cuit,
                    'apellido_nombre_ata'   => $apellido_nombre,
                    'nro_permiso_embarque'  => $permiso_embarque,
                    
                    'pesajeweb'             => 1,
                    'sincronizado'          => 0,
                    'observaciones'         => NULL,
                    'created_by'            => Auth::user()->name,
                ]);
                
                // ACTUALIZAR MOV_STOCK
                /*
                ACA_Mov_Stock::create([
                    'id_bd'         => $request->planta,
                    'id_mov_stock'  => ACA_Mov_Stock::where('id_bd', $request->planta)->max('id_mov_stock'),
                    'fecha'         => $request->fecha,
                    'id_producto'   => $request->producto,
                    'cantidad'      => (float)$neto,
                    'e_s'           => true, //SI ES TRUE ES SALIDA
                    'id_motivo'     => 1, //MOVIMIENTO BASCULA, relacionado con la tabla motivos
                    'm3'            => 0,
                    'id_pesaje'     => $pesaje->id_pesaje,
                    'id_det_lote'   => 0,
                    'por_balanza'   => 0,
                    'observaciones' => NULL,
                    'created_by'    => Auth::user()->name
                ]);*/
                
                /*
                Mov_Stock::create([
                    "Nro_Comprobante"       => $tipo_comprobante[0]->nro_actual,
                    "_IDTalonario"          => $id_talonario, //Hace referencia a la tabla Tipo_comprobantes?
                    "_IDPlanta"             => $request->planta, //A que tabla hace referencia?
                    "Fecha"                 => $request->fecha,
                    "Fecha_reproceso"       => NULL, //Cuando se utiliza?
                    "Tipo"                  => NULL, //Cuando se utiliza?
//Tipo 0 si es entrada, 1 si es salida                    
//Como se si es de entrada o salida?
                    //m3 no esta, se utilizaba para algo?
                    "Concepto"              => NULL, //Cuando se utiliza?
                    //COncepto gral, ej: por mov de bascula
                    "_IDComprobante"        => 0, //Cuando se utiliza?
                    //en id comprobante pongo el id pesaje.
                    "TipoCompAfec"          => NULL, //Cuando se utiliza?
                    // si es 1 viene de balanza (web o local)
                    "Fecha_Anulacion"       => NULL,
                    "Total"                 => (float)$neto,
                    //sumatoria del detalle (cantidad)
                    "_IDProyecto"           => NULL, //Cuando se utiliza?
                    // para web siempre es 0
                    "_IDMotivo"             => 1, //(MOVIMIENTO BASCULA) Referencio a la tabla motivos de sql server? o access?
                    //
                    "_IDDestino"            => 0, //Cuando se utiliza?
                    // lo pongo 0
                    "Usuario_Responsable"   => Auth::user()->name,
                    "Obs"                   => NULL, //Cuando se utiliza?
                    // En obs el porque de la anulacion
                    "Maquina"               => NULL, //Cuando se utiliza?
                    // lo pongo siempre en null (obviarlo)
                    "PorBalanza"            => FALSE, //Cuando se utiliza?
                    //Si es 1, se importo desde balanza, en mi caso va 0
                    "_IDPesaje"             => $pesaje->id_pesaje, //Web o Local (expotec)?
                    //El producto lo sacamos de la relacion con PESAJES?
                    "_IDPesajeMovBolson"    => 0, //Cuando se utiliza?
                    // va el id del mov de stock afectado del bolson, va el id de pesaje si asocie bolsones, sino lo dejo en 0
                    "_IDMovStockBalanza"    => 0, //Cuando se utiliza?
                    "_IDLote"               => 0, //Cuando se utiliza?
                    "_Creado_por"           => Auth::user()->name,
                    "_Fecha_creacion"       => date('d/m/Y H:i'),
                    "_Modificado_por"       => NULL,
                    "_Fecha_modificacion"   => NULL,
                    "_Observaciones"        => NULL,
                ]);
                */
                
                DB::commit();
                return Response::json( array("msg" => "Se realizó el pesaje correctamente!") , 200);
            } catch (\PDOException $ex) {
                DB::rollBack();
                return Response::json( array("error" => $ex->getMessage()) , 400);
            }
            
            
        }else{
            //COMO NO ES PESAJE COMPLETO, GUARDO SIN UTILIZAR UN NRO DE COMPROBANTE
            try{
                DB::beginTransaction();
                
                $ult_id_pesaje      =   ACA_Pesaje::where('id_bd', $request->planta)->max('id_pesaje');
                
                ACA_Pesaje::create([
                    'id_bd'                 => $request->planta,
                    'id_pesaje'             => (int)$ult_id_pesaje + 1,
                    'id_talonario'          => NULL,
                    'nro_comprobante'       => NULL,
                    'fecha'                 => $request->fecha,
                    'id_rodado'             => ACA_Rodado::findOrFail($request->camion)->id_rodado,
                    'patente_acoplado'      => $request->pat_acoplado,
                    'conductor'             => $request->conductor,
                    'id_producto'           => $request->producto,
                    'id_tipo_precio'        => 0,
                    'precio'                => 0,
                    'id_planta_origen'      => $planta_origen,
                    'id_planta_destino'     => $planta_destino,
                    'id_proveedor'          => $proveedor,
                    'id_acopiador'          => 0,
                    'es_acopiador'          => 0,
                    'orden_acopio'          => 0,
                    'val_ref'               => 0,
                    'id_embalaje'           => $embalaje,
                    'cant_embalaje'         => (isset($request->cantidad)) ? $request->cantidad : 0,
                    'peso_emb_x_unidad'     => $peso_emb_x_unidad,
                    'id_tipo_humedad'       => $id_humedad,
                    'porcentaje_humedad'    => $porc_humedad,
                    'porcentaje_palo'       => 0,
                    'peso_palo'             => 0,
                    'm3'                    => 0,
                    'fecha_bruto'           => $fecha_bruto, //AL EDITAR NO MODIFICAR
                    'fecha_anulacion'       => NULL,
                    'usuario_anulacion'     => NULL,
                    'motivo_anulacion'      => NULL,
                    'bruto'                 => (float)$bruto,
                    'tara'                  => (float)$tara,
                    'total_embalaje'        => (float)$embalaje_peso,
                    'peso_humedad'          => (float)$humedad_peso,
                    'neto'                  => (float)$neto,
                    'estado'                => $estado_pesaje,
                    'id_chacra'             => $chacra,
                    'te_certificado'        => $certificado,
                    'id_pesaje_base_orig'   => 0,
                    'id_pesaje_dup'         => 0,
                    'id_mig_orig'           => 0,
                    'migrado'               => 0,
                    'modificado'            => 0,
                    'lote_chacra'           => $lote,
                    'tamanio'               => $tamanio,
                    'malezas'               => $malezas,
                    'calidad'               => $calidad,
                    'acaros'                => $acaros,
                    'temperatura'           => $temperatura,
                    'expo'                  => $expo,
                    
                    'porc_malla_30'         => $porc_palo,
                    'buque'                 => $buque,
                    'codigo_aduana'         => $codigo_aduana,
                    'reserva'               => $reserva,
                    'codigo_balanza'        => $codigo_balanza,
                    'nro_certif_hab'        => $nro_certificado,
                    'vencimiento'           => $vencimiento,
                    'nro_contenedor'        => $nro_contenedor,
                    'afip'                  => $precintos,
                    'linea'                 => $linea,
                    'otros'                 => $otros,
                    'cuit_ata'              => $cuit,
                    'apellido_nombre_ata'   => $apellido_nombre,
                    'nro_permiso_embarque'  => $permiso_embarque,
                    
                    'pesajeweb'             => 1,
                    'sincronizado'          => 0,
                    'observaciones'         => NULL,
                    'created_by'            => Auth::user()->name,
                ]);
                
                DB::commit();
                return Response::json( array("msg" => "Se realizó el pesaje correctamente!") , 200);
            } catch (\PDOException $ex) {
                DB::rollBack();
                return Response::json( array("error" => $ex->getMessage()) , 400);
            }
        }
        
//        ESTADO = 0 = UNIDAD CARGADA
//        ESTADO = 1 = UNIDAD DESCARGADA
//        ESTADO = 2 = PESAJE COMPLETO
        
    }

}
