<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDO;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Auth;
use App\Model\ACA\ACA_Producto;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Model\ACA\ACA_Mov_Stock;
use App\ActualizacionBD;
use Illuminate\Support\Facades\Route;
use App\Model\ACA\ACA_Proveedor;
use App\Model\ACA\ACA_Chacra;
use Whossun\Toastr\Facades\Toastr;
use App\Model\ACA\ACA_Pesaje;
use PDOException;
use PHPJasper\PHPJasper;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\App;
use PDF;







class BrotesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function exportarpdf(Request $request){

        $resultado = $this->getBrotes($request);
        
        $pdf = PDF::loadView('pdf_brotes', compact('resultado','request') );
        
        return $pdf->download('REPORTE_BROTES_'.time().'.pdf');
        
        
//        return PDF::loadFile(public_path().'/myfile.html')->save('/path-to/my_stored_file.pdf')->stream('download.pdf');
        
        
        
        
        
//        $input = 'C:\xampp\htdocs\cf/vendor/geekcom/phpjasper/examples/hello_world.jasper';  
//        $output =  'C:\xampp\htdocs\cf/vendor/geekcom/phpjasper/examples';
//        $options = [ 
//            'format' => ['pdf'] 
//        ];
//        
//        $jasper = new PHPJasper;
//
//        $jasper->process(
//            $input,
//            $output,
//            $options
//        )->execute();
//        
//        $file = File::get('C:\xampp\htdocs\cf/vendor/geekcom/phpjasper/examples/hello_world.pdf');
//        $response = Response::make($file, 200);
//        $response->header('Content-Type', 'application/pdf');
//        return $response;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('reportes.brotes');
    }
    
    public function stock(Request $request){
//        return view('actualizando');
        return view('reportes.stock');
    }
    
    public function stockApdf(Request $request){
        $resultado = $this->getStock($request, $a=true);
        
        $pdf = PDF::loadView('pdf_stock', compact('resultado','request') );
        
        return $pdf->download('REPORTE_BROTES_'.time().'.pdf');
    }
    
    public function getStock(Request $request, $llamado_interno = null){
        $bd = array();
        $brotes = array();

        try{
            

            if(isset($request->toArray()['aca'])){
                $planta[] = 91;
            }

            if(isset($request->toArray()['cg'])){
                $planta[] = 88;
            }

            if(isset($request->toArray()['cv'])){
                $planta[] = 89;
            }

            if(isset($request->toArray()['obso'])){
                $planta[] = 87;
            }

            if(isset($request->toArray()['ta'])){
                $planta[] = 9;
            }
            if(isset($request->toArray()['dcg'])){
                $planta[] = 121;
            }
            if(isset($request->toArray()['cvm'])){
                $planta[] = 90;
            }
            if(isset($request->toArray()['obto'])){
                $planta[] = 86;
            }
            if(isset($request->toArray()['obit'])){
                $planta[] = 97;
            }
            if(isset($request->toArray()['obmy'])){
                $planta[] = 98;
            }
            if(isset($request->toArray()['cv2'])){
                $planta[] = 99;
            }
            if(isset($request->toArray()['dcr'])){
                $planta[] = 120;
            }
            if(isset($request->toArray()['dch'])){
                $planta[] = 122;
            }
            if(isset($request->toArray()['db'])){
                $planta[] = 124;
            }
            
            
            $pesajes = DB::table('PRODUCTOS as pe')->select(DB::raw('pe.Codigo as CODIGO, pe.Descripcion as DESCRIPCION, SUM(pe.Cantidad) as TOTAL'));
            $pesajes = $pesajes->where('pe.Activo',1);
            $pesajes = $pesajes->where('pe.Cantidad','>',0);
            if(isset($planta)){
                $pesajes = $pesajes->whereIn('pe._IDBalanza',$planta);
            }else{
                $pesajes = $pesajes->whereIn('pe._IDBalanza',[9999]);
            }
            
            $pesajes = $pesajes->groupBy('pe.Codigo', 'pe.Descripcion')->orderBy('pe.Codigo')->get(['CODIGO','DESCRIPCION','TOTAL']);

        } catch (\PDOException $e){
            echo $e->getMessage();
        }
        
        if($llamado_interno != null)
            return $pesajes;
        else
            echo json_encode($pesajes);
    }

    public function brotes(Request $request){

        $bd = array();
        $brotes = array();


        if(isset($request->toArray()['fecha_desde'])){
            $fecha_desde = $request->toArray()['fecha_desde'];
        }else{
            $fecha_desde = 0;
        }
            

        if(isset($request->toArray()['fecha_hasta'])){
            $fecha_hasta = $request->toArray()['fecha_hasta'];
        }else{
            $fecha_hasta = 0;
        }
            

        if($request->toArray()['id_origen'] == 'default'){
            $producto = 0;
        }else{
            if($request->toArray()['id_origen'] == "chacra"){
                $producto = 78;
            }
            if($request->toArray()['id_origen'] == "proveedor"){
                $producto = 77;
            }
        }

        if(($request->toArray()['id_certificado']) == 'default'){
            $certificado = 2;
            
        }else{
            if(($request->toArray()['id_certificado']) == 'certificado'){
                $certificado = 1;
            }
            if(($request->toArray()['id_certificado']) == 'no_certificado'){
                $certificado = 0;
            }
        }
        

        try{
            

            if(isset($request->toArray()['aca'])){
                $planta[] = 91;
            }

            if(isset($request->toArray()['cg'])){
                $planta[] = 88;
            }

            if(isset($request->toArray()['cv'])){
                $planta[] = 89;
            }

            if(isset($request->toArray()['obso'])){
                $planta[] = 87;
            }

            if(isset($request->toArray()['ta'])){
                $planta[] = 9;
            }
            
            
            $derivado_a_otros_secadero = DB::table(DB::raw('PESAJES as PE'))
                            ->select(DB::raw('sum(Neto) as TOT_DERIV_SECADEROS'))
                            ->whereIn(DB::raw('PE._IDBalanza'),$planta)
                            ->where(DB::raw('PE.Fecha_Anulacion'),NULL)
                            ->where(DB::raw('CAST(PE.Fecha as Date)'),'>=', $fecha_desde)
                            ->where(DB::raw('CAST(PE.Fecha as Date)'),'<=', $fecha_hasta);
            
            if($certificado != 2)
                $derivado_a_otros_secadero = $derivado_a_otros_secadero->where(DB::raw('PE.Te_Certificado'), $certificado);
            
            $derivado_a_otros_secadero = $derivado_a_otros_secadero->where(DB::raw('PE._IDProducto'),$producto)
                ->whereIn(DB::raw('PE._IDPlantaOrigen'),$planta)
                ->whereNotIn(DB::raw('PE._IDPlantaDestino'),$planta)
                ->where(DB::raw('PE._IDProveedor'),0)
                ->where(DB::raw('PE._IDBalanza'),DB::raw('PE._IDPlantaOrigen'))
                ->where(DB::raw('PE.Estado'),2)
                ->get();
            
//            echo $derivado_a_otros_secadero[0]->TOTAL;die();
            
            $derivado_a_propio = DB::table(DB::raw('PESAJES as PE'))
                            ->select(DB::raw('sum(Neto) as TOT_DERIV_PROPIO'))
                            ->whereIn(DB::raw('PE._IDBalanza'),$planta)
                            ->where(DB::raw('PE.Fecha_Anulacion'),NULL)
                            ->where(DB::raw('CAST(PE.Fecha as Date)'),'>=', $fecha_desde)
                            ->where(DB::raw('CAST(PE.Fecha as Date)'),'<=', $fecha_hasta);
            
            if($certificado != 2)
                $derivado_a_propio = $derivado_a_propio->where(DB::raw('PE.Te_Certificado'), $certificado);
            
            $derivado_a_propio = $derivado_a_propio->where(DB::raw('PE._IDProducto'),$producto)
                ->whereIn(DB::raw('PE._IDPlantaOrigen'),$planta)
                ->whereIn(DB::raw('PE._IDPlantaDestino'),$planta)
                ->where(DB::raw('PE._IDProveedor'),0)
                ->where(DB::raw('PE._IDBalanza'),DB::raw('PE._IDPlantaOrigen'))
                ->where(DB::raw('PE.Estado'),2)
                ->get();
            
//            echo $derivado_a_propio;die();
            
            if(isset($request->toArray()['agrupado_x_lote'])){
                $agrupado_x_lote = 1;
                $agrupado_lote = ','.ACA_Pesaje::$LOTE_CHACRA . ' as LOTE';
            }else{
                $agrupado_x_lote = 0;
                $agrupado_lote = '';
            }
            
            if($producto == 77){
                $columna = 'pe.Cod_Proveedor';
                $pesajes = DB::table('PESAJES as pe')->select(DB::raw($columna .' as CODIGO,'.'(select Razon_Social from EXPOTEC_COMPRAS.dbo.PROVEEDORES prov where prov.Codigo = pe.Cod_Proveedor) as DESCRIPCION'.$agrupado_lote.', SUM(pe.neto) as TOTAL'));
                //ACTIVANDO O DESACTIVANDO LO SIGUIENTE VEO SI ES DERIVADO O NO
//                $pesajes = $pesajes->where(ACA_Pesaje::$ID_PLANTA_ORIGEN,0);
            }else if($producto == 78){
                $columna = 'pe.Cod_Chacra';
                $pesajes = DB::table('PESAJES as pe')->select(DB::raw($columna .' as CODIGO,'.'(select Descripcion from CHACRAS ch where ch.Codigo = pe.Cod_Chacra) as DESCRIPCION'.$agrupado_lote.', SUM(pe.neto) as TOTAL'));
            }
            
            
            $pesajes = $pesajes->where('pe.Fecha_Anulacion',NULL);
            $pesajes = $pesajes->where('pe._IDProducto', $producto);
            $pesajes = $pesajes->whereIn(ACA_Pesaje::$ID_PLANTA_DESTINO,$planta );
            
            if(isset($planta)){
                $pesajes = $pesajes->whereIn('pe._IDBalanza',$planta);
            }else{
                $pesajes = $pesajes->whereIn('pe._IDBalanza',[9999]);
            }
            
            $pesajes = $pesajes->where('pe.Estado',2);
            
            
            if($certificado != 2){
                $pesajes = $pesajes->where('pe.Te_Certificado',$certificado);
            }
            
            if($fecha_desde != 0){
                $pesajes = $pesajes->where(DB::raw('CAST(pe.Fecha as Date)'),'>=',$fecha_desde);
                //$pesajes = $pesajes->where('pe.Fecha','>=',$fecha_desde);
            }
            
            if($fecha_hasta != 0){
                $pesajes = $pesajes->where(DB::raw('CAST(pe.Fecha as Date)'),'<=',$fecha_hasta);
                //$pesajes = $pesajes->where('pe.Fecha','<=',$fecha_hasta);
            }
            
            
            
            if($agrupado_x_lote == 1){
                $pesajes = $pesajes->groupBy($columna, ACA_Pesaje::$LOTE_CHACRA)->orderBy($columna, ACA_Pesaje::$LOTE_CHACRA)->get(['CODIGO','LOTE','TOTAL']);
            }else{
                $pesajes = $pesajes->groupBy($columna)->orderBy($columna)->get(['CODIGO','LOTE','TOTAL']);
            }
            
            

        } catch (\PDOException $e){
            echo $e->getMessage();
        }
        
//        $pesajes["DERIV_SECA"] = $derivado_a_otros_secadero[0];
        $pesajes["DERIVADOS"] = array('terceros' => $derivado_a_otros_secadero[0], 'propio' => $derivado_a_propio[0]);
        
//        echo $pesajes; die();

        echo json_encode($pesajes);
    }
    
    public function getBrotes(Request $request){

        $bd = array();
        $brotes = array();


        if(isset($request->toArray()['fecha_desde'])){
            $fecha_desde = $request->toArray()['fecha_desde'];
        }else{
            $fecha_desde = 0;
        }
            

        if(isset($request->toArray()['fecha_hasta'])){
            $fecha_hasta = $request->toArray()['fecha_hasta'];
        }else{
            echo 'aca no 2';
            $fecha_hasta = 0;
        }
            

        if($request->toArray()['id_origen'] == 'default'){
            $producto = 0;
        }else{
            if($request->toArray()['id_origen'] == "chacra"){
                $producto = 78;
            }
            if($request->toArray()['id_origen'] == "proveedor"){
                $producto = 77;
            }
        }

        if(($request->toArray()['id_certificado']) == 'default'){
            $certificado = 2;
            
        }else{
            if(($request->toArray()['id_certificado']) == 'certificado'){
                $certificado = 1;
            }
            if(($request->toArray()['id_certificado']) == 'no_certificado'){
                $certificado = 0;
            }
        }
        

        try{
            

            if(isset($request->toArray()['aca'])){
                $planta[] = 91;
            }

            if(isset($request->toArray()['cg'])){
                $planta[] = 88;
            }

            if(isset($request->toArray()['cv'])){
                $planta[] = 89;
            }

            if(isset($request->toArray()['obso'])){
                $planta[] = 87;
            }

            if(isset($request->toArray()['ta'])){
                $planta[] = 9;
            }
            
            if(isset($request->toArray()['agrupado_x_lote'])){
                $agrupado_x_lote = 1;
                $agrupado_lote = ','.ACA_Pesaje::$LOTE_CHACRA . ' as LOTE';
            }else{
                $agrupado_x_lote = 0;
                $agrupado_lote = '';
            }
            
            if($producto == 77){
                $columna = 'pe.Cod_Proveedor';
                $pesajes = DB::table('PESAJES as pe')->select(DB::raw($columna .' as CODIGO,'.'(select Razon_Social from EXPOTEC_COMPRAS.dbo.PROVEEDORES prov where prov.Codigo = pe.Cod_Proveedor) as DESCRIPCION'.$agrupado_lote.', SUM(pe.neto) as TOTAL'));
                //ACTIVANDO O DESACTIVANDO LO SIGUIENTE VEO SI ES DERIVADO O NO
//                $pesajes = $pesajes->where(ACA_Pesaje::$ID_PLANTA_ORIGEN,0);
            }else if($producto == 78){
                $columna = 'pe.Cod_Chacra';
                $pesajes = DB::table('PESAJES as pe')->select(DB::raw($columna .' as CODIGO,'.'(select Descripcion from CHACRAS ch where ch.Codigo = pe.Cod_Chacra) as DESCRIPCION'.$agrupado_lote.', SUM(pe.neto) as TOTAL'));
            }
            
            
            $pesajes = $pesajes->where('pe.Fecha_Anulacion',NULL);
            $pesajes = $pesajes->where('pe._IDProducto', $producto);
            $pesajes = $pesajes->whereIn(ACA_Pesaje::$ID_PLANTA_DESTINO,$planta );
            //->where('pe._IDProveedor','<>',0);
            //->where('pe._IDPlantaOrigen',0);
            
            if(isset($planta)){
                $pesajes = $pesajes->whereIn('pe._IDBalanza',$planta);
            }else{
                $pesajes = $pesajes->whereIn('pe._IDBalanza',[9999]);
            }
            
            $pesajes = $pesajes->where('pe.Estado',2);
            
            
            if($certificado != 2){
                $pesajes = $pesajes->where('pe.Te_Certificado',$certificado);
            }
            
            if($fecha_desde != 0){
                $pesajes = $pesajes->where(DB::raw('CAST(pe.Fecha as Date)'),'>=',$fecha_desde);
                //$pesajes = $pesajes->where('pe.Fecha','>=',$fecha_desde);
            }
            
            if($fecha_hasta != 0){
                $pesajes = $pesajes->where(DB::raw('CAST(pe.Fecha as Date)'),'<=',$fecha_hasta);
                //$pesajes = $pesajes->where('pe.Fecha','<=',$fecha_hasta);
            }
            
            
            
            if($agrupado_x_lote == 1){
                $pesajes = $pesajes->groupBy($columna, ACA_Pesaje::$LOTE_CHACRA)->orderBy($columna, ACA_Pesaje::$LOTE_CHACRA)->get(['CODIGO','LOTE','TOTAL']);
            }else{
                $pesajes = $pesajes->groupBy($columna)->orderBy($columna)->get(['CODIGO','LOTE','TOTAL']);
            }
            
            

        } catch (\PDOException $e){
            echo $e->getMessage();
        }

        return $pesajes;
    }

    public function getBDName($param0) {
        $a = ActualizacionBD::select('nombre_bd')->where('id',$param0)->first();
        return $a->toArray()['nombre_bd'];
    }

    public function getProductName($id_producto, $id_bd) {
        $a = ACA_Producto::select('descripcion')->where('id_bd',$id_bd)->where('id_producto',$id_producto)->first();
        return $a->toArray()['descripcion'];
    }

    public function getNombreProveedor($id_proveedor, $bd) {
        $a = ACA_Proveedor::select('razon_social')->where('id_bd',$bd)->where('id_proveedor',$id_proveedor)->first();
        return $a->toArray()['razon_social'];
    }

    public function getNombreChacra($id_chacra, $bd) {

        $a =    ACA_Chacra::select('descripcion')
                ->where('id_bd',$bd)
                ->where('id_chacra',$id_chacra)->first();

        return $a->toArray()['descripcion'];
    }

    public function setData($brotes, $bd, $origen, $certificado, $fecha_desde, $fecha_hasta) {
        $pesajes = DB::table('pesajes');
        $chacras = DB::table('pesajes');
        $proveedores = DB::table('pesajes');

            if($origen == 'chacra')
                $pesajes->selectRaw('id_chacra, sum(neto) as total');

            if($origen == 'proveedor')
                $pesajes->selectRaw('Cod_Proveedor, sum(neto) as total');
                //aca_modifique

            if($origen == 'default'){
                $chacras->selectRaw('id_chacra, sum(neto) as total');
                $proveedores->selectRaw('Cod_Proveedor, sum(neto) as total');
                //aca_modifique
            }


            if($origen == 'default'){
                $chacras->where('id_bd', $bd);
                $chacras->where(DB::raw('CAST(fecha as Date)'),'>=',$fecha_desde);
                //$chacras->where('fecha','>=',$fecha_desde);
                $chacras->where(DB::raw('CAST(fecha as Date)'),'<=',$fecha_hasta);
                //$chacras->where('fecha','<=',$fecha_hasta);

                $proveedores->where('id_bd', $bd);
                $proveedores->where(DB::raw('CAST(fecha as Date)'),'>=',$fecha_desde);
                $proveedores->where(DB::raw('CAST(fecha as Date)'),'<=',$fecha_hasta);
                //$proveedores->where('fecha','>=',$fecha_desde);
                //$proveedores->where('fecha','<=',$fecha_hasta);
            }else{
                $pesajes->where('id_bd', $bd);
                $pesajes->where(DB::raw('CAST(fecha as Date)'),'>=',$fecha_desde);
                $pesajes->where(DB::raw('CAST(fecha as Date)'),'<=',$fecha_hasta);
                //$pesajes->where('fecha','>=',$fecha_desde);
                //$pesajes->where('fecha','<=',$fecha_hasta);
            }


            if($certificado != 'no_importa'){
                if($certificado === 'si'){
                    if($origen == 'default'){
                        $chacras->where('te_certificado', 1);
                        $proveedores->where('te_certificado', 1);
                    }else{
                        $pesajes->where('te_certificado', 1);
                    }
                }
                if($certificado === 'no'){
                    if($origen == 'default'){
                        $chacras->where('te_certificado', 0);
                        $proveedores->where('te_certificado', 0);
                    }else{
                        $pesajes->where('te_certificado', 0);
                    }

                }
            }

            if($origen == 'default'){
                $chacras->where('id_producto',78);
                $chacras->where('id_chacra','<>',0);
                //$chacras->where('id_proveedor','<>',0);
                //AGREGAR QUE LA FECHA DE ANULACION SEA NULL
                $chacras->groupBy('id_chacra','Cod_Proveedor');
                //aca_modifique
                $chacras->orderBy('id_chacra');

                $proveedores->where('id_producto',77);
                //$proveedores->where('id_proveedor','<>',0);
                $proveedores->groupBy('Cod_Proveedor');
                //aca_modifique
                $proveedores->orderBy('Cod_Proveedor');
                //aca_modifique
                
                //dd($proveedores->get());die();

                foreach($proveedores->get() as $a => $value){
                    $nombre_proveedor = $this->getNombreProveedor($value->id_proveedor, $bd);
                    if(array_key_exists ( $nombre_proveedor , $brotes )){
                        $brotes[$nombre_proveedor] = $brotes[$nombre_proveedor] + (int)$value->total;
                    }else{
                        $brotes[$nombre_proveedor] = (int)$value->total;
                    }
                }

                foreach($chacras->get() as $a => $value){
                    $nombre_chacra = $this->getNombreChacra($value->id_chacra, $bd);
//                    if($nombre_chacra !== null){
                        if(array_key_exists ( $nombre_chacra , $brotes )){
                            $brotes[$nombre_chacra] = $brotes[$nombre_chacra] + (int)$value->total;
                        }else{
                            $brotes[$nombre_chacra] = (int)$value->total;
                        }
//                    }
                }

            }

            if($origen == 'chacra'){
                $pesajes->where('id_producto',78);
                $pesajes->where('id_chacra','<>',0);
                //$pesajes->where('id_proveedor','<>',0);
                $pesajes->groupBy('id_chacra','Cod_Proveedor');
                //aca_modifique
                $pesajes->orderBy('id_chacra');


                foreach($pesajes->get() as $a => $value){
                    $nombre_chacra = $this->getNombreChacra($value->id_chacra, $bd);
//                    if($nombre_chacra !== null){
                        if(array_key_exists ( $nombre_chacra , $brotes )){
                            $brotes[$nombre_chacra] = $brotes[$nombre_chacra] + (int)$value->total;
                        }else{
                            $brotes[$nombre_chacra] = (int)$value->total;
                        }
//                    }
                }

            }

            if($origen == 'proveedor'){
                $pesajes->where('id_producto',77);
                //$pesajes->where('id_proveedor','<>',0);
                $pesajes->groupBy('Cod_Proveedor');
                //aca_modifique
                $pesajes->orderBy('Cod_Proveedor');
                //aca_modifique
                
                foreach($pesajes->get() as $a => $value){
                    $nombre_proveedor = $this->getNombreProveedor($value->id_proveedor, $bd);
                    if(array_key_exists ( $nombre_proveedor , $brotes )){
                        $brotes[$nombre_proveedor] = $brotes[$nombre_proveedor] + (int)$value->total;
                    }else{
                        $brotes[$nombre_proveedor] = (int)$value->total;
                    }
                }
            }

            return $brotes;
    }

}
