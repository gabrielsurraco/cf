<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class Control_hvController extends Controller {
    
    public function index(){
        return view('secaderos.control_hv');
    }
    


    
    
    public function buscar(Request $request){
        
        $this->validate($request, [
            'fecha_desde' => 'required',
            'fecha_hasta' => 'required',
        ]);
        
        $fecha_desde = $request->toArray()['fecha_desde'];
        $fecha_hasta = $request->toArray()['fecha_hasta'];
        $derivaciones = $request->toArray()['derivaciones'];
        
        return json_encode('a');
    }
    
}
