<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\ACA\ACA_Pesaje;
use App\Model\ACA\ACA_Producto;
use App\Model\ACA\ACA_Mov_Stock;
use App\Planta;
use Carbon\Carbon;
use PHPJasper\PHPJasper;
use App\ActualizacionBD;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function actualizando(){
        
        $derivado_1718 =    DB::table(DB::raw('PESAJES as PE'))
                            ->select(DB::raw('(select BT.Descripcion from BALANZASTEMP BT where BT.ID = PE._IDBalanza ) as PLANTA, (select  PL.Descripcion from PLANTAS PL where PL._IDBalanza = PE._IDPlantaOrigen) as ORIGEN, (select  PL.Descripcion from PLANTAS PL where PL._IDBalanza = PE._IDPlantaDestino) as DESTINO, _IDProducto as PRODUCTO, Te_Certificado CERT, sum(Neto) as TOTAL'))
                            ->whereIn(DB::raw('PE._IDBalanza'),[9,87,88,89,91])
                            ->where(DB::raw('PE.Fecha_Anulacion'),NULL)
                            ->where(DB::raw('CAST(PE.Fecha as Date)'),'>=','01/09/2017')
                            ->whereIn(DB::raw('PE._IDProducto'),[77,78])
                            ->whereIn(DB::raw('PE._IDPlantaOrigen'),[9,87,88,89,91])
                            ->where(DB::raw('PE._IDProveedor'),0)
                            ->where(DB::raw('PE._IDBalanza'),DB::raw('PE._IDPlantaOrigen'))
                            ->where(DB::raw('PE.Estado'),2)
                            ->groupBy(DB::raw('PE._IDBalanza'),DB::raw('PE._IDPlantaOrigen'),DB::raw('PE._IDPlantaDestino'),DB::raw('PE._IDProducto'),DB::raw('PE.Te_Certificado'))
                            ->get();
        
        $derivado_1617 =    DB::table(DB::raw('PESAJES as PE'))
                            ->select(DB::raw('(select BT.Descripcion from BALANZASTEMP BT where BT.ID = PE._IDBalanza ) as PLANTA, (select  PL.Descripcion from PLANTAS PL where PL._IDBalanza = PE._IDPlantaOrigen) as ORIGEN, (select  PL.Descripcion from PLANTAS PL where PL._IDBalanza = PE._IDPlantaDestino) as DESTINO, _IDProducto as PRODUCTO, Te_Certificado CERT, sum(Neto) as TOTAL'))
                            ->whereIn(DB::raw('PE._IDBalanza'),[9,87,88,89,91])
                            ->where(DB::raw('PE.Fecha_Anulacion'),NULL)
                            ->where(DB::raw('CAST(PE.Fecha as Date)'),'>=','01/09/2016')
                            ->where(DB::raw('CAST(PE.Fecha as Date)'),'<=','28/07/2017')
                            ->whereIn(DB::raw('PE._IDProducto'),[77,78])
                            ->whereIn(DB::raw('PE._IDPlantaOrigen'),[9,87,88,89,91])
                            ->where(DB::raw('PE._IDProveedor'),0)
                            ->where(DB::raw('PE._IDBalanza'),DB::raw('PE._IDPlantaOrigen'))
                            ->where(DB::raw('PE.Estado'),2)
                            ->groupBy(DB::raw('PE._IDBalanza'),DB::raw('PE._IDPlantaOrigen'),DB::raw('PE._IDPlantaDestino'),DB::raw('PE._IDProducto'),DB::raw('PE.Te_Certificado'))
                            ->get();
//        dd($derivado_1617);
                            
//        Select
//        (select Descripcion from BALANZASTEMP where ID =_IDBalanza ) PLANTA,
//        (select  Descripcion from PLANTAS where _IDBalanza = dbo.PESAJES._IDPlantaOrigen) ORIGEN, 
//        (select  Descripcion from PLANTAS where _IDBalanza = _IDPlantaDestino) DESTINO, 
//        _IDProducto, 
//        Te_Certificado, 
//        sum(Neto) 
//        from PESAJES
//        where _IDBalanza in(9,87,88,89,91)
//        and Fecha_Anulacion is null
//        and cast( Fecha as Date) >= '01/09/2017'
//        and _IDProducto in (77,78)
//        and _IDPlantaOrigen in(9,87,88,89,91)
//        and _IDProveedor = 0
//        and _IDBalanza = _IDPlantaOrigen
//        and Estado = 2
//        group by _IDBalanza, _IDPlantaOrigen, _IDPlantaDestino, _IDProducto, Te_Certificado
    }
    
    public function getReprocesado($id_balanza){
        $reprocesado = ACA_Mov_Stock::select(DB::raw('SUM(Cantidad) as CANTIDAD'))
                                ->where(ACA_Mov_Stock::$ID_BALANZA,$id_balanza)
                                ->where(ACA_Mov_Stock::$FECHA_REPROCESO,'<>',NULL)
                                ->where(DB::raw('CAST('.ACA_Mov_Stock::$FECHA.' as Date)'),'>=','01/09/2017')
                                ->first();
        return $reprocesado["CANTIDAD"];
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        return view('actualizando');
        
        
        
        $fecha_actualizacion = ActualizacionBD::select('Fecha_Sincronizacion')->where('ID', 88)->get()->pluck('Fecha_Sincronizacion');
        $fecha_actualizacion_format = (new Carbon($fecha_actualizacion[0]))->format('d/m/Y');
        $dia = (new Carbon($fecha_actualizacion[0]))->format('d');
        $mes = (new Carbon($fecha_actualizacion[0]))->format('m');
        
         
        
        $derivado_1718 =    DB::table(DB::raw('PESAJES as PE'))
                            ->select(DB::raw('(select BT.Descripcion from BALANZASTEMP BT where BT.ID = PE._IDBalanza ) as PLANTA, (select  PL.Descripcion from PLANTAS PL where PL._IDBalanza = PE._IDPlantaOrigen) as ORIGEN, (select  PL.Descripcion from PLANTAS PL where PL._IDBalanza = PE._IDPlantaDestino) as DESTINO,  sum(Neto) as TOTAL'))
                            ->whereIn(DB::raw('PE._IDBalanza'),[9,87,88,89,91])
                            ->where(DB::raw('PE.Fecha_Anulacion'),NULL)
                            ->where(DB::raw('CAST(PE.Fecha as Date)'),'>=','01/09/2017')
                            ->whereIn(DB::raw('PE._IDProducto'),[77,78])
                            ->whereIn(DB::raw('PE._IDPlantaOrigen'),[9,87,88,89,91])
                            ->whereNotIn(DB::raw('PE._IDPlantaDestino'),[9,87,88,89,91])
                            ->where(DB::raw('PE._IDProveedor'),0)
                            ->where(DB::raw('PE._IDBalanza'),DB::raw('PE._IDPlantaOrigen'))
                            ->where(DB::raw('PE.Estado'),2)
                            ->groupBy(DB::raw('PE._IDBalanza'),DB::raw('PE._IDPlantaOrigen'),DB::raw('PE._IDPlantaDestino'))
                            ->orderBy(DB::raw('PE._IDBalanza'))
                            ->get();
        
        $derivado_1617 =    DB::table(DB::raw('PESAJES as PE'))
                            ->select(DB::raw('(select BT.Descripcion from BALANZASTEMP BT where BT.ID = PE._IDBalanza ) as PLANTA, (select  PL.Descripcion from PLANTAS PL where PL._IDBalanza = PE._IDPlantaOrigen) as ORIGEN, (select  PL.Descripcion from PLANTAS PL where PL._IDBalanza = PE._IDPlantaDestino) as DESTINO, sum(Neto) as TOTAL'))
                            ->whereIn(DB::raw('PE._IDBalanza'),[9,87,88,89,91])
                            ->where(DB::raw('PE.Fecha_Anulacion'),NULL)
                            ->where(DB::raw('CAST(PE.Fecha as Date)'),'>=','01/09/2016')
                            ->where(DB::raw('CAST(PE.Fecha as Date)'),'<=',$dia.'/'.$mes.'/'.(date('Y')-1))
                            ->whereIn(DB::raw('PE._IDProducto'),[77,78])
                            ->whereIn(DB::raw('PE._IDPlantaOrigen'),[9,87,88,89,91])
                            ->whereNotIn(DB::raw('PE._IDPlantaDestino'),[9,87,88,89,91])
                            ->where(DB::raw('PE._IDProveedor'),0)
                            ->where(DB::raw('PE._IDBalanza'),DB::raw('PE._IDPlantaOrigen'))
                            ->where(DB::raw('PE.Estado'),2)
                            ->groupBy(DB::raw('PE._IDBalanza'),DB::raw('PE._IDPlantaOrigen'),DB::raw('PE._IDPlantaDestino'))
                            ->orderBy(DB::raw('PE._IDBalanza'))
                            ->get();
        
        
        /*
         * --TABAY 9 PROVEEDORES TV01 = 77 TV02=78 --RECIBIDO --pesaje completo --CERTIFICADO
select * from PESAJES where _IDBalanza = 9 and Fecha_Anulacion is null and Fecha is not null and (Fecha >= '01/09/2017')
and _IDProducto = 77 and _IDPlantaDestino = 9  and Estado = 2 and Te_Certificado = 1;
         */
        
        $hv_x_chacra_tv02_last_zafra = ACA_Pesaje::select( ACA_Pesaje::$COD_CHACRA , DB::raw('SUM(neto) as peso'))
                        ->where(ACA_Pesaje::$FECHA_ANULACION, NULL)
                        ->where(ACA_Pesaje::$FECHA,'<>',NULL)
                        ->where(DB::raw('CAST('.ACA_Pesaje::$FECHA.' as Date)'),'>=','01/09/2016')
                        ->where(DB::raw('CAST('.ACA_Pesaje::$FECHA.' as Date)'),'<=',date('d').'/'.date('m').'/'.(date('Y')-1))
                        ->where(ACA_Pesaje::$ID_PRODUCTO,78)
                        ->where(ACA_Pesaje::$ESTADO, 2)
                        ->where(ACA_Pesaje::$ID_PLANTA_ORIGEN, 0)
                        ->where(ACA_Pesaje::$ID_PLANTA_DESTINO,'<>', 0)
                        ->groupBy(ACA_Pesaje::$COD_CHACRA)
                        ->orderBy(ACA_Pesaje::$COD_CHACRA)
                        ->get(['Cod_Chacra','peso']);
        
        $hv_x_chacra_tv02 = ACA_Pesaje::select(ACA_Pesaje::$COD_CHACRA , DB::raw('SUM(neto) as peso'))
                        ->where(ACA_Pesaje::$FECHA_ANULACION, NULL)
                        ->where(DB::raw('CAST('.ACA_Pesaje::$FECHA.' as Date)'),'<>',NULL)
                        ->where(DB::raw('CAST('.ACA_Pesaje::$FECHA.' as Date)'),'>=','01/09/2017')
                        ->where(ACA_Pesaje::$ID_PRODUCTO,78)
                        ->where(ACA_Pesaje::$ESTADO, 2)
                        //->where(ACA_Pesaje::$ID_PLANTA_ORIGEN, 0)
                        ->where(ACA_Pesaje::$ID_PLANTA_DESTINO,'<>', 0)
                        ->groupBy(ACA_Pesaje::$COD_CHACRA)
                        ->orderBy(ACA_Pesaje::$COD_CHACRA)
                        ->get();
        
//        dd($hv_x_chacra_tv02[0]->chacra->Descripcion);die();
        
        foreach($hv_x_chacra_tv02 as $hv_tv02){
            foreach($hv_x_chacra_tv02_last_zafra as $hv_tv02_last){
                if($hv_tv02->Cod_Chacra == $hv_tv02_last->Cod_Chacra){
                    $existe = "si";
                    $hv_last_year = $hv_tv02_last;
                    break;
                }else{
                    $existe = "no";
                }
                
            }
            
            if($existe == "si"){
                
                if($hv_tv02->chacra == null)
                    die("Existen tickets tv02 sin chacra asignada. Avisar al administrador del sitio");
                
                $rinde_chacras[] = array(
                "codigo"        => $hv_tv02->Cod_Chacra,
                "descripcion"   => $hv_tv02->chacra->Descripcion,
                "existe"        => "si",
                "peso"          => $hv_tv02->peso,
                "incremento"    => number_format((($hv_tv02->peso * 100) / $hv_last_year->peso) - 100,1)
                );
            }
            
            
            
            if($existe == "no"){
                
                if($hv_tv02->chacra == null)
                    die("Existen tickets tv02 sin chacra asignada. Avisar al administrador del sitio");
                    
                
                $rinde_chacras[] = array(
                "codigo"        => $hv_tv02->Cod_Chacra,
                "descripcion"   => $hv_tv02->chacra->Descripcion,
                "existe"        => "no",
                "peso"          => $hv_tv02->peso,
                "incremento"    => 0
                );
            }
            
        }
        
        
        //PRODUCCION SECADEROS
        $prod_secaderos = ACA_Mov_Stock::select(ACA_Mov_Stock::$ID_BALANZA, DB::raw('SUM(cantidad) as cantidad'))
        ->where(DB::raw('CAST('.ACA_Mov_Stock::$FECHA.' as Date)'),'>=','01/09/2017')
        ->where(ACA_Mov_Stock::$ID_MOTIVO,7)
        ->where(ACA_Mov_Stock::$E_S,0)
        ->whereNotIn(ACA_Mov_Stock::$COD_PRODUCTO,['CHIP'])
        ->whereNotIn(ACA_Mov_Stock::$ID_BALANZA,['86','90']) //no incluye obto ni cvm
        ->whereNotIn(ACA_Mov_Stock::$ID_PRODUCTO,[32]) //no incluye desecho offgrade
        ->groupBy(ACA_Mov_Stock::$ID_BALANZA)
        ->get();
        
        $prod_secaderos_last_year = ACA_Mov_Stock::select(ACA_Mov_Stock::$ID_BALANZA, DB::raw('SUM(cantidad) as cantidad'))
        ->where(DB::raw('CAST('.ACA_Mov_Stock::$FECHA.' as Date)'),'>=','01/09/2016')
        ->where(DB::raw('CAST('.ACA_Mov_Stock::$FECHA.' as Date)'),'<=',date('d').'/'.date('m').'/'.(date('Y')-1))
        ->where(ACA_Mov_Stock::$ID_MOTIVO,7)
        ->where(ACA_Mov_Stock::$E_S,0)
        ->whereNotIn(ACA_Mov_Stock::$COD_PRODUCTO,['CHIP'])
        ->whereNotIn(ACA_Mov_Stock::$ID_BALANZA,['86','90'])
        ->whereNotIn(ACA_Mov_Stock::$ID_PRODUCTO,[32])
        ->groupBy(ACA_Mov_Stock::$ID_BALANZA)
        ->get();
        
        foreach($prod_secaderos as $new_prod){
            
            foreach($prod_secaderos_last_year as $old_prod){
                if($new_prod->_IDBalanza == $old_prod->_IDBalanza){
                    $old_prod = $old_prod;
                    break;
                }
            }
            
            $reprocesado = $this->getReprocesado($new_prod->planta["_IDBalanza"]);
            
            $rinde_secaderos[] = array(
                "idplanta"      => $new_prod->_IDPlanta,
                "descripcion"   => $new_prod->planta->Descripcion,
                "cantidad"      => number_format($new_prod->cantidad),
                "incremento"    => number_format(((($new_prod->cantidad - $reprocesado) * 100) / $old_prod->cantidad) - 100, 1),
                "reprocesado"   => $reprocesado
            );
            
        }
        
        
        $cod_balanza = ACA_Producto::select('_IDBalanza')->where('Codigo','TV01')->get();
        
        $hv_certi_proveedores                       = 0;
        $hv_no_certi_proveedores                    = 0;
        $hv_certi_proveedores_last_year             = 0;
        $hv_no_certi_proveedores_last_year          = 0;
        
        $hv_certi_propio                    = 0;
        $hv_no_certi_propio                 = 0;
        $hv_certi_propio_last_year          = 0;
        $hv_no_certi_propio_last_year       = 0;
        
        foreach ($cod_balanza as $cod){
            
            $total_hv_certi_proveedores =    ACA_Pesaje::select(DB::raw('SUM(neto) as peso'))
                                            ->where(ACA_Pesaje::$ID_BALANZA, $cod['_IDBalanza'])
                                            ->where(ACA_Pesaje::$FECHA_ANULACION, NULL)
                                            ->where(ACA_Pesaje::$FECHA,'<>',NULL)
                                            ->where(DB::raw('CAST('.ACA_Pesaje::$FECHA.' as Date)'),'>=','01/09/2017')
                                            ->where(ACA_Pesaje::$ID_PRODUCTO,77)
                                            ->where(ACA_Pesaje::$ID_PLANTA_DESTINO, $cod['_IDBalanza'])
                                            ->where(ACA_Pesaje::$ESTADO, 2)
                                            ->where(ACA_Pesaje::$TE_CERTIFICADO, 1)
                                            ->get();
            
//            $total_hv_certi_proveedores_a_secadero = DB::table(DB::raw('PESAJES as PE'))
//                            ->select(DB::raw('sum(Neto) as HV_CERT_PROV_DERIV_SECADEROS'))
//                            ->whereIn(DB::raw('PE._IDBalanza') , $cod['_IDBalanza'])
//                            ->where(DB::raw('PE.Fecha_Anulacion') , NULL)
//                            ->where(DB::raw('CAST(PE.Fecha as Date)') , '>=' , '01/09/2017')
//                            ->where(DB::raw('PE.Te_Certificado') , 1)
//                            ->where(DB::raw('PE._IDProducto') , 77)
//                            ->whereIn(DB::raw('PE._IDPlantaOrigen') , [9,87,88,89,91])
//                            ->whereNotIn(DB::raw('PE._IDPlantaDestino') , [9,87,88,89,91])
//                            ->where(DB::raw('PE._IDProveedor') , 0)
//                            ->where(DB::raw('PE._IDBalanza'),DB::raw('PE._IDPlantaOrigen'))
//                            ->where(DB::raw('PE.Estado') , 2)
//                            ->get();
            
            $total_hv_certi_proveedores_last_year =    ACA_Pesaje::select(DB::raw('SUM(neto) as peso'))->where(ACA_Pesaje::$ID_BALANZA, $cod['_IDBalanza'])
                                            ->where(ACA_Pesaje::$FECHA_ANULACION, NULL)
                                            ->where(ACA_Pesaje::$FECHA,'<>',NULL)
                                            ->where(DB::raw('CAST('.ACA_Pesaje::$FECHA.' as Date)'),'>=','01/09/2016')
                                            ->where(DB::raw('CAST('.ACA_Pesaje::$FECHA.' as Date)'),'<=',date('d').'/'.date('m').'/'.(date('Y')-1))
                                            ->where(ACA_Pesaje::$ID_PRODUCTO,77)
                                            ->where(ACA_Pesaje::$ID_PLANTA_DESTINO, $cod['_IDBalanza'])
                                            ->where(ACA_Pesaje::$ESTADO, 2)
                                            ->where(ACA_Pesaje::$TE_CERTIFICADO, 1)
                                            ->get();
            
            
            $total_hv_no_certi_proveedores =    ACA_Pesaje::select(DB::raw('SUM(neto) as peso'))->where(ACA_Pesaje::$ID_BALANZA, $cod['_IDBalanza'])
                                            ->where(ACA_Pesaje::$FECHA_ANULACION, NULL)
                                            ->where(ACA_Pesaje::$FECHA,'<>',NULL)
                                            ->where(DB::raw('CAST('.ACA_Pesaje::$FECHA.' as Date)'),'>=','01/09/2017')
                                            ->where(ACA_Pesaje::$ID_PRODUCTO,77)
                                            ->where(ACA_Pesaje::$ID_PLANTA_DESTINO, $cod['_IDBalanza'])
                                            ->where(ACA_Pesaje::$ESTADO, 2)
                                            ->where(ACA_Pesaje::$TE_CERTIFICADO, 0)
                                            ->get();
            
            
            $total_hv_no_certi_proveedores_last_year =    ACA_Pesaje::select(DB::raw('SUM(neto) as peso'))->where(ACA_Pesaje::$ID_BALANZA, $cod['_IDBalanza'])
                                            ->where(ACA_Pesaje::$FECHA_ANULACION, NULL)
                                            ->where(ACA_Pesaje::$FECHA,'<>',NULL)
                                            ->where(DB::raw('CAST('.ACA_Pesaje::$FECHA.' as Date)'),'>=','01/09/2016')
                                            ->where(DB::raw('CAST('.ACA_Pesaje::$FECHA.' as Date)'),'<=',date('d').'/'.date('m').'/'.(date('Y')-1))
                                            ->where(ACA_Pesaje::$ID_PRODUCTO,77)
                                            ->where(ACA_Pesaje::$ID_PLANTA_DESTINO, $cod['_IDBalanza'])
                                            ->where(ACA_Pesaje::$ESTADO, 2)
                                            ->where(ACA_Pesaje::$TE_CERTIFICADO, 0)
                                            ->get();
            
            
            
        
            $hv_certi_proveedores                       = $hv_certi_proveedores                     + (float)$total_hv_certi_proveedores[0]['peso'];
            $hv_no_certi_proveedores                    = $hv_no_certi_proveedores                  + (float)$total_hv_no_certi_proveedores[0]['peso'];
            $hv_certi_proveedores_last_year             = $hv_certi_proveedores_last_year           + (float)$total_hv_certi_proveedores_last_year[0]['peso'];
            $hv_no_certi_proveedores_last_year          = $hv_no_certi_proveedores_last_year        + (float)$total_hv_no_certi_proveedores_last_year[0]['peso'];
            
            $total_hv_certi_propio =    ACA_Pesaje::select(DB::raw('SUM(neto) as peso'))->where(ACA_Pesaje::$ID_BALANZA, $cod['_IDBalanza'])
                                            ->where(ACA_Pesaje::$FECHA_ANULACION, NULL)
                                            ->where(ACA_Pesaje::$FECHA,'<>',NULL)
                                            ->where(DB::raw('CAST('.ACA_Pesaje::$FECHA.' as Date)'),'>=','01/09/2017')
                                            ->where(ACA_Pesaje::$ID_PRODUCTO,78)
                                            ->where(ACA_Pesaje::$ID_PLANTA_DESTINO, $cod['_IDBalanza'])
                                            ->where(ACA_Pesaje::$ESTADO, 2)
                                            ->where(ACA_Pesaje::$TE_CERTIFICADO, 1)
                                            ->get();
            
            
            $total_hv_no_certi_propio =    ACA_Pesaje::select(DB::raw('SUM(neto) as peso'))->where(ACA_Pesaje::$ID_BALANZA, $cod['_IDBalanza'])
                                            ->where(ACA_Pesaje::$FECHA_ANULACION, NULL)
                                            ->where(ACA_Pesaje::$FECHA,'<>',NULL)
                                            ->where(DB::raw('CAST('.ACA_Pesaje::$FECHA.' as Date)'),'>=','01/09/2017')
                                            ->where(ACA_Pesaje::$ID_PRODUCTO,78)
                                            ->where(ACA_Pesaje::$ID_PLANTA_DESTINO, $cod['_IDBalanza'])
                                            ->where(ACA_Pesaje::$ESTADO, 2)
                                            ->where(ACA_Pesaje::$TE_CERTIFICADO, 0)
                                            ->get();
            
            
            $total_hv_certi_propio_last_year =    ACA_Pesaje::select(DB::raw('SUM(neto) as peso'))->where(ACA_Pesaje::$ID_BALANZA, $cod['_IDBalanza'])
                                            ->where(ACA_Pesaje::$FECHA_ANULACION, NULL)
                                            ->where(ACA_Pesaje::$FECHA,'<>',NULL)
                                            ->where(DB::raw('CAST('.ACA_Pesaje::$FECHA.' as Date)'),'>=','01/09/2016')
                                            ->where(DB::raw('CAST('.ACA_Pesaje::$FECHA.' as Date)'),'<=',date('d').'/'.date('m').'/'.(date('Y')-1))
                                            ->where(ACA_Pesaje::$ID_PRODUCTO,78)
                                            ->where(ACA_Pesaje::$ID_PLANTA_DESTINO, $cod['_IDBalanza'])
                                            ->where(ACA_Pesaje::$ESTADO, 2)
                                            ->where(ACA_Pesaje::$TE_CERTIFICADO, 1)
                                            ->get();
            
            
            $total_hv_no_certi_propio_last_year =    ACA_Pesaje::select(DB::raw('SUM(neto) as peso'))->where(ACA_Pesaje::$ID_BALANZA, $cod['_IDBalanza'])
                                            ->where(ACA_Pesaje::$FECHA_ANULACION, NULL)
                                            ->where(ACA_Pesaje::$FECHA,'<>',NULL)
                                            ->where(DB::raw('CAST('.ACA_Pesaje::$FECHA.' as Date)'),'>=','01/09/2016')
                                            ->where(DB::raw('CAST('.ACA_Pesaje::$FECHA.' as Date)'),'<=',date('d').'/'.date('m').'/'.(date('Y')-1))
                                            ->where(ACA_Pesaje::$ID_PRODUCTO,78)
                                            ->where(ACA_Pesaje::$ID_PLANTA_DESTINO, $cod['_IDBalanza'])
                                            ->where(ACA_Pesaje::$ESTADO, 2)
                                            ->where(ACA_Pesaje::$TE_CERTIFICADO, 0)
                                            ->get();
            
        
            $hv_certi_propio                    = $hv_certi_propio                      + (float)$total_hv_certi_propio[0]['peso'];
            $hv_no_certi_propio                 = $hv_no_certi_propio                   + (float)$total_hv_no_certi_propio[0]['peso'];
            $hv_certi_propio_last_year          = $hv_certi_propio_last_year            + (float)$total_hv_certi_propio_last_year[0]['peso'];
            $hv_no_certi_propio_last_year       = $hv_no_certi_propio_last_year         + (float)$total_hv_no_certi_propio_last_year[0]['peso'];
            
        }
        
        $total_cert_2016_2017       = $hv_certi_proveedores_last_year + $hv_certi_propio_last_year;
        $total_cert_2017_2018       = $hv_certi_proveedores + $hv_certi_propio;
        $total_no_cert_2016_2017    = $hv_no_certi_proveedores_last_year + $hv_no_certi_propio_last_year;
        $total_no_cert_2017_2018    = $hv_no_certi_proveedores + $hv_no_certi_propio;
        $total_2016_2017            = $total_cert_2016_2017 + $total_no_cert_2016_2017;
        $total_2017_2018            = $total_cert_2017_2018 + $total_no_cert_2017_2018;
        
        $porc_total_cert            = (($total_cert_2017_2018 * 100) / $total_cert_2016_2017) - 100; //$total_cert_2016_2017
        $porc_total_cert            = number_format($porc_total_cert,1);
        $porc_total_no_cert         = (($total_no_cert_2017_2018 * 100) / $total_no_cert_2016_2017) - 100;
        $porc_total_no_cert         = number_format($porc_total_no_cert,1);
        $porc_total                 = (($total_2017_2018 * 100) / $total_2016_2017) - 100;
        $porc_total                 = number_format($porc_total,1);
        
        $total_cert_2016_2017       = number_format($total_cert_2016_2017);   
        $total_cert_2017_2018       = number_format($total_cert_2017_2018);   
        $total_no_cert_2016_2017    = number_format($total_no_cert_2016_2017);
        $total_no_cert_2017_2018    = number_format($total_no_cert_2017_2018);
        $total_2016_2017            = number_format($total_2016_2017);        
        $total_2017_2018            = number_format($total_2017_2018); 
        
        $porc_cert_prov        = (($hv_certi_proveedores * 100) / $hv_certi_proveedores_last_year) - 100;
        $porc_cert_prov        = number_format($porc_cert_prov,1);
        $porc_no_cert_prov     = (($hv_no_certi_proveedores * 100) / $hv_no_certi_proveedores_last_year) - 100;
        $porc_no_cert_prov     = number_format($porc_no_cert_prov,1);
        
        $porc_cert_propio     = (($hv_certi_propio * 100) / $hv_certi_propio_last_year) - 100;
        $porc_cert_propio     = number_format($porc_cert_propio,1);
        $porc_no_cert_propio     = (($hv_no_certi_propio * 100) / $hv_no_certi_propio_last_year) - 100;
        $porc_no_cert_propio     = number_format($porc_no_cert_propio,1);
        
        
        $hv_certi_proveedores                       = number_format($hv_certi_proveedores);
        $hv_no_certi_proveedores                    = number_format($hv_no_certi_proveedores);
        $hv_certi_proveedores_last_year             = number_format($hv_certi_proveedores_last_year);
        $hv_no_certi_proveedores_last_year          = number_format($hv_no_certi_proveedores_last_year);
        
        
        $hv_certi_propio                    = number_format($hv_certi_propio);
        $hv_no_certi_propio                 = number_format($hv_no_certi_propio);
        $hv_certi_propio_last_year          = number_format($hv_certi_propio_last_year);
        $hv_no_certi_propio_last_year       = number_format($hv_no_certi_propio_last_year);
        
                
             
        
        
        return  view('home', compact('hv_certi_proveedores',
                                    'hv_no_certi_propio_last_year',
                                    'hv_certi_propio_last_year',
                                    'hv_certi_proveedores_last_year',
                                    'hv_no_certi_proveedores_last_year',
                                    'hv_no_certi_proveedores',
                                    'hv_certi_propio',
                                    'hv_no_certi_propio',
                                    'total_cert_2016_2017',
                                    'total_cert_2017_2018',
                                    'total_no_cert_2016_2017',
                                    'total_no_cert_2017_2018',
                                    'total_2016_2017',
                                    'total_2017_2018',
                                    'porc_cert_prov',
                                    'derivado_1617',
                                    'derivado_1718',
                                    'porc_total_cert','porc_total_no_cert','porc_total','porc_no_cert_prov',
                                    'porc_cert_propio','porc_no_cert_propio','stock','hv_x_chacra_tv02_last_zafra',
                                    'rinde_chacras','prod_secaderos_last_year','rinde_secaderos','fecha_actualizacion_format')
                );
        
        
    }
}
