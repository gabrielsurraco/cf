<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDO;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Auth;
use App\Model\ACA\ACA_Producto;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Model\ACA\ACA_Mov_Stock;
use App\ActualizacionBD;
use Illuminate\Support\Facades\Route;
use App\Model\ACA\ACA_Pesaje;




class SecaderosController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        
        
        $productos = DB::table('productos')->select(array('codigo','descripcion'))
                        ->where('activo',1)
                     ->groupBy('codigo','descripcion')->orderBy('codigo', 'asc')->get();

        $p = array();
        foreach($productos as $producto => $value){
            $p[$value->codigo] = $value->descripcion;
        }

        return view('reportes.secaderos', compact('p'));
        
    }
    
    public function buscar(Request $request){
        
        $bd = array();
        
        if(isset($request->toArray()['aca']))
            array_push($bd, 1);
        
        if(isset($request->toArray()['cg']))
            array_push($bd, 2);
        
        if(isset($request->toArray()['cv']))
            array_push($bd, 3);
        
        if(isset($request->toArray()['obso']))
            array_push($bd, 4);
        
        if(isset($request->toArray()['ta']))
            array_push($bd, 5);
        
//        if($request->toArray()['id_producto'] != 'default')
//            $codigo = $request->toArray()['id_producto'];
        
        if(isset($request->toArray()['fecha_desde'])){
            $fecha_desde = $request->toArray()['fecha_desde'];
        }
        
        if(isset($request->toArray()['fecha_hasta'])){
            $fecha_hasta = $request->toArray()['fecha_hasta'];
        }
        
        try{
            $movimientos = DB::table('mov_stock')
            ->selectRaw('id_bd, id_producto, sum(cantidad) as sum')
            ->whereIN('id_bd', $bd)
            ->where('fecha','>=',$fecha_desde)
            ->where('fecha','<=',$fecha_hasta)
            ->where('id_motivo',7)
            ->groupBy('id_bd','id_producto')
            ->orderBy('id_bd','asc','id_producto')
            ->get();
            
            $datos = [];
            $i = 0;
            $total = null;
            $aca = 0;   $cg = 0; $cv = 0; $obso = 0; $ta = 0;
            
            /*
             * @param1 $fecha_desde
             * @param2 $fecha_hasta
             * @param3 id_talonario
             * @param4 id_bd
             * @param5 planta_origen
             * @param6 planta_destino
             */
//            public function getAcaPesajes(){
//                
//            }


            //te recibido
            $tv_aca_destino =   DB::table('pesajes')
                        ->selectRaw('sum(neto) as suma')
                        ->where('fecha','>=',$fecha_desde)
                        ->where('fecha','<=',$fecha_hasta)
                        ->where('id_talonario',8)
                        ->where('id_bd',1)
                        ->where('id_planta_destino',91)->first();
            
            //te derivado
            $tv_aca_origen =   DB::table('pesajes')
                        ->selectRaw('sum(neto) as suma')
                        ->where('fecha','>=', $fecha_desde)
                        ->where('fecha','<=', $fecha_hasta)
                        ->where('id_talonario',8)
                        ->where('id_bd',1)
                        ->where('id_planta_origen',91)->first();
            $brote_aca = $tv_aca_destino->suma - $tv_aca_origen->suma;
            
            $tv_cg_destino =   DB::table('pesajes')
                        ->selectRaw('sum(neto) as suma')
                        ->where('fecha','>=',$fecha_desde)
                        ->where('fecha','<=',$fecha_hasta)
                        ->where('id_talonario',8)
                        ->where('id_bd',2)
                        ->where('id_planta_destino',88)->first();
            
            $tv_cg_origen =   DB::table('pesajes')
                        ->selectRaw('sum(neto) as suma')
                        ->where('fecha','>=',$fecha_desde)
                        ->where('fecha','<=',$fecha_hasta)
                        ->where('id_talonario',8)
                        ->where('id_bd',2)
                        ->where('id_planta_origen',88)->first();
            $brote_cg = $tv_cg_destino->suma - $tv_cg_origen->suma;
            
            $tv_cv_destino =   DB::table('pesajes')
                        ->selectRaw('sum(neto) as suma')
                        ->where('fecha','>=',$fecha_desde)
                        ->where('fecha','<=',$fecha_hasta)
                        ->where('id_talonario',8)
                        ->where('id_bd',3)
                        ->where('id_planta_destino',89)->first();
            
            $tv_cv_origen =   DB::table('a_c_a__pesajes')
                        ->selectRaw('sum(neto) as suma')
                        ->where('fecha','>=',$fecha_desde)
                        ->where('fecha','<=',$fecha_hasta)
                        ->where('id_talonario',8)
                        ->where('id_bd',3)
                        ->where('id_planta_origen',89)->first();
            $brote_cv = $tv_cv_destino->suma - $tv_cv_origen->suma;
            
            $tv_obso_destino =   DB::table('a_c_a__pesajes')
                        ->selectRaw('sum(neto) as suma')
                        ->where('fecha','>=',$fecha_desde)
                        ->where('fecha','<=',$fecha_hasta)
                        ->where('id_talonario',8)
                        ->where('id_bd',4)
                        ->where('id_planta_destino',87)->first();
            
            $tv_obso_origen =   DB::table('pesajes')
                        ->selectRaw('sum(neto) as suma')
                        ->where('fecha','>=',$fecha_desde)
                        ->where('fecha','<=',$fecha_hasta)
                        ->where('id_talonario',8)
                        ->where('id_bd',4)
                        ->where('id_planta_origen',87)->first();
            $brote_obso = $tv_obso_destino->suma - $tv_obso_origen->suma;
            
            $tv_ta_destino =   DB::table('pesajes')
                        ->selectRaw('sum(neto) as suma')
                        ->where('fecha','>=',$fecha_desde)
                        ->where('fecha','<=',$fecha_hasta)
                        ->where('id_talonario',8)
                        ->where('id_bd',5)
                        ->where('id_planta_destino',9)->first();
            
            $tv_ta_origen =   DB::table('a_c_a__pesajes')
                        ->selectRaw('sum(neto) as suma')
                        ->where('fecha','>=',$fecha_desde)
                        ->where('fecha','<=',$fecha_hasta)
                        ->where('id_talonario',8)
                        ->where('id_bd',5)
                        ->where('id_planta_origen',9)->first();
            $brote_ta = $tv_ta_destino->suma - $tv_ta_origen->suma;
            
            
            foreach($movimientos as $movimiento => $value){
                if($value->id_bd == 1)
                    $aca    = $aca + round($value->sum, 2);
                if($value->id_bd == 2)
                    $cg     = $cg + round($value->sum, 2);
                if($value->id_bd == 3)
                    $cv     = $cv + round($value->sum, 2);
                if($value->id_bd == 4)
                    $obso   = $obso + round($value->sum, 2);
                if($value->id_bd == 5)
                    $ta     = $ta + round($value->sum, 2);
            }
            
//            echo $aca . "<br/>";
//            echo $cg . "<br/>";
//            echo $cv . "<br/>";
//            echo $obso . "<br/>";
//            echo $ta . "<br/>";
//            die();
            
            foreach($movimientos as $movimiento => $value){
                
                $total  = $total + round($value->sum, 2);
                $dbname = $this->getBDName($value->id_bd);
                $product_name = $this->getProductName($value->id_producto, $value->id_bd);
                
                if($value->id_bd == 1){
                    $datos[$i] =    array(  'base'      => $dbname, 
                                            'producto'  => $product_name,
                                            'suma'      => round($value->sum, 2),
                                            'representa' => round(($value->sum / $aca)* 100 ,2)
                                        );
                }
                if($value->id_bd == 2){
                    $datos[$i] =    array(  'base'      => $dbname, 
                                            'producto'  => $product_name,
                                            'suma'      => round($value->sum, 2),
                                            'representa'=> round(($value->sum / $cg)* 100 ,2)  
                                        );
                }
                if($value->id_bd == 3){
                    $datos[$i] =    array(  'base'      => $dbname, 
                                            'producto'  => $product_name,
                                            'suma'      => round($value->sum, 2),
                                            'representa'=> round(($value->sum / $cv)* 100 ,2)    
                                        );
                }
                if($value->id_bd == 4){
                    $datos[$i] =    array(  'base'      => $dbname, 
                                            'producto'  => $product_name,
                                            'suma'      => round($value->sum, 2),
                                            'representa'=> round(($value->sum / $obso)* 100,2)     
                                        ); 
                }
                if($value->id_bd == 5){
                    $datos[$i] =    array(  'base'      => $dbname, 
                                            'producto'  => $product_name,
                                            'suma'      => round($value->sum, 2),
                                            'representa'=> round(($value->sum / $ta)* 100,2)     
                                        ); 
                }
                
                $i++;
            }
            
            
            array_push($datos, array('base' => 1, 'producto' => 2,'total' => $total));
            array_push($datos, array(   'aca'   => $aca,
                                        'cg'    => $cg,
                                        'cv'    => $cv,
                                        'obso'  => $obso,
                                        'ta'    => $ta));
            
            array_push($datos, array(   'brote_cg'      => (int)$brote_cg,
                                        'brote_cv'      => (int)$brote_cv,
                                        'brote_obso'    => (int)$brote_obso,
                                        'brote_ta'      => (int)$brote_ta,
                                        'brote_aca'     => (int)$brote_aca));
            
//            echo $brote_aca . "<br/>";
//            echo $brote_ta . "<br/>";
//            echo $brote_obso . "<br/>";
//            echo $brote_cv . "<br/>";
//            echo $brote_cg . "<br/>";
//            die();
            
            $rendimientos = array();
            
            if($brote_aca !== 0){
                $rendimientos[0] = array('rendimiento_aca'     => round(($aca/$brote_aca)*100,2));
//                array_push($datos, array('rendimiento_aca'     => round(($aca/$brote_aca)*100,2)));
            }else{
                $rendimientos[0] = array('rendimiento_aca'     => 0);
//                array_push($datos, array('rendimiento_aca'     => 0));
            }
            
            if($brote_ta !== 0){
                $rendimientos[0] = array('rendimiento_ta'      => round(($ta/$brote_ta)*100,2));
//                array_push($datos, array('rendimiento_ta'      => round(($ta/$brote_ta)*100,2)));
            }else{
                $rendimientos[0] = array('rendimiento_ta'      => 0);
//                array_push($datos, array('rendimiento_ta'      => 0));
            }
            
            if($brote_obso !== 0){
                $rendimientos[0] = array('rendimiento_obso'    => round(($obso/$brote_obso)*100,2));
//                array_push($datos, array('rendimiento_obso'    => round(($obso/$brote_obso)*100,2)));
            }else{
                $rendimientos[0] = array('rendimiento_obso'    => 0);
//                array_push($datos, array('rendimiento_obso'    => 0));
            }
            
            if($brote_cv !== 0){
                array_push($datos, array('rendimiento_cv'      => round(($cv/$brote_cv)*100,2)));
            }else{
                array_push($datos, array('rendimiento_cv'      => 0));
            }
            
            if($brote_cg !== 0){
                $rendimientos[0] = array('rendimiento_cg'      => round(($cg/$brote_cg)*100,2));
//                array_push($datos, array('rendimiento_cg'      => round(($cg/$brote_cg)*100,2)));
            }else{
                $rendimientos[0] = array('rendimiento_cg'      => 0);
//                array_push($datos, array('rendimiento_cg'      => 0));
            }
            
            array_push($datos, $rendimientos);
            
//            array_push($datos, array(   'rendimiento_cg'      => round(($cg/$brote_cg)*100,2),
//                                        'rendimiento_cv'      => round(($cv/$brote_cv)*100,2),
//                                        'rendimiento_obso'    => round(($obso/$brote_obso)*100,2),
//                                        'rendimiento_ta'      => round(($ta/$brote_ta)*100,2),
//                                        'rendimiento_aca'     => round(($aca/$brote_aca)*100,2)));
//            var_dump($datos);die();
        } catch (Exception $e){
            echo $e->getMessage();
        }
        

        echo json_encode($datos);
    }

    public function getBDName($param0) {
        $a = ActualizacionBD::select('nombre_bd')->where('id',$param0)->first();
        return $a->toArray()['nombre_bd'];
    }

    public function getProductName($id_producto, $id_bd) {
        $a = ACA_Producto::select('descripcion')->where('id_bd',$id_bd)->where('id_producto',$id_producto)->first();
        return $a->toArray()['descripcion'];
    }

}
