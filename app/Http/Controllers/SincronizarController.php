<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDO;
use Carbon\Carbon;
use DateTime;
use Illuminate\Routing\Redirector;
use App\Model\ACA\ACA_Categoria_Producto;
use App\Model\ACA\ACA_Chacra;
use App\Model\ACA\ACA_Cliente;
use App\Model\ACA\ACA_Det_Lista_Precio;
use App\Model\ACA\ACA_Det_Lote;
use App\Model\ACA\ACA_Det_Remito;
use App\Model\ACA\ACA_Embalaje;
use App\Model\ACA\ACA_Lista_Precio;
use App\Model\ACA\ACA_Lote;
use App\Model\ACA\ACA_Motivo;
use App\Model\ACA\ACA_Mov_Stock;
use App\Model\ACA\ACA_Pais;
use App\Model\ACA\ACA_Param_Importa_Tipificacion;
use App\Model\ACA\ACA_Parametro;
use App\Model\ACA\ACA_Pesaje;
use App\Model\ACA\ACA_Procesamiento_Vale;
use App\Model\ACA\ACA_Producto;
use App\Model\ACA\ACA_Proveedor;
use App\Model\ACA\ACA_Provincia;
use App\Model\ACA\ACA_Remito;
use App\Model\ACA\ACA_Rodado;
use App\Model\ACA\ACA_Saldo;
use App\Model\ACA\ACA_Tipo_Comprobante;
use App\Model\ACA\ACA_Tipo_Humedad;
use App\Model\ACA\ACA_Tipo_Precio;
use App\Model\ACA\ACA_Transporte;
use App\ActualizacionBD;
use Illuminate\Support\Facades\Auth;
use Exception;
use Illuminate\Support\Facades\Response;
use PDOException;
use App\Bolson;
use App\TrazaBolson;
use Illuminate\Support\Facades\DB;






class SincronizarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function sincronizarBase(Request $request){
        $base = ActualizacionBD::findOrFail($request->id_bd);

        $path_base = app_path() . "/expotec_data/" . $base->Nomenclatura . "/balanza.mdb";

        try{
            $this->fromAccessToSQL($path_base , $request->id_bd);
            ActualizacionBD::where(ActualizacionBD::$ID, $base->ID)
            ->update([ActualizacionBD::$USUARIO_SINCRONIZACION => Auth::user()->name,ActualizacionBD::$FECHA_SINCRONIZACION => date('d/m/Y H:i:s')]);
            $base = ActualizacionBD::findOrFail($request->id_bd);
            $mensaje = "La base " . $base[ActualizacionBD::$NOMENCLATURA] . " se sincronizó correctamente.";
            return response()->json(array("msg" => $mensaje,"actualizacion" => array('usuario' => $base[ActualizacionBD::$USUARIO_SINCRONIZACION],'fecha' => $base[ActualizacionBD::$FECHA_SINCRONIZACION])), 200);

        }catch(\PDOException $ex){
            return response()->json(array("msg" => $ex->getMessage()), 400);
        }

    }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bases_access = ActualizacionBD::where('estado',true)->get();
        return view('sincronizar', compact('bases_access'));
    }

    public function buscarDatos($path, $sql) {

        try{
            if (!file_exists($path)){
                $result = null;
                die("Could not find database file.");
            }else{
                $db = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)};charset=UTF-8; DBQ=$path; Uid=; Pwd=expobal;");
                $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $result = $db->query($sql);
            }

            return $result;
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }

    }

    //      <editor-fold defaultstate="collapsed" desc="fromAccessToSQL">
    public function fromAccessToSQL($pathAccessDB, $prefijo) {


        try{
            
            $sqlcategoriaproducto  = "select ccpp.[id], ccpp.[descripcion], ccpp.[_Creado_Por] ,ccpp.[_Fecha_Creacion] , ccpp.[_Modificado_Por], ccpp.[_Fecha_Modificacion] , ccpp.[_Observaciones] from CATEGORIASPRODUCTOS ccpp";
            $this->cargarCategoriaProducto($this->buscarDatos($pathAccessDB, $sqlcategoriaproducto) , ACA_Categoria_Producto::class, $prefijo);

            $sqlchacra  = "select * from CHACRAS";
            $this->cargarChacra($this->buscarDatos($pathAccessDB, $sqlchacra) , ACA_Chacra::class, $prefijo);

            $sqlcliente  = "select * from CLIENTES";
            $this->cargarCliente($this->buscarDatos($pathAccessDB, $sqlcliente) , ACA_Cliente::class, $prefijo);

            $sqlDetListaPrecio  = "select * from DETLISTAPRECIOS";
            $this->cargarDetListaPrecio($this->buscarDatos($pathAccessDB, $sqlDetListaPrecio) , ACA_Det_Lista_Precio::class, $prefijo);

            $sqlDetLote  = "select * from DETLOTE";
            $this->cargarDetLote($this->buscarDatos($pathAccessDB, $sqlDetLote) , ACA_Det_Lote::class, $prefijo);

            $sqlDetRemito  = "select * from DETREMITOS";
            $this->cargarDetRemito($this->buscarDatos($pathAccessDB, $sqlDetRemito) , ACA_Det_Remito::class, $prefijo);

            $sql  = "select * from EMBALAJES";
            $this->cargarEmbalaje($this->buscarDatos($pathAccessDB, $sql) , ACA_Embalaje::class, $prefijo);

            $sql  = "select * from LISTAPRECIOS";
            $this->cargarListaPrecio($this->buscarDatos($pathAccessDB, $sql) , ACA_Lista_Precio::class, $prefijo);
            
            $sql  = "select * from LOTES";
            $this->cargarLotes($this->buscarDatos($pathAccessDB, $sql) , ACA_Lote::class, $prefijo);
            
            $sql  = "select * from MOTIVOS";
            $this->cargarMotivos($this->buscarDatos($pathAccessDB, $sql) , ACA_Motivo::class, $prefijo);
            
            $sql    = "select * from MOV_STOCK";
            $this->cargarMovStock($this->buscarDatos($pathAccessDB, $sql) , ACA_Mov_Stock::class, $prefijo);

            $sql  = "select * from PAISES";
            $this->cargarPaises($this->buscarDatos($pathAccessDB, $sql) , ACA_Pais::class, $prefijo);

            $sql  = "select * from PARAM_IMPORTATIPIFICACION";
            $this->cargarParamImportaTipificacion($this->buscarDatos($pathAccessDB, $sql) , ACA_Param_Importa_Tipificacion::class, $prefijo);

            $sql  = "select * from PARAMETROS";
            $this->cargarParametros($this->buscarDatos($pathAccessDB, $sql) , ACA_Parametro::class, $prefijo);
            
             // A VECES HAY QUE SINCRONIZAR ESTA TABLA POR SEPARADO
            $sql  = "select * from PESAJES";
            $this->cargarPesajes($this->buscarDatos($pathAccessDB, $sql) , ACA_Pesaje::class, $prefijo);
            
            
             $sql  = "select * from PROCESAMIENTOVALES";
            $this->cargarProcesamientoVales($this->buscarDatos($pathAccessDB, $sql) , ACA_Procesamiento_Vale::class, $prefijo);

            $sql  = "select * from PRODUCTOS";
            $this->cargarProductos($this->buscarDatos($pathAccessDB, $sql) , ACA_Producto::class, $prefijo);

            $sql  = "select * from PROVEEDORES";
            $this->cargarProveedores($this->buscarDatos($pathAccessDB, $sql) , ACA_Proveedor::class, $prefijo);

            $sql  = "select * from PROVINCIAS";
            $this->cargarProvincias($this->buscarDatos($pathAccessDB, $sql) , ACA_Provincia::class, $prefijo);

            $sql  = "select * from REMITOS";
            $this->cargarRemitos($this->buscarDatos($pathAccessDB, $sql) , ACA_Remito::class, $prefijo);

            $sql  = "select * from RODADOS";
            $this->cargarRodados($this->buscarDatos($pathAccessDB, $sql) , ACA_Rodado::class, $prefijo);
            
            //CV2 - DB - DCG - DCH - OBMY - CVM
            if($prefijo != 99 && $prefijo != 124 && $prefijo != 121 && $prefijo != 122 && $prefijo != 98 && $prefijo != 90){
                $sql  = "select * from _SALDOS";
                $this->cargarSaldos($this->buscarDatos($pathAccessDB, $sql) , ACA_Saldo::class,$prefijo);
            }

            $sql  = "select * from TIPOCOMPROBANTE";
            $this->cargarTipoComprobante($this->buscarDatos($pathAccessDB, $sql) , ACA_Tipo_Comprobante::class, $prefijo);

            $sql  = "select * from TIPOHUMEDAD";
            $this->cargarTipoHumedad($this->buscarDatos($pathAccessDB, $sql) , ACA_Tipo_Humedad::class, $prefijo);

            $sql  = "select * from TIPOPRECIO";
            $this->cargarTipoPrecio($this->buscarDatos($pathAccessDB, $sql) , ACA_Tipo_Precio::class, $prefijo);

            $sql  = "select * from TRANSPORTES";
            $this->cargarTransportes($this->buscarDatos($pathAccessDB, $sql) , ACA_Transporte::class, $prefijo);
            
            
            $sql = "select * from BOLSONES";
            $this->cargarBolsones($this->buscarDatos($pathAccessDB, $sql), Bolson::class, $prefijo);

            $sql = "select * from REL_TRAZA_BOLSONES";
            $this->cargarTrazaBolsones($this->buscarDatos($pathAccessDB, $sql), TrazaBolson::class, $prefijo );
            
            
        } catch (Exception $ex) {
            echo $ex->getMessage();
            die($ex->getMessage());
        }


    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarBolsones">
      public function cargarBolsones($result, $model, $prefijo) {

        if($result != NULL){
            $row = $result->fetchall();

           $model::where(Bolson::$ID_BALANZA , $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['Fecha'])){
                    $fecha = DateTime::createFromFormat('Y-m-d H:i:s', $result['Fecha'])->format('d/m/Y H:i:s');
                }else{
                    $fecha = null;
                }

                if(!empty($result['_Fecha_Creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_Modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }



                $bolson = new $model;

                $bolson[Bolson::$FECHA]               = $fecha;
                $bolson[Bolson::$ID_BALANZA]          = $prefijo;
                $bolson[Bolson::$ID_BOLSON]           = $result['Id'];
                $bolson[Bolson::$ID_PRODUCTO]         = $result['_IDProducto'];
                $bolson[Bolson::$CANTIDAD]            = $result['Cantidad'];
                $bolson[Bolson::$M3]                  = $result['M3'];
                $bolson[Bolson::$BOLSON]              = utf8_encode($result['Bolson']);
                $bolson[Bolson::$PLANTA_ORIGEN]       = utf8_encode($result['Planta_origen']);
                $bolson[Bolson::$DENSIDAD]            = utf8_encode($result['Densidad']);
                $bolson[Bolson::$PORC_FINO]           = utf8_encode($result['Porc_fino']);
                $bolson[Bolson::$CAPATAZ]             = utf8_encode($result['Capataz']);
                $bolson[Bolson::$ANIOZAFRA]           = $result['AnioZafra'];
                $bolson[Bolson::$IMPRESORA]           = utf8_encode($result['Impresora']);
                $bolson[Bolson::$MANUAL]              = $result['Manual'];
                $bolson[Bolson::$IDPESAJEORIGEN]      = $result['_IDPesajeOrigen'];
                $bolson[Bolson::$NROTICKETORIGEN]     = $result['NroTicketOrigen'];
                $bolson[Bolson::$SINCRONIZADO]        = $result['Sincronizado'];
                
                if(isset($result['Nro_Lote']))
                    $bolson[Bolson::$NRO_LOTE]            = utf8_encode($result['Nro_Lote']);
                
                $bolson[Bolson::$CREADO_POR]          = utf8_encode($result['_Creado_por']);
                $bolson[Bolson::$FECHA_CREACION]      = $fecha_creacion;
                $bolson[Bolson::$MODIFICADO_POR]      = utf8_encode($result['_Modificado_por']);
                $bolson[Bolson::$FECHA_MODIFICACION]  = $fecha_modif;
                $bolson[Bolson::$OBSERVACIONES]       = utf8_encode($result['_Observaciones']);
                $bolson->save();

                // return "asdasd";break;

                unset($result);
            }
        }
      }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarTrazaBolsones">
      public function cargarTrazaBolsones($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();

           $model::where(TrazaBolson::$ID_BALANZA , $prefijo)->delete();
            foreach($row as $r => $result){


                if(!empty($result['Fecha_operacion'])){
                    $fecha_operacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['Fecha_operacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_operacion = null;
                }


                $traza_bolson = new $model;

                $traza_bolson[TrazaBolson::$FECHA_OPERACION]      = $fecha_operacion;
                $traza_bolson[TrazaBolson::$ID_BALANZA]           = $prefijo;
                $traza_bolson[TrazaBolson::$ID_PLANTA]            = $result['_IDPlanta'];
                $traza_bolson[TrazaBolson::$ID_REL_TRAZA_BOLSON]  = $result['Id'];
                $traza_bolson[TrazaBolson::$ID_BOLSON]            = $result['_IDBolson'];
                $traza_bolson[TrazaBolson::$ID_PESAJE]            = $result['_IDPesaje'];
                $traza_bolson[TrazaBolson::$ID_MOV_STOCK]         = $result['_IDMovStock'];
                $traza_bolson[TrazaBolson::$ID_DET_LOTE]          = $result['_IDDetLote'];
                $traza_bolson->save();



                unset($result);
            }
        }
      }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarCliente">
    public function cargarCliente($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();



            $model::where(ACA_Cliente::$ID_BALANZA, $prefijo)->delete();

            foreach($row as $r => $result){

                if(!empty($result['_Fecha_creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

//                $cliente = ACA_Cliente::select(ACA_Cliente::$ID_CLIENTE, ACA_Cliente::$FECHA_MODIFICACION)->where([
//                    [ACA_Cliente::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Cliente::$ID_CLIENTE,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($cliente[ACA_Cliente::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $cliente[ACA_Cliente::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Cliente::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Cliente::$ID_CLIENTE,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Cliente::$ID_CLIENTE    => $result['ID'],
//                            ACA_Cliente::$ID_BALANZA    => $prefijo,
//                            ACA_Cliente::$CODIGO        => $result['Codigo'],
//                            ACA_Cliente::$RAZON_SOCIAL  => utf8_encode($result['Razon_Social']),
//                            ACA_Cliente::$DOMICILIO     => utf8_encode($result['Domicilio']),
//                            ACA_Cliente::$LOCALIDAD     => utf8_encode($result['Localidad']),
//                            ACA_Cliente::$CP            => $result['CP'],
//                            ACA_Cliente::$ID_PAIS       => $result['_IDPais'],
//                            ACA_Cliente::$ID_PROVINCIA  => $result['_IDProvincia'],
//                            ACA_Cliente::$CUIT          => $result['CUIT'],
//                            ACA_Cliente::$EMAIL         => utf8_encode($result['Email']),
//                            ACA_Cliente::$WEB           => utf8_encode($result['Web']),
//                            ACA_Cliente::$TELEFONO      => $result['Telefono'],
//                            ACA_Cliente::$FAX           => $result['Fax'],
//                            ACA_Cliente::$OBSERVACIONES        => utf8_encode($result['Obs']),
//                            ACA_Cliente::$ACTIVO               => $result['Activo'],
//                            ACA_Cliente::$CREADO_POR           => $result['_Creado_por'],
//                            ACA_Cliente::$MODIFICADO_POR       => $result['_Modificado_por'],
//                            ACA_Cliente::$FECHA_CREACION       => $fecha_creacion,
//                            ACA_Cliente::$FECHA_MODIFICACION   => $fecha_modif,
//                        ]);
//                        echo "MODIFICO";
//                    }
//                }else{
                    $CLIENTE = new $model;
                    $CLIENTE[ACA_Cliente::$ID_CLIENTE        ]   = $result['ID'];
                    $CLIENTE[ACA_Cliente::$ID_BALANZA        ]   = $prefijo;
                    $CLIENTE[ACA_Cliente::$CODIGO            ]   = $result['Codigo'];
                    $CLIENTE[ACA_Cliente::$RAZON_SOCIAL      ]   = utf8_encode($result['Razon_Social']);
                    $CLIENTE[ACA_Cliente::$DOMICILIO         ]   = utf8_encode($result['Domicilio']);
                    $CLIENTE[ACA_Cliente::$LOCALIDAD         ]   = utf8_encode($result['Localidad']);
                    $CLIENTE[ACA_Cliente::$CP                ]   = $result['CP'];
                    $CLIENTE[ACA_Cliente::$ID_PAIS           ]   = $result['_IDPais'];
                    $CLIENTE[ACA_Cliente::$ID_PROVINCIA      ]   = $result['_IDProvincia'];
                    $CLIENTE[ACA_Cliente::$CUIT              ]   = $result['CUIT'];
                    $CLIENTE[ACA_Cliente::$EMAIL             ]   = utf8_encode($result['Email']);
                    $CLIENTE[ACA_Cliente::$WEB               ]   = utf8_encode($result['Web']);
                    $CLIENTE[ACA_Cliente::$TELEFONO          ]   = $result['Telefono'];
                    $CLIENTE[ACA_Cliente::$FAX               ]   = $result['Fax'];
                    $CLIENTE[ACA_Cliente::$OBSERVACIONES     ]   = utf8_encode($result['Obs']);
                    $CLIENTE[ACA_Cliente::$ACTIVO            ]   = $result['Activo'];
                    $CLIENTE[ACA_Cliente::$CREADO_POR        ]   = $result['_Creado_por'];
                    $CLIENTE[ACA_Cliente::$MODIFICADO_POR    ]   = $result['_Modificado_por'];
                    $CLIENTE[ACA_Cliente::$FECHA_CREACION    ]   = $fecha_creacion;
                    $CLIENTE[ACA_Cliente::$FECHA_MODIFICACION]   = $fecha_modif;
                    $CLIENTE->save();
//                    echo "CREO";
//                }

                unset($result);
            }
        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarChacra">
    public function cargarChacra($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();

            $model::where(ACA_Chacra::$ID_BALANZA, $prefijo)->delete();

            foreach($row as $r => $result){

                if(!empty($result['_Fecha_creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }
                //CV
                if($prefijo == 89){
                    $chacra_propia = $result['Chacra_Propia'];
                }else{
                    $chacra_propia = $result['Chacra_propia'];
                }

//                $chacras = ACA_Chacra::select(ACA_Chacra::$ID_CHACRA, ACA_Chacra::$FECHA_MODIFICACION)->where([
//                    [ACA_Chacra::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Chacra::$ID_CHACRA,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($chacras[ACA_Chacra::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $chacras[ACA_Chacra::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Chacra::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Chacra::$ID_CHACRA,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Chacra::$ID_CHACRA             => $result['ID'],
//                            ACA_Chacra::$ID_BALANZA            => $prefijo,
//                            ACA_Chacra::$CODIGO                => $result['Codigo'],
//                            ACA_Chacra::$DESCRIPCION           => utf8_encode($result['Descripcion']),
//                            ACA_Chacra::$CHACRA_PROPIA         => $chacra_propia,
//                            ACA_Chacra::$OBS                   => utf8_encode($result['Obs']),
//                            ACA_Chacra::$CREADO_POR            => utf8_encode($result['_Creado_por']),
//                            ACA_Chacra::$MODIFICADO_POR        => utf8_encode($result['_Modificado_por']),
//                            ACA_Chacra::$FECHA_CREACION        => $fecha_creacion,
//                            ACA_Chacra::$FECHA_MODIFICACION    => $fecha_modif
//                        ]);
//                        echo "MODIFICO";
//                    }
//                }else{
                    $CHACRA = new $model;
                    $CHACRA[ACA_Chacra::$ID_CHACRA]             = $result['ID'];
                    $CHACRA[ACA_Chacra::$ID_BALANZA]            = $prefijo;
                    $CHACRA[ACA_Chacra::$CODIGO]                = $result['Codigo'];
                    $CHACRA[ACA_Chacra::$DESCRIPCION]           = utf8_encode($result['Descripcion']);
                    $CHACRA[ACA_Chacra::$CHACRA_PROPIA]         = $chacra_propia;
                    $CHACRA[ACA_Chacra::$OBS]                   = utf8_encode($result['Obs']);
                    $CHACRA[ACA_Chacra::$CREADO_POR]            = utf8_encode($result['_Creado_por']);
                    $CHACRA[ACA_Chacra::$MODIFICADO_POR]        = utf8_encode($result['_Modificado_por']);
                    $CHACRA[ACA_Chacra::$FECHA_CREACION]        = $fecha_creacion;
                    $CHACRA[ACA_Chacra::$FECHA_MODIFICACION]    = $fecha_modif;
                    $CHACRA->save();
//                }
                unset($result);
            }
        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarCategoriaProducto">
    public function cargarCategoriaProducto($result, $model, $prefijo) {

        if($result != NULL){
            $row = $result->fetchall();

            $model::where(ACA_Categoria_Producto::$ID_BALANZA, $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_Creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_Modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

//                $cat_prod = ACA_Categoria_Producto::select(ACA_Categoria_Producto::$ID_CAT_PRODUCT , ACA_Categoria_Producto::$FECHA_MODIFICACION)
//                ->where([
//                    [ACA_Categoria_Producto::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Categoria_Producto::$ID_CAT_PRODUCT,'=',$result['id']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($cat_prod[ACA_Categoria_Producto::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $cat_prod[ACA_Categoria_Producto::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Categoria_Producto::$ID_BALANZA , '=' , $prefijo],
//                            [ACA_Categoria_Producto::$ID_CAT_PRODUCT , '=' , $result['id']]
//                        ])
//                        ->update([
//                            ACA_Categoria_Producto::$ID_BALANZA         => $prefijo,
//                            ACA_Categoria_Producto::$ID_CAT_PRODUCT     => $result['id'],
//                            ACA_Categoria_Producto::$DESCRIPCION        => utf8_encode($result['descripcion']),
//                            ACA_Categoria_Producto::$OBSERVACIONES      => $result['_Observaciones'],
//                            ACA_Categoria_Producto::$CREADO_POR         => utf8_encode($result['_Creado_Por']),
//                            ACA_Categoria_Producto::$MODIFICADO_POR     => utf8_encode($result['_Modificado_Por']),
//                            ACA_Categoria_Producto::$FECHA_CREACION     => $fecha_creacion,
//                            ACA_Categoria_Producto::$FECHA_MODIFICACION => $fecha_modif
//                        ]);
//                    }
//                }else{
                    $CAT_PRODUCTO = new $model;
                    $CAT_PRODUCTO[ACA_Categoria_Producto::$ID_BALANZA]          = $prefijo;
                    $CAT_PRODUCTO[ACA_Categoria_Producto::$ID_CAT_PRODUCT]      = $result['id'];
                    $CAT_PRODUCTO[ACA_Categoria_Producto::$DESCRIPCION]         = utf8_encode($result['descripcion']);
                    $CAT_PRODUCTO[ACA_Categoria_Producto::$OBSERVACIONES]       = $result['_Observaciones'];
                    $CAT_PRODUCTO[ACA_Categoria_Producto::$CREADO_POR]          = utf8_encode($result['_Creado_Por']);
                    $CAT_PRODUCTO[ACA_Categoria_Producto::$MODIFICADO_POR]      = utf8_encode($result['_Modificado_Por']);
                    $CAT_PRODUCTO[ACA_Categoria_Producto::$FECHA_CREACION]      = $fecha_creacion;
                    $CAT_PRODUCTO[ACA_Categoria_Producto::$FECHA_MODIFICACION]  = $fecha_modif;
                    $CAT_PRODUCTO->save();
//                }

                unset($result);
            }
        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarDetListaPrecio">
    public function cargarDetListaPrecio($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();

            $model::where(ACA_Det_Lista_Precio::$ID_BALANZA, $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_Creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_Modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

//                $det_lista_precio = ACA_Det_Lista_Precio::select(ACA_Det_Lista_Precio::$ID_DET_LISTA_PRECIO, ACA_Det_Lista_Precio::$FECHA_MODIFICACION)->where([
//                    [ACA_Det_Lista_Precio::$ID_BALANZA ,'=',$prefijo],
//                    [ACA_Det_Lista_Precio::$ID_DET_LISTA_PRECIO , '=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($det_lista_precio[ACA_Det_Lista_Precio::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $det_lista_precio[ACA_Det_Lista_Precio::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Det_Lista_Precio::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Det_Lista_Precio::$ID_DET_LISTA_PRECIO,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Det_Lista_Precio::$ID_DET_LISTA_PRECIO   => $result['ID'],
//                            ACA_Det_Lista_Precio::$ID_BALANZA            => $prefijo,
//                            ACA_Det_Lista_Precio::$ID_LISTA_PRECIO       => $result['_IDListaPrecio'],
//                            ACA_Det_Lista_Precio::$ID_PRODUCTO           => $result['_IDProducto'],
//                            ACA_Det_Lista_Precio::$PRECIO                => $result['Precio'],
//                            ACA_Det_Lista_Precio::$OBSERVACIONES         => $result['_Observaciones'],
//                            ACA_Det_Lista_Precio::$CREADO_POR            => utf8_encode($result['_Creado_por']),
//                            ACA_Det_Lista_Precio::$MODIFICADO_POR        => utf8_encode($result['_Modificado_por']),
//                            ACA_Det_Lista_Precio::$FECHA_CREACION        => $fecha_creacion,
//                            ACA_Det_Lista_Precio::$FECHA_MODIFICACION    => $fecha_modif
//                        ]);

//                    }
//                }else{
                    $DET_LISTA_PRECIO = NEW $model;
                    $DET_LISTA_PRECIO[ACA_Det_Lista_Precio::$ID_DET_LISTA_PRECIO]   = $result['ID'];
                    $DET_LISTA_PRECIO[ACA_Det_Lista_Precio::$ID_BALANZA         ]   = $prefijo;
                    $DET_LISTA_PRECIO[ACA_Det_Lista_Precio::$ID_LISTA_PRECIO    ]   = $result['_IDListaPrecio'];
                    $DET_LISTA_PRECIO[ACA_Det_Lista_Precio::$ID_PRODUCTO        ]   = $result['_IDProducto'];
                    $DET_LISTA_PRECIO[ACA_Det_Lista_Precio::$PRECIO             ]   = $result['Precio'];
                    $DET_LISTA_PRECIO[ACA_Det_Lista_Precio::$OBSERVACIONES      ]   = $result['_Observaciones'];
                    $DET_LISTA_PRECIO[ACA_Det_Lista_Precio::$CREADO_POR         ]   = utf8_encode($result['_Creado_por']);
                    $DET_LISTA_PRECIO[ACA_Det_Lista_Precio::$MODIFICADO_POR     ]   = utf8_encode($result['_Modificado_por']);
                    $DET_LISTA_PRECIO[ACA_Det_Lista_Precio::$FECHA_CREACION     ]   = $fecha_creacion;
                    $DET_LISTA_PRECIO[ACA_Det_Lista_Precio::$FECHA_MODIFICACION ]   = $fecha_modif;
                    $DET_LISTA_PRECIO->save();
//                }

                unset($result);
            }
        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarDetLote">
    public function cargarDetLote($result, $model, $prefijo) {

        if($result != NULL){
            $row = $result->fetchall();

            $model::where(ACA_Det_Lote::$ID_BALANZA, $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

//                $det_lote = ACA_Det_Lote::select(ACA_Det_Lote::$ID_DET_LOTE,ACA_Det_Lote::$FECHA_MODIFICACION)->where([
//                    [ACA_Det_Lote::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Det_Lote::$ID_DET_LOTE,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($det_lote[ACA_Det_Lote::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $det_lote[ACA_Det_Lote::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Det_Lote::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Det_Lote::$ID_DET_LOTE,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Det_Lote::$ID_DET_LOTE       => $result['ID'],
//                            ACA_Det_Lote::$ID_BALANZA             => $prefijo,
//                            ACA_Det_Lote::$ID_PRODUCTO       => $result['_IDProducto'],
//                            ACA_Det_Lote::$ID_LOTE           => $result['_IDLote'],
//                            ACA_Det_Lote::$PORCENTAJE        => $result['Porcentaje'],
//                            ACA_Det_Lote::$CANT_BOLSAS       => $result['Cant_Bolsas'],
//                            ACA_Det_Lote::$KILOS_NETOS       => $result['Kilos_Netos'],
//                            ACA_Det_Lote::$OBSERVACIONES     => utf8_encode($result['_Observaciones']),
//                            ACA_Det_Lote::$CREADO_POR        => utf8_encode($result['_Creado_por']),
//                            ACA_Det_Lote::$MODIFICADO_POR        => utf8_encode($result['_Modificado_por']),
//                            ACA_Det_Lote::$FECHA_CREACION        => $fecha_creacion,
//                            ACA_Det_Lote::$FECHA_MODIFICACION        => $fecha_modif
//                        ]);
//                        echo "MODIFICO";
//                    }
//                }else{
                    $DET_LOTE = new $model;
                    $DET_LOTE[ACA_Det_Lote::$ID_DET_LOTE       ]  = $result['ID'];
                    $DET_LOTE[ACA_Det_Lote::$ID_BALANZA        ]  = $prefijo;
                    $DET_LOTE[ACA_Det_Lote::$ID_PRODUCTO       ]  = $result['_IDProducto'];
                    $DET_LOTE[ACA_Det_Lote::$ID_LOTE           ]  = $result['_IDLote'];
                    $DET_LOTE[ACA_Det_Lote::$PORCENTAJE        ]  = $result['Porcentaje'];
                    $DET_LOTE[ACA_Det_Lote::$CANT_BOLSAS       ]  = $result['Cant_Bolsas'];
                    $DET_LOTE[ACA_Det_Lote::$KILOS_NETOS       ]  = $result['Kilos_Netos'];
                    $DET_LOTE[ACA_Det_Lote::$OBSERVACIONES     ]  = utf8_encode($result['_Observaciones']);
                    $DET_LOTE[ACA_Det_Lote::$CREADO_POR        ]  = utf8_encode($result['_Creado_por']);
                    $DET_LOTE[ACA_Det_Lote::$MODIFICADO_POR    ]  = utf8_encode($result['_Modificado_por']);
                    $DET_LOTE[ACA_Det_Lote::$FECHA_CREACION    ]  = $fecha_creacion;
                    $DET_LOTE[ACA_Det_Lote::$FECHA_MODIFICACION]  = $fecha_modif;
                    $DET_LOTE->save();
//                }
                unset($result);

            }
        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarDetRemito">
    public function cargarDetRemito($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();

            $model::where(ACA_Det_Remito::$ID_BALANZA, $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

//                $det_remito = ACA_Det_Remito::select(ACA_Det_Remito::$ID_DET_REMITO,ACA_Det_Remito::$FECHA_MODIFICACION)->where([
//                    [ACA_Det_Remito::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Det_Remito::$ID_DET_REMITO,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($det_remito[ACA_Det_Remito::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $det_remito[ACA_Det_Remito::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Det_Remito::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Det_Remito::$ID_DET_REMITO,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Det_Remito::$ID_DET_REMITO     => $result['ID'],
//                            ACA_Det_Remito::$ID_BALANZA             => $prefijo,
//                            ACA_Det_Remito::$ID_REMITO         => $result['_IDRemito'],
//                            ACA_Det_Remito::$ID_LOTE           => $result['_IDLote'],
//                            ACA_Det_Remito::$CANTIDAD_BOLSAS   => (int) $result['Cant_Bolsas'],
//                            ACA_Det_Remito::$TARA_X_BOLSA      => (int) $result['Tara_x_Bolsa'],
//                            ACA_Det_Remito::$PALLETS           => (int) $result['Pallets'],
//                            ACA_Det_Remito::$PESO_X_PALLETS    => (int) $result['Peso_x_Pallets'],
//                            ACA_Det_Remito::$OBSERVACIONES     => $result['_Observaciones'],
//                            ACA_Det_Remito::$CREADO_POR        => utf8_encode($result['_Creado_por']),
//                            ACA_Det_Remito::$MODIFICADO_POR        => utf8_encode($result['_Modificado_por']),
//                            ACA_Det_Remito::$FECHA_CREACION        => $fecha_creacion,
//                            ACA_Det_Remito::$FECHA_MODIFICACION        => $fecha_modif
//                        ]);
//                        echo "MODIFICO";
//                    }
//                }else{
                    $DET_REMITO = new $model;
                    $DET_REMITO[ACA_Det_Remito::$ID_DET_REMITO     ] = $result['ID'];
                    $DET_REMITO[ACA_Det_Remito::$ID_BALANZA        ] = $prefijo;
                    $DET_REMITO[ACA_Det_Remito::$ID_REMITO         ] = $result['_IDRemito'];
                    $DET_REMITO[ACA_Det_Remito::$ID_LOTE           ] = $result['_IDLote'];
                    $DET_REMITO[ACA_Det_Remito::$CANTIDAD_BOLSAS   ] = (int) $result['Cant_Bolsas'];
                    $DET_REMITO[ACA_Det_Remito::$TARA_X_BOLSA      ] = (int) $result['Tara_x_Bolsa'];
                    $DET_REMITO[ACA_Det_Remito::$PALLETS           ] = (int) $result['Pallets'];
                    $DET_REMITO[ACA_Det_Remito::$PESO_X_PALLETS    ] = (int) $result['Peso_x_Pallets'];
                    $DET_REMITO[ACA_Det_Remito::$OBSERVACIONES     ] = $result['_Observaciones'];
                    $DET_REMITO[ACA_Det_Remito::$CREADO_POR        ] = utf8_encode($result['_Creado_por']);
                    $DET_REMITO[ACA_Det_Remito::$MODIFICADO_POR    ] = utf8_encode($result['_Modificado_por']);
                    $DET_REMITO[ACA_Det_Remito::$FECHA_CREACION    ] = $fecha_creacion;
                    $DET_REMITO[ACA_Det_Remito::$FECHA_MODIFICACION] = $fecha_modif;
                    $DET_REMITO->save();
//                }
                unset($result);

            }
        }
    }
    //      </editor-fold>

    //    <editor-fold defaultstate="collapsed" desc="cargarEmbalaje">
    public function cargarEmbalaje($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();

            $model::where(ACA_Embalaje::$ID_BALANZA, $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_Creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_Modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

//                $embalaje = ACA_Embalaje::select(ACA_Embalaje::$ID_EMBALAJE,ACA_Embalaje::$FECHA_MODIFICACION)->where([
//                    [ACA_Embalaje::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Embalaje::$ID_EMBALAJE,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($embalaje[ACA_Embalaje::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $embalaje[ACA_Embalaje::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Embalaje::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Embalaje::$ID_EMBALAJE,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Embalaje::$ID_EMBALAJE       => $result['ID'],
//                            ACA_Embalaje::$ID_BALANZA             => $prefijo,
//                            ACA_Embalaje::$CODIGO            => $result['Codigo'],
//                            ACA_Embalaje::$DESCRIPCION       => $result['Descripcion'],
//                            ACA_Embalaje::$PESO              => $result['Peso'],
//                            ACA_Embalaje::$OBS                => $result['Obs'],
//                            ACA_Embalaje::$CREADO_POR         => utf8_encode($result['_Creado_por']),
//                            ACA_Embalaje::$MODIFICADO_POR        => utf8_encode($result['_Modificado_por']),
//                            ACA_Embalaje::$FECHA_CREACION        => $fecha_creacion,
//                            ACA_Embalaje::$FECHA_MODIFICACION        => $fecha_modif
//                        ]);
//                        echo "MODIFICO";
//                    }
//                }else{
                    $EMBALAJE = new $model;
                    $EMBALAJE[ACA_Embalaje::$ID_EMBALAJE       ] = $result['ID'];
                    $EMBALAJE[ACA_Embalaje::$ID_BALANZA        ] = $prefijo;
                    $EMBALAJE[ACA_Embalaje::$CODIGO            ] = $result['Codigo'];
                    $EMBALAJE[ACA_Embalaje::$DESCRIPCION       ] = $result['Descripcion'];
                    $EMBALAJE[ACA_Embalaje::$PESO              ] = $result['Peso'];
                    $EMBALAJE[ACA_Embalaje::$OBS               ] = $result['Obs'];
                    $EMBALAJE[ACA_Embalaje::$CREADO_POR        ] = utf8_encode($result['_Creado_por']);
                    $EMBALAJE[ACA_Embalaje::$MODIFICADO_POR    ] = utf8_encode($result['_Modificado_por']);
                    $EMBALAJE[ACA_Embalaje::$FECHA_CREACION    ] = $fecha_creacion;
                    $EMBALAJE[ACA_Embalaje::$FECHA_MODIFICACION] = $fecha_modif;
                    $EMBALAJE->save();
//                }

                unset($result);
            }
        }
    }
    // </editor-fold>

    //    <editor-fold defaultstate="collapsed" desc="cargarListaPrecio">
    public function cargarListaPrecio($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();

            $model::where(ACA_Lista_Precio::$ID_BALANZA, $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_Creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_Modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

                if(!empty($result['Fecha'])){
                    $date = DateTime::createFromFormat('Y-m-d H:i:s', $result['Fecha'])->format('d-m-Y H:i:s');
                }else{
                    $date = null;
                }

//                $lista_precio = ACA_Lista_Precio::select(ACA_Lista_Precio::$ID_LISTA_PRECIO, ACA_Lista_Precio::$FECHA_MODIFICACION)->where([
//                    [ACA_Lista_Precio::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Lista_Precio::$ID_LISTA_PRECIO,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($lista_precio[ACA_Lista_Precio::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $lista_precio[ACA_Lista_Precio::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Lista_Precio::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Lista_Precio::$ID_LISTA_PRECIO,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Lista_Precio::$ID_LISTA_PRECIO      => $result['ID'],
//                            ACA_Lista_Precio::$ID_BALANZA           => $prefijo,
//                            ACA_Lista_Precio::$FECHA                => $date,
//                            ACA_Lista_Precio::$ID_PROVEEDOR         => $result['_IDProveedor'],
//                            ACA_Lista_Precio::$OBSERVACIONES        => utf8_encode($result['_Observaciones']),
//                            ACA_Lista_Precio::$CREADO_POR           => utf8_encode($result['_Creado_por']),
//                            ACA_Lista_Precio::$MODIFICADO_POR       => utf8_encode($result['_Modificado_por']),
//                            ACA_Lista_Precio::$FECHA_CREACION       => $fecha_creacion,
//                            ACA_Lista_Precio::$FECHA_MODIFICACION   => $fecha_modif
//                        ]);
//                        echo "MODIFICO";
//                    }
//                }else{
                    $LISTA_PRECIO = new $model;
                    $LISTA_PRECIO[ACA_Lista_Precio::$ID_LISTA_PRECIO   ] = $result['ID'];
                    $LISTA_PRECIO[ACA_Lista_Precio::$ID_BALANZA        ] = $prefijo;
                    $LISTA_PRECIO[ACA_Lista_Precio::$FECHA             ] = $date;
                    $LISTA_PRECIO[ACA_Lista_Precio::$ID_PROVEEDOR      ] = $result['_IDProveedor'];
                    $LISTA_PRECIO[ACA_Lista_Precio::$OBSERVACIONES     ] = utf8_encode($result['_Observaciones']);
                    $LISTA_PRECIO[ACA_Lista_Precio::$CREADO_POR        ] = utf8_encode($result['_Creado_por']);
                    $LISTA_PRECIO[ACA_Lista_Precio::$MODIFICADO_POR    ] = utf8_encode($result['_Modificado_por']);
                    $LISTA_PRECIO[ACA_Lista_Precio::$FECHA_CREACION    ] = $fecha_creacion;
                    $LISTA_PRECIO[ACA_Lista_Precio::$FECHA_MODIFICACION] = $fecha_modif;
                    $LISTA_PRECIO->save();
//                }

                unset($result);
            }
        }
    }
    //    </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarLotes">
    public function cargarLotes($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();

            $model::where(ACA_Lote::$ID_BALANZA, $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_Creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_Modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

                if(!empty($result['Fecha'])){
                    $date = DateTime::createFromFormat('Y-m-d H:i:s', $result['Fecha'])->format('d-m-Y H:i:s');
                }else{
                    $date = null;
                }

//                $lote = ACA_Lote::select(ACA_Lote::$ID_LOTE , ACA_Lote::$FECHA_MODIFICACION)->where([
//                    [ACA_Lote::$ID_BALANZA , '=' ,$prefijo],
//                    [ACA_Lote::$ID_LOTE , '=' ,$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($lote[ACA_Lote::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $lote[ACA_Lote::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Lote::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Lote::$ID_LOTE,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Lote::$ID_LOTE              => $result['ID'],
//                            ACA_Lote::$ID_BALANZA           => $prefijo,
//                            ACA_Lote::$CODIGO               => $result['Codigo'],
//                            ACA_Lote::$DESCRIPCION          => $result['Descripcion'],
//                            ACA_Lote::$FECHA                => $date,
//                            ACA_Lote::$CANT_BOLSAS          => $result['Cant_Bolsas'],
//                            ACA_Lote::$KILOS_X_BOLSA        => $result['Kilos_x_Bolsa'],
//                            ACA_Lote::$KILOS_NETO           => $result['Kilos_Neto'],
//                            ACA_Lote::$CERTIFICADO          => $result['Certificado'],
//                            ACA_Lote::$OBSERVACIONES        => $result['_Observaciones'],
//                            ACA_Lote::$CREADO_POR           => utf8_encode($result['_Creado_por']),
//                            ACA_Lote::$MODIFICADO_POR       => utf8_encode($result['_Modificado_por']),
//                            ACA_Lote::$FECHA_CREACION       => $fecha_creacion,
//                            ACA_Lote::$FECHA_MODIFICACION   => $fecha_modif
//                        ]);
//                    }
//                }else{
                    $LOTE = new $model;
                    $LOTE[ACA_Lote::$ID_LOTE           ] = $result['ID'];
                    $LOTE[ACA_Lote::$ID_BALANZA        ] = $prefijo;
                    $LOTE[ACA_Lote::$CODIGO            ] = $result['Codigo'];
                    $LOTE[ACA_Lote::$DESCRIPCION       ] = $result['Descripcion'];
                    $LOTE[ACA_Lote::$FECHA             ] = $date;
                    $LOTE[ACA_Lote::$CANT_BOLSAS       ] = $result['Cant_Bolsas'];
                    $LOTE[ACA_Lote::$KILOS_X_BOLSA     ] = $result['Kilos_x_Bolsa'];
                    $LOTE[ACA_Lote::$KILOS_NETO        ] = $result['Kilos_Neto'];
                    $LOTE[ACA_Lote::$CERTIFICADO       ] = $result['Certificado'];
                    $LOTE[ACA_Lote::$SINCRONIZADO       ] = $result['Sincronizado'];
                    $LOTE[ACA_Lote::$OBSERVACIONES     ] = $result['_Observaciones'];
                    $LOTE[ACA_Lote::$CREADO_POR        ] = utf8_encode($result['_Creado_por']);
                    $LOTE[ACA_Lote::$MODIFICADO_POR    ] = utf8_encode($result['_Modificado_por']);
                    $LOTE[ACA_Lote::$FECHA_CREACION    ] = $fecha_creacion;
                    $LOTE[ACA_Lote::$FECHA_MODIFICACION] = $fecha_modif;
                    $LOTE->save();
//                }
                unset($result);

            }
        }
    }
    //      </editor-fold>

    //    <editor-fold defaultstate="collapsed" desc="cargarMotivos">
    public function cargarMotivos($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();

            $model::where(ACA_Motivo::$ID_BALANZA, $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_Creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_Modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

//                $motivo = ACA_Motivo::select(ACA_Motivo::$ID_MOTIVO,ACA_Motivo::$FECHA_MODIFICACION)->where([
//                    [ACA_Motivo::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Motivo::$ID_MOTIVO,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($motivo[ACA_Motivo::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $motivo[ACA_Motivo::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Motivo::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Motivo::$ID_MOTIVO,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Motivo::$ID_MOTIVO          => $result['ID'],
//                            ACA_Motivo::$ID_BALANZA         => $prefijo,
//                            ACA_Motivo::$CODIGO             => utf8_encode($result['Codigo']),
//                            ACA_Motivo::$DESCRIPCION        => utf8_encode($result['Descripcion']),
//                            ACA_Motivo::$OBS                => utf8_encode($result['Obs']),
//                            ACA_Motivo::$CREADO_POR         => utf8_encode($result['_Creado_por']),
//                            ACA_Motivo::$MODIFICADO_POR     => utf8_encode($result['_Modificado_por']),
//                            ACA_Motivo::$FECHA_CREACION     => $fecha_creacion,
//                            ACA_Motivo::$FECHA_MODIFICACION => $fecha_modif
//                        ]);
//                    }
//                }else{
                    $MOTIVO = new $model;
                    $MOTIVO[ACA_Motivo::$ID_MOTIVO         ] = $result['ID'];
                    $MOTIVO[ACA_Motivo::$ID_BALANZA        ] = $prefijo;
                    $MOTIVO[ACA_Motivo::$CODIGO            ] = utf8_encode($result['Codigo']);
                    $MOTIVO[ACA_Motivo::$DESCRIPCION       ] = utf8_encode($result['Descripcion']);
                    $MOTIVO[ACA_Motivo::$OBS               ] = utf8_encode($result['Obs']);
                    $MOTIVO[ACA_Motivo::$CREADO_POR        ] = utf8_encode($result['_Creado_por']);
                    $MOTIVO[ACA_Motivo::$MODIFICADO_POR    ] = utf8_encode($result['_Modificado_por']);
                    $MOTIVO[ACA_Motivo::$FECHA_CREACION    ] = $fecha_creacion;
                    $MOTIVO[ACA_Motivo::$FECHA_MODIFICACION] = $fecha_modif;
                    $MOTIVO->save();
//                }

                unset($result);
            }
        }
    }
            //</editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarMovStock">
    public function cargarMovStock($result, $model, $prefijo) {
        if($result != NULL){

            //OBTO - CVM
            if($prefijo == 86 || $prefijo == 90 || $prefijo == 87){
                $model::where(ACA_Mov_Stock::$ID_BALANZA, $prefijo)->delete();
                
                while ($row = $result->fetch()) {

                    if(!empty($row['_Fecha_Creacion'])){
                        $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $row['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                    }else{
                        $fecha_creacion = null;
                    }

                    if(!empty($row['_Fecha_Modificacion'])){
                        $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $row['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                    }else{
                        $fecha_modif = null;
                    }

                    if(!empty($row['Fecha'])){
                        $date = DateTime::createFromFormat('Y-m-d H:i:s', $row['Fecha'])->format('d-m-Y H:i:s');
                    }else{
                        $date = null;
                    }
                    
                    if(!empty($row['Fecha_reproceso'])){
                        $fecha_reproceso = DateTime::createFromFormat('Y-m-d H:i:s', $row['Fecha_reproceso'])->format('d-m-Y H:i:s');
                    }else{
                        $fecha_reproceso = null;
                    }

                    $MOV_STOCK = new $model;
                    $MOV_STOCK[ACA_Mov_Stock::$ID_MOV_STOCK      ] = $row['ID'];
                    $MOV_STOCK[ACA_Mov_Stock::$ID_BALANZA        ] = $prefijo;
                    $MOV_STOCK[ACA_Mov_Stock::$FECHA             ] = $date;
                    $MOV_STOCK[ACA_Mov_Stock::$FECHA_REPROCESO   ] = $fecha_reproceso;
                    $MOV_STOCK[ACA_Mov_Stock::$ID_PRODUCTO       ] = $row['_IDProducto'];
                    
                    $producto = ACA_Producto::where(ACA_Producto::$ID_BALANZA, $prefijo)
                            ->where(ACA_Producto::$ID_PRODUCTO, $row['_IDProducto'])
                            ->first();
                    
                    $MOV_STOCK[ACA_Mov_Stock::$COD_PRODUCTO      ] = $producto['Codigo'];
                    
                    $MOV_STOCK[ACA_Mov_Stock::$CANTIDAD          ] = $row['Cantidad'];
                    $MOV_STOCK[ACA_Mov_Stock::$E_S               ] = $row['E_S'];
                    $MOV_STOCK[ACA_Mov_Stock::$ID_MOTIVO         ] = $row['_IDMotivo'];
                    $MOV_STOCK[ACA_Mov_Stock::$M3                ] = $row['M3'];
                    $MOV_STOCK[ACA_Mov_Stock::$ID_PESAJE         ] = $row['_IDPesaje'];
                    $MOV_STOCK[ACA_Mov_Stock::$ID_DET_LOTE       ] = $row['_IDDetLote'];
                    $MOV_STOCK[ACA_Mov_Stock::$POR_BALANZA       ] = $row['PorBalanza'];
                    $MOV_STOCK[ACA_Mov_Stock::$OBSERVACIONES     ] = utf8_encode($row['Obs']);
                    $MOV_STOCK[ACA_Mov_Stock::$CREADO_POR        ] = utf8_encode($row['_Creado_por']);
                    $MOV_STOCK[ACA_Mov_Stock::$MODIFICADO_POR    ] = utf8_encode($row['_Modificado_por']);
                    $MOV_STOCK[ACA_Mov_Stock::$FECHA_CREACION    ] = $fecha_creacion;
                    $MOV_STOCK[ACA_Mov_Stock::$FECHA_MODIFICACION] = $fecha_modif;
                    $MOV_STOCK->save();

                    unset($row);
                }
            }else{
                $row = $result->fetchall();
                $model::where(ACA_Mov_Stock::$ID_BALANZA, $prefijo)->delete();
                foreach($row as $r => $result){

                    if(!empty($result['_Fecha_Creacion'])){
                        $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                    }else{
                        $fecha_creacion = null;
                    }

                    if(!empty($result['_Fecha_Modificacion'])){
                        $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                    }else{
                        $fecha_modif = null;
                    }

                    if(!empty($result['Fecha'])){
                        $date = DateTime::createFromFormat('Y-m-d H:i:s', $result['Fecha'])->format('d-m-Y H:i:s');
                    }else{
                        $date = null;
                    }
                    
                    if(!empty($result['Fecha_reproceso'])){
                        $fecha_reproceso = DateTime::createFromFormat('Y-m-d H:i:s', $result['Fecha_reproceso'])->format('d-m-Y H:i:s');
                    }else{
                        $fecha_reproceso = null;
                    }

                    $MOV_STOCK = new $model;
                    $MOV_STOCK[ACA_Mov_Stock::$ID_MOV_STOCK      ] = $result['ID'];
                    $MOV_STOCK[ACA_Mov_Stock::$ID_BALANZA        ] = $prefijo;
                    $MOV_STOCK[ACA_Mov_Stock::$FECHA             ] = $date;
                    $MOV_STOCK[ACA_Mov_Stock::$FECHA_REPROCESO   ] = $fecha_reproceso;
                    $MOV_STOCK[ACA_Mov_Stock::$ID_PRODUCTO       ] = $result['_IDProducto'];
                    
                    
                    $producto = ACA_Producto::where(ACA_Producto::$ID_BALANZA, $prefijo)
                            ->where(ACA_Producto::$ID_PRODUCTO, $result['_IDProducto'])
                            ->first();
                    
                    $MOV_STOCK[ACA_Mov_Stock::$COD_PRODUCTO      ] = $producto['Codigo'];
                    
                    
                    $MOV_STOCK[ACA_Mov_Stock::$CANTIDAD          ] = $result['Cantidad'];
                    $MOV_STOCK[ACA_Mov_Stock::$E_S               ] = $result['E_S'];
                    $MOV_STOCK[ACA_Mov_Stock::$ID_MOTIVO         ] = $result['_IDMotivo'];
                    $MOV_STOCK[ACA_Mov_Stock::$M3                ] = $result['M3'];
                    $MOV_STOCK[ACA_Mov_Stock::$ID_PESAJE         ] = $result['_IDPesaje'];
                    $MOV_STOCK[ACA_Mov_Stock::$ID_DET_LOTE       ] = $result['_IDDetLote'];
                    $MOV_STOCK[ACA_Mov_Stock::$POR_BALANZA       ] = $result['PorBalanza'];
                    $MOV_STOCK[ACA_Mov_Stock::$OBSERVACIONES     ] = utf8_encode($result['Obs']);
                    $MOV_STOCK[ACA_Mov_Stock::$CREADO_POR        ] = utf8_encode($result['_Creado_por']);
                    $MOV_STOCK[ACA_Mov_Stock::$MODIFICADO_POR    ] = utf8_encode($result['_Modificado_por']);
                    $MOV_STOCK[ACA_Mov_Stock::$FECHA_CREACION    ] = $fecha_creacion;
                    $MOV_STOCK[ACA_Mov_Stock::$FECHA_MODIFICACION] = $fecha_modif;
                    $MOV_STOCK->save();

                }
            }

        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarPaises">
    public function cargarPaises($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();
            
            $model::where(ACA_Pais::$ID_BALANZA, $prefijo)->delete();
            
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

//                $pais = ACA_Pais::select(ACA_Pais::$ID_PAIS, ACA_Pais::$FECHA_MODIFICACION)->where([
//                    [ACA_Pais::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Pais::$ID_PAIS,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($pais[ACA_Pais::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $pais[ACA_Pais::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Pais::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Pais::$ID_PAIS,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Pais::$ID_PAIS              => $result['ID'],
//                            ACA_Pais::$ID_BALANZA           => $prefijo,
//                            ACA_Pais::$NOMBRE               => $result['Nombre'],
//                            ACA_Pais::$OBSERVACIONES        => $result['_Observaciones'],
//                            ACA_Pais::$CREADO_POR           => utf8_encode($result['_Creado_por']),
//                            ACA_Pais::$MODIFICADO_POR       => utf8_encode($result['_Modificado_por']),
//                            ACA_Pais::$FECHA_CREACION       => $fecha_creacion,
//                            ACA_Pais::$FECHA_MODIFICACION   => $fecha_modif
//                        ]);
//                    }
//                }else{
                    $PAIS = new $model;
                    $PAIS[ACA_Pais::$ID_PAIS           ] = $result['ID'];
                    $PAIS[ACA_Pais::$ID_BALANZA        ] = $prefijo;
                    $PAIS[ACA_Pais::$NOMBRE            ] = $result['Nombre'];
                    $PAIS[ACA_Pais::$OBSERVACIONES     ] = $result['_Observaciones'];
                    $PAIS[ACA_Pais::$CREADO_POR        ] = utf8_encode($result['_Creado_por']);
                    $PAIS[ACA_Pais::$MODIFICADO_POR    ] = utf8_encode($result['_Modificado_por']);
                    $PAIS[ACA_Pais::$FECHA_CREACION    ] = $fecha_creacion;
                    $PAIS[ACA_Pais::$FECHA_MODIFICACION] = $fecha_modif;
                    $PAIS->save();
//                }

                unset($result);
            }
        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarParamImportaTipificacion">
    public function cargarParamImportaTipificacion($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();

            $model::where(ACA_Param_Importa_Tipificacion::$ID_BALANZA, $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

//                $param_imp_t = ACA_Param_Importa_Tipificacion::select(ACA_Param_Importa_Tipificacion::$ID_PARAM_IMP_TIP, ACA_Param_Importa_Tipificacion::$FECHA_MODIFICACION)->where([
//                    [ACA_Param_Importa_Tipificacion::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Param_Importa_Tipificacion::$ID_PARAM_IMP_TIP,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($param_imp_t[ACA_Param_Importa_Tipificacion::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $param_imp_t[ACA_Param_Importa_Tipificacion::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Param_Importa_Tipificacion::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Param_Importa_Tipificacion::$ID_PARAM_IMP_TIP,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Param_Importa_Tipificacion::$ID_PARAM_IMP_TIP   => $result['ID'],
//                            ACA_Param_Importa_Tipificacion::$ID_BALANZA         => $prefijo,
//                            ACA_Param_Importa_Tipificacion::$PLANTA             => $result['Planta'],
//                            ACA_Param_Importa_Tipificacion::$ID_PRODUCTO        => $result['_IDProducto'],
//                            ACA_Param_Importa_Tipificacion::$OBSERVACIONES      => $result['Obs'],
//                            ACA_Param_Importa_Tipificacion::$CREADO_POR         => utf8_encode($result['_Creado_por']),
//                            ACA_Param_Importa_Tipificacion::$MODIFICADO_POR     => utf8_encode($result['_Modificado_por']),
//                            ACA_Param_Importa_Tipificacion::$FECHA_CREACION     => $fecha_creacion,
//                            ACA_Param_Importa_Tipificacion::$FECHA_MODIFICACION => $fecha_modif
//                        ]);
//                        echo "MODIFICO";
//                    }
//                }else{
                    $PARAM_IT = new $model;
                    $PARAM_IT[ACA_Param_Importa_Tipificacion::$ID_PARAM_IMP_TIP  ] = $result['ID'];
                    $PARAM_IT[ACA_Param_Importa_Tipificacion::$ID_BALANZA        ] = $prefijo;
                    $PARAM_IT[ACA_Param_Importa_Tipificacion::$PLANTA            ] = $result['Planta'];
                    $PARAM_IT[ACA_Param_Importa_Tipificacion::$ID_PRODUCTO       ] = $result['_IDProducto'];
                    $PARAM_IT[ACA_Param_Importa_Tipificacion::$OBSERVACIONES     ] = $result['Obs'];
                    $PARAM_IT[ACA_Param_Importa_Tipificacion::$CREADO_POR        ] = utf8_encode($result['_Creado_por']);
                    $PARAM_IT[ACA_Param_Importa_Tipificacion::$MODIFICADO_POR    ] = utf8_encode($result['_Modificado_por']);
                    $PARAM_IT[ACA_Param_Importa_Tipificacion::$FECHA_CREACION    ] = $fecha_creacion;
                    $PARAM_IT[ACA_Param_Importa_Tipificacion::$FECHA_MODIFICACION] = $fecha_modif;
                    $PARAM_IT->save();
//                }
                unset($result);

            }
        }
    }
    //      </editor-fold>

    /*
     * PARAMETROS Se actualiza todo siempre
     */
    //      <editor-fold defaultstate="collapsed" desc="cargarParametros">
    public function cargarParametros($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();

            $model::where(ACA_Parametro::$ID_BALANZA, $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_Creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                //OBMY
                if($prefijo == 98){
                    $lotes = utf8_encode($result['lotes']);
                }else{
                    $lotes = utf8_encode($result['Lotes']);
                }

                if(!empty($result['_Fecha_Modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

                $PARAMETRO = new $model;
                $PARAMETRO[ACA_Parametro::$MODULO                ] = utf8_encode($result['Modulo']);
                $PARAMETRO[ACA_Parametro::$ID_BALANZA            ] = $prefijo;
                $PARAMETRO[ACA_Parametro::$RIE                   ] = $result['RIE'];
                $PARAMETRO[ACA_Parametro::$DIAS_RIE              ] = $result['DIAS_RIE'];
                $PARAMETRO[ACA_Parametro::$PATH_RX               ] = $result['Path_RX'];
                $PARAMETRO[ACA_Parametro::$ID_TALONARIO_TICKET_TV] = $result['_IDTalonario_Ticket_TV'];
                $PARAMETRO[ACA_Parametro::$ID_TALONARIO_TICKET   ] = $result['_IDTalonario_Ticket'];
                $PARAMETRO[ACA_Parametro::$ID_TALONARIO_REMITO   ] = $result['_IDTalonario_Remito'];
                $PARAMETRO[ACA_Parametro::$ID_MOTIVO_STOCK       ] = $result['_IDMotivo_Stock'];
                $PARAMETRO[ACA_Parametro::$ID_MOTIVO_LOTES       ] = $result['_IDMotivo_Lotes'];
                $PARAMETRO[ACA_Parametro::$CODIGOS_PRODUCTOS     ] = utf8_encode($result['CodigosProductos']);
                $PARAMETRO[ACA_Parametro::$MUESTRA_COLOR         ] = $result['MuestraColor'];
                $PARAMETRO[ACA_Parametro::$MUESTRA_COMPROBANTE   ] = $result['MuestraComprobantes'];
                $PARAMETRO[ACA_Parametro::$BOLSAS_X_DEF          ] = utf8_encode($result['BolsasXDef']);
                $PARAMETRO[ACA_Parametro::$PESO_PALLET           ] = (int)$result['Peso_Pallet'];
                $PARAMETRO[ACA_Parametro::$TARAS_X_BOLSA         ] = $result['TarasxBolsa'];
                $PARAMETRO[ACA_Parametro::$TE_VERDE              ] = $result['TeVerde'];
                $PARAMETRO[ACA_Parametro::$LEÑA                  ] = $result['Lenia'];
                $PARAMETRO[ACA_Parametro::$CLAVE                 ] = $result['Clave'];
                $PARAMETRO[ACA_Parametro::$CLAVE_PESAJE          ] = $result['ClavePesajes'];
                $PARAMETRO[ACA_Parametro::$NAV_PESAJES           ] = $result['NavPesajes'];
                $PARAMETRO[ACA_Parametro::$LOTES                 ] = $lotes;
                $PARAMETRO[ACA_Parametro::$INFO_DEPTO            ] = utf8_encode($result['InfoDepto']);
                $PARAMETRO[ACA_Parametro::$ESVB6                 ] = $result['ESVB6'];
                $PARAMETRO[ACA_Parametro::$BASE_COMPUESTA        ] = $result['BaseCompuesta'];
                $PARAMETRO[ACA_Parametro::$MODIFICA_HISTORICO    ] = $result['ModificaHistorico'];
                $PARAMETRO[ACA_Parametro::$OBSERVACIONES         ] = $result['_Observaciones'];
                $PARAMETRO[ACA_Parametro::$CREADO_POR            ] = utf8_encode($result['_Creado_por']);
                $PARAMETRO[ACA_Parametro::$MODIFICADO_POR        ] = utf8_encode($result['_Modificado_por']);
                $PARAMETRO[ACA_Parametro::$FECHA_CREACION        ] = $fecha_creacion;
                $PARAMETRO[ACA_Parametro::$FECHA_MODIFICACION    ] = $fecha_modif;
                $PARAMETRO->save();
                unset($result);
            }
        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarPesajes">
    public function cargarPesajes($result, $model, $prefijo) {
        
        if($result != NULL){
            
            $model::where(ACA_Pesaje::$ID_BALANZA, $prefijo)->delete();
            $ultimo_id = 0;
            
            while ($row = $result->fetch()) {
               
                if(!empty($row['Fecha'])){
                    $date = DateTime::createFromFormat('Y-m-d H:i:s', $row['Fecha'])->format('d/m/Y H:i:s');
                }else{
                    $date = null;
                }
                
                if(!empty($row['_Fecha_Creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $row['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($row['_Fecha_Modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $row['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }


                if(!empty($row['Fecha_Bruto'])){
                    $fecha_bruto = DateTime::createFromFormat('Y-m-d H:i:s', $row['Fecha_Bruto'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_bruto = null;
                }
                

                if(!empty($row['Fecha_Anulacion'])){
                    $fecha_anulacion = DateTime::createFromFormat('Y-m-d H:i:s', $row['Fecha_Anulacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_anulacion = null;
                }
                

                // CV - TABAY - CV2 - OBTO - CVM
                if($prefijo == 89 || $prefijo == 9 || $prefijo == 99 || $prefijo == 86 || $prefijo == 90){
                    $lote_chacra = $row['Lote_Chacra'];
                }else{
                    $lote_chacra = $row['Lote_chacra'];
                }
                
                
                if(isset($row['ID'])){
                    
                    $PESAJE = new $model;
                    $PESAJE[ACA_Pesaje::$ID_PESAJE            ] = $row['ID'];
                    
                    $PESAJE[ACA_Pesaje::$ID_BALANZA           ] = $prefijo;
                    
                    if(isset($row['_IDTalonario']))
                        $PESAJE[ACA_Pesaje::$ID_TALONARIO         ] = $row['_IDTalonario'];
                    else
                        $PESAJE[ACA_Pesaje::$ID_TALONARIO         ] = 0;
                    
                    if(isset($row['Nro_Comprobante']) && !empty($row['Nro_Comprobante']))
                        $PESAJE[ACA_Pesaje::$NRO_COMPROBANTE      ] = $row['Nro_Comprobante'];
                    else
                       $PESAJE[ACA_Pesaje::$NRO_COMPROBANTE      ] = 0; 
                    
                    $PESAJE[ACA_Pesaje::$FECHA                ] = $date;
                    $PESAJE[ACA_Pesaje::$ID_RODADO            ] = (int)$row['_IDRodado'];
                    
                    if(isset($row['Patente_Acoplado']) && !empty($row['Patente_Acoplado'])){
                        $PESAJE[ACA_Pesaje::$PATENTE_ACOPLADO     ] = utf8_encode($row['Patente_Acoplado']);
                    }else{
                        $PESAJE[ACA_Pesaje::$PATENTE_ACOPLADO     ] = '0';
                    }
                    $PESAJE[ACA_Pesaje::$CONDUCTOR            ] = utf8_encode($row['Conductor']);
                    $PESAJE[ACA_Pesaje::$ID_PRODUCTO          ] = (int)$row['_IDProducto'];
                    $PESAJE[ACA_Pesaje::$ID_TIPO_PRECIO       ] = (int)$row['_IDTipoPrecio'];
                    $PESAJE[ACA_Pesaje::$PRECIO               ] = (double)$row['Precio'];
                    $PESAJE[ACA_Pesaje::$ID_PLANTA_ORIGEN     ] = (int)$row['_IDPlantaOrigen'];
                    $PESAJE[ACA_Pesaje::$ID_PLANTA_DESTINO    ] = (int)$row['_IDPlantaDestino'];
                    $PESAJE[ACA_Pesaje::$ID_PROVEEDOR         ] = (int)$row['_IDProveedor'];
                    
                    if($PESAJE[ACA_Pesaje::$ID_PROVEEDOR] != 0 && $PESAJE[ACA_Pesaje::$ID_PLANTA_ORIGEN] == 0 && $PESAJE[ACA_Pesaje::$ID_PRODUCTO] == 77){
                        $prov = ACA_Proveedor::where(ACA_Proveedor::$ID_PROVEEDOR,(int)$row['_IDProveedor'])
                                ->where(ACA_Proveedor::$ID_BALANZA, $prefijo)
                                ->where(ACA_Proveedor::$ES_ACOPIADOR,0)
                                ->first();

                        $proveedor_de_compras = DB::connection('EXPOTEC_COMPRAS')->table('PROVEEDORES')->select('Codigo','Razon_Social')
                                ->where('Codigo', $prov["Codigo"])->whereIn('Aplica_a',[0,2])
                                ->first();
                        
                        if(count($proveedor_de_compras) > 0){
                            $PESAJE[ACA_Pesaje::$COD_PROVEEDOR] = (int)$proveedor_de_compras->Codigo;
                        }
                    }
                    
                    if((int)$PESAJE[ACA_Pesaje::$ID_PROVEEDOR] == 323 && $prefijo == 9){
                        $PESAJE[ACA_Pesaje::$COD_PROVEEDOR] = 3058;
                    }
                    
                    if((int)$PESAJE[ACA_Pesaje::$ID_PROVEEDOR] == 1080 && $prefijo == 88){
                        $PESAJE[ACA_Pesaje::$COD_PROVEEDOR] = 100910;
                    }
                    
                    if((int)$PESAJE[ACA_Pesaje::$ID_PROVEEDOR] == 1014 && $prefijo == 87){
                        $PESAJE[ACA_Pesaje::$COD_PROVEEDOR] = 100340;
                    }
                    
                    if((int)$PESAJE[ACA_Pesaje::$ID_PROVEEDOR] == 1031 && $prefijo == 89){
                        $PESAJE[ACA_Pesaje::$COD_PROVEEDOR] = 7960;
                    }
                    
                    
                    $PESAJE[ACA_Pesaje::$ID_ACOPIADOR         ] = (int)$row['_IDAcopiador'];
                    $PESAJE[ACA_Pesaje::$ES_ACOPIADOR         ] = $row['Es_Acopiador'];
                    $PESAJE[ACA_Pesaje::$ORDEN_ACOPIO         ] = (int)$row['Orden_Acopio'];
                    $PESAJE[ACA_Pesaje::$VAL_REF              ] = (int)$row['VALREF'];
                    $PESAJE[ACA_Pesaje::$ID_EMBALAJE          ] = (int)$row['_IDEmbalaje'];
                    $PESAJE[ACA_Pesaje::$CANT_EMBALAJE        ] = (int)$row['Cant_Embalaje'];
                    $PESAJE[ACA_Pesaje::$PESO_EMB_X_UNIDAD    ] = (int)$row['Peso_Emb_x_Unidad'];
                    $PESAJE[ACA_Pesaje::$ID_TIPO_HUMEDAD      ] = (int)$row['_IDTipoHumedad'];
                    $PESAJE[ACA_Pesaje::$PORCENTAJE_HUMEDAD   ] = (int)$row['Porc_Humedad'];
                    $PESAJE[ACA_Pesaje::$PORCENTAJE_PALO      ] = (int)$row['Porc_Palo'];
                    $PESAJE[ACA_Pesaje::$PESO_PALO            ] = (double)$row['Peso_Palo'];
                    $PESAJE[ACA_Pesaje::$M3                   ] = (int)$row['M3'];
                    $PESAJE[ACA_Pesaje::$FECHA_BRUTO          ] = $fecha_bruto;
                    $PESAJE[ACA_Pesaje::$FECHA_ANULACION      ] = $fecha_anulacion;
                    $PESAJE[ACA_Pesaje::$USUARIO_ANULACION    ] = utf8_encode($row['Usuario_Anulacion']);
                    $PESAJE[ACA_Pesaje::$MOTIVO_ANULACION     ] = utf8_encode($row['Motivo_Anulacion']);
                    $PESAJE[ACA_Pesaje::$BRUTO                ] = (double)$row['Bruto'];
                    $PESAJE[ACA_Pesaje::$TARA                 ] = (double)$row['Tara'];
                    $PESAJE[ACA_Pesaje::$TOTAL_EMBALAJE       ] = (double)$row['Total_Embalaje'];
                    $PESAJE[ACA_Pesaje::$PESO_HUMEDAD         ] = (double)$row['Peso_Humedad'];
                    $PESAJE[ACA_Pesaje::$NETO                 ] = (double)$row['Neto'];
                    $PESAJE[ACA_Pesaje::$ESTADO               ] = $row['Estado'];
                    $PESAJE[ACA_Pesaje::$ID_CHACRA            ] = (int)$row['_IDChacra'];
                    
                    if((int)$row['_IDChacra'] == 0){
                        $PESAJE[ACA_Pesaje::$COD_CHACRA] = 0;
                    }else{
                        $chacra = ACA_Chacra::where( ACA_Chacra::$ID_BALANZA , $prefijo)->where(ACA_Chacra::$ID_CHACRA, (int)$row['_IDChacra'])->first();
                        $PESAJE[ACA_Pesaje::$COD_CHACRA] = $chacra->Codigo;
                    }
                    
                    $PESAJE[ACA_Pesaje::$TE_CERTIFICADO       ] = $row['Te_Certificado'];
                    $PESAJE[ACA_Pesaje::$ID_PESAJE_BASE_ORIGEN] = (int)$row['IDPESAJEBASEORIG'];
                    $PESAJE[ACA_Pesaje::$ID_PESAJE_DUP        ] = (int)$row['IDPESAJEDUP'];
                    $PESAJE[ACA_Pesaje::$ID_MIG_ORIG          ] = (int)$row['IDMIGORIG'];
                    $PESAJE[ACA_Pesaje::$MIGRADO              ] = $row['MIGRADO'];
                    $PESAJE[ACA_Pesaje::$MODIFICADO           ] = $row['MODIFICADO'];
                    $PESAJE[ACA_Pesaje::$LOTE_CHACRA          ] = $lote_chacra;
                    $PESAJE[ACA_Pesaje::$TAMAÑO               ] = utf8_encode($row['Tamanio']);
                    $PESAJE[ACA_Pesaje::$MALEZAS              ] = utf8_encode($row['Malezas']);
                    $PESAJE[ACA_Pesaje::$CALIDAD              ] = utf8_encode($row['Calidad']);
                    $PESAJE[ACA_Pesaje::$ACAROS               ] = utf8_encode($row['Acaros']);
                    $PESAJE[ACA_Pesaje::$TEMPERATURA          ] = utf8_encode($row['Temperatura']);
                    $PESAJE[ACA_Pesaje::$PORC_MALLA_30        ] = (isset($row['PORC_MALLA_30'])) ? (int)$row['PORC_MALLA_30'] : NULL;
                    $PESAJE[ACA_Pesaje::$EXPO                 ] = (isset($row['EXPO']) || !empty($row['EXPO'])) ? $row['EXPO'] : 0;
                    $PESAJE[ACA_Pesaje::$BUQUE                ] = isset($row['BUQUE']) ? utf8_encode($row['BUQUE']) : NULL;
                    $PESAJE[ACA_Pesaje::$CODIGO_ADUANA        ] = isset($row['CODIGO_ADUANA']) ? utf8_encode($row['CODIGO_ADUANA']) : NULL;
                    $PESAJE[ACA_Pesaje::$RESERVA              ] = isset($row['RESERVA']) ? utf8_encode($row['RESERVA']) : NULL;
                    $PESAJE[ACA_Pesaje::$CODIGO_BALANZA       ] = isset($row['CODIGO_BALANZA']) ? utf8_encode($row['CODIGO_BALANZA']) : NULL;
                    $PESAJE[ACA_Pesaje::$NRO_CERT_HAB         ] = isset($row['NRO_CERTIF_HAB']) ? utf8_encode($row['NRO_CERTIF_HAB']) : NULL;
                    $PESAJE[ACA_Pesaje::$VENCIMIENTO          ] = isset($row['VENCIMIENTO']) ? utf8_encode($row['VENCIMIENTO']) : NULL;
                    $PESAJE[ACA_Pesaje::$NRO_CONTENEDOR       ] = isset($row['NRO_CONTENEDOR']) ? utf8_encode($row['NRO_CONTENEDOR']) : NULL;
                    $PESAJE[ACA_Pesaje::$AFIP                 ] = isset($row['AFIP']) ? utf8_encode($row['AFIP']) : NULL;
                    $PESAJE[ACA_Pesaje::$LINEA                ] = isset($row['LINEA']) ? utf8_encode($row['LINEA']) : NULL;
                    $PESAJE[ACA_Pesaje::$OTROS                ] = isset($row['OTROS']) ? utf8_encode($row['OTROS']) : NULL;
                    $PESAJE[ACA_Pesaje::$CUIT_ATA             ] = isset($row['CUIT_ATA']) ? utf8_encode($row['CUIT_ATA']) : NULL;
                    $PESAJE[ACA_Pesaje::$APELLIDO_NOMBRE_ATA  ] = isset($row['APELLIDO_NOMBRE_ATA']) ? utf8_encode($row['APELLIDO_NOMBRE_ATA']) : NULL;
                    $PESAJE[ACA_Pesaje::$NRO_PERMISO_EMBARQUE ] = isset($row['NRO_PERMISO_EMBARQUE']) ? utf8_encode($row['NRO_PERMISO_EMBARQUE']) : NULL;
                    $PESAJE[ACA_Pesaje::$PESAJEWEB            ] = true;
                    $PESAJE[ACA_Pesaje::$SINCRONIZADO         ] = true;
                    $PESAJE[ACA_Pesaje::$OBSERVACIONES        ] = utf8_encode($row['Obs']);
                    $PESAJE[ACA_Pesaje::$CREADO_POR           ] = utf8_encode($row['_Creado_por']);
                    $PESAJE[ACA_Pesaje::$MODIFICADO_POR       ] = utf8_encode($row['_Modificado_por']);
                    $PESAJE[ACA_Pesaje::$FECHA_CREACION       ] = $fecha_creacion;
                    $PESAJE[ACA_Pesaje::$FECHA_MODIFICACION   ] = $fecha_modif;
                    $PESAJE->save();
                    
                    $ultimo_id = $row['ID'];
                }
                
                
            }
            unset($row);
        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarProcesamientoVales">
    public function cargarProcesamientoVales($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();
            $model::where(ACA_Procesamiento_Vale::$ID_BALANZA, $prefijo)->delete();
            foreach($row as $r => $result){


                if(!empty($result['_Fecha_Creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_Modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

//                $proc_vales = ACA_Procesamiento_Vale::select(ACA_Procesamiento_Vale::$ID_PROC_VAL_BAL,ACA_Procesamiento_Vale::$FECHA_MODIFICACION)->where([
//                    [ACA_Procesamiento_Vale::$ID_BALANZA, '=' ,$prefijo],
//                    [ACA_Procesamiento_Vale::$ID_PROC_VAL_BAL,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($proc_vales[ACA_Procesamiento_Vale::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $proc_vales[ACA_Procesamiento_Vale::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Procesamiento_Vale::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Procesamiento_Vale::$ID_PROC_VAL_BAL,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Procesamiento_Vale::$ID_PROC_VAL_BAL        => $result['ID'],
//                            ACA_Procesamiento_Vale::$ID_BALANZA             => $prefijo,
//                            ACA_Procesamiento_Vale::$NRO_LOTE               => $result['Nro_Lote'],
//                            ACA_Procesamiento_Vale::$RUTA                   => $result['Ruta'],
//                            ACA_Procesamiento_Vale::$TIPO_PROCESO           => $result['Tipo_proceso'],
//                            ACA_Procesamiento_Vale::$OPERACION              => $result['Operacion'],
//                            ACA_Procesamiento_Vale::$TIPO_ARCHIVO           => $result['TipoArchivo'],
//                            ACA_Procesamiento_Vale::$ARCHIVO_COMIENZA_CON   => $result['ArchivoComienzaCon'],
//                            ACA_Procesamiento_Vale::$ARCHIVO_FIJO           => $result['ArchivoFijo'],
//                            ACA_Procesamiento_Vale::$OBSERVACIONES          => $result['_Observaciones'],
//                            ACA_Procesamiento_Vale::$CREADO_POR             => utf8_encode($result['_Creado_por']),
//                            ACA_Procesamiento_Vale::$MODIFICADO_POR         => utf8_encode($result['_Modificado_por']),
//                            ACA_Procesamiento_Vale::$FECHA_CREACION         => $fecha_creacion,
//                            ACA_Procesamiento_Vale::$FECHA_MODIFICACION     => $fecha_modif
//                        ]);
//                    }
//                }else{
                    $PROCESAMIENTO_VALE = new $model;
                    $PROCESAMIENTO_VALE[ACA_Procesamiento_Vale::$ID_PROC_VAL_BAL     ] = $result['ID'];
                    $PROCESAMIENTO_VALE[ACA_Procesamiento_Vale::$ID_BALANZA          ] = $prefijo;
                    $PROCESAMIENTO_VALE[ACA_Procesamiento_Vale::$NRO_LOTE            ] = $result['Nro_Lote'];
                    $PROCESAMIENTO_VALE[ACA_Procesamiento_Vale::$RUTA                ] = $result['Ruta'];
                    $PROCESAMIENTO_VALE[ACA_Procesamiento_Vale::$TIPO_PROCESO        ] = $result['Tipo_proceso'];
                    $PROCESAMIENTO_VALE[ACA_Procesamiento_Vale::$OPERACION           ] = $result['Operacion'];
                    $PROCESAMIENTO_VALE[ACA_Procesamiento_Vale::$TIPO_ARCHIVO        ] = $result['TipoArchivo'];
                    $PROCESAMIENTO_VALE[ACA_Procesamiento_Vale::$ARCHIVO_COMIENZA_CON] = $result['ArchivoComienzaCon'];
                    $PROCESAMIENTO_VALE[ACA_Procesamiento_Vale::$ARCHIVO_FIJO        ] = $result['ArchivoFijo'];
                    $PROCESAMIENTO_VALE[ACA_Procesamiento_Vale::$OBSERVACIONES       ] = $result['_Observaciones'];
                    $PROCESAMIENTO_VALE[ACA_Procesamiento_Vale::$CREADO_POR          ] = utf8_encode($result['_Creado_por']);
                    $PROCESAMIENTO_VALE[ACA_Procesamiento_Vale::$MODIFICADO_POR      ] = utf8_encode($result['_Modificado_por']);
                    $PROCESAMIENTO_VALE[ACA_Procesamiento_Vale::$FECHA_CREACION      ] = $fecha_creacion;
                    $PROCESAMIENTO_VALE[ACA_Procesamiento_Vale::$FECHA_MODIFICACION  ] = $fecha_modif;
                    $PROCESAMIENTO_VALE->save();
//                }

                unset($result);
            }
        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarProductos">
    public function cargarProductos($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();
            $model::where(ACA_Producto::$ID_BALANZA, $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_Creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_Modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

                if(!empty($result['Fecha_Saldo_Inicial'])){
                    $date = DateTime::createFromFormat('Y-m-d H:i:s', $result['Fecha_Saldo_Inicial'])->format('d-m-Y H:i:s');
                }else{
                    $date = null;
                }

//                $productos = ACA_Producto::select(ACA_Producto::$ID_PRODUCTO , ACA_Producto::$FECHA_MODIFICACION)->where([
//                    [ACA_Producto::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Producto::$ID_PRODUCTO ,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($productos[ACA_Producto::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $productos[ACA_Producto::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Producto::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Producto::$ID_PRODUCTO,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Producto::$ID_PRODUCTO          => $result['ID'],
//                            ACA_Producto::$ID_BALANZA           => $prefijo,
//                            ACA_Producto::$CODIGO               => $result['Codigo'],
//                            ACA_Producto::$DESCRIPCION          => utf8_encode($result['Descripcion']),
//                            ACA_Producto::$CANTIDAD             => (int)$result['Cantidad'],
//                            ACA_Producto::$M3                   => (int)$result['M3'],
//                            ACA_Producto::$CODIGO_BALANZA       => (int)$result['Codigo_Balanza'],
//                            ACA_Producto::$PORCENTAJE           => (int)$result['Porcentaje'],
//                            ACA_Producto::$ACTIVO               => $result['Activo'],
//                            ACA_Producto::$ID_CATEGORIA         => $result['_IDCategoria'],
//                            ACA_Producto::$PRECIO               => (int)$result['Precio'],
//                            ACA_Producto::$FECHA_SALDO_INICIAL  => $date,
//                            ACA_Producto::$SALDO_INICIAL        => (int)$result['Saldo_Inicial'],
//                            ACA_Producto::$STOCK_MAXIMO         => (int)$result['Stock_Maximo'],
//                            ACA_Producto::$SOTCK_MINIMO         => (int)$result['Stock_Minimo'],
//                            ACA_Producto::$OBS                  => $result['Obs'],
//                            ACA_Producto::$CREADO_POR           => utf8_encode($result['_Creado_por']),
//                            ACA_Producto::$MODIFICADO_POR       => utf8_encode($result['_Modificado_por']),
//                            ACA_Producto::$FECHA_CRECION        => $fecha_creacion,
//                            ACA_Producto::$FECHA_MODIFICACION   => $fecha_modif
//                        ]);
//                    }
//                }else{
                    $PRODUCTO = new $model;
                    $PRODUCTO[ACA_Producto::$ID_PRODUCTO        ] = $result['ID'];
                    $PRODUCTO[ACA_Producto::$ID_BALANZA         ] = $prefijo;
                    $PRODUCTO[ACA_Producto::$CODIGO             ] = $result['Codigo'];
                    $PRODUCTO[ACA_Producto::$DESCRIPCION        ] = utf8_encode($result['Descripcion']);
                    $PRODUCTO[ACA_Producto::$CANTIDAD           ] = (int)$result['Cantidad'];
                    $PRODUCTO[ACA_Producto::$M3                 ] = (int)$result['M3'];
                    $PRODUCTO[ACA_Producto::$CODIGO_BALANZA     ] = (int)$result['Codigo_Balanza'];
                    $PRODUCTO[ACA_Producto::$PORCENTAJE         ] = (int)$result['Porcentaje'];
                    $PRODUCTO[ACA_Producto::$ACTIVO             ] = $result['Activo'];
                    $PRODUCTO[ACA_Producto::$ID_CATEGORIA       ] = $result['_IDCategoria'];
                    $PRODUCTO[ACA_Producto::$PRECIO             ] = (int)$result['Precio'];
                    $PRODUCTO[ACA_Producto::$FECHA_SALDO_INICIAL] = $date;
                    $PRODUCTO[ACA_Producto::$SALDO_INICIAL      ] = (int)$result['Saldo_Inicial'];
                    $PRODUCTO[ACA_Producto::$STOCK_MAXIMO       ] = (int)$result['Stock_Maximo'];
                    $PRODUCTO[ACA_Producto::$SOTCK_MINIMO       ] = (int)$result['Stock_Minimo'];
                    $PRODUCTO[ACA_Producto::$OBS                ] = $result['Obs'];
                    $PRODUCTO[ACA_Producto::$CREADO_POR         ] = utf8_encode($result['_Creado_por']);
                    $PRODUCTO[ACA_Producto::$MODIFICADO_POR     ] = utf8_encode($result['_Modificado_por']);
                    $PRODUCTO[ACA_Producto::$FECHA_CRECION      ] = $fecha_creacion;
                    $PRODUCTO[ACA_Producto::$FECHA_MODIFICACION ] = $fecha_modif;
                    $PRODUCTO->save();
//                }

                unset($result);
            }
        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarProveedores">
    public function cargarProveedores($result, $model, $prefijo){
        if($result != NULL){
            $row = $result->fetchall();
            $model::where(ACA_Proveedor::$ID_BALANZA, $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_Creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_Modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

//                $proveedores = ACA_Proveedor::select(ACA_Proveedor::$ID_PROVEEDOR, ACA_Proveedor::$FECHA_MODIFICACION)->where([
//                    [ACA_Proveedor::$ID_BALANZA ,'=',$prefijo],
//                    [ACA_Proveedor::$ID_PROVEEDOR,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($proveedores[ACA_Proveedor::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $proveedores[ACA_Proveedor::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Proveedor::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Proveedor::$ID_PROVEEDOR,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Proveedor::$ID_PROVEEDOR        => $result['ID'],
//                            ACA_Proveedor::$ID_BALANZA          => $prefijo,
//                            ACA_Proveedor::$CODIGO              => (int)$result['Codigo'],
//                            ACA_Proveedor::$RAZON_SOCIAL        => utf8_encode($result['Razon_Social']),
//                            ACA_Proveedor::$DOMICILIO           => utf8_encode($result['Domicilio']),
//                            ACA_Proveedor::$LOCALIDAD           => utf8_encode($result['Localidad']),
//                            ACA_Proveedor::$CP                  => utf8_encode($result['CP']),
//                            ACA_Proveedor::$ID_PAIS             => $result['_IDPais'],
//                            ACA_Proveedor::$ID_PROVINCIA        => $result['_IDProvincia'],
//                            ACA_Proveedor::$CUIT                => utf8_encode($result['CUIT']),
//                            ACA_Proveedor::$EMAIL               => utf8_encode($result['Email']),
//                            ACA_Proveedor::$TELEFONO            => utf8_encode($result['Telefono']),
//                            ACA_Proveedor::$WEB                 => utf8_encode($result['Web']),
//                            ACA_Proveedor::$FAX                 => utf8_encode($result['Fax']),
//                            ACA_Proveedor::$ES_ACOPIADOR        => $result['Es_Acopiador'],
//                            ACA_Proveedor::$HECTAREA            => (int)$result['Hectarea'],
//                            ACA_Proveedor::$ACTIVO              => $result['Activo'],
//                            ACA_Proveedor::$PROVCF              => $result['ProvCF'],
//                            ACA_Proveedor::$COSECHERO           => $result['Cosechero'],
//                            ACA_Proveedor::$CERTIFICADO         => $result['Certificado'],
//                            ACA_Proveedor::$OBSERVACIONES       => $result['Obs'],
//                            ACA_Proveedor::$CREADO_POR          => utf8_encode($result['_Creado_por']),
//                            ACA_Proveedor::$MODIFICADO_POR      => utf8_encode($result['_Modificado_por']),
//                            ACA_Proveedor::$FECHA_CREACION      => $fecha_creacion,
//                            ACA_Proveedor::$FECHA_MODIFICACION  => $fecha_modif
//                        ]);
//                    }
//                }else{
                    $PROVEEDOR = new $model;
                    $PROVEEDOR[ACA_Proveedor::$ID_PROVEEDOR      ] = $result['ID'];
                    $PROVEEDOR[ACA_Proveedor::$ID_BALANZA        ] = $prefijo;
                    $PROVEEDOR[ACA_Proveedor::$CODIGO            ] = (int)$result['Codigo'];
                    $PROVEEDOR[ACA_Proveedor::$RAZON_SOCIAL      ] = utf8_encode($result['Razon_Social']);
                    $PROVEEDOR[ACA_Proveedor::$DOMICILIO         ] = utf8_encode($result['Domicilio']);
                    $PROVEEDOR[ACA_Proveedor::$LOCALIDAD         ] = utf8_encode($result['Localidad']);
                    $PROVEEDOR[ACA_Proveedor::$CP                ] = utf8_encode($result['CP']);
                    $PROVEEDOR[ACA_Proveedor::$ID_PAIS           ] = $result['_IDPais'];
                    $PROVEEDOR[ACA_Proveedor::$ID_PROVINCIA      ] = $result['_IDProvincia'];
                    $PROVEEDOR[ACA_Proveedor::$CUIT              ] = utf8_encode($result['CUIT']);
                    $PROVEEDOR[ACA_Proveedor::$EMAIL             ] = utf8_encode($result['Email']);
                    $PROVEEDOR[ACA_Proveedor::$TELEFONO          ] = utf8_encode($result['Telefono']);
                    $PROVEEDOR[ACA_Proveedor::$WEB               ] = utf8_encode($result['Web']);
                    $PROVEEDOR[ACA_Proveedor::$FAX               ] = utf8_encode($result['Fax']);
                    $PROVEEDOR[ACA_Proveedor::$ES_ACOPIADOR      ] = $result['Es_Acopiador'];
                    $PROVEEDOR[ACA_Proveedor::$HECTAREA          ] = (int)$result['Hectarea'];
                    $PROVEEDOR[ACA_Proveedor::$ACTIVO            ] = $result['Activo'];
                    $PROVEEDOR[ACA_Proveedor::$PROVCF            ] = $result['ProvCF'];
                    $PROVEEDOR[ACA_Proveedor::$COSECHERO         ] = $result['Cosechero'];
                    $PROVEEDOR[ACA_Proveedor::$CERTIFICADO       ] = $result['Certificado'];
                    $PROVEEDOR[ACA_Proveedor::$OBSERVACIONES     ] = $result['Obs'];
                    $PROVEEDOR[ACA_Proveedor::$CREADO_POR        ] = utf8_encode($result['_Creado_por']);
                    $PROVEEDOR[ACA_Proveedor::$MODIFICADO_POR    ] = utf8_encode($result['_Modificado_por']);
                    $PROVEEDOR[ACA_Proveedor::$FECHA_CREACION    ] = $fecha_creacion;
                    $PROVEEDOR[ACA_Proveedor::$FECHA_MODIFICACION] = $fecha_modif;
                    $PROVEEDOR->save();
//                }

                unset($result);
            }
        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarProvincias">
    public function cargarProvincias($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();
            $model::where(ACA_Provincia::$ID_BALANZA , $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

//                $provincias = ACA_Provincia::select(ACA_Provincia::$ID, ACA_Provincia::$FECHA_MODIFICACION)->where([
//                    [ACA_Provincia::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Provincia::$ID_PROVINCIA,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($provincias[ACA_Provincia::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $provincias[ACA_Provincia::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Provincia::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Provincia::$ID_PROVINCIA,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Provincia::$ID_PROVINCIA        => $result['ID'],
//                            ACA_Provincia::$ID_BALANZA          => $prefijo,
//                            ACA_Provincia::$NOMBRE              => utf8_encode($result['Nombre']),
//                            ACA_Provincia::$ID_PAIS             => $result['_IDPais'],
//                            ACA_Provincia::$OBSERVACIONES       => utf8_encode($result['_Observaciones']),
//                            ACA_Provincia::$CREADO_POR          => utf8_encode($result['_Creado_por']),
//                            ACA_Provincia::$MODIFICADO_POR      => utf8_encode($result['_Modificado_por']),
//                            ACA_Provincia::$FECHA_CREACION      => $fecha_creacion,
//                            ACA_Provincia::$FECHA_MODIFICACION  => $fecha_modif
//                        ]);
//                    }
//                }else{
                    $PROVINCIA = new $model;
                    $PROVINCIA[ACA_Provincia::$ID_PROVINCIA      ] = $result['ID'];
                    $PROVINCIA[ACA_Provincia::$ID_BALANZA        ] = $prefijo;
                    $PROVINCIA[ACA_Provincia::$NOMBRE            ] = utf8_encode($result['Nombre']);
                    $PROVINCIA[ACA_Provincia::$ID_PAIS           ] = $result['_IDPais'];
                    $PROVINCIA[ACA_Provincia::$OBSERVACIONES     ] = utf8_encode($result['_Observaciones']);
                    $PROVINCIA[ACA_Provincia::$CREADO_POR        ] = utf8_encode($result['_Creado_por']);
                    $PROVINCIA[ACA_Provincia::$MODIFICADO_POR    ] = utf8_encode($result['_Modificado_por']);
                    $PROVINCIA[ACA_Provincia::$FECHA_CREACION    ] = $fecha_creacion;
                    $PROVINCIA[ACA_Provincia::$FECHA_MODIFICACION] = $fecha_modif;
                    $PROVINCIA->save();
//                }
                unset($result);

            }
        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarRemitos">
    public function cargarRemitos($result, $model, $prefijo){
        if($result != NULL){
            $row = $result->fetchall();
            
            $model::where(ACA_Remito::$ID_BALANZA, $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

                if(!empty($result['Fecha'])){
                    $fecha = DateTime::createFromFormat('Y-m-d H:i:s', $result['Fecha'])->format('d-m-Y H:i:s');
                }else{
                    $fecha = null;
                }

                if(!empty($result['Fecha_Exp'])){
                    $fecha_exportacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['Fecha_Exp'])->format('d-m-Y H:i:s');
                }else{
                    $fecha_exportacion = null;
                }

                if(!empty($result['Fecha_Llegada'])){
                    $fecha_llegada = DateTime::createFromFormat('Y-m-d H:i:s', $result['Fecha_Llegada'])->format('d-m-Y H:i:s');
                }else{
                    $fecha_llegada = null;
                }

                if(!empty($result['Fecha_Anulacion'])){
                    $fecha_anulacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['Fecha_Anulacion'])->format('d-m-Y H:i:s');
                }else{
                    $fecha_anulacion = null;
                }

//                $remitos = ACA_Remito::select(ACA_Remito::$ID_REMITO,ACA_Remito::$FECHA_MODIFICACION)->where([
//                    [ACA_Remito::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Remito::$ID_REMITO,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($remitos[ACA_Remito::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $remitos[ACA_Remito::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Remito::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Remito::$ID_REMITO,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Remito::$ID_REMITO             => $result['ID'],
//                            ACA_Remito::$ID_BALANZA            => $prefijo,
//                            ACA_Remito::$ID_TALONARIO          => $result['_IDTalonario'],
//                            ACA_Remito::$NRO_COMPROBANTE       => $result['Nro_Comprobante'],
//                            ACA_Remito::$FECHA                 => $fecha,
//                            ACA_Remito::$ID_CLIENTE            => $result['_IDCliente'],
//                            ACA_Remito::$DOMICILIO_CLIENTE     => utf8_encode($result['Domicilio_Cliente']),
//                            ACA_Remito::$CUIT_CLIENTE          => $result['CUIT_Cliente'],
//                            ACA_Remito::$VAPOR                 => utf8_encode($result['Vapor']),
//                            ACA_Remito::$PRECINTOS             => utf8_encode($result['Precintos']),
//                            ACA_Remito::$PERMISO_EMBARQUE      => utf8_encode($result['Permiso_Embarque']),
//                            ACA_Remito::$ID_TRANSPORTE         => $result['_IDTransporte'],
//                            ACA_Remito::$DOMICILIO_TRANSPORTE  => utf8_encode($result['Domicilio_Transporte']),
//                            ACA_Remito::$CUIT_TRANSPORTE       => $result['CUIT_Transporte'],
//                            ACA_Remito::$MARCA_CAMION          => utf8_encode($result['Marca_Camion']),
//                            ACA_Remito::$PATENTE               => utf8_encode($result['Patente']),
//                            ACA_Remito::$CONDUCTOR             => utf8_encode($result['Conductor']),
//                            ACA_Remito::$CONTENEDOR            => utf8_encode($result['Contenedor']),
//                            ACA_Remito::$PRECINTO_MARITIMO     => (int)$result['Precinto_Maritimo'],
//                            ACA_Remito::$DGR                   => utf8_encode($result['DGR']),
//                            ACA_Remito::$FDA                   => utf8_encode($result['FDA']),
//                            ACA_Remito::$MARCA_PERMISO         => utf8_encode($result['Marca_Permiso']),
//                            ACA_Remito::$MARCA_CF              => utf8_encode($result['Marca_CF']),
//                            ACA_Remito::$EXPORTADO             => $result['Exportado'],
//                            ACA_Remito::$FECHA_EXP             => $fecha_exportacion,
//                            //Hora_Exp (falta)
//                            ACA_Remito::$VALOR_DECLARADO       => (double)$result['Valor_Declarado'],
//                            ACA_Remito::$FLETE_TRATADO         => utf8_encode($result['Flete_Tratado']),
//                            ACA_Remito::$FECHA_LLEGADA         => $fecha_llegada,
//                            ACA_Remito::$MERCADO_INTERNO       => $result['Mercado_Interno'],
//                            ACA_Remito::$FECHA_ANULACION       => $fecha_anulacion,
//                            ACA_Remito::$USUARIO_ANULACION     => utf8_encode($result['Usuario_Anulacion']),
//                            ACA_Remito::$MOTIVO_ANULACION      => utf8_encode($result['Motivo_Anulacion']),
//                            ACA_Remito::$OBS                   => utf8_encode($result['Obs']),
//                            ACA_Remito::$CREADO_POR            => utf8_encode($result['_Creado_por']),
//                            ACA_Remito::$MODIFICADO_POR        => utf8_encode($result['_Modificado_por']),
//                            ACA_Remito::$FECHA_CREACION        => $fecha_creacion,
//                            ACA_Remito::$FECHA_MODIFICACION    => $fecha_modif
//                        ]);
//                    }
//                }else{
                    $REMITO = new $model;
                    $REMITO[ACA_Remito::$ID_REMITO           ] = $result['ID'];
                    $REMITO[ACA_Remito::$ID_BALANZA          ] = $prefijo;
                    $REMITO[ACA_Remito::$ID_TALONARIO        ] = $result['_IDTalonario'];
                    $REMITO[ACA_Remito::$NRO_COMPROBANTE     ] = $result['Nro_Comprobante'];
                    $REMITO[ACA_Remito::$FECHA               ] = $fecha;
                    $REMITO[ACA_Remito::$ID_CLIENTE          ] = $result['_IDCliente'];
                    $REMITO[ACA_Remito::$DOMICILIO_CLIENTE   ] = utf8_encode($result['Domicilio_Cliente']);
                    $REMITO[ACA_Remito::$CUIT_CLIENTE        ] = $result['CUIT_Cliente'];
                    $REMITO[ACA_Remito::$VAPOR               ] = utf8_encode($result['Vapor']);
                    $REMITO[ACA_Remito::$PRECINTOS           ] = utf8_encode($result['Precintos']);
                    $REMITO[ACA_Remito::$PERMISO_EMBARQUE    ] = utf8_encode($result['Permiso_Embarque']);
                    $REMITO[ACA_Remito::$ID_TRANSPORTE       ] = $result['_IDTransporte'];
                    $REMITO[ACA_Remito::$DOMICILIO_TRANSPORTE] = utf8_encode($result['Domicilio_Transporte']);
                    $REMITO[ACA_Remito::$CUIT_TRANSPORTE     ] = $result['CUIT_Transporte'];
                    $REMITO[ACA_Remito::$MARCA_CAMION        ] = utf8_encode($result['Marca_Camion']);
                    $REMITO[ACA_Remito::$PATENTE             ] = utf8_encode($result['Patente']);
                    $REMITO[ACA_Remito::$CONDUCTOR           ] = utf8_encode($result['Conductor']);
                    $REMITO[ACA_Remito::$CONTENEDOR          ] = utf8_encode($result['Contenedor']);
                    $REMITO[ACA_Remito::$PRECINTO_MARITIMO   ] = (int)$result['Precinto_Maritimo'];
                    $REMITO[ACA_Remito::$DGR                 ] = utf8_encode($result['DGR']);
                    $REMITO[ACA_Remito::$FDA                 ] = utf8_encode($result['FDA']);
                    $REMITO[ACA_Remito::$MARCA_PERMISO       ] = utf8_encode($result['Marca_Permiso']);
                    $REMITO[ACA_Remito::$MARCA_CF            ] = utf8_encode($result['Marca_CF']);
                    $REMITO[ACA_Remito::$EXPORTADO           ] = $result['Exportado'];
                    $REMITO[ACA_Remito::$FECHA_EXP           ] = $fecha_exportacion;
                    $REMITO[ACA_Remito::$VALOR_DECLARADO     ] = (double)$result['Valor_Declarado'];
                    $REMITO[ACA_Remito::$FLETE_TRATADO       ] = utf8_encode($result['Flete_Tratado']);
                    $REMITO[ACA_Remito::$FECHA_LLEGADA       ] = $fecha_llegada;
                    $REMITO[ACA_Remito::$MERCADO_INTERNO     ] = $result['Mercado_Interno'];
                    $REMITO[ACA_Remito::$FECHA_ANULACION     ] = $fecha_anulacion;
                    $REMITO[ACA_Remito::$USUARIO_ANULACION   ] = utf8_encode($result['Usuario_Anulacion']);
                    $REMITO[ACA_Remito::$MOTIVO_ANULACION    ] = utf8_encode($result['Motivo_Anulacion']);
                    $REMITO[ACA_Remito::$OBS                 ] = utf8_encode($result['Obs']);
                    $REMITO[ACA_Remito::$CREADO_POR          ] = utf8_encode($result['_Creado_por']);
                    $REMITO[ACA_Remito::$MODIFICADO_POR      ] = utf8_encode($result['_Modificado_por']);
                    $REMITO[ACA_Remito::$FECHA_CREACION      ] = $fecha_creacion;
                    $REMITO[ACA_Remito::$FECHA_MODIFICACION  ] = $fecha_modif;
                    $REMITO->save();
//                }

                unset($result);
            }
        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarRodados">
    public function cargarRodados($result, $model, $prefijo){
        if($result != NULL){
            $row = $result->fetchall();

            //TODO: Controlar que todos sean asi
            $model::where(ACA_Rodado::$ID_BALANZA, $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_Creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_Modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

//                $rodado = ACA_Rodado::select(ACA_Rodado::$ID_RODADO,ACA_Rodado::$FECHA_MODIFICACION)->where([
//                    [ACA_Rodado::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Rodado::$ID_RODADO,'=',$result['ID']]
//                ])->first();


                $RODADO = new $model;
                $RODADO[ACA_Rodado::$ID_RODADO         ] = $result['ID'];
                $RODADO[ACA_Rodado::$ID_BALANZA        ] = $prefijo;
                $RODADO[ACA_Rodado::$CODIGO            ] = utf8_encode($result['Codigo']);
                $RODADO[ACA_Rodado::$DESCRIPCION       ] = utf8_encode($result['Descripcion']);
                $RODADO[ACA_Rodado::$TARA              ] = (int)$result['Tara'];
                $RODADO[ACA_Rodado::$ES_PROPIO         ] = $result['Es_Propio'];
                $RODADO[ACA_Rodado::$OBS               ] = utf8_encode($result['Obs']);
                $RODADO[ACA_Rodado::$OBSERVACIONES     ] = utf8_encode($result['_Observaciones']);
                $RODADO[ACA_Rodado::$CREADO_POR        ] = utf8_encode($result['_Creado_por']);
                $RODADO[ACA_Rodado::$MODIFICADO_POR    ] = utf8_encode($result['_Modificado_por']);
                $RODADO[ACA_Rodado::$FECHA_CREACION    ] = $fecha_creacion;
                $RODADO[ACA_Rodado::$FECHA_MODIFICACION] = $fecha_modif;
                $RODADO->save();

                unset($result);
            }
        }
    }
    //      </editor-fold>

    /*
     * SALDOS se elimina y se inserta todo
     */
    //      <editor-fold defaultstate="collapsed" desc="cargarSaldos">
    public function cargarSaldos($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();

            $model::where(ACA_Saldo::$ID_BALANZA, $prefijo)->delete();
            foreach($row as $r => $result){

                $SALDO = new $model;
                $SALDO[ACA_Saldo::$ID_SALDO  ] = $result['Id'];
                $SALDO[ACA_Saldo::$ID_BALANZA] = $prefijo;
                $SALDO[ACA_Saldo::$CODIGO    ] = utf8_encode($result['Codigo']);
                $SALDO[ACA_Saldo::$SALDO     ] = $result['Saldo'];
                $SALDO->save();
                unset($result);
            }
        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarTipoComprobante">
    public function cargarTipoComprobante($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();
            $model::where(ACA_Tipo_Comprobante::$ID_BALANZA, $prefijo)->delete();
            
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

                //OBIT - CVM - CV - CG - DCR - DCH
                // if ($prefijo == 97 || $prefijo == 90 || $prefijo == 89 || $prefijo == 88 || $prefijo == 120 || $prefijo == 122) {
                  if(!empty($result['ARCHIVOEXPO'])){
                      $archivo_export = $result['ARCHIVOEXPO'];
                  }else{
                      $archivo_export = null;
                  }
                // }


//                $tipo_comprobante = ACA_Tipo_Comprobante::select(ACA_Tipo_Comprobante::$ID_TIPO_COMPROBANTE,ACA_Tipo_Comprobante::$FECHA_MODIFICACION)->where([
//                    [ACA_Tipo_Comprobante::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Tipo_Comprobante::$ID_TIPO_COMPROBANTE,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($tipo_comprobante->updated_at)){
//                    if($fecha_modif !== $tipo_comprobante->updated_at){

//                        $model::where([
//                            [ACA_Tipo_Comprobante::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Tipo_Comprobante::$ID_TIPO_COMPROBANTE,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Tipo_Comprobante::$ID_TIPO_COMPROBANTE  => $result['ID'],
//                            ACA_Tipo_Comprobante::$ID_BALANZA           => $prefijo,
//                            ACA_Tipo_Comprobante::$DESCRIPCION          => utf8_encode($result['Descripcion']),
//                            ACA_Tipo_Comprobante::$TOTAL_A_EMITIR       => $result['Total_a_emitir'],
//                            ACA_Tipo_Comprobante::$NRO_INICIAL          => $result['Nro_Inicial'],
//                            ACA_Tipo_Comprobante::$NRO_FINAL            => $result['Nro_Final'],
//                            ACA_Tipo_Comprobante::$NRO_ACTUAL           => $result['Nro_actual'],
//                            ACA_Tipo_Comprobante::$ARCHIVO              => utf8_encode($result['Archivo']),
//                            ACA_Tipo_Comprobante::$COPIAS               => $result['Copias'],
//                            ACA_Tipo_Comprobante::$ARCHIVO_EXPO         => $archivo_export,
//                            ACA_Tipo_Comprobante::$COPIA_DIGITAL        => $result['CopiaDigital'],
//                            ACA_Tipo_Comprobante::$TIPO                 => $result['Tipo'],
//                            ACA_Tipo_Comprobante::$PREVISUALIZAR        => $result['Previsualizar'],
//                            ACA_Tipo_Comprobante::$OBSERVACIONES        => $result['_Observaciones'],
//                            ACA_Tipo_Comprobante::$CREADO_POR           => utf8_encode($result['_Creado_por']),
//                            ACA_Tipo_Comprobante::$MODIFICADO_POR       => utf8_encode($result['_Modificado_por']),
//                            ACA_Tipo_Comprobante::$FECHA_CREACION       => $fecha_creacion,
//                            ACA_Tipo_Comprobante::$FECHA_MODIFICACION   => $fecha_modif
//                        ]);
//                    }
//                }else{
                    $TIPO_COMPROBANTE = new $model;
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$ID_TIPO_COMPROBANTE] = $result['ID'];
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$ID_BALANZA         ] = $prefijo;
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$DESCRIPCION        ] = utf8_encode($result['Descripcion']);
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$TOTAL_A_EMITIR     ] = $result['Total_a_emitir'];
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$NRO_INICIAL        ] = $result['Nro_Inicial'];
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$NRO_FINAL          ] = $result['Nro_Final'];
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$NRO_ACTUAL         ] = $result['Nro_actual'];
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$ARCHIVO            ] = utf8_encode($result['Archivo']);
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$COPIAS             ] = $result['Copias'];
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$ARCHIVO_EXPO       ] = $archivo_export;
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$COPIA_DIGITAL      ] = $result['CopiaDigital'];
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$TIPO               ] = $result['Tipo'];
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$PREVISUALIZAR      ] = $result['Previsualizar'];
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$OBSERVACIONES      ] = $result['_Observaciones'];
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$CREADO_POR         ] = utf8_encode($result['_Creado_por']);
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$MODIFICADO_POR     ] = utf8_encode($result['_Modificado_por']);
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$FECHA_CREACION     ] = $fecha_creacion;
                    $TIPO_COMPROBANTE[ACA_Tipo_Comprobante::$FECHA_MODIFICACION ] = $fecha_modif;
                    $TIPO_COMPROBANTE->save();
//                }

                unset($result);
            }
        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarTipoPrecio">
    public function cargarTipoPrecio($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();
            $model::where(ACA_Tipo_Precio::$ID_BALANZA, $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_Creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_Modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

//                $tipo_precio = ACA_Tipo_Precio::select(ACA_Tipo_Precio::$ID_TIPO_PRECIO, ACA_Tipo_Precio::$FECHA_MODIFICACION)->where([
//                    [ACA_Tipo_Precio::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Tipo_Precio::$ID_TIPO_PRECIO,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($tipo_precio[ACA_Tipo_Precio::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $tipo_precio[ACA_Tipo_Precio::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Tipo_Precio::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Tipo_Precio::$ID_TIPO_PRECIO,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Tipo_Precio::$ID_TIPO_PRECIO        => $result['ID'],
//                            ACA_Tipo_Precio::$ID_BALANZA            => $prefijo,
//                            ACA_Tipo_Precio::$CODIGO                => utf8_encode($result['Codigo']),
//                            ACA_Tipo_Precio::$DESCRIPCION           => utf8_encode($result['Descripcion']),
//                            ACA_Tipo_Precio::$ID_CATEGORIA          => $result['_IDCategoria'],
//                            ACA_Tipo_Precio::$PRECIO                => (double)$result['Precio'],
//                            ACA_Tipo_Precio::$PRECIO_ACOPIADOR      => $result['Precio_Acopiador'],
//                            ACA_Tipo_Precio::$OBSERVACIONES         => $result['_Observaciones'],
//                            ACA_Tipo_Precio::$CREADO_POR            => utf8_encode($result['_Creado_por']),
//                            ACA_Tipo_Precio::$MODIFICADO_POR        => utf8_encode($result['_Modificado_por']),
//                            ACA_Tipo_Precio::$FECHA_CREACION        => $fecha_creacion,
//                            ACA_Tipo_Precio::$FECHA_MODIFICACION    => $fecha_modif
//                        ]);
//                    }
//                }else{
                    $TIPO_PRECIO = new $model;
                    $TIPO_PRECIO[ACA_Tipo_Precio::$ID_TIPO_PRECIO    ] = $result['ID'];
                    $TIPO_PRECIO[ACA_Tipo_Precio::$ID_BALANZA        ] = $prefijo;
                    $TIPO_PRECIO[ACA_Tipo_Precio::$CODIGO            ] = utf8_encode($result['Codigo']);
                    $TIPO_PRECIO[ACA_Tipo_Precio::$DESCRIPCION       ] = utf8_encode($result['Descripcion']);
                    $TIPO_PRECIO[ACA_Tipo_Precio::$ID_CATEGORIA      ] = $result['_IDCategoria'];
                    $TIPO_PRECIO[ACA_Tipo_Precio::$PRECIO            ] = (double)$result['Precio'];
                    $TIPO_PRECIO[ACA_Tipo_Precio::$PRECIO_ACOPIADOR  ] = $result['Precio_Acopiador'];
                    $TIPO_PRECIO[ACA_Tipo_Precio::$OBSERVACIONES     ] = $result['_Observaciones'];
                    $TIPO_PRECIO[ACA_Tipo_Precio::$CREADO_POR        ] = utf8_encode($result['_Creado_por']);
                    $TIPO_PRECIO[ACA_Tipo_Precio::$MODIFICADO_POR    ] = utf8_encode($result['_Modificado_por']);
                    $TIPO_PRECIO[ACA_Tipo_Precio::$FECHA_CREACION    ] = $fecha_creacion;
                    $TIPO_PRECIO[ACA_Tipo_Precio::$FECHA_MODIFICACION] = $fecha_modif;
                    $TIPO_PRECIO->save();
//                }
                unset($result);

            }
        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarTipoHumedad">
    public function cargarTipoHumedad($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();
            $model::where(ACA_Tipo_Humedad::$ID_BALANZA , $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_Creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_Modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

//                $tipo_humedad = ACA_Tipo_Humedad::select(ACA_Tipo_Humedad::$ID_TIPO_HUMEDAD,ACA_Tipo_Humedad::$FECHA_MODIFICACION)->where([
//                    [ACA_Tipo_Humedad::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Tipo_Humedad::$ID_TIPO_HUMEDAD,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($tipo_humedad[ACA_Tipo_Humedad::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $tipo_humedad[ACA_Tipo_Humedad::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Tipo_Humedad::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Tipo_Humedad::$ID_TIPO_HUMEDAD,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Tipo_Humedad::$ID_TIPO_HUMEDAD      => $result['ID'],
//                            ACA_Tipo_Humedad::$ID_BALANZA           => $prefijo,
//                            ACA_Tipo_Humedad::$CODIGO               => utf8_encode($result['Codigo']),
//                            ACA_Tipo_Humedad::$DESCRIPCION          => utf8_encode($result['Descripcion']),
//                            ACA_Tipo_Humedad::$PORCENTAJE           => (int)$result['Porcentaje'],
//                            ACA_Tipo_Humedad::$OBS                  => utf8_encode($result['Obs']),
//                            ACA_Tipo_Humedad::$CREADO_POR           => utf8_encode($result['_Creado_por']),
//                            ACA_Tipo_Humedad::$MODIFICADO_POR       => utf8_encode($result['_Modificado_por']),
//                            ACA_Tipo_Humedad::$FECHA_CREACION       => $fecha_creacion,
//                            ACA_Tipo_Humedad::$FECHA_MODIFICACION   => $fecha_modif
//                        ]);
//                    }
//                }else{
                    $TIPO_HUMEDAD = new $model;
                    $TIPO_HUMEDAD[ACA_Tipo_Humedad::$ID_TIPO_HUMEDAD   ] = $result['ID'];
                    $TIPO_HUMEDAD[ACA_Tipo_Humedad::$ID_BALANZA        ] = $prefijo;
                    $TIPO_HUMEDAD[ACA_Tipo_Humedad::$CODIGO            ] = utf8_encode($result['Codigo']);
                    $TIPO_HUMEDAD[ACA_Tipo_Humedad::$DESCRIPCION       ] = utf8_encode($result['Descripcion']);
                    $TIPO_HUMEDAD[ACA_Tipo_Humedad::$PORCENTAJE        ] = (int)$result['Porcentaje'];
                    $TIPO_HUMEDAD[ACA_Tipo_Humedad::$OBS               ] = utf8_encode($result['Obs']);
                    $TIPO_HUMEDAD[ACA_Tipo_Humedad::$CREADO_POR        ] = utf8_encode($result['_Creado_por']);
                    $TIPO_HUMEDAD[ACA_Tipo_Humedad::$MODIFICADO_POR    ] = utf8_encode($result['_Modificado_por']);
                    $TIPO_HUMEDAD[ACA_Tipo_Humedad::$FECHA_CREACION    ] = $fecha_creacion;
                    $TIPO_HUMEDAD[ACA_Tipo_Humedad::$FECHA_MODIFICACION] = $fecha_modif;
                    $TIPO_HUMEDAD->save();
//                }

                unset($result);
            }
        }
    }
    //      </editor-fold>

    //      <editor-fold defaultstate="collapsed" desc="cargarTransportes">
    public function cargarTransportes($result, $model, $prefijo) {
        if($result != NULL){
            $row = $result->fetchall();
            $model::where(ACA_Transporte::$ID_BALANZA , $prefijo)->delete();
            foreach($row as $r => $result){

                if(!empty($result['_Fecha_Creacion'])){
                    $fecha_creacion = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Creacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_creacion = null;
                }

                if(!empty($result['_Fecha_Modificacion'])){
                    $fecha_modif = DateTime::createFromFormat('Y-m-d H:i:s', $result['_Fecha_Modificacion'])->format('d/m/Y H:i:s');
                }else{
                    $fecha_modif = null;
                }

//                $transporte = ACA_Transporte::select(ACA_Transporte::$ID_TRANSPORTE,ACA_Transporte::$FECHA_MODIFICACION)->where([
//                    [ACA_Transporte::$ID_BALANZA,'=',$prefijo],
//                    [ACA_Transporte::$ID_TRANSPORTE,'=',$result['ID']]
//                ])->first();

                /*
                 * SI EXISTE Y DIFIEREN DATOS MODIFICO, SINO CREO
                 */
//                if(isset($transporte[ACA_Transporte::$FECHA_MODIFICACION])){
//                    if($fecha_modif !== $transporte[ACA_Transporte::$FECHA_MODIFICACION]){

//                        $model::where([
//                            [ACA_Transporte::$ID_BALANZA,'=',$prefijo],
//                            [ACA_Transporte::$ID_TRANSPORTE,'=',$result['ID']]
//                        ])
//                        ->update([
//                            ACA_Transporte::$ID_TRANSPORTE      => $result['ID'],
//                            ACA_Transporte::$ID_BALANZA         => $prefijo,
//                            ACA_Transporte::$RAZON_SOCIAL       => utf8_encode($result['Razon_Social']),
//                            ACA_Transporte::$DOMICILIO          => utf8_encode($result['Domicilio']),
//                            ACA_Transporte::$CUIT               => $result['Cuit'],
//                            ACA_Transporte::$OBS                => utf8_encode($result['Obs']),
//                            ACA_Transporte::$CREADO_POR         => utf8_encode($result['_Creado_por']),
//                            ACA_Transporte::$MODIFICADO_POR     => utf8_encode($result['_Modificado_por']),
//                            ACA_Transporte::$FECHA_CREACION     => $fecha_creacion,
//                            ACA_Transporte::$FECHA_MODIFICACION => $fecha_modif
//                        ]);
//                    }
//                }else{
                    $TRANSPORTE = new $model;
                    $TRANSPORTE[ACA_Transporte::$ID_TRANSPORTE     ] = $result['ID'];
                    $TRANSPORTE[ACA_Transporte::$ID_BALANZA        ] = $prefijo;
                    $TRANSPORTE[ACA_Transporte::$RAZON_SOCIAL      ] = utf8_encode($result['Razon_Social']);
                    $TRANSPORTE[ACA_Transporte::$DOMICILIO         ] = utf8_encode($result['Domicilio']);
                    $TRANSPORTE[ACA_Transporte::$CUIT              ] = $result['Cuit'];
                    $TRANSPORTE[ACA_Transporte::$OBS               ] = utf8_encode($result['Obs']);
                    $TRANSPORTE[ACA_Transporte::$CREADO_POR        ] = utf8_encode($result['_Creado_por']);
                    $TRANSPORTE[ACA_Transporte::$MODIFICADO_POR    ] = utf8_encode($result['_Modificado_por']);
                    $TRANSPORTE[ACA_Transporte::$FECHA_CREACION    ] = $fecha_creacion;
                    $TRANSPORTE[ACA_Transporte::$FECHA_MODIFICACION] = $fecha_modif;
                    $TRANSPORTE->save();
//                }

                unset($result);
            }
        }
    }
    //      </editor-fold>

}
