<?php

namespace App\Http\Controllers;
use App\Role;
use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use PDOException;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role as Roles; 
use Spatie\Permission\Models\Permission as Permisos;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class UsuariosController extends Controller
{
    public function indexUsuarios(){
        $usuarios = User::all();
        $roles = Role::all();
        $permisos = Permission::all();
        return view("usuarios.index_usuarios", compact("usuarios","roles","permisos"));
    }
    
    public function indexRolesPermisos(){
        $roles = Role::all();
        $permisos = Permission::all();
        return view("usuarios.roles_y_permisos", compact("roles","permisos"));
    }
    
    public function crearUsuario(Request $request){
        
        $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
        
        User::create([
            'name'          => $request['name'],
            'email'         => $request['email'],
            'password'      => bcrypt($request['password']),
        ]);
        
         return redirect('/usuarios')->with('msg',"El usuario fué creado correctamente!");
        
    }
    
    public function eliminarUsuario(Request $request){
        if($request->ajax()){
            try {
                User::destroy($request->id_usuario);
                return response()->json("El Usuario se eliminó correctamente!",200);
            } catch (\PDOException $ex) {
                return response()->json($ex->getMessage(),500);
            }
        }
    }
    
    public function editarUsuario($usuario){
        $usuario = User::findOrFail($usuario);
        return view("auth.editar_usuario", compact("usuario"));
    }
    
    public function guardarUsuario(Request $request){
        $this->validate($request,[
            'name' => 'required|max:255',
            'password' => 'required|min:6|confirmed',
        ]);
        
        User::where('id',$request->id_usuario)->update([
            'name'          => $request['name'],
            'password'      => bcrypt($request['password']),
        ]);
        
         return redirect('/usuarios')->with('msg',"El usuario fué modificado correctamente!");
    }
    
    public function eliminarRol(Request $request){
        if ($request->ajax()){
            try{
                Role::destroy($request->rol_id);
                return response()->json("El Rol se eliminó correctamente!",200);
            } catch (\PDOException $ex) {
                return response()->json($ex->getMessage(),500);
            }
        }
    }
    
    public function eliminarPermiso(Request $request){
        if ($request->ajax()){
            try{
                Permission::destroy($request->id_permiso);
                return response()->json("El Permiso se eliminó correctamente!",200);
            } catch (\PDOException $ex) {
                return response()->json($ex->getMessage(),500);
            }
        }
    }
    
    public function crearRol(Request $request){
        if ($request->ajax()){
            try{
                $rol = Role::create([
                    'name' => $request->name
                ]);
            } catch (\PDOException $ex) {
                return response()->json(array("mensaje" => $ex->getMessage()),400);
            }
            return response()->json($rol,200);
        }
    }
    
    public function crearPermiso(Request $request){
        if ($request->ajax()){
            try{
                $rol = Permission::create([
                    'name' => $request->name
                ]);
            } catch (\PDOException $ex) {
                return response()->json(array("mensaje" => $ex->getMessage()),400);
            }
            return response()->json($rol,200);
        }
    }
    
    public function editarRol(Request $request){
        if ($request->ajax()){
            
            try{
                $rol = Role::where("id",$request->id_rol)->update(['name' => $request->name]);
            } catch (\PDOException $ex) {
                return response()->json(array("mensaje" => $ex->getMessage()),400);
            }
            
            return response()->json($rol,200);
        }
    }
    
    public function editarPermiso(Request $request){
        if ($request->ajax()){
            
            try{
                $permiso = Permission::where("id", $request->id_permiso)->update(['name' => $request->name]);
            } catch (\PDOException $ex) {
                return response()->json(array("mensaje" => $ex->getMessage()),400);
            }
            
            return response()->json($permiso,200);
        }
    }
    
    public function getPermissionsOfRole(Request $request){
        if ($request->ajax()){

            try{
                $permisos = DB::table('role_has_permissions')->where('role_id', $request->id_rol)->get(['permission_id']);
                
                $lista_permisos = array();
                foreach($permisos as $permiso => $value){
                    $lista_permisos[] = $value->permission_id;
                }
                
                $lista_permisos_fix = Permission::whereIn('id', $lista_permisos)->get();
                
            } catch (\PDOException $ex) {
                return response()->json(array("mensaje" => $ex->getMessage()),400);
            }
            
            return response()->json($lista_permisos_fix,200);
        }
    }
    
    public function deletePermissionsOfRole(Request $request){
        if ($request->ajax()){
            try{
                DB::table('role_has_permissions')->where('permission_id', $request->id_permiso)->where('role_id',$request->id_rol)->delete();
            } catch (\PDOException $ex) {
                return response()->json(array("mensaje" => $ex->getMessage()),400);
            }
            
            return response()->json(array("mensaje" => "ok"),200);
        }
    }
    
    public function assignPermissionToRole(Request $request){
        if ($request->ajax()){
            
            try{
                $mensaje = DB::table('role_has_permissions')->insert(
                    ['permission_id' => $request->id_permiso , 'role_id' => $request->id_rol]
                );
            } catch (\PDOException $ex) {
                if($ex->getCode() == 23000){
                    return response()->json(array("msg" => "El permiso ya fué asignado") , 400);
                }else{
                    return response()->json(array("msg" => ($ex->getCode() . " " .$ex->getMessage())) , 400);
                }
            }
            
            return response()->json(array("msg" => "El permiso se asoció correctamente."), 200);
        }
    }
    
    public function getRolesUsuario(Request $request){
        if($request->ajax()){
            try{
                $roles = User::find($request->id_usuario)->roles;
                return response()->json($roles, 200);
            } catch (\PDOException $ex) {
                return response()->json(array("msg" => ($ex->getCode() . " " .$ex->getMessage())) , 400);
            }
        }
    }
    
    public function getPermisosUsuario(Request $request){
        if($request->ajax()){
            try{
                $permisos = User::find($request->id_usuario)->permissions;
                return response()->json($permisos, 200);
            } catch (\PDOException $ex) {
                return response()->json(array("msg" => ($ex->getCode() . " " .$ex->getMessage())) , 400);
            }
        }
    }
    
    public function eliminarPermisoUsuario(Request $request){
        if($request->ajax()){
            try{
                User::find($request->id_usuario)->revokePermissionTo(Permission::findOrFail($request->id_permiso)->name);
                return response()->json(array("msg" => "Permiso desafectado!"), 200);
            } catch (\PDOException $ex) {
                return response()->json(array("msg" => ($ex->getCode() . " " .$ex->getMessage())) , 400);
            }
        }
    }
    
    public function eliminarRolUsuario(Request $request){
        if($request->ajax()){
            try{
                User::find($request->id_usuario)->removeRole(Role::findOrFail($request->id_rol)->name);
                return response()->json(array("msg" => "Rol desafectado!"), 200);
            } catch (\PDOException $ex) {
                return response()->json(array("msg" => ($ex->getCode() . " " .$ex->getMessage())) , 400);
            }
        }
    }
    
    public function agregarPermisoUsuario(Request $request){
        if($request->ajax()){
            try{
                User::find($request->id_usuario)->givePermissionTo(Permission::findOrFail($request->id_permiso)->name);
                return response()->json(array("msg" => "Permiso agregado!"), 200);
            } catch (\PDOException $ex) {
                if($ex->getCode() == 23000){
                    return response()->json(array("msg" => "El permiso ya fué asignado") , 400);
                }else{
                    return response()->json(array("msg" => ($ex->getCode() . " " .$ex->getMessage())) , 400);
                }
            }
        }
    }
    
    public function agregarRolUsuario(Request $request){
        if($request->ajax()){
            try{
                User::find($request->id_usuario)->assignRole(Role::findOrFail($request->id_rol)->name);
                return response()->json(array("msg" => "Rol asignado!"), 200);
            } catch (\PDOException $ex) {
                if($ex->getCode() == 23000){
                    return response()->json(array("msg" => "El rol ya fué asignado") , 400);
                }else{
                    return response()->json(array("msg" => ($ex->getCode() . " " .$ex->getMessage())) , 400);
                }
            }
        }
    }
    
    
    
}
