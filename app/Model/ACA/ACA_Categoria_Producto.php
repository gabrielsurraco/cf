<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ACA_Categoria_Producto extends Model
{
    protected $table   = 'CATEGORIASPRODUCTOS_AUX';

    protected $guarded = [];

    public static $ID = 'id';
    public static $ID_BALANZA = '_IDBalanza';
    public static $ID_CAT_PRODUCT = '_IDCategoriaProducto';
    public static $DESCRIPCION = 'Descripcion';
    public static $OBSERVACIONES = '_Observaciones';
    public static $CREADO_POR = '_Creado_por';
    public static $FECHA_CREACION = '_Fecha_Creacion';
    public static $MODIFICADO_POR = '_Modificado_por';
    public static $FECHA_MODIFICACION = '_Fecha_Modificacion';

    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }

    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }
}
