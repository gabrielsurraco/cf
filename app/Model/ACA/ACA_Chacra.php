<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ACA_Chacra extends Model
{
    protected $table = 'CHACRAS_AUX';

    protected $guarded = [];

    public static $ID_BALANZA         = '_IDBalanza';
    public static $ID_CHACRA          = '_IDChacra';
    public static $CODIGO             = 'Codigo';
    public static $DESCRIPCION        = 'Descripcion';
    public static $CHACRA_PROPIA      = 'Chacra_Propia';
    public static $OBSERVACIONES      = '_Observaciones';
    public static $OBS                = 'Obs';
    public static $CREADO_POR         = '_Creado_por';
    public static $FECHA_CREACION     = '_Fecha_Creacion';
    public static $MODIFICADO_POR     = '_Modificado_por';
    public static $FECHA_MODIFICACION = '_Fecha_Modificacion';

    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }

    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }
}
