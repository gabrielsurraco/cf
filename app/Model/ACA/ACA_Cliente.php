<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ACA_Cliente extends Model
{
    protected $table = 'CLIENTES_AUX';

    protected $guarded = ['id'];

    public static $ID_CLIENTE = '_IDCliente';
    public static $ID_BALANZA = '_IDBalanza';
    public static $CODIGO = 'Codigo';
    public static $RAZON_SOCIAL = 'Razon_Social';
    public static $DOMICILIO = 'Domicilio';
    public static $LOCALIDAD = 'Localidad';
    public static $CP = 'CP';
    public static $ID_PAIS = '_IDPais';
    public static $ID_PROVINCIA = '_IDProvincia';
    public static $CUIT = 'CUIT';
    public static $EMAIL = 'Email';
    public static $WEB = 'Web';
    public static $TELEFONO = 'Telefono';
    public static $FAX = 'Fax';
    public static $OBSERVACIONES = '_Observaciones';
    public static $ACTIVO = 'Activo';
    public static $CREADO_POR = '_Creado_por';
    public static $MODIFICADO_POR = '_Modificado_por';
    public static $FECHA_CREACION = '_Fecha_Creacion';
    public static $FECHA_MODIFICACION = '_Fecha_Modificacion';


    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }

    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }
}
