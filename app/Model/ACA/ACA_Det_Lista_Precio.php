<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ACA_Det_Lista_Precio extends Model{
    protected $table = 'DETLISTAPRECIOS';
    
    protected $guarded = ['id'];
    
    public static $ID                       = 'id';
    public static $ID_BALANZA               = '_IDBalanza';
    public static $ID_DET_LISTA_PRECIO      = '_IDDetListaPrecios';
    public static $ID_LISTA_PRECIO          = '_IDListaPrecio';
    public static $ID_PRODUCTO              = '_IDProducto';
    public static $PRECIO                   = 'Precio';
    public static $OBSERVACIONES            = '_Observaciones';
    public static $CREADO_POR               = '_Creado_por';
    public static $FECHA_CREACION           = '_Fecha_Creacion';
    public static $MODIFICADO_POR           = '_Modificado_por';
    public static $FECHA_MODIFICACION       = '_Fecha_Modificacion';
    
    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }
    
    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }
}
