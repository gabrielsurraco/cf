<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ACA_Det_Lote extends Model
{
    protected $table = 'DETLOTE';

    protected $guarded = ['id'];

    public static $ID                   = 'id';
    public static $ID_BALANZA           = '_IDBalanza';
    public static $ID_DET_LOTE          = '_IDDetLote';
    public static $ID_PRODUCTO          = '_IDProducto';
    public static $ID_LOTE              = '_IDLote';
    public static $PORCENTAJE           = 'Porcentaje';
    public static $CANT_BOLSAS          = 'Cant_Bolsas';
    public static $KILOS_NETOS          = 'Kilos_Netos';
    public static $OBSERVACIONES        = '_Observaciones';
    public static $CREADO_POR           = '_Creado_por';
    public static $FECHA_CREACION       = '_Fecha_Creacion';
    public static $MODIFICADO_POR       = '_Modificado_por';
    public static $FECHA_MODIFICACION   = '_Fecha_Modificacion';


    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }

    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }


}
