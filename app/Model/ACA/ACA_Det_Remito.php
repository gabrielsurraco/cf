<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ACA_Det_Remito extends Model
{
    protected $table = 'DETREMITOS';

    protected $guarded = ['id'];

    public static $ID = 'id';
    public static $ID_BALANZA = '_IDBalanza';
    public static $ID_DET_REMITO = '_IDDetRemito';
    public static $ID_REMITO = '_IDRemito';
    public static $ID_LOTE = '_IDLote';
    public static $CANTIDAD_BOLSAS = 'Cant_Bolsas';
    public static $TARA_X_BOLSA = 'Tara_x_Bolsa';
    public static $PALLETS = 'Pallets';
    public static $PESO_X_PALLETS = 'Peso_x_Pallets';
    public static $OBSERVACIONES = '_Observaciones';
    public static $CREADO_POR = '_Creado_por';
    public static $FECHA_CREACION = '_Fecha_Creacion';
    public static $MODIFICADO_POR = '_Modificado_por';
    public static $FECHA_MODIFICACION = '_Fecha_Modificacion';

    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }

    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }
}
