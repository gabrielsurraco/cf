<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ACA_Lote extends Model
{
    protected $table = 'LOTES';

    protected $guarded = [];

    public static $ID = 'id';
    public static $ID_BALANZA = '_IDBalanza';
    public static $ID_LOTE = '_IDLote';
    public static $CODIGO = 'Codigo';
    public static $DESCRIPCION = 'Descripcion';
    public static $FECHA = 'Fecha';
    public static $CANT_BOLSAS = 'Cant_Bolsas';
    public static $KILOS_X_BOLSA = 'Kilos_x_Bolsa';
    public static $KILOS_NETO = 'Kilos_Neto';
    public static $CERTIFICADO = 'Certificado';
    public static $SINCRONIZADO = 'Sincronizado';
    public static $OBSERVACIONES = '_Observaciones';
    public static $CREADO_POR = '_Creado_por';
    public static $FECHA_CREACION = '_Fecha_Creacion';
    public static $MODIFICADO_POR = '_Modificado_por';
    public static $FECHA_MODIFICACION = '_Fecha_Modificacion';




    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }

    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }
}
