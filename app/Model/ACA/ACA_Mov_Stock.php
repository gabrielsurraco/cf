<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Planta;

class ACA_Mov_Stock extends Model
{
    protected $table = 'MOV_STOCK';
    
    protected $guarded = ['id'];
    
    public static $ID                   = 'id'; 
    public static $ID_BALANZA           = '_IDBalanza';
    public static $ID_MOV_STOCK         = '_IDMovStock';
    public static $FECHA                = 'Fecha';
    public static $FECHA_REPROCESO      = 'Fecha_reproceso';
    public static $ID_PRODUCTO          = '_IDProducto';
    public static $COD_PRODUCTO         = 'Cod_Producto';
    public static $CANTIDAD             = 'Cantidad';
    public static $E_S                  = 'E_S';
    public static $ID_MOTIVO            = '_IDMotivo';
    public static $M3                   = 'M3';
    public static $ID_PESAJE            = '_IDPesaje';
    public static $ID_DET_LOTE          = '_IDDetLote';
    public static $POR_BALANZA          = 'Por_Balanza';
    public static $OBSERVACIONES        = '_Observaciones';
    public static $CREADO_POR           = '_Creado_por';
    public static $FECHA_CREACION       = '_Fecha_Creacion';
    public static $MODIFICADO_POR       = '_Modificado_por';
    public static $FECHA_MODIFICACION   = '_Fecha_Modificacion';
    
    
    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }
    
    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }
    
    public function planta(){
        return $this->belongsTo(Planta::class, '_IDBalanza','_IDBalanza');
    }
}
