<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ACA_Pais extends Model
{
    protected $table = 'PAISES_AUX';

    protected $guarded = ['id'];

    PUBLIC STATIC $ID                   = 'id';
    PUBLIC STATIC $ID_BALANZA           = '_IDBalanza';
    PUBLIC STATIC $ID_PAIS              = '_IDPais';
    PUBLIC STATIC $NOMBRE               = 'Nombre';
    PUBLIC STATIC $OBSERVACIONES        = '_Observaciones';
    PUBLIC STATIC $CREADO_POR           = '_Creado_por';
    PUBLIC STATIC $FECHA_CREACION       = '_Fecha_Creacion';
    PUBLIC STATIC $MODIFICADO_POR       = '_Modificado_por';
    PUBLIC STATIC $FECHA_MODIFICACION   = '_Fecha_Modificacion';


    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }

    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }
}
