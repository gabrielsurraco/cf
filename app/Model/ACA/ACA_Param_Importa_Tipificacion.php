<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ACA_Param_Importa_Tipificacion extends Model
{
    protected $table = 'PARAM_IMPORTATIPIFICACION';
    
    protected $guarded = ['id'];
    
    PUBLIC STATIC $ID                   = 'id';
    PUBLIC STATIC $ID_BALANZA           = '_IDBalanza';
    PUBLIC STATIC $ID_PARAM_IMP_TIP     = '_IDParamIt';
    PUBLIC STATIC $PLANTA               = 'Planta';
    PUBLIC STATIC $ID_PRODUCTO          = '_IDProducto';
    PUBLIC STATIC $OBSERVACIONES        = '_Observaciones';
    PUBLIC STATIC $CREADO_POR           = '_Creado_por';
    PUBLIC STATIC $FECHA_CREACION       = '_Fecha_Creacion';
    PUBLIC STATIC $MODIFICADO_POR       = '_Modificado_por';
    PUBLIC STATIC $FECHA_MODIFICACION   = '_Fecha_Modificacion';
    
    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }
    
    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }
    
}
