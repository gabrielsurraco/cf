<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;

class ACA_Parametro extends Model{

    protected $table = '_STK_PARAMETROS';

    protected $guarded = ['id'];

    public static $ID                       = 'id';
    public static $ID_BALANZA               = '_IDBalanza';
    public static $MODULO                   = 'Modulo';
    public static $RIE                      = 'Rie';
    public static $DIAS_RIE                 = 'Dias_Rie';
    public static $PATH_RX                  = 'Path_Rx';
    public static $ID_TALONARIO_TICKET_TV   = '_IDTalonarioTicketTv';
    public static $ID_TALONARIO_TICKET      = '_IDTalonarioTicket';
    public static $ID_TALONARIO_REMITO      = '_IDTalonarioRemito';
    public static $ID_MOTIVO_STOCK          = '_IDMotivoStock';
    public static $ID_MOTIVO_LOTES          = '_IDMotivoLotes';
    public static $CODIGOS_PRODUCTOS        = 'Codigos_Productos';
    public static $MUESTRA_COLOR            = 'Muestra_Color';
    public static $MUESTRA_COMPROBANTE      = 'Muestra_Comprobante';
    public static $BOLSAS_X_DEF             = 'Bolsas_x_Def';
    public static $PESO_PALLET              = 'Peso_Pallet';
    public static $TARAS_X_BOLSA            = 'Taras_x_Bolsa';
    public static $TE_VERDE                 = 'Te_Verde';
    public static $LEÑA                     = 'Lenia';
    public static $CLAVE                    = 'Clave';
    public static $CLAVE_PESAJE             = 'Clave_Pesajes';
    public static $NAV_PESAJES              = 'Nav_Pesajes';
    public static $LOTES                    = 'Lotes';
    public static $INFO_DEPTO               = 'Info_Depto';
    public static $ESVB6                    = 'Esvb6';
    public static $BASE_COMPUESTA           = 'Base_Compuesta';
    public static $MODIFICA_HISTORICO       = 'Modifica_Historico';
    public static $OBSERVACIONES            = '_Observaciones';
    public static $CREADO_POR               = '_Creado_por';
    public static $FECHA_CREACION           = '_Fecha_Creacion';
    public static $MODIFICADO_POR           = '_Modificado_por';
    public static $FECHA_MODIFICACION       = '_Fecha_Modificacion';

    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }
}
