<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Model\ACA\ACA_Rodado;
use Illuminate\Support\Facades\DB;
use App\Model\ACA\ACA_Producto;
use App\Planta;

class ACA_Pesaje extends Model
{
    protected $table = 'PESAJES';

    protected $guarded = [ 'id'];

    public static $ID                     = 'id';
    public static $ID_BALANZA             = '_IDBalanza';
    public static $ID_PESAJE              = '_IDPesaje';
    public static $ID_TALONARIO           = '_IDTalonario';
    public static $NRO_COMPROBANTE        = 'Nro_Comprobante';
    public static $FECHA                  = 'Fecha';
    public static $ID_RODADO              = '_IDRodado';
    public static $PATENTE_ACOPLADO       = 'Patente_Acoplado';
    public static $CONDUCTOR              = 'Conductor';
    public static $ID_PRODUCTO            = '_IDProducto';
    public static $ID_TIPO_PRECIO         = '_IDTipoPrecio';
    public static $PRECIO                 = 'Precio';
    public static $ID_PLANTA_ORIGEN       = '_IDPlantaOrigen';
    public static $ID_PLANTA_DESTINO      = '_IDPlantaDestino';
    public static $ID_PROVEEDOR           = '_IDProveedor';
    
    public static $COD_PROVEEDOR           = 'Cod_Proveedor';
    
    public static $ID_ACOPIADOR           = '_IDAcopiador';
    public static $ES_ACOPIADOR           = 'Es_Acopiador';
    public static $ORDEN_ACOPIO           = 'Orden_Acopio';
    public static $VAL_REF                = 'VALREF';
    public static $ID_EMBALAJE            = '_IDEmbalaje';
    public static $CANT_EMBALAJE          = 'Cant_Embalaje';
    public static $PESO_EMB_X_UNIDAD      = 'Peso_Emb_x_Unidad';
    public static $ID_TIPO_HUMEDAD        = '_IDTipoHumedad';
    public static $PORCENTAJE_HUMEDAD     = 'porc_humedad';
    public static $PORCENTAJE_PALO        = 'porc_palo';
    public static $PESO_PALO              = 'Peso_Palo';
    public static $M3                     = 'M3';
    public static $FECHA_BRUTO            = 'Fecha_Bruto';
    public static $FECHA_ANULACION        = 'Fecha_Anulacion';
    public static $USUARIO_ANULACION      = 'Usuario_Anulacion';
    public static $MOTIVO_ANULACION       = 'Motivo_Anulacion';
    public static $BRUTO                  = 'Bruto';
    public static $TARA                   = 'Tara';
    public static $TOTAL_EMBALAJE         = 'Total_Embalaje';
    public static $PESO_HUMEDAD           = 'Peso_Humedad';
    public static $NETO                   = 'Neto';
    public static $ESTADO                 = 'Estado';
    public static $ID_CHACRA              = '_IDChacra';
    
    public static $COD_CHACRA              = 'Cod_Chacra';
    
    public static $TE_CERTIFICADO         = 'Te_Certificado';
    public static $ID_PESAJE_BASE_ORIGEN  = 'IDPESAJEBASEORIG';
    public static $ID_PESAJE_DUP          = 'IDPESAJEDUP';
    public static $ID_MIG_ORIG            = 'IDMIGORIG';
    public static $MIGRADO                = 'Migrado';
    public static $MODIFICADO             = 'Modificado';
    public static $LOTE_CHACRA            = 'Lote_Chacra';
    public static $TAMAÑO                 = 'Tamanio';
    public static $MALEZAS                = 'Malezas';
    public static $CALIDAD                = 'Calidad';
    public static $ACAROS                 = 'Acaros';
    public static $TEMPERATURA            = 'Temperatura';
    public static $PORC_MALLA_30          = 'Porc_Malla_30';
    public static $EXPO                   = 'Expo';
    public static $BUQUE                  = 'Buque';
    public static $CODIGO_ADUANA          = 'Codigo_Aduana';
    public static $RESERVA                = 'Reserva';
    public static $CODIGO_BALANZA         = 'Codigo_Balanza';
    public static $NRO_CERT_HAB           = 'Nro_Certif_Hab';
    public static $VENCIMIENTO            = 'Vencimiento';
    public static $NRO_CONTENEDOR         = 'Nro_Contenedor';
    public static $AFIP                   = 'Afip';
    public static $LINEA                  = 'Linea';
    public static $OTROS                  = 'Otros';
    public static $OBS                    = 'Obs';
    public static $CUIT_ATA               = 'Cuit_Ata';
    public static $APELLIDO_NOMBRE_ATA    = 'Apellido_Nombre_Ata';
    public static $NRO_PERMISO_EMBARQUE   = 'Nro_Permiso_Embarque';
    public static $PESAJEWEB              = 'Pesajeweb';
    public static $SINCRONIZADO           = 'Sincronizado';
    public static $OBSERVACIONES          = '_Observaciones';
    public static $CREADO_POR             = '_Creado_por';
    public static $FECHA_CREACION         = '_Fecha_Creacion';
    public static $MODIFICADO_POR         = '_Modificado_por';
    public static $FECHA_MODIFICACION     = '_Fecha_Modificacion';

    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }

    protected function getFechaAttribute($value){
        if($value != null)
            return Carbon::parse($value)->format('d/m/Y H:i');
        else
            return "N/A";
    }


    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }


    public function camion(){
        return $this->belongsTo(ACA_Rodado::class,'id_rodado');
    }
    
    public function chacra(){
        return $this->belongsTo(ACA_Chacra::class, 'Cod_Chacra','Codigo');
    }
    
    public function planta(){
        return $this->belongsTo(Planta::class, '_IDBalanza','_IDBalanza');
    }


}
