<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ACA_Procesamiento_Vale extends Model
{
    protected $table = 'PROCESAMIENTOVALES';
    
    protected $guarded = ['id'];
    
    public static $ID                   = 'id';
    public static $ID_BALANZA           = '_IDBalanza';
    public static $ID_PROC_VAL_BAL      = '_IDProcesamientoValesBalanza';
    public static $NRO_LOTE             = 'Nro_Lote';
    public static $RUTA                 = 'Ruta';
    public static $TIPO_PROCESO         = 'Tipo_proceso';
    public static $OPERACION            = 'Operacion';
    public static $TIPO_ARCHIVO         = 'TipoArchivo';
    public static $ARCHIVO_COMIENZA_CON = 'ArchivoComienzaCon';
    public static $ARCHIVO_FIJO         = 'ArchivoFijo';
    public static $OBSERVACIONES        = '_Observaciones';
    public static $CREADO_POR           = '_Creado_por';
    public static $FECHA_CREACION       = '_Fecha_Creacion';
    public static $MODIFICADO_POR       = '_Modificado_por';
    public static $FECHA_MODIFICACION   = '_Fecha_Modificacion';
    
    
    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }
    
    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }
}
