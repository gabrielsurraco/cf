<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ACA_Producto extends Model
{
    protected $table = 'PRODUCTOS';

    protected $guarded = ['id'];

    public static $ID                   = 'id';
    public static $ID_BALANZA           = '_IDBalanza';
    public static $ID_PRODUCTO          = '_IDProducto';
    public static $CODIGO               = 'Codigo';
    public static $DESCRIPCION          = 'Descripcion';
    public static $CANTIDAD             = 'Cantidad';
    public static $M3                   = 'M3';
    public static $CODIGO_BALANZA       = 'Codigo_Balanza';
    public static $PORCENTAJE           = 'Porcentaje';
    public static $ACTIVO               = 'Activo';
    public static $ID_CATEGORIA         = '_IDCategoria';
    public static $PRECIO               = 'Precio';
    public static $FECHA_SALDO_INICIAL  = 'Fecha_Saldo_Inicial';
    public static $SALDO_INICIAL        = 'Saldo_Inicial';
    public static $STOCK_MAXIMO         = 'Stock_Maximo';
    public static $SOTCK_MINIMO         = 'Stock_Minimo';
    public static $OBS                  = 'Obs';
    public static $CREADO_POR           = '_Creado_por';
    public static $FECHA_CRECION        = '_Fecha_Creacion';
    public static $MODIFICADO_POR       = '_Modificado_por';
    public static $FECHA_MODIFICACION   = '_Fecha_Modificacion';

    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }

    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }
}
