<?php

namespace App\Model\ACA;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class ACA_Proveedor extends Model
{
    protected $table = 'PROVEEDORES';
    
    protected $guarded = ['id'];
    
    public static $ID                   = 'id';
    public static $ID_BALANZA           = '_IDBalanza';
    public static $ID_PROVEEDOR         = '_IDProveedor';
    public static $CODIGO               = 'Codigo';
    public static $RAZON_SOCIAL         = 'Razon_Social';
    public static $DOMICILIO            = 'Domicilio';
    public static $LOCALIDAD            = 'Localidad';
    public static $CP                   = 'CP';
    public static $ID_PAIS              = '_IDPais';
    public static $ID_PROVINCIA         = '_IDProvincia';
    public static $CUIT                 = 'CUIT';
    public static $EMAIL                = 'Email';
    public static $WEB                  = 'Web';
    public static $TELEFONO             = 'Telefono';
    public static $FAX                  = 'Fax';
    public static $ES_ACOPIADOR         = 'Es_Acopiador';
    public static $HECTAREA             = 'Hectarea';
    public static $ACTIVO               = 'Activo';
    public static $PROVCF               = 'ProvCF';
    public static $COSECHERO            = 'Cosechero';
    public static $CERTIFICADO          = 'Certificado';
    public static $OBSERVACIONES        = '_Observaciones';
    public static $CREADO_POR           = '_Creado_por';
    public static $FECHA_CREACION       = '_Fecha_Creacion';
    public static $MODIFICADO_POR       = '_Modificado_por';
    public static $FECHA_MODIFICACION   = '_Fecha_Modificacion';
   
    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }
    
    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }
}
