<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ACA_Provincia extends Model
{
    protected $table = 'PROVINCIAS_AUX';

    protected $guarded = ['id'];

    public static $ID                   = 'id';
    public static $ID_BALANZA           = '_IDBalanza';
    public static $ID_PROVINCIA         = '_IDProvincia';
    public static $NOMBRE               = 'Nombre';
    public static $ID_PAIS              = '_IDPais';
    public static $OBSERVACIONES        = '_Observaciones';
    public static $CREADO_POR           = '_Creado_por';
    public static $FECHA_CREACION       = '_Fecha_Creacion';
    public static $MODIFICADO_POR       = '_Modificado_por';
    public static $FECHA_MODIFICACION   = '_Fecha_Modificacion';

    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }

    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }
}
