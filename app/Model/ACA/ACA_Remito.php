<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ACA_Remito extends Model
{
    protected $table = 'REMITOS';
    
    protected $guarded = ['id'];
    
    public static $ID                   = 'id'; 
    public static $ID_BALANZA           = '_IDBalanza'; 
    public static $ID_REMITO            = '_IDRemito'; 
    public static $ID_TALONARIO         = '_IDTalonario'; 
    public static $NRO_COMPROBANTE      = 'Nro_Comprobante'; 
    public static $FECHA                = 'Fecha'; 
    public static $ID_CLIENTE           = '_IDCliente'; 
    public static $DOMICILIO_CLIENTE    = 'Domicilio_Cliente'; 
    public static $CUIT_CLIENTE         = 'CUIT_Cliente'; 
    public static $VAPOR                = 'Vapor'; 
    public static $PRECINTOS            = 'Precintos'; 
    public static $PERMISO_EMBARQUE     = 'Permiso_Embarque'; 
    public static $ID_TRANSPORTE        = '_IDTransporte'; 
    public static $DOMICILIO_TRANSPORTE = 'Domicilio_Transporte'; 
    public static $CUIT_TRANSPORTE      = 'CUIT_Transporte'; 
    public static $MARCA_CAMION         = 'Marca_Camion'; 
    public static $PATENTE              = 'Patente'; 
    public static $CONDUCTOR            = 'Conductor'; 
    public static $CONTENEDOR           = 'Contenedor'; 
    public static $PRECINTO_MARITIMO    = 'Precinto_Maritimo'; 
    public static $DGR                  = 'DGR'; 
    public static $FDA                  = 'FDA'; 
    public static $MARCA_PERMISO        = 'Marca_Permiso'; 
    public static $MARCA_CF             = 'Marca_CF'; 
    public static $EXPORTADO            = 'Exportado'; 
    public static $FECHA_EXP            = 'Fecha_Exp'; 
    public static $HORA_EXP             = 'Hora_Exp'; 
    public static $VALOR_DECLARADO      = 'Valor_Declarado'; 
    public static $FLETE_TRATADO        = 'Flete_Tratado'; 
    public static $FECHA_LLEGADA        = 'Fecha_Llegada'; 
    public static $MERCADO_INTERNO      = 'Mercado_Interno'; 
    public static $FECHA_ANULACION      = 'Fecha_Anulacion'; 
    public static $USUARIO_ANULACION    = 'Usuario_Anulacion'; 
    public static $MOTIVO_ANULACION     = 'Motivo_Anulacion'; 
    public static $OBS                  = 'Obs'; 
    public static $CREADO_POR           = '_Creado_por'; 
    public static $FECHA_CREACION       = '_Fecha_Creacion'; 
    public static $MODIFICADO_POR       = '_Modificado_por'; 
    public static $FECHA_MODIFICACION   = '_Fecha_Modificacion'; 


    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }
    
    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }
}
