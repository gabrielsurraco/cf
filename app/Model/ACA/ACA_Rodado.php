<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ACA_Rodado extends Model
{
    protected $table = 'RODADOS_AUX';

    protected $guarded = [];

    public static $ID                   = 'id';
    public static $ID_BALANZA           = '_IDBalanza';
    public static $ID_RODADO            = '_IDRodado';
    public static $CODIGO               = 'Codigo';
    public static $DESCRIPCION          = 'Descripcion';
    public static $TARA                 = 'Tara';
    public static $ES_PROPIO            = 'Es_Propio';
    public static $OBS                  = 'Obs';
    public static $CREADO_POR           = '_Creado_por';
    public static $FECHA_CREACION       = '_Fecha_Creacion';
    public static $MODIFICADO_POR       = '_Modificado_por';
    public static $FECHA_MODIFICACION   = '_Fecha_Modificacion';
    public static $OBSERVACIONES        = '_Observaciones';

    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }

    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }
}
