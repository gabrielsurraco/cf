<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ACA_Tipo_Comprobante extends Model
{
//    public static $ID_BD = '_IDPlanta';
//    public static $ID_TIPO_COMPROBANTE = '_IDTipoComprobante';

    protected $table = 'TIPOCOMPROBANTE';

    protected $guarded = ['id'];

    public static $ID                   = 'id';
    public static $ID_BALANZA           = '_IDBalanza';
    public static $ID_TIPO_COMPROBANTE  = '_IDTipoComprobante';
    public static $DESCRIPCION          = 'Descripcion';
    public static $TOTAL_A_EMITIR       = 'Total_a_emitir';
    public static $NRO_INICIAL          = 'Nro_Inicial';
    public static $NRO_FINAL            = 'Nro_Final';
    public static $NRO_ACTUAL           = 'Nro_actual';
    public static $ARCHIVO              = 'Archivo';
    public static $COPIAS               = 'Copias';
    public static $COPIA_DIGITAL        = 'CopiaDigital';
    public static $TIPO                 = 'Tipo';
    public static $PREVISUALIZAR        = 'Previsualizar';
    public static $OBSERVACIONES        = '_Observaciones';
    public static $ARCHIVO_EXPO         = 'ArchivoExpo';
    public static $CREADO_POR           = '_Creado_por';
    public static $FECHA_CREACION       = '_Fecha_Creacion';
    public static $MODIFICADO_POR       = '_Modificado_por';
    public static $FECHA_MODIFICACION   = '_Fecha_Modificacion';

    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }

    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }
}
