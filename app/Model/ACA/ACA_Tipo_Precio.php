<?php

namespace App\Model\ACA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ACA_Tipo_Precio extends Model
{
    protected $table = 'TIPOPRECIO_AUX';

    protected $guarded = ['id'];

    public static $ID                   = 'id';
    public static $ID_BALANZA           = '_IDBalanza';
    public static $ID_TIPO_PRECIO       = '_IDTipoPrecio';
    public static $CODIGO               = 'Codigo';
    public static $DESCRIPCION          = 'Descripcion';
    public static $ID_CATEGORIA         = '_IDCategoria';
    public static $PRECIO               = 'Precio';
    public static $PRECIO_ACOPIADOR     = 'Precio_Acopiador';
    public static $OBSERVACIONES        = '_Observaciones';
    public static $CREADO_POR           = '_Creado_por';
    public static $FECHA_CREACION       = '_Fecha_Creacion';
    public static $MODIFICADO_POR       = '_Modificado_por';
    public static $FECHA_MODIFICACION   = '_Fecha_Modificacion';

    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }

    protected function getUpdatedAtAttribute($value) {
         return (new Carbon($value))->format('d/m/Y H:i:s');
    }
}
