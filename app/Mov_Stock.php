<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mov_Stock extends Model
{
    protected $connection = 'EXPOTEC_STOCK';
    
    protected $table = "MOV_STOCK";
    protected $fillable = [
        "ID",
        "Nro_Comprobante",
        "_IDTalonario",
        "_IDPlanta",
        "Fecha",
        "Fecha_reproceso",
        "Tipo",
        "Concepto",
        "_IDComprobante",
        "TipoCompAfec",
        "Fecha_Anulacion",
        "Total",
        "_IDProyecto",
        "_IDMotivo",
        "_IDDestino",
        "Usuario_Responsable",
        "Obs",
        "Maquina",
        "PorBalanza",
        "_IDPesaje",
        "_IDPesajeMovBolson",
        "_IDMovStockBalanza",
        "_IDLote",
        "_Creado_por",
        "_Fecha_creacion",
        "_Modificado_por",
        "_Fecha_modificacion",
        "_Observaciones",
    ];
}
