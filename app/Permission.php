<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Spatie\Permission\Contracts\Permission;

use Carbon\Carbon;

use Spatie\Permission\Contracts\Permission as PermissionContract;
use Spatie\Permission\Traits\RefreshesPermissionCache;

class Permission extends Model implements PermissionContract
{ 
    use RefreshesPermissionCache;
    
    public $guarded = ['id'];
    
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setTable(config('laravel-permission.table_names.permissions'));
    }
    
    public function roles()
    {
        return $this->belongsToMany(
            config('laravel-permission.models.role'),
            config('laravel-permission.table_names.role_has_permissions')
        );
    }
    
    public function users()
    {
        return $this->belongsToMany(
            config('auth.model') ?: config('auth.providers.users.model'),
            config('laravel-permission.table_names.user_has_permissions')
        );
    }
    
    public static function findByName($name)
    {
        $permission = static::getPermissions()->where('name', $name)->first();

        if (! $permission) {
            throw new PermissionDoesNotExist();
        }

        return $permission;
    }
    
    public function getCreatedAtAttribute( $value ){
        return (new Carbon($value))->format('d/m/Y H:i:s');
    }
    
    public function setCreatedAtAttribute( $value ) {
        $fecha =  Carbon::createFromFormat('Y-m-d H:i:s', $value);
        $this->attributes['created_at'] = $fecha->format('d/m/Y H:i:s');
    }

    public function setUpdatedAtAttribute( $value ) {
        $fecha =  Carbon::createFromFormat('Y-m-d H:i:s', $value);
        $this->attributes['updated_at'] = $fecha->format('d/m/Y H:i:s');
    }
    
    
}
