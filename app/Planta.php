<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planta extends Model
{
    protected $table = 'PLANTAS';
    
    protected $guarded = ["id"];


    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }
}
