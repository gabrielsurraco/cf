<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Contracts\Role as RoleContract;
use Spatie\Permission\Traits\RefreshesPermissionCache;
use Spatie\Permission\Traits\HasPermissions;

use Carbon\Carbon;

class Role extends Model implements RoleContract
{
    use HasPermissions;
    use RefreshesPermissionCache;
    
    public $guarded = ['id'];
    
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setTable(config('laravel-permission.table_names.roles'));
    }
    
    public function permissions()
    {
        return $this->belongsToMany(
            config('laravel-permission.models.permission'),
            config('laravel-permission.table_names.role_has_permissions')
        );
    }
    
    public function users()
    {
        return $this->belongsToMany(
            config('auth.model') ?: config('auth.providers.users.model'),
            config('laravel-permission.table_names.user_has_roles')
        );
    }
    
    public static function findByName($name)
    {
        $role = static::where('name', $name)->first();

        if (! $role) {
            throw new RoleDoesNotExist();
        }

        return $role;
    }
    
    public function hasPermissionTo($permission)
    {
        if (is_string($permission)) {
            $permission = app(Permission::class)->findByName($permission);
        }

        return $this->permissions->contains('id', $permission->id);
    }
    
    public function getCreatedAtAttribute( $value ){
        return (new Carbon($value))->format('d/m/Y H:i:s');
    }
    
    public function setCreatedAtAttribute( $value ) {
        $fecha =  Carbon::createFromFormat('Y-m-d H:i:s', $value);
        $this->attributes['created_at'] = $fecha->format('d/m/Y H:i:s');
    }

    public function setUpdatedAtAttribute( $value ) {
        $fecha =  Carbon::createFromFormat('Y-m-d H:i:s', $value);
        $this->attributes['updated_at'] = $fecha->format('d/m/Y H:i:s');
    }

}
