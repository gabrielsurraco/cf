<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrazaBolson extends Model
{
    protected $table = 'rel_traza_bolsones';

    PUBLIC STATIC $ID               = "id";
    PUBLIC STATIC $FECHA_OPERACION  = "Fecha_operacion";
    PUBLIC STATIC $ID_BALANZA       = "_IDBalanza";
    PUBLIC STATIC $ID_PLANTA        = "_IDPlanta";
    PUBLIC STATIC $ID_REL_TRAZA_BOLSON  = "_IDRelTrazaBolson";
    PUBLIC STATIC $ID_BOLSON        = "_IDBolson";
    PUBLIC STATIC $ID_PESAJE        = "_IDPesaje";
    PUBLIC STATIC $ID_MOV_STOCK     = "_IDMovStock";
    PUBLIC STATIC $ID_DET_LOTE      = "_IDDetLote";

    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }
}
