<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateACADetLotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DETLOTE', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_IDBalanza')->default(0);
            $table->integer('_IDDetLote')->default(0);
            $table->integer('_IDProducto')->nullable();
            $table->integer('_IDLote')->nullable();
            $table->integer('Porcentaje')->nullable();
            $table->integer('Cant_Bolsas')->nullable();
            $table->integer('Kilos_Netos')->nullable();
            $table->string('_Creado_por')->nullable();
            $table->datetime('_Fecha_Creacion')->nullable();
            $table->string('_Modificado_por')->nullable();
            $table->datetime('_Fecha_Modificacion')->nullable();
            $table->string('_Observaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DETLOTE');
    }

    protected function getDateFormat(){
        return 'd/m/Y H:i:s';
    }
}
