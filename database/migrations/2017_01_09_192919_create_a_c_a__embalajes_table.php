<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateACAEmbalajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('EMBALAJES_AUX', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_IDBalanza');
            $table->integer('_IDEmbalaje');
            $table->string('Codigo')->nullable();
            $table->string('Descripcion')->nullable();
            $table->double('Peso',10,2)->nullable();
            $table->string('Obs')->nullable();
            $table->string('_Creado_por')->nullable();
            $table->datetime('_Fecha_Creacion')->nullable();
            $table->string('_Modificado_por')->nullable();
            $table->datetime('_Fecha_Modificacion')->nullable();
            $table->string('_Observaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('EMBALAJES_AUX');
    }
}
