<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateACALotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LOTES', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_IDBalanza')->default(0);
            $table->integer('_IDLote')->default(0);
            $table->string('Codigo')->nullable();
            $table->string('Descripcion')->nullable();
            $table->date('Fecha')->nullable();
            $table->integer('Cant_Bolsas')->nullable();
            $table->integer('Kilos_x_Bolsa')->nullable();
            $table->integer('Kilos_Neto')->nullable();
            $table->boolean('Certificado')->nullable();
            $table->boolean('Sincronizado')->nullable();
            $table->string('_Creado_por')->nullable();
            $table->datetime('_Fecha_Creacion')->nullable();
            $table->string('_Modificado_por')->nullable();
            $table->datetime('_Fecha_Modificacion')->nullable();
            $table->string('_Observaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('LOTES');
    }
}
