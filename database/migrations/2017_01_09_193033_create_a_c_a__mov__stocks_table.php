<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateACAMovStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MOV_STOCK', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_IDBalanza');
            $table->integer('_IDMovStock');
            $table->datetime('Fecha')->nullable();
            $table->datetime('Fecha_reproceso')->nullable();
            $table->integer('_IDProducto')->nullable();
            $table->string('Cod_Producto',10)->nullable();
            $table->double('Cantidad',18,2)->nullable();
            $table->boolean('E_S')->nullable();
            $table->integer('_IDMotivo')->nullable();
            $table->double('M3',18,2)->nullable();
            $table->integer('_IDPesaje')->nullable();
            $table->integer('_IDDetLote')->nullable();
            $table->boolean('Por_Balanza')->nullable();
            $table->string('_Creado_por')->nullable();
            $table->datetime('_Fecha_Creacion')->nullable();
            $table->string('_Modificado_por')->nullable();
            $table->datetime('_Fecha_Modificacion')->nullable();
            $table->string('_Observaciones')->nullable();
            $table->index('_IDBalanza');
            $table->index('_IDMovStock');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MOV_STOCK');
    }
}
