<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateACAPaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PAISES_AUX', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_IDBalanza');
            $table->integer('_IDPais');
            $table->string('Nombre')->nullable();
            $table->string('_Creado_por')->nullable();
            $table->datetime('_Fecha_Creacion')->nullable();
            $table->string('_Modificado_por')->nullable();
            $table->datetime('_Fecha_Modificacion')->nullable();
            $table->string('_Observaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PAISES_AUX');
    }
}
