<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateACAParametrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_STK_PARAMETROS', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_IDBalanza')->default(0);
            $table->string('Modulo')->nullable();
            $table->integer('Rie')->nullable();
            $table->integer('Dias_Rie')->nullable();
            $table->string('Path_Rx')->nullable();
            $table->integer('_IDTalonarioTicketTv')->nullable();
            $table->integer('_IDTalonarioTicket')->nullable();
            $table->integer('_IDTalonarioRemito')->nullable();
            $table->integer('_IDMotivoStock')->nullable();
            $table->integer('_IDMotivoLotes')->nullable();
            $table->string('Codigos_Productos')->nullable();
            $table->boolean('Muestra_Color')->nullable();
            $table->boolean('Muestra_Comprobante')->nullable();
            $table->string('Bolsas_x_Def')->nullable();
            $table->integer('Peso_Pallet')->nullable();
            $table->string('Taras_x_Bolsa')->nullable();
            $table->string('Te_Verde')->nullable();
            $table->string('Lenia')->nullable();
            $table->string('Clave')->nullable();
            $table->string('Clave_Pesajes')->nullable();
            $table->boolean('Nav_Pesajes')->nullable();
            $table->string('Lotes',1000)->nullable();
            $table->string('Info_Depto')->nullable();
            $table->boolean('Esvb6')->nullable();
            $table->boolean('Base_Compuesta')->nullable();
            $table->boolean('Modifica_Historico')->nullable();
            $table->string('_Creado_por')->nullable();
            $table->datetime('_Fecha_Creacion')->nullable();
            $table->string('_Modificado_por')->nullable();
            $table->datetime('_Fecha_Modificacion')->nullable();
            $table->string('_Observaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Parametros');
        Schema::dropIfExists('PARAMETROS');
        Schema::dropIfExists('_STK_PARAMETROS');

    }
}
