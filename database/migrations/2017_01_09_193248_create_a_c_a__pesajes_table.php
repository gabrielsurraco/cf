<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateACAPesajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PESAJES', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_IDBalanza')->default(0);
            $table->integer('_IDPesaje')->default(0);
            $table->integer('_IDTalonario')->nullable();
            $table->string('Nro_Comprobante')->nullable();
            $table->datetime('Fecha')->nullable();
            $table->integer('_IDRodado')->nullable();
            $table->string('Patente_Acoplado')->nullable();
            $table->string('Conductor')->nullable();
            $table->integer('_IDProducto')->nullable();
            $table->integer('_IDTipoPrecio')->nullable();
            $table->double('Precio',18,2)->nullable();
            $table->integer('_IDPlantaOrigen')->nullable();
            $table->integer('_IDPlantaDestino')->nullable();
            $table->integer('_IDProveedor')->nullable();
            $table->integer('Cod_Proveedor')->nullable();
            $table->integer('_IDAcopiador')->nullable();
            $table->boolean('Es_Acopiador')->nullable();
            $table->integer('Orden_Acopio')->nullable();
            $table->integer('VALREF')->nullable();
            $table->integer('_IDEmbalaje')->nullable();
            $table->integer('Cant_Embalaje')->nullable();
            $table->double('Peso_Emb_x_Unidad',18,2)->nullable();
            $table->integer('_IDTipoHumedad')->nullable();
            $table->integer('porc_humedad')->nullable();
            $table->integer('porc_palo')->nullable();
            $table->double('Peso_Palo',18,2)->nullable();
            $table->integer('M3')->nullable();
            $table->datetime('Fecha_Bruto')->nullable();
            $table->datetime('Fecha_Anulacion')->nullable();
            $table->string('Usuario_Anulacion')->nullable();
            $table->string('Motivo_Anulacion')->nullable();
            $table->double('Bruto',18,2)->nullable();
            $table->double('Tara',18,2)->nullable();
            $table->double('Total_Embalaje',18,2)->nullable();
            $table->double('Peso_Humedad',18,2)->nullable();
            $table->double('Neto',18,2)->nullable();
            $table->integer('Estado')->nullable();
            $table->integer('_IDChacra')->nullable();
            $table->integer('Cod_Chacra')->nullable();
            $table->boolean('Te_Certificado')->nullable();
            $table->integer('IDPESAJEBASEORIG')->nullable();
            $table->integer('IDPESAJEDUP')->nullable();
            $table->integer('IDMIGORIG')->nullable();
            $table->boolean('Migrado')->nullable();
            $table->boolean('Modificado')->nullable();
            $table->string('Lote_Chacra')->nullable();
            $table->string('Tamanio')->nullable();
            $table->string('Malezas')->nullable();
            $table->string('Calidad')->nullable();
            $table->string('Acaros')->nullable();
            $table->string('Temperatura')->nullable();

            $table->integer('Porc_Malla_30')->nullable();
            $table->boolean('Expo')->nullable();
            $table->string('Buque')->nullable();
            $table->string('Codigo_Aduana')->nullable();
            $table->string('Reserva')->nullable();
            $table->string('Codigo_Balanza')->nullable();
            $table->string('Nro_Certif_Hab')->nullable();
            $table->string('Vencimiento')->nullable();
            $table->string('Nro_Contenedor')->nullable();
            $table->string('Afip')->nullable();
            $table->string('Linea')->nullable();
            $table->string('Otros')->nullable();
            $table->string('Obs')->nullable();
            $table->string('Cuit_Ata')->nullable();
            $table->string('Apellido_Nombre_Ata')->nullable();
            $table->string('Nro_Permiso_Embarque')->nullable();

            $table->boolean('Pesajeweb')->nullable();
            $table->boolean('Sincronizado')->nullable();


            $table->string('_Creado_por')->nullable();
            $table->datetime('_Fecha_Creacion')->nullable();
            $table->string('_Modificado_por')->nullable();
            $table->datetime('_Fecha_Modificacion')->nullable();
            $table->string('_Observaciones')->nullable();
            $table->index('_IDBalanza');
            $table->index('_IDPesaje');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PESAJES');
    }
}
