<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateACAProcesamientoValesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PROCESAMIENTOVALES', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_IDBalanza')->default(0);
            $table->integer('_IDProcesamientoValesBalanza')->default(0);
            $table->integer('Nro_Lote')->nullable();
            $table->string('Ruta')->nullable();
            $table->integer('Tipo_proceso')->nullable();
            $table->integer('Operacion')->nullable();
            $table->integer('TipoArchivo')->nullable();
            $table->string('ArchivoComienzaCon')->nullable();
            $table->string('ArchivoFijo')->nullable();

            $table->string('_Creado_por')->nullable();
            $table->datetime('_Fecha_Creacion')->nullable();
            $table->string('_Modificado_por')->nullable();
            $table->datetime('_Fecha_Modificacion')->nullable();
            $table->string('_Observaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PROCESAMIENTOVALES');
    }
}
