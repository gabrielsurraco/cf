<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateACAProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PRODUCTOS', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_IDBalanza');
            $table->integer('_IDProducto');
            $table->string('Codigo')->nullable();
            $table->string('Descripcion')->nullable();
            $table->double('Cantidad',18,2)->nullable();
            $table->integer('M3')->nullable();
            $table->integer('Codigo_Balanza')->nullable();
            $table->integer('Porcentaje')->nullable();
            $table->boolean('Activo')->nullable();
            $table->integer('_IDCategoria')->nullable();
            $table->integer('Precio')->nullable();
            $table->date('Fecha_Saldo_Inicial')->nullable();
            $table->integer('Saldo_Inicial')->nullable();
            $table->integer('Stock_Maximo')->nullable();
            $table->integer('Stock_Minimo')->nullable();
            $table->string('Obs')->nullable();
            $table->string('_Creado_por')->nullable();
            $table->datetime('_Fecha_Creacion')->nullable();
            $table->string('_Modificado_por')->nullable();
            $table->datetime('_Fecha_Modificacion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PRODUCTOS');
    }
}
