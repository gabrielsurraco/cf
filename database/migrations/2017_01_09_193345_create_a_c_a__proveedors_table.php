<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateACAProveedorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PROVEEDORES', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_IDBalanza');
            $table->integer('_IDProveedor');
            $table->string('Codigo')->nullable();
            $table->string('Razon_Social')->nullable();
            $table->string('Domicilio')->nullable();
            $table->string('Localidad')->nullable();
            $table->string('CP')->nullable();
            $table->integer('_IDPais')->nullable();
            $table->integer('_IDProvincia')->nullable();
            $table->string('CUIT')->nullable();
            $table->string('Email')->nullable();
            $table->string('Web')->nullable();
            $table->string('Telefono')->nullable();
            $table->string('Fax')->nullable();
            $table->boolean('Es_Acopiador')->nullable();
            $table->integer('Hectarea')->nullable();
            $table->boolean('Activo')->nullable();
            $table->integer('ProvCF')->nullable();
            $table->boolean('Cosechero')->nullable();
            $table->boolean('Certificado')->nullable();
            $table->string('_Creado_por')->nullable();
            $table->datetime('_Fecha_Creacion')->nullable();
            $table->string('_Modificado_por')->nullable();
            $table->datetime('_Fecha_Modificacion')->nullable();
            $table->string('_Observaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PROVEEDORES');
    }
}
