<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateACARemitosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('REMITOS', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_IDBalanza')->default(0);
            $table->integer('_IDRemito');
            $table->integer('_IDTalonario')->nullable();
            $table->string('Nro_Comprobante')->nullable();
            $table->datetime('Fecha')->nullable();
            $table->integer('_IDCliente')->nullable();
            $table->string('Domicilio_Cliente')->nullable();
            $table->string('CUIT_Cliente')->nullable();
            $table->string('Vapor')->nullable();
            $table->string('Precintos')->nullable();
            $table->string('Permiso_Embarque')->nullable();
            $table->integer('_IDTransporte')->nullable();
            $table->string('Domicilio_Transporte')->nullable();
            $table->string('CUIT_Transporte')->nullable();
            $table->string('Marca_Camion')->nullable();
            $table->string('Patente')->nullable();
            $table->string('Conductor')->nullable();
            $table->string('Contenedor')->nullable();
            $table->string('Precinto_Maritimo')->nullable();
            $table->string('DGR')->nullable();
            $table->string('FDA')->nullable();
            $table->string('Marca_Permiso')->nullable();
            $table->string('Marca_CF')->nullable();
            $table->boolean('Exportado')->nullable();
            $table->datetime('Fecha_Exp')->nullable();
            $table->string('Hora_Exp')->nullable();
            $table->double('Valor_Declarado',18,2)->nullable();
            $table->string('Flete_Tratado')->nullable();
            $table->date('Fecha_Llegada')->nullable();
            $table->boolean('Mercado_Interno')->nullable();
            $table->date('Fecha_Anulacion')->nullable();
            $table->string('Usuario_Anulacion')->nullable();
            $table->string('Motivo_Anulacion')->nullable();
            $table->string('Obs')->nullable();
            $table->string('_Creado_por')->nullable();
            $table->datetime('_Fecha_Creacion')->nullable();
            $table->string('_Modificado_por')->nullable();
            $table->datetime('_Fecha_Modificacion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('REMITOS');
    }
}
