<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateACATipoComprobantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TIPOCOMPROBANTE', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_IDBalanza')->default(0);
            $table->integer('_IDTipoComprobante');
            $table->string('Descripcion')->nullable();
            $table->string('Total_a_emitir')->nullable();
            $table->string('Nro_Inicial')->nullable();
            $table->string('Nro_Final')->nullable();
            $table->string('Nro_actual')->nullable();
            $table->string('Archivo')->nullable();
            $table->integer('Copias')->nullable();
            $table->integer('CopiaDigital')->nullable();
            $table->integer('Tipo')->nullable();
            $table->boolean('Previsualizar')->nullable();
            $table->string('ArchivoExpo')->nullable();
            $table->string('_Creado_por')->nullable();
            $table->datetime('_Fecha_Creacion')->nullable();
            $table->string('_Modificado_por')->nullable();
            $table->datetime('_Fecha_Modificacion')->nullable();
            $table->string('_Observaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TIPOCOMPROBANTE');
    }
}
