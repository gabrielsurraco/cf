<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateACATipoPreciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TIPOPRECIO_AUX', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_IDBalanza');
            $table->integer('_IDTipoPrecio');
            $table->string('Codigo')->nullable();
            $table->string('Descripcion')->nullable();
            $table->integer('_IDCategoria')->nullable();
            $table->double('Precio',18,2)->nullable();
            $table->double('Precio_Acopiador',18,2)->nullable();
            $table->string('_Creado_por')->nullable();
            $table->datetime('_Fecha_Creacion')->nullable();
            $table->string('_Modificado_por')->nullable();
            $table->datetime('_Fecha_Modificacion')->nullable();
            $table->string('_Observaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TIPOPRECIO_AUX');
    }
}
