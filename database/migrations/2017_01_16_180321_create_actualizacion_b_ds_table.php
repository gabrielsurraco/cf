<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActualizacionBDsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('BALANZASTEMP', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('Descripcion')->nullable();
            $table->string('Ubicacion')->nullable();
            $table->integer('Nro_Punto_De_Venta');
            $table->string('Nro_FDA',15)->nullable();
            $table->string('Nomenclatura',4)->nullable();
            $table->string('Domicilio',50)->nullable();
            $table->string('Membrete',255)->nullable();

            $table->boolean('Estado')->nullable();
            $table->datetime('Fecha_Sincronizacion')->nullable();
            $table->string('Usuario_Sincronizacion')->nullable();
            $table->string('_Creado_por')->nullable();
            $table->datetime('_Fecha_Creacion')->nullable();
            $table->string('_Modificado_por')->nullable();
            $table->datetime('_Fecha_Modificacion')->nullable();
            $table->string('_Observaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('BALANZASTEMP');
    }
}
