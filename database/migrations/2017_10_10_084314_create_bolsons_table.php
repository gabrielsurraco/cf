<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBolsonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bolsones', function (Blueprint $table) {
            $table->increments('id');
            $table->date('Fecha')->nullable();
            $table->integer('_IDProducto')->nullable();
            $table->integer('_IDBolson')->nullable();
            $table->integer('_IDBalanza')->default(0);
            $table->float('Cantidad')->nullable();
            $table->float('M3')->nullable();
            $table->string('Bolson')->nullable();
            $table->string('Planta_origen')->nullable();
            $table->string('Densidad')->nullable();
            $table->string('Porc_fino')->nullable();
            $table->string('Capataz')->nullable();
            $table->integer('AnioZafra')->nullable();
            $table->string('Impresora')->nullable();
            $table->integer('Manual')->nullable();
            $table->integer('_IDPesajeOrigen')->nullable();
            $table->string('NroTicketOrigen')->nullable();
            $table->boolean('Sincronizado')->nullable();
            $table->string('Nro_Lote')->nullable();
            $table->string('_Creado_por')->nullable();
            $table->datetime('_Fecha_Creacion')->nullable();
            $table->string('_Modificado_por')->nullable();
            $table->datetime('_Fecha_Modificacion')->nullable();
            $table->string('_Observaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bolsones');
    }
}
