<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrazaBolsonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rel_traza_bolsones', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('Fecha_operacion');
            $table->integer('_IDBalanza');
            $table->integer('_IDPlanta');
            $table->integer('_IDRelTrazaBolson');
            $table->integer('_IDBolson');
            $table->integer('_IDPesaje');
            $table->integer('_IDMovStock');
            $table->integer('_IDDetLote');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rel_traza_bolsones');
    }
}
