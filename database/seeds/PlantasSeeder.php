<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Planta;
use Illuminate\Support\Facades\DB;



class PlantasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Planta::create([
            '_IDBalanza'             => 9,
            'Descripcion'           => 'TABAY',
            'Nomenclatura'          => 'TA'
        ]);
        
        Planta::create([
            '_IDBalanza'             => 46,
            'Descripcion'           => 'OKULOVICH',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 48,
            'Descripcion'           => 'BONZAI',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 49,
            'Descripcion'           => 'ROGACHESKY',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 50,
            'Descripcion'           => 'DIGA',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 51,
            'Descripcion'           => 'KLEYUK',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 52,
            'Descripcion'           => 'COOP.R.MONTOYA',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 53,
            'Descripcion'           => 'LENZ',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 54,
            'Descripcion'           => 'COPETEGLIA',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 55,
            'Descripcion'           => 'BENITEZ',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 56,
            'Descripcion'           => 'KWASZKA',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 57,
            'Descripcion'           => 'MUZICA',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 58,
            'Descripcion'           => 'CORREA',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 59,
            'Descripcion'           => 'BEKIS',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 60,
            'Descripcion'           => 'HEIMLIZ',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 61,
            'Descripcion'           => 'HOLC',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 62,
            'Descripcion'           => 'LEUSTAK',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 63,
            'Descripcion'           => 'MENDEZ',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 64,
            'Descripcion'           => 'LASCHUK',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 65,
            'Descripcion'           => 'TEA EL ALCAZAR',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 66,
            'Descripcion'           => 'NICONHOY',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 67,
            'Descripcion'           => 'MYSLIWCZUK',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 68,
            'Descripcion'           => 'EL TREBOL SRL',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 69,
            'Descripcion'           => 'GUTKOSKI',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 70,
            'Descripcion'           => 'SCHUNKE',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 71,
            'Descripcion'           => 'EXA',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 72,
            'Descripcion'           => 'RYWAKA',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 73,
            'Descripcion'           => 'CHIRINIUK',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 74,
            'Descripcion'           => 'CALEFACTORES',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 75,
            'Descripcion'           => 'CHACRA',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 76,
            'Descripcion'           => 'DIÑA',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 86,
            'Descripcion'           => 'OBERA/TIPIFICA.',
            'Nomenclatura'          => 'OBTO'
        ]);
        
        Planta::create([
            '_IDBalanza'             => 87,
            'Descripcion'           => 'OBERA/SECADERO',
            'Nomenclatura'          => 'OBSO'
        ]);
        
        Planta::create([
            '_IDBalanza'             => 88,
            'Descripcion'           => 'CAMPO GRANDE',
            'Nomenclatura'          => 'CG'
        ]);
        
        Planta::create([
            '_IDBalanza'             => 89,
            'Descripcion'           => 'CAMPO VIERA',
            'Nomenclatura'          => 'CV'
        ]);
        
        Planta::create([
            '_IDBalanza'             => 90,
            'Descripcion'           => 'CV MOLINO',
            'Nomenclatura'          => 'CVM'
        ]);
        
        Planta::create([
            '_IDBalanza'             => 91,
            'Descripcion'           => 'CHACRA ACARAGUA',
            'Nomenclatura'          => 'CA'
        ]);
        
        Planta::create([
            '_IDBalanza'             => 97,
            'Descripcion'           => 'OBERA/ITALIA',
            'Nomenclatura'          => 'OBIT'
        ]);
        
        Planta::create([
            '_IDBalanza'             => 98,
            'Descripcion'           => 'OBERA/MOL.YERBA',
            'Nomenclatura'          => 'OBMY'
        ]);
        
        Planta::create([
            '_IDBalanza'             => 99,
            'Descripcion'           => 'CAMPO VIERA II',
            'Nomenclatura'          => 'CVII'
        ]);
        
        Planta::create([
            '_IDBalanza'             => 100,
            'Descripcion'           => 'ACOPIO 2 DE MAYO',
            'Nomenclatura'          => 'A2M'
        ]);
        
        Planta::create([
            '_IDBalanza'             => 101,
            'Descripcion'           => 'LONATI EDUARDO',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 102,
            'Descripcion'           => 'CALCOL',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 103,
            'Descripcion'           => 'SAN ESTEBAN',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 104,
            'Descripcion'           => 'LEZCZINSKI',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 105,
            'Descripcion'           => 'INFUSIONES NATURALES',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 106,
            'Descripcion'           => 'AGROTEALES',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 107,
            'Descripcion'           => 'CAMBIASO HNOS (CHILE)',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 108,
            'Descripcion'           => 'CORPORA TRESMONTES',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 109,
            'Descripcion'           => 'PUERTO BSAS',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 110,
            'Descripcion'           => 'KOST SA',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 111,
            'Descripcion'           => 'LA VIRGINIA',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 112,
            'Descripcion'           => 'CAPELL FIDEICOMISO',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 113,
            'Descripcion'           => 'URUGUAY',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 114,
            'Descripcion'           => 'VISUAR SA',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 116,
            'Descripcion'           => 'ARGENTE',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 118,
            'Descripcion'           => 'COOP. PICADA LIBERTAD',
            'Nomenclatura'          => 'CPL'
        ]);
        
        Planta::create([
            '_IDBalanza'             => 120,
            'Descripcion'           => 'DEPOSITO COSTA RICA',
            'Nomenclatura'          => 'DCR'
        ]);
        
        Planta::create([
            '_IDBalanza'             => 121,
            'Descripcion'           => 'DEPOSITO CG EXTERNO',
            'Nomenclatura'          => 'DCG'
        ]);
        
        Planta::create([
            '_IDBalanza'             => 122,
            'Descripcion'           => 'DEPOSITO CHAPA',
            'Nomenclatura'          => 'DCH'
        ]);
        
        Planta::create([
            '_IDBalanza'             => 124,
            'Descripcion'           => 'DEPOSITO BELTRAME',
            'Nomenclatura'          => 'DB'
        ]);
        
        Planta::create([
            '_IDBalanza'             => 125,
            'Descripcion'           => 'DON MAXIMO SRL',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 126,
            'Descripcion'           => 'ESTABLECIMIENTO RUTA 210',
        ]);
        
        Planta::create([
            '_IDBalanza'             => 128,
            'Descripcion'           => 'RUTA 5 PIETROCZUK',
        ]);
        
        
        
    }
}
