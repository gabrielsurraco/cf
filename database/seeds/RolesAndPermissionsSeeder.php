<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;
use App\User;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        //ABM USUARIOS
        Permission::create(['name' => 'CREAR_USUARIOS']);
        Permission::create(['name' => 'EDITAR_USUARIOS']);
        Permission::create(['name' => 'ELIMINAR_USUARIOS']);
        
        //PESAJES
        Permission::create(['name' => 'MODIFICA_PESAJE']);
        Permission::create(['name' => 'ANULA_PESAJE']);
        
        // ROL ADMINISTRADOR y sus PERMISOS (TODOS)
        $role = Role::create(['name' => 'ADMINISTRADOR']);
        $role->givePermissionTo('CREAR_USUARIOS');
        $role->givePermissionTo('EDITAR_USUARIOS');
        $role->givePermissionTo('ELIMINAR_USUARIOS');
        $role->givePermissionTo('MODIFICA_PESAJE');
        $role->givePermissionTo('ANULA_PESAJE');
        
        //ROL DE CAMPO VIERA
        $role = Role::create(['name' => 'PESADOR_CV']);
        
        //ROL DE CAMPO GRANDE
        $role = Role::create(['name' => 'PESADOR_CG']);
        
        //ROL DE OBERA
        $role = Role::create(['name' => 'PESADOR_OBERA']);
        
        //ROL DE TABAY
        $role = Role::create(['name' => 'PESADOR_TABAY']);
        
        //ROL DE ACARAGUA
        $role = Role::create(['name' => 'PESADOR_ACARAGUA']);
        
        $user = User::first();
        $user->assignRole('ADMINISTRADOR');

    }
}
