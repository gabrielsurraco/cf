<?php

use Illuminate\Database\Seeder;

use App\User;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'      => 'Gabriel Surraco',
            'email'     => 'gabriel.surraco@casafuentes.com.ar',
//            'tipo_usuario' => 'administrador',
//            'planta'    => 'OBSO',
            'password'  => bcrypt('admin'),
        ]);
    }
}
