$(function () {
    
    var datosGraf;
    
    $.ajax({
        type: "POST",
        url: "/home/datosgraficos",
        data: {_token : $("meta[name='csrf-token']").attr("content")}, 
        success: function(data){
            var lineData = {
                labels: ["January", "February", "March", "April", "May", "June", "July","Agosto","Septiembre","Octubre","Noviembre","Diciembre","January", "February", "March", "April", "May", "June", "July","Agosto","Septiembre","Octubre","Noviembre","Diciembre","January", "February", "March", "April", "May", "June", "July","Agosto","Septiembre","Octubre","Noviembre","Diciembre","January", "February"],
                datasets: [
                    {
                        label: "Example dataset",
                        fillColor: "rgba(220,220,220,0.5)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: JSON.parse(data)[0]
                    },
                    {
                        label: "Example dataset",
                        fillColor: "rgba(26,179,148,0.5)",
                        strokeColor: "rgba(26,179,148,0.7)",
                        pointColor: "rgba(26,179,148,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(26,179,148,1)",
                        data: JSON.parse(data)[1]
                    }
                ]
            };

            var lineOptions = {
                scaleShowGridLines: true,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                bezierCurve: true,
                bezierCurveTension: 0.4,
                pointDot: true,
                pointDotRadius: 3,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 20,
                datasetStroke: true,
                datasetStrokeWidth: 2,
                datasetFill: true,
                responsive: true,
            };


            var ctx = document.getElementById("lineChart").getContext("2d");
            var myNewChart = new Chart(ctx).Line(lineData, lineOptions);
//            console.log(data);


        },
        error: function(){}
    });
    
//    console.log(datosGraf.toString());

    

    

});