@extends('layouts.backend')

@section('css')
<link href="{{ asset('backend/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('backend/css/plugins/dataTables/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('backend/css/plugins/dataTables/dataTables.responsive.css') }}" rel="stylesheet">
<link href="{{ asset('backend/css/plugins/dataTables/dataTables.tableTools.min.css') }}" rel="stylesheet">

<link href="{{ asset('backend/css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" media="screen">
<link href="{{ asset('backend/css/plugins/datetimepicker/bootstrap-datetimepicker.css') }}" rel="stylesheet" media="screen">

<!-- Select2 -->
<link href="{{ asset('backend/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('backend/css/plugins/select2/select2-bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="row wrapper border-bottom white-bg ">
<!--    <div class="col-lg-12 m-t-md m-b-sm">
        <a class="btn btn-sm btn-primary" href="/balanza/nuevo-pesaje"> <i class="fa fa-plus-circle"></i> Nuevo Pesaje </a>
    </div>-->
    <div class="col-lg-12 m-t-sm">
        <div class="clients-list">
            <ul class="nav nav-tabs">
                <span class="pull-right small text-muted"><a class="btn btn-sm btn-primary" href="/balanza/nuevo-pesaje"> <i class="fa fa-plus-circle"></i> Nuevo Pesaje </a></span>
                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"><i class="fa fa-balance-scale"></i> Pesajes</a></li>
                <li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="false"><i class="fa fa-clock-o"></i> Pendientes de Ingreso</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="full-height-scroll" style="overflow: hidden; width: auto; height: 100%;margin-bottom: 25px;">
                            <h4 class="m-t-md m-b-md">Formulario de Búsqueda</h4>
                            <div class="col-md-12">
                                <div class="col-md-2 form-group">
                                    {{ Form::label('fecha_desde', 'Fecha Desde') }}
                                    {{ Form::text("fecha_desde", null, array("class" => "form-control fecha", "placeholder" => "dd/mm/yyyy" )) }}
                                </div>
                                <div class="col-md-2 form-group">
                                    {{ Form::label('fecha_hasta', 'Fecha Hasta') }}
                                    {{ Form::text("fecha_hasta", null, array("class" => "form-control fecha", "placeholder" => "dd/mm/yyyy" )) }}
                                </div>
                                <div class="col-md-3 form-group">
                                    {{ Form::label('producto', 'Producto') }}
                                    <div class="input-group">
                                        <span class="input-group-addon "><i class="fa fa-leaf"></i></span>
                                        <select name="producto" class="form-control producto" style="width: 100%;">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 form-group">
                                    {{ Form::label('planta', 'Planta Pesaje') }}
                                    <div class="input-group">
                                        <span class="input-group-addon "><i class="fa fa-industry"></i></span>
                                        <select name="planta" class="form-control planta" style="width: 100%;">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 form-group">
                                    {{ Form::label('proveedor', 'Proveedor') }}
                                    <div class="input-group">
                                        <span class="input-group-addon "><i class="fa fa-cubes"></i></span>
                                        <select name="proveedor" class="form-control proveedor" style="width: 100%;">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 form-group">
                                    {{ Form::label('origen', 'Origen') }}
                                    <div class="input-group">
                                        <span class="input-group-addon "><i class="fa fa-arrow-down"></i></span>
                                        <select name="origen" class="form-control planta" style="width: 100%;">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-3 form-group">
                                    {{ Form::label('destino', 'Destino') }}
                                    <div class="input-group">
                                        <span class="input-group-addon "><i class="fa fa-arrow-right"></i></span>
                                        <select name="destino" class="form-control planta" style="width: 100%;">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-3 form-group">
                                    {{ Form::label('camion', 'Camión') }}
                                    <div class="input-group">
                                        <span class="input-group-addon "><i class="fa fa-truck"></i></span>
                                        <select name="camion" class="form-control camion" style="width: 100%;">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-3 form-group">
                                    {{ Form::label('chacra', 'Chacra') }}
                                    {{ Form::select("chacra",array("" => "") ,null, array("class" => "form-control chacra" ,"style" => "width: 100%;")) }}
                                </div>
                                
                                <div class="col-md-3 form-group">
                                    {{ Form::label('certificado', 'Certificado') }}
                                    {{ Form::select("certificado",array("default" => "Seleccionar...","si" => "Si", "no" => "No") ,null, array("class" => "form-control" ,"style" => "width: 100%;")) }}
                                </div>
                                
                            </div>
                            <div style="border-bottom: 1px solid #999;">
                                <h4 class="m-t-md m-b-md">Visualización de los datos</h4>
                            </div>
                            <table  class="table table-bordered table-striped" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Planta</th>
                                        <th>Camion</th>
                                        <th>Proveedor</th>
                                        <th>Producto</th>
                                        <th>Neto</th>
                                        <th>Origen</th>
                                        <th>Destino</th>
                                        <th>Estado</th>
                                        <th>Accion</th>
                                    </tr>
                                </thead>
                                <tbody class="body_incompletos">
                                    @foreach($pesajes as $pesaje)
                                    <tr>
                                        <td>{{ $pesaje->fecha }}</td>
                                        <td>{{ $pesaje->id_bd }}</td>
                                        <td>{{ $pesaje->camion->codigo }}</td>
                                        <td>{{ (new \App\Helper)->getProveedorName($pesaje->id_proveedor) }}</td>
                                        <td>{{ (new \App\Helper)->getProductoName($pesaje->id_producto) }}</td>
                                        <td>{{ $pesaje->neto }}</td>
                                        <td>{{ (new \App\Helper)->getPlantaName($pesaje->id_planta_origen) }}</td>
                                        <td>{{ (new \App\Helper)->getPlantaName($pesaje->id_planta_destino) }}</td>
                                        <td><?php if($pesaje->nro_comprobante != ""){ echo $pesaje->nro_comprobante . " (Completo)"; }else{ echo "<label class='label label-danger'>Incompleto</label>"; } ?></td>
                                        <td><a class='btn btn-success btn-xs'><i class='fa fa-edit'></i></a>  <a class='btn btn-danger btn-xs'><i class='fa fa-trash'></i></a></td></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                   <tr>
                                        <th>Fecha</th>
                                        <th>Planta</th>
                                        <th>Camion</th>
                                        <th>Proveedor</th>
                                        <th>Producto</th>
                                        <th>Neto</th>
                                        <th>Origen</th>
                                        <th>Destino</th>
                                        <th>Estado</th>
                                        <th>Accion</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div style="text-align: center;" class="m-b-md">
                                {{ $pesajes->links() }}
                            </div>
                        </div>
                </div>
                
                <div id="tab-3" class="tab-pane">
                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
                        <div class="full-height-scroll" style="overflow: hidden; width: auto; height: 100%;">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <th>Fecha Envío</th>
                                    <th>Planta Pesaje</th>
                                    <th>Camión</th>
                                    <th>Producto</th>
                                    <th>Neto</th>
                                    <th>Origen</th>
                                    <th>Destino</th>
                                    <th>Estado</th>
                                    <th>Acción</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>10/12/2017</td>
                                            <td>CV </td>
                                            <td>AXC-456</td>
                                            <td>Malla 100</td>
                                            <td>2500</td>
                                            <td>ACA</td>
                                            <td>CV</td>
                                            <td class="client-status"><span class="label label-warning">Pendiente</span></td>
                                            <td><a class="btn btn-xs btn-success"><i class="fa fa-hand-o-up"></i> Dar ingreso</a></td>
                                        </tr>
                                        <tr>
                                            <td>10/12/2017</td>
                                            <td>CV </td>
                                            <td>AXC-456</td>
                                            <td>Boca 5</td>
                                            <td>3500</td>
                                            <td>CG</td>
                                            <td>CV</td>
                                            <td class="client-status"><span class="label label-warning">Pendiente</span></td>
                                            <td><a class="btn btn-xs btn-success"><i class="fa fa-hand-o-up"></i> Dar ingreso</a></td>
                                        </tr>
                                        <tr>
                                            <td>10/12/2017</td>
                                            <td>CVM </td>
                                            <td>AXC-456</td>
                                            <td>Malla 20</td>
                                            <td>3500</td>
                                            <td>Tabay</td>
                                            <td>CVM</td>
                                            <td class="client-status"><span class="label label-warning">Pendiente</span></td>
                                            <td><a class="btn btn-xs btn-success"><i class="fa fa-hand-o-up"></i> Dar ingreso</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@section('javascript')
<!-- Data picker -->
<script src="{{ asset('backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<script type="text/javascript" src="{{ asset('backend/js/plugins/datetimepicker/moment.min.js') }}" charset="UTF-8"></script>
<script type="text/javascript" src="{{ asset('backend/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}" charset="UTF-8"></script>

<!-- Data Tables -->
<script src="{{ asset('backend/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('backend/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('backend/js/plugins/dataTables/dataTables.responsive.js') }}"></script>
<script src="{{ asset('backend/js/plugins/dataTables/dataTables.tableTools.min.js') }}"></script>
<script src="{{ asset('backend/js/plugins/select2/select2.min.js') }} "></script>

<script type="text/javascript">
    
    
    var dateNow = new Date();
    $('.fecha').datetimepicker({
        locale: 'es',
        defaultDate: dateNow
    });
    
    $(".producto").select2({
        theme: "bootstrap",
        ajax: {
          url: '{{ url("api/get-producto/") }}',
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term,
              page: params.page,
            };
          },
          processResults: function (data, params) {
            return {
              results: data.results,
              pagination: {
                more: data.pagination.more
              }
            };
          },
          cache: true
        },
        placeholder: "Seleccione el Producto...",
        allowClear: true
    });
    
    $(".planta").select2({
        theme: "bootstrap",
        ajax: {
          url: '{{ url("api/get-planta/") }}',
          dataType: 'json',
          delay: 250,
          data: function (params) {
            var planta = $(".planta").val(); 
            if(planta == "default"){
                alert("Planta...");
            }
            return {
              q: params.term, // search term
              page: params.page,
              planta: planta
            };
          },
          processResults: function (data, params) {
            params.page = params.page || 1;

            return {
              results: data.items,
              pagination: {
                more: (params.page * 30) < data.total_count
              }
            };
          },
          cache: true
        },
        placeholder: "Seleccionar...",
        allowClear: true
    });
    
    $(".proveedor").select2({
        theme: "bootstrap",
        ajax: {
          url: '{{ url("api/get-proveedor/") }}',
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term,
              page: params.page,
            };
          },
          processResults: function (data, params) {
            return {
              results: data.results,
              pagination: {
                more: data.pagination.more
              }
            };
          },
          cache: true
        },
        placeholder: "Proveedor...",
        allowClear: true
    });
    
    $(".camion").select2({
        theme: "bootstrap",
        ajax: {
          url: '{{ url("api/get-camion/") }}',
          dataType: 'json',
          delay: 250,
          data: function (params) {
            var planta = $(".planta").val(); 
            if(planta == "default"){
                alert("Seleccione Planta de Pesaje");
            }
            return {
              q: params.term,
              page: params.page,
              planta: planta
            };
          },
          processResults: function (data, params) {
            return {
              results: data.results,
              pagination: {
                more: data.pagination.more
              }
            };
          },
          cache: true
        },
        placeholder: "Seleccione la Patente...",
        allowClear: true
    });
    
    $(".chacra").select2({
        theme: "bootstrap",
        ajax: {
          url: '{{ url("api/get-chacra/") }}',
          dataType: 'json',
          delay: 250,
          data: function (params) {
            var planta = $(".planta").val(); 
            if(planta == "default"){
                alert("Seleccione Planta de Pesaje");
            }
            return {
              q: params.term, // search term
              page: params.page,
              planta: planta
            };
          },
          processResults: function (data, params) {
            params.page = params.page || 1;

            return {
              results: data.items,
              pagination: {
                more: (params.page * 30) < data.total_count
              }
            };
          },
          cache: true
        },
        placeholder: "Seleccione la Chacra...",
        allowClear: true
    });
    
    
    //AGREGO HASHCHANGE POR SI PRESIONA LAS FLECHAS DEL NAVEGADOR
    $(window).on('hashchange', function() {
        if (window.location.hash) {
            var page = window.location.hash.replace('#?page=', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            } else {
                getPesajes(page);
            }
        }
    });
    
    //CONTROLO ACCIÓN DEL LINK A PAGINAR
    $(document).ready(function() {
        $(document).on('click', '.pagination a', function (e) {
            getPesajes($(this).attr('href').split('page=')[1]);
            e.preventDefault();
        });
    });
    
    //
    function getPesajes(page) {
        $.ajax({
            url : '?page=' + page,
            dataType: 'json',
        }).done(function (data) {
            location.hash = "?page="+page;
            $('.body_incompletos').empty();
            
            //RELLENO EL BODY DE LA TABLA
            $.each(data.data, function(key, value){
                var estado;
                
                if(value.nro_comprobante != ""){
                    estado = value.nro_comprobante + " (Completo)";
                }else{
                    estado = "<label class='label label-danger'>Incompleto</label>";
                }
                
                var tr = $("<tr><td>"+value.fecha+"</td><td>"+value.id_bd+"</td><td>"+ value.id_rodado.codigo+"</td><td>"+value.id_proveedor.nombre+"</td><td>"+value.id_producto.nombre+"</td><td>"+value.neto+"</td><td>"+value.id_planta_origen.nombre+"</td><td>"+value.id_planta_destino.nombre+"</td><td>"+estado+"</td><td><a class='btn btn-success btn-xs'><i class='fa fa-edit'></i></a>  <a class='btn btn-danger btn-xs'><i class='fa fa-trash'></i></a></td></tr>");
                $('.body_incompletos').append(tr);
            });
            
            //LIMPIO LOS LINKS DEL PAGINADO
            $("ul.pagination").empty();
            
            //PARA IR AL INICIO
            var al_inicio;
            if(data.current_page == 1)
                al_inicio = $("<li class='disabled'><a href=''><span> < </span></a></li>");
            else
                al_inicio = $("<li><a href='http://192.168.3.14:8080/balanza?page=1'><span> < </span></a></li>");
            
            $("ul.pagination").append(al_inicio);
            
            //PARA IR A LA PÁGINA ANTERIOR
            var beforePage;
            if(data.current_page > 1)
                beforePage = $("<li><a href='"+data.prev_page_url+"'><span>&laquo;</span></a></li>");
            else
                beforePage = $("<li class='disabled'><span>&laquo;</span></li>");
            
            $("ul.pagination").append(beforePage);
            
            //AGREGO ENLACES DE PAGINADO
            var currentPage = data.current_page;
            var lastPage = data.last_page;
            for(var cont = 0; cont < 10; cont++){
                if(currentPage <= lastPage){
                    if(cont == 0)
                        li =  $("<li class='active'><a href='http://192.168.3.14:8080/balanza?page="+currentPage+"'>"+currentPage+"</a></li>");
                    else
                        li =  $("<li><a href='http://192.168.3.14:8080/balanza?page="+currentPage+"'>"+currentPage+"</a></li>");
                    
                    $("ul.pagination").append(li);
                }
                currentPage++;
            }
            
            //PARA IR A LA PÁGINA SIGUIENTE
            var afterPage;
            if(data.current_page == data.last_page)
                afterPage = $("<li href='disabled' ><span>&raquo;</span></li>");
            else
                afterPage = $("<li ><a href='"+data.next_page_url+"'><span>&raquo;</span></a></li>");
            
            $("ul.pagination").append(afterPage);
            
            //PARA IR A LA ÚLTIMA PÁGINA
            var al_final;
            if(data.current_page == data.last_page)
                al_final= $("<li class='disabled'><a href=''><span> < </span></a></li>");
            else
                al_final = $("<li><a href='http://192.168.3.14:8080/balanza?page="+data.last_page+"'><span> > </span></a></li>");
            
            $("ul.pagination").append(al_final);
            
        }).fail(function () {
            alert('Posts could not be loaded.');
        });
    }
    
    
    $(document).ready(function(){
        
        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: 'dd/mm/yyyy',
        });
        
        $('.dataTables-example').dataTable({
            responsive: true,
        });
        
    });
    
</script>

@stop



@endsection
