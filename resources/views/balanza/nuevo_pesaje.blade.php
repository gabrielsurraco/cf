@extends('layouts.backend')

@section('css')
<link href="{{ asset('backend/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('backend/css/plugins/dataTables/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('backend/css/plugins/dataTables/dataTables.responsive.css') }}" rel="stylesheet">
<link href="{{ asset('backend/css/plugins/dataTables/dataTables.tableTools.min.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('backend/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('backend/css/plugins/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('backend/css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" media="screen">
<link href="{{ asset('backend/css/plugins/datetimepicker/bootstrap-datetimepicker.css') }}" rel="stylesheet" media="screen">
<style type="text/css">
    .ocultar-si-no-es-tv{
        display: none;
    }
</style>
  
@stop

@section('content')


    
<div class="row wrapper border-bottom white-bg ">
    
    

{{ Form::open(['url' => '#', 'method' => 'post', 'id' => 'form']) }}
<div class="m-t-sm">
    {{ csrf_field() }}
    
    @if(Session::has('message'))
    <div class="alert alert-danger ">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ Session::get('message') }}
    </div>
    @endif
    
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                    <div class="col-md-4">
                        {{ Form::select("planta",array("default" => "Seleccione Planta de Pesaje...") + $factories ,null, array("class" => "form-control planta" )) }}
                        @if ($errors->has('planta'))
                            <span class="help-block text-warning" >
                                <strong>
                                    {{ $errors->first('planta') }}
                                </strong>
                            </span>
                        @endif
                    </div>
<!--                <div class="col-md-8" >
                    <h3 class="pull-right"> -0001267</h3>
                    <h3 class="pull-right ">Nro. Ticket: &nbsp;&nbsp;&nbsp;</h3>
                </div>-->
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6" >
                    <div class="form-group col-md-6">
                        {{ Form::label('fecha', 'Fecha') }}
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" name="fecha" class="form-control form_datetime">
                        </div>
                            <span class="help-block text-warning fecha_mensaje hidden" >
                                <strong>
                                </strong>
                            </span>
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('camion', 'Camión') }}
                        
                        <div class="input-group">
                            <span class="input-group-addon "><i class="fa fa-truck"></i></span>
                            <select name="camion" class="form-control camion" style="width: 100%;">
                                @if($errors->count() > 0)
                                    @if(Session::has('rodados'))
                                        <option value="{{ Session::get('rodados')[0]['id'] }}" selected="">{{ Session::get('rodados')[0]["text"] }}</option>
                                    @else
                                        <option value=""></option>
                                    @endif
                                @endif
                            </select>
                        </div>
                        <span class="help-block text-warning camion_mensaje hidden" >
                                <strong>
                                </strong>
                        </span>
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('pat_acoplado', 'Patente Acoplado') }}
                        {{ Form::text("pat_acoplado", null, array("class" => "form-control", "placeholder" => "Ingrese Valor..." )) }}
                    </div>
                    
                    <div class="form-group col-md-6">
                        {{ Form::label('conductor', 'Conductor') }}
                        
                        <div class="input-group">
                            <span class="input-group-addon "><i class="fa fa-user-circle-o"></i></span>
                            {{ Form::text("conductor", null, array("class" => "form-control", "placeholder" => "Ingrese Valor..." )) }}
                        </div>
                    </div>
                    
                    
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="form-group col-md-6">
                            {{ Form::label('producto', 'Producto') }}
                            
                            <div class="input-group">
                                <span class="input-group-addon "><i class="fa fa-leaf"></i></span>
                                <select name="producto" class="form-control producto" style="width: 100%;">
                                    @if($errors->count() > 0)
                                        @if(Session::has('productos'))
                                            <option value="{{ Session::get('productos')[0]['id'] }}" selected="">{{ Session::get('productos')[0]["text"] }}</option>
                                        @else
                                            <option value=""></option>
                                        @endif
                                    @endif
                                </select>
                            </div>
                            <span class="help-block text-warning producto_mensaje hidden" >
                                <strong>
                                </strong>
                            </span>
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('proveedor', 'Proveedor') }}
                            <div class="input-group">
                                <span class="input-group-addon "><i class="fa fa-cubes"></i></span>
                                <select name="proveedor" class="form-control proveedor" style="width: 100%;">
                                    @if($errors->count() > 0)
                                        @if(Session::has('proveedores'))
                                            <option value="{{ Session::get('proveedores')[0]['id'] }}" selected="">{{ Session::get('proveedores')[0]["text"] }}</option>
                                        @else
                                            <option value=""></option>
                                        @endif
                                    @endif
                                </select>
                            </div>
                            <span class="help-block text-warning proveedor_mensaje hidden" >
                                <strong>
                                </strong>
                            </span>
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('planta_origen', 'Planta Origen') }}
                            <div class="input-group">
                                <span class="input-group-addon "><i class="fa fa-level-down"></i></span>
                                <select name="planta_origen" class="form-control planta_origen" style="width: 100%;">
                                    @if($errors->count() > 0)
                                        @if(Session::has('planta_origen'))
                                            <option value="{{ Session::get('planta_origen')[0]['id'] }}" selected="">{{ Session::get('planta_origen')[0]["text"] }}</option>
                                        @else
                                            <option value=""></option>
                                        @endif
                                    @endif
                                </select>
                            </div>
                            
                            <span class="help-block text-warning planta_origen_mensaje" >
                                <strong>
                                </strong>
                            </span>
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('planta_destino', 'Planta Destino') }}
                            <div class="input-group">
                                <span class="input-group-addon "><i class="fa fa-level-up"></i></span>
                                <select name="planta_destino" class="form-control planta_origen" style="width: 100%;">
                                    @if($errors->count() > 0)
                                        @if(Session::has('planta_destino'))
                                            <option value="{{ Session::get('planta_destino')[0]['id'] }}" selected="">{{ Session::get('planta_destino')[0]["text"] }}</option>
                                        @else
                                            <option value=""></option>
                                        @endif
                                    @endif
                                </select>
                            </div>
                            
                            <span class="help-block text-warning planta_destino_mensaje hidden" >
                                <strong>
                                </strong>
                            </span>
                        </div>
                        <div class="ocultar-si-no-es-tv">
                            <div class="form-group col-md-6">
                                {{ Form::label('chacra', 'Chacra') }}
                                {{ Form::select("chacra",array("" => "") ,null, array("class" => "form-control chacra" ,"style" => "width: 100%;")) }}
                                <span class="help-block text-warning chacra_mensaje hidden" >
                                    <strong>
                                    </strong>
                                </span>
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('lote', 'Lote') }}
                                {{ Form::text("lote", null, array("class" => "form-control" )) }}
                                <span class="help-block text-warning lote_mensaje hidden" >
                                    <strong>
                                    </strong>
                                </span>
                            </div>
                            
                        </div>
                        <div class="form-group col-md-6" >
                            {{ Form::label('embalaje_tipo', 'Embalaje') }}
                            <select  name="embalaje_tipo" class="form-control embalaje_tipo" style="width: 100%;">
                                @if($errors->count() > 0)
                                    @if(Session::has('embalaje_tipo'))
                                        <option peso="{{ round(Session::get('embalaje_tipo')[0]['peso'], 2) }}" value="{{ Session::get('embalaje_tipo')[0]['id'] }}" selected="">{{ Session::get('embalaje_tipo')[0]["descripcion"] . ' - ' . round(Session::get('embalaje_tipo')[0]["peso"],2) . ' Kg.' }}</option>
                                    @else
                                        <option value="default">Seleccionar...</option>
                                    @endif
                                @endif
                                
                            </select>
                            <span class="help-block text-warning embalaje_tipo_mensaje hidden" >
                                <strong>
                                </strong>
                            </span>
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('cantidad', 'Cantidad') }}
                            {{ Form::text("cantidad",null, array("class" => "form-control cantidad", "placeholder" => "0" )) }}
                            <span class="help-block text-warning cantidad_mensaje hidden" >
                                <strong>
                                </strong>
                            </span>
                        </div>
                        
                        
                    </div>
                    
                    
                    
                </div>
                <div class="ocultar-si-no-es-tv">
                    <div class="col-md-3" >
                    <div class="form-group col-md-12">
                        {{ Form::label('tamanio', 'Tamaño') }}
                        <div class="input-group">
                            <span class="input-group-addon "><i class="fa fa-pencil-square-o"></i></span>
                            {{ Form::select("tamanio",array('1 - 1 a 8 cm' => '1 a 8 CM','2 - 9 a 15 cm' => '9 a 15 CM','3 - 16 a 20 cm' => '16 a 20 CM','4 - + 20 cm' => '+20CM') ,null, array("class" => "form-control","placeholder" => "Seleccionar..." )) }}
                        </div>
                        <span class="help-block text-warning tamanio_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                    <div class="form-group col-md-12">
                        {{ Form::label('malezas', 'Malezas') }}
                        <div class="input-group">
                            <span class="input-group-addon "><i class="fa fa-pencil-square-o"></i></span>
                            {{ Form::select("malezas",array("SI" => "SI", "NO" => "NO") ,null, array("class" => "form-control","placeholder" => "Seleccionar..." )) }}
                        </div>
                        <span class="help-block text-warning malezas_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                    <div class="form-group col-md-12">
                        {{ Form::label('calidad', 'Calidad') }}
                        <div class="input-group">
                            <span class="input-group-addon "><i class="fa fa-pencil-square-o"></i></span>
                            {{ Form::select("calidad",array('TIERNO' => 'TIERNO','MADURO' => 'MADURO','MADURO C/ZAPATO' => 'MADURO C/ZAPATO', 'TIERNO C/ZAPATO' => 'TIERNO C/ZAPATO') + array("maduro" => "MADURO") ,null, array("class" => "form-control","placeholder" => "Seleccionar..." )) }}
                        </div>
                        <span class="help-block text-warning calidad_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                    <div class="form-group col-md-12">
                        {{ Form::label('acaros', 'Acaros') }}
                        <div class="input-group">
                            <span class="input-group-addon "><i class="fa fa-pencil-square-o"></i></span>
                            {{ Form::select("acaros",array("SI" => "SI", "NO" => "NO") ,null, array("class" => "form-control","placeholder" => "Seleccionar..." )) }}
                        </div>
                        
                        <span class="help-block text-warning acaros_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                    <div class="form-group col-md-12">
                        {{ Form::label('temperatura', 'Temperatura') }}
                        <div class="input-group">
                            <span class="input-group-addon "><i class="fa fa-pencil-square-o"></i></span>
                            {{ Form::select("temperatura", array("NORMAL" => "NORMAL","CALIENTE" => "CALIENTE") ,null, array("class" => "form-control" ,"placeholder" => "Seleccionar...")) }}
                        </div>
                        <span class="help-block text-warning temperatura_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                    <div class="form-group col-md-12">
                        {{ Form::label("humedad_hoja", "Humedad") }}
                        <div class="input-group">
                            <span class="input-group-addon "><i class="fa fa-pencil-square-o"></i></span>
                            {{ Form::select("humedad_hoja",array("default" => "Seleccionar...") ,null, array("class" => "form-control humedad_hoja","placeholder" => "Seleccionar..." )) }}
                        </div>
                        <span class="help-block text-warning humedad_hoja_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                </div>
                </div>
                
                <div class="col-md-3" >
                        <div class="form-group">
                                {{ Form::label('porcentaje_palo', 'Porcentaje de Palo %') }}
                                {{ Form::number("porcentaje_palo", 0, array("class" => "form-control palo","min" => "0","max" => "100" )) }}
                                <span class="help-block text-warning palo_mensaje hidden" >
                                    <strong>
                                    </strong>
                                </span>
                        </div>
                        <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Referencia</th>
                            <th>Kg</th>
                            <th>#</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="text-align: right; vertical-align: middle;">Bruto</td>
                                <td>
                                {{ Form::text("bruto",0, array("class" => "form-control peso_bruto input-sm","min" => "0","style" => "with: auto;", "placeholder" => "0" )) }}
                                </td>
                                <td>
                                    <a class="btn btn-xs btn-primary" id="getbruto"><i class="fa fa-refresh"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; vertical-align: middle;">Tara</td>
                                <td>
                                    {{ Form::text("tara",0, array("class" => "form-control peso_tara input-sm","min" => "0","style" => "with: auto;", "placeholder" => "0" )) }}
                                </td>
                                <td><a class="btn btn-xs btn-primary" id="gettara"><i class="fa fa-refresh"></i></a></td>
                            </tr>
                            <tr>
                                <td style="text-align: right; vertical-align: middle;">Embalaje</td>
                                <td>
                                {{ Form::text("embalaje_peso",0, array("class" => "form-control peso_embalaje input-sm","style" => "with: auto;", "placeholder" => "0" )) }}
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; vertical-align: middle;">Humedad</td>
                                <td>
                                    {{ Form::text("humedad_peso",0, array("class" => "form-control peso_humedad input-sm","style" => "with: auto;", "placeholder" => "0" )) }}
                                </td>
                                <!--<td></td>-->
                            </tr>
                            <tr>
                                <td style="text-align: right; vertical-align: middle;">Palo</td>
                                <td >
                                        
                                        {{ Form::text("palo",0, array("class" => "form-control peso_palo input-sm","style" => "with: auto;", "placeholder" => "0" )) }}
                                </td>
                                <!--<td></td>-->
                            </tr>
                            <tr>
                                <td style="text-align: right; background-color: #f5f5f5;"><strong>NETO</strong></td>
                                <td style="background-color: #f5f5f5;">{{ Form::text("neto",0, array("class" => "form-control neto input-sm","style" => "with: auto;", "placeholder" => "0" )) }}</td>
                                <!--<td></td>-->
                            </tr>
                        </tbody>
                    </table>
                   
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-check-inline col-md-6">
                        <label class="form-check-inline ">
                            <input class="form-check-input" type="checkbox" name="certificado" value="1" > Producto Certificado
                        </label>
                        <label class="form-check-inline m-l-sm">
                            <input class="form-check-input expo" type="checkbox" name="expo" value="1" > Exportación
                        </label>
                        <label class="form-check-inline m-l-sm">
                            <input class="form-check-input" type="checkbox" name="estado" value="1" > Pesaje Completo
                        </label>
                        <label class="form-check-inline m-l-sm">
                            <input class="form-check-input derivado" type="checkbox" name="derivado" value="1" > Derivado
                        </label>
                    </div>
                </div>
                <div class="formulario_derivacion" style="display: none;">
                <div class="col-md-12 m-t-md">
                    <div class="form-group col-md-6">
                        {{ Form::label('derivado_a', 'Derivado a') }}
                        <div class="input-group">
                            <span class="input-group-addon "><i class="fa fa-mail-forward"></i></span>
                            {{ Form::select("derivado_a",array("" => "") ,null, array("class" => "form-control derivado_a" ,"style" => "width: 100%;")) }}
                        </div>

                        <span class="help-block text-warning derivado_a_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                </div>
                </div>
                <div class="formulario-exportacion" style="display: none;">
                <div class="col-md-12 m-t-md">
                    <h3>Resolución General 3890/2016 AFIP (Control de Peso y Volumen)</h3>
                </div>
                <div class="col-md-12 m-t-md">
                    <div class="form-group col-md-3" >
                        {{ Form::label('buque', 'Buque *') }}
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-ship"></i></span>
                            {{ Form::text("buque",null, array("class" => "form-control input-sm",'style' => 'height: 32px' )) }}
                        </div>
                        
                        <span class="help-block text-warning buque_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                    <div class="form-group col-md-3" >
                        {{ Form::label('reserva', 'Reserva *') }}
                        {{ Form::text("reserva",null, array("class" => "form-control input-sm" ,'style' => 'height: 32px')) }}
                        <span class="help-block text-warning reserva_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                    <div class="form-group col-md-3" >
                        {{ Form::label('codigo_aduana', 'Código Aduana *') }}
                        {{ Form::text("codigo_aduana",null, array("class" => "form-control input-sm" ,'style' => 'height: 32px')) }}
                        <span class="help-block text-warning codigo_aduana_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                    <div class="form-group col-md-3" >
                        {{ Form::label('codigo_balanza', 'Código Balanza') }}
                        {{ Form::text("codigo_balanza",null, array("class" => "form-control input-sm" ,'style' => 'height: 32px')) }}
                        <span class="help-block text-warning codigo_balanza_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group col-md-6">
                        {{ Form::label('nro_certificado','Nro. Certificado Hab. *') }}
                        {{ Form::text('nro_certificado',null,array('class' => 'form-control input-sm' ,'style' => 'height: 32px')) }}
                        <span class="help-block text-warning nro_certificado_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('nro_contenedor','Nro. Contenedor*') }}
                        {{ Form::text('nro_contenedor',null,array('class' => 'form-control input-sm' ,'style' => 'height: 32px')) }}
                        <span class="help-block text-warning nro_contenedor_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('vencimiento','Vencimiento') }}
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" name="vencimiento" class="form-control form_datetime input-sm">
                        </div>
                        <span class="help-block text-warning vencimiento_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                    
                </div>
                <div class="col-md-12">
                    <div class="form-group col-md-3">
                        {{ Form::label('precintos','Precintos') }}
                        {{ Form::text('precintos',null,array('class' => 'form-control input-sm','style' => 'height: 32px')) }}
                        <span class="help-block text-warning precintos_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('linea','Linea') }}
                        {{ Form::text('linea',null,array('class' => 'form-control input-sm','style' => 'height: 32px')) }}
                        <span class="help-block text-warning linea_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('otros','Otros') }}
                        {{ Form::text('otros',null,array('class' => 'form-control input-sm','style' => 'height: 32px')) }}
                        <span class="help-block text-warning otros_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group col-md-3">
                        {{ Form::label('cuit','CUIT ATA') }}
                        {{ Form::text('cuit',null,array('class' => 'form-control input-sm','style' => 'height: 32px')) }}
                        <span class="help-block text-warning cuit_mensaje hidden">
                            <strong>
                            </strong>
                        </span>
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('apellido_nombre','Apellido y Nombre ATA') }}
                        {{ Form::text('apellido_nombre',null,array('class' => 'form-control input-sm','style' => 'height: 32px')) }}
                        <span class="help-block text-warning apellido_nombre_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('permiso_embarque','Nro. Permiso Embarque *') }}
                        {{ Form::text('permiso_embarque',null,array('class' => 'form-control input-sm','style' => 'height: 32px')) }}
                        <span class="help-block text-warning permiso_embarque_mensaje hidden" >
                            <strong>
                            </strong>
                        </span>
                    </div>
                </div>
                </div>
            </div>
            
        </div>
    </div>
    
</div>
<div class="row m-b-lg">
    <div class="col-md-12">
        <a class="btn btn-md btn-primary guardar-pesaje" > Guardar</a>
        <a class="btn btn-md btn-danger m-r-xs" href="/balanza"> Cancelar</a>
    </div>
</div>
{{ Form::close() }}   
    
</div>

@section('javascript')
<!-- Data picker -->
<!--<script src="{{ asset('backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>-->

<!-- Data Tables -->
<script src="{{ asset('backend/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('backend/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('backend/js/plugins/dataTables/dataTables.responsive.js') }}"></script>
<script src="{{ asset('backend/js/plugins/dataTables/dataTables.tableTools.min.js') }}"></script>

<script src="{{ asset('backend/js/plugins/select2/select2.min.js') }} "></script>
<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">

<script type="text/javascript" src="{{ asset('backend/js/plugins/datetimepicker/moment.min.js') }}" charset="UTF-8"></script>
<script type="text/javascript" src="{{ asset('backend/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}" charset="UTF-8"></script>
<!-- Sweet alert -->
<script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        
        var total = 0;
    
        $(".expo").change(function(){
            if(this.checked){
                $(".formulario-exportacion").show();
            }else{
                $(".formulario-exportacion").hide();
            }
        });
        
        $(".derivado").change(function(){
            if(this.checked){
                $(".formulario_derivacion").show();
            }else{
                $(".formulario_derivacion").hide();
            }
        });
        
        var dateNow = new Date();
        $('.form_datetime').datetimepicker({
            locale: 'es',
            defaultDate: dateNow
        });
        
        function recalcular(){
            cantidad_embalaje   = $(".cantidad").val();
            peso_embalaje       = $(".embalaje_tipo option:selected").attr("peso");
            
            if(typeof peso_embalaje !== "undefined"){
                total_embalaje      = (cantidad_embalaje * peso_embalaje);
                $(".peso_embalaje").val(total_embalaje.toFixed(2));
            }else{
                total_embalaje = parseFloat(0);
            }
            
            tara    = parseFloat($(".peso_tara").val());
            bruto   = parseFloat($(".peso_bruto").val());
            humedad = parseFloat(0); //tomar del input
            palo = parseFloat(0); //tomar del input
            
            neto = parseFloat(bruto) - parseFloat((tara + total_embalaje + humedad + palo));
            
            porc_humedad    = $("#humedad_hoja option:selected").attr("porcentaje");
            porc_palo       = $(".palo").val();
            
            if(typeof porc_humedad != 'undefined' ){
                humedad         = (neto * porc_humedad) / 100;
                $(".peso_humedad").val(humedad.toFixed(2));
            }
            
            if(typeof porc_palo != 'undefined' && porc_palo != 0 ){
                palo         = (neto * porc_palo) / 100;
                $(".peso_palo").val(palo.toFixed(2));
            }
            
            neto = parseFloat(bruto) - parseFloat((tara + total_embalaje + humedad + palo));
            
            $(".neto").val(neto.toFixed(2));
        }
        
        var timeout;
        var onkeypressControlarPeso = function(){
            if(timeout){ clearTimeout(timeout);}
            timeout = setTimeout(function() {
                recalcular();
            },500);
        };
        
        $(".humedad_hoja").change(onkeypressControlarPeso);
        $(".palo").on("keypress",onkeypressControlarPeso);
        $(".embalaje_tipo").change(onkeypressControlarPeso);
        $(".cantidad").on("keypress",onkeypressControlarPeso);
        $(".peso_bruto").on("keypress", onkeypressControlarPeso);
        $(".peso_tara").on("keypress", onkeypressControlarPeso);
        $(".peso_palo").on("keypress", onkeypressControlarPeso);
        $(".peso_embalaje").on("keypress", onkeypressControlarPeso);
        $(".peso_humedad").on("keypress", onkeypressControlarPeso);
        
        $(".planta").change(function(){
            var planta = $(".planta").val(); 
            $.ajax({
                type: "POST",
                url: "getembalaje",
                async: true,
                data: "planta="+planta+"&_token="+$("meta[name='csrf-token']").attr("content"), // serializes the form's elements.
                success: function(data)
                {
                    
                    var select = $(".embalaje_tipo");
                    select.empty();
                    select.append("<option value='default'>Seleccionar...</option>");
                    for(var i=0; i < data.resultado.length; i++){
                        select.append("<option peso='"+data.resultado[i].peso+"' id_embalaje='"+data.resultado[i].id_embalaje+"' value='"+data.resultado[i].id+"'>"+data.resultado[i].descripcion+" - "+data.resultado[i].peso+" Kg. </option>")
                    }
                },
                error: function(msg){
                alert(msg);
                }
            });
            
            $.ajax({
                type: "POST",
                url: "gethumedad",
                async: true,
                data: "planta="+planta+"&_token="+$("meta[name='csrf-token']").attr("content"), // serializes the form's elements.
                success: function(data)
                {
                    
                    var select = $(".humedad_hoja");
                    select.empty();
//                    select.append("<option value='default'>Seleccionar...</option>");
                    for(var i=0; i < data.resultado.length; i++){
                        select.append("<option id_tipo_humedad='"+data.resultado[i].id_tipo_humedad+"' porcentaje='"+data.resultado[i].porcentaje+"' value='"+data.resultado[i].id+"'>"+data.resultado[i].porcentaje+" % </option>")
                    }
                },
                error: function(msg){
                alert(msg);
                }
            });
        });
        
        $("#getbruto").click(function(){
            var planta = $(".planta").val(); 
            if(planta == "default"){
                alert("Seleccione Planta de Pesaje");
            }else{
                $.ajax({
                    type: "POST",
                    url: "getpeso",
                    data: "planta="+planta+"&_token="+$("meta[name='csrf-token']").attr("content"), // serializes the form's elements.
                    success: function(data)
                    {
                        $(".peso_bruto").val(data.peso);
//                        if(data.peso > 25){
//                            $(".peso_tara").val(0);
//                        }
                        recalcular();
                    },
                    error: function(msg){
                    alert(msg);
                    }
                });
            }
        });
        
        $("#gettara").click(function(){
            var planta = $(".planta").val(); 
            if(planta == "default"){
                alert("Seleccione Planta de Pesaje");
            }else{
                $.ajax({
                    type: "POST",
                    url: "getpeso",
                    data: "planta="+planta+"&_token="+$("meta[name='csrf-token']").attr("content"), // serializes the form's elements.
                    success: function(data)
                    {
                        $(".peso_tara").val(data.peso);
//                        if(data.peso > 25){
//                            $(".peso_bruto").val(0);
//                        }
                            
                        recalcular();
                    },
                    error: function(msg){
                    alert(msg);
                    }
                });
            }
        });
        
        $('.producto').on("select2:unselecting", function(e){
            $('.ocultar-si-no-es-tv').hide();
        }).trigger('change');
        
        $('.producto').on("select2:selecting", function(e){
            var datos = e.params.args.data.text.split(" - ");
            
            if(datos[0] === 'TV01' || datos[0] === 'TV02'){
                $('.ocultar-si-no-es-tv').show();
            }else{
                $('.ocultar-si-no-es-tv').hide();
            }
        }).trigger('change');
        
        $(".camion").select2({
            theme: "bootstrap",
            ajax: {
              url: '{{ url("api/get-camion/") }}',
              dataType: 'json',
              delay: 250,
              data: function (params) {
                var planta = $(".planta").val(); 
                if(planta == "default"){
                    alert("Seleccione Planta de Pesaje");
                }
                return {
                  q: params.term,
                  page: params.page,
                  planta: planta
                };
              },
              processResults: function (data, params) {
                return {
                  results: data.results,
                  pagination: {
                    more: data.pagination.more
                  }
                };
              },
              cache: true
            },
            placeholder: "Seleccione la Patente...",
            allowClear: true
        });
        
        $(".producto").select2({
            theme: "bootstrap",
            ajax: {
              url: '{{ url("api/get-producto/") }}',
              dataType: 'json',
              delay: 250,
              data: function (params) {
                return {
                  q: params.term,
                  page: params.page,
                };
              },
              processResults: function (data, params) {
                return {
                  results: data.results,
                  pagination: {
                    more: data.pagination.more
                  }
                };
              },
              cache: true
            },
            placeholder: "Seleccione el Producto...",
            allowClear: true
        });
        
        $(".planta_origen").select2({
            theme: "bootstrap",
            ajax: {
              url: '{{ url("api/get-planta/") }}',
              dataType: 'json',
              delay: 250,
              data: function (params) {
                var planta = $(".planta").val(); 
                if(planta == "default"){
                    alert("Seleccione Planta de Pesaje");
                }
                return {
                  q: params.term, // search term
                  page: params.page,
                  planta: planta
                };
              },
              processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                  results: data.items,
                  pagination: {
                    more: (params.page * 30) < data.total_count
                  }
                };
              },
              cache: true
            },
            placeholder: "Seleccione la Planta Origen...",
            allowClear: true
        });
        
        $(".planta_destino").select2({
            theme: "bootstrap",
            ajax: {
              url: '{{ url("api/get-planta/") }}',
              dataType: 'json',
              delay: 250,
              data: function (params) {
                var planta = $(".planta").val(); 
                if(planta == "default"){
                    alert("Seleccione Planta de Pesaje");
                }
                return {
                  q: params.term, // search term
                  page: params.page,
                  planta: planta
                };
              },
              processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                  results: data.items,
                  pagination: {
                    more: (params.page * 30) < data.total_count
                  }
                };
              },
              cache: true
            },
            placeholder: "Seleccione la Planta Destino...",
            allowClear: true
        });
        
        $(".derivado_a").select2({
            theme: "bootstrap",
            ajax: {
              url: '{{ url("api/get-planta/") }}',
              dataType: 'json',
              delay: 250,
              data: function (params) {
                var planta = $(".planta").val(); 
                if(planta == "default"){
                    alert("Seleccione Planta de Pesaje");
                }
                return {
                  q: params.term, // search term
                  page: params.page,
                  planta: planta
                };
              },
              processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                  results: data.items,
                  pagination: {
                    more: (params.page * 30) < data.total_count
                  }
                };
              },
              cache: true
            },
            placeholder: "Seleccione la Planta Destino...",
            allowClear: true
        });
        
        $(".chacra").select2({
            theme: "bootstrap",
            ajax: {
              url: '{{ url("api/get-chacra/") }}',
              dataType: 'json',
              delay: 250,
              data: function (params) {
                var planta = $(".planta").val(); 
                if(planta == "default"){
                    alert("Seleccione Planta de Pesaje");
                }
                return {
                  q: params.term, // search term
                  page: params.page,
                  planta: planta
                };
              },
              processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                  results: data.items,
                  pagination: {
                    more: (params.page * 30) < data.total_count
                  }
                };
              },
              cache: true
            },
            placeholder: "Seleccione la Chacra...",
            allowClear: true
        });
        
        $(".proveedor").select2({
            theme: "bootstrap",
            ajax: {
              url: '{{ url("api/get-proveedor/") }}',
              dataType: 'json',
              delay: 250,
              data: function (params) {
                return {
                  q: params.term,
                  page: params.page,
                };
              },
              processResults: function (data, params) {
                return {
                  results: data.results,
                  pagination: {
                    more: data.pagination.more
                  }
                };
              },
              cache: true
            },
            placeholder: "Seleccione el Proveedor...",
            allowClear: true
        });
        
        $('.dataTables-example').dataTable({
            responsive: true,
        });
    
        $("#brotes").submit(function(e) {

        var url = "brotes"; // the script where you handle the form input.
        var t = $('.dataTables-example').DataTable();
        t.clear();
        t.draw();
        $.ajax({
        type: "POST",
                url: url,
                data: $("#brotes").serialize(), // serializes the form's elements.
                success: function(data)
                {
                var a = JSON.parse(data);
                var t = $('.dataTables-example').DataTable();
                t.clear();
                t.draw();
                var suma = 0;
                for (var key in a) {
                if (key === 'length' || !a.hasOwnProperty(key)) continue;
                var value = a[key];
                suma = suma + parseInt(value);
                t.row.add([
                        key,
                        value + ' Kg.',
                ]).draw(false);
                }

                $('#total_general').html(suma + ' KG');
                },
                error: function(msg){
                alert(msg);
                }

        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
    
        $(".guardar-pesaje").click(function(){
            var datos = $('#form').serialize();
            
            $.ajax({
                type: "POST",
                url: "/balanza/guardar-pesaje",
                data: datos,
                success: function(data){
                    if(typeof data.msg !== 'undefined'){
                        swal("Éxito!", data.msg, "success");
                    }
                },
                beforeSend: function(){
                    $(".camion_mensaje").addClass("hidden");
                    $(".camion_mensaje strong").text("");
                    $(".planta_mensaje").addClass("hidden");
                    $(".planta_mensaje strong").text("");
                    $(".producto_mensaje").addClass("hidden");
                    $(".producto_mensaje strong").text("");
                    $(".fecha_mensaje").addClass("hidden");
                    $(".fecha_mensaje strong").text("");
                    $(".planta_origen_mensaje").addClass("hidden");
                    $(".planta_origen_mensaje strong").text("");
                    $(".planta_destino_mensaje").addClass("hidden");
                    $(".planta_destino_mensaje strong").text("");
                    $(".proveedor_mensaje").addClass("hidden");
                    $(".proveedor_mensaje strong").text("");
                    
                    
                    $(".tamanio_mensaje").addClass("hidden");
                    $(".tamanio_mensaje strong").text("");
                    $(".malezas_mensaje").addClass("hidden");
                    $(".malezas_mensaje strong").text("");
                    $(".calidad_mensaje").addClass("hidden");
                    $(".calidad_mensaje strong").text("");
                    $(".acaros_mensaje").addClass("hidden");
                    $(".acaros_mensaje strong").text("");
                    $(".temperatura_mensaje").addClass("hidden");
                    $(".temperatura_mensaje strong").text("");
                    $(".humedad_hoja_mensaje").addClass("hidden");
                    $(".humedad_hoja_mensaje strong").text("");
                    
                    $(".lote_mensaje").addClass("hidden");
                    $(".lote_mensaje strong").text("");
                    $(".chacra_mensaje").addClass("hidden");
                    $(".chacra_mensaje strong").text("");
                    
                    
                    $(".buque_mensaje").addClass("hidden");
                    $(".buque_mensaje strong").text("");
                    $(".reserva_mensaje").addClass("hidden");
                    $(".reserva_mensaje strong").text("");
                    $(".codigo_aduana_mensaje").addClass("hidden");
                    $(".codigo_aduana_mensaje strong").text("");
                    $(".nro_certificado_mensaje").addClass("hidden");
                    $(".nro_certificado_mensaje strong").text("");
                    $(".nro_contenedor_mensaje").addClass("hidden");
                    $(".nro_contenedor_mensaje strong").text("");
                    $(".permiso_embarque_mensaje").addClass("hidden");
                    $(".permiso_embarque_mensaje strong").text("");
                    
                    
                    $(".derivado_a_mensaje").addClass("hidden");
                    $(".derivado_a_mensaje strong").text("");
                    
                },
                error: function(data){
                    
                    if(typeof data.responseJSON.error !== 'undefined'){
                        if(typeof data.responseJSON.error.camion !== 'undefined'){
                            $(".camion_mensaje").removeClass("hidden");
                            $(".camion_mensaje strong").text(data.responseJSON.error.camion);
                        }
                        if(typeof data.responseJSON.error.planta !== 'undefined'){
                            $(".planta_mensaje").removeClass("hidden");
                            $(".planta_mensaje strong").text(data.responseJSON.error.planta);
                        }
                        if(typeof data.responseJSON.error.producto !== 'undefined'){
                            $(".producto_mensaje").removeClass("hidden");
                            $(".producto_mensaje strong").text(data.responseJSON.error.producto);
                        }
                        if(typeof data.responseJSON.error.fecha !== 'undefined'){
                            $(".fecha_mensaje").removeClass("hidden");
                            $(".fecha_mensaje strong").text(data.responseJSON.error.fecha);
                        }
                        if(typeof data.responseJSON.error.planta_origen !== 'undefined'){
                            $(".planta_origen_mensaje").removeClass("hidden");
                            $(".planta_origen_mensaje strong").text(data.responseJSON.error.planta_origen);
                        }
                        if(typeof data.responseJSON.error.planta_destino !== 'undefined'){
                            $(".planta_destino_mensaje").removeClass("hidden");
                            $(".planta_destino_mensaje strong").text(data.responseJSON.error.planta_destino);
                        }
                        if(typeof data.responseJSON.error.proveedor !== 'undefined'){
                            $(".proveedor_mensaje").removeClass("hidden");
                            $(".proveedor_mensaje strong").text(data.responseJSON.error.proveedor);
                        }
                        
                        
                        
                        
                        if(typeof data.responseJSON.error.tamanio !== 'undefined'){
                            $(".tamanio_mensaje").removeClass("hidden");
                            $(".tamanio_mensaje strong").text(data.responseJSON.error.tamanio);
                        }
                        if(typeof data.responseJSON.error.malezas !== 'undefined'){
                            $(".malezas_mensaje").removeClass("hidden");
                            $(".malezas_mensaje strong").text(data.responseJSON.error.malezas);
                        }
                        if(typeof data.responseJSON.error.calidad !== 'undefined'){
                            $(".calidad_mensaje").removeClass("hidden");
                            $(".calidad_mensaje strong").text(data.responseJSON.error.calidad);
                        }
                        if(typeof data.responseJSON.error.acaros !== 'undefined'){
                            $(".acaros_mensaje").removeClass("hidden");
                            $(".acaros_mensaje strong").text(data.responseJSON.error.acaros);
                        }
                        if(typeof data.responseJSON.error.temperatura !== 'undefined'){
                            $(".temperatura_mensaje").removeClass("hidden");
                            $(".temperatura_mensaje strong").text(data.responseJSON.error.temperatura);
                        }
                        if(typeof data.responseJSON.error.humedad_hoja !== 'undefined'){
                            $(".humedad_hoja_mensaje").removeClass("hidden");
                            $(".humedad_hoja_mensaje strong").text(data.responseJSON.error.humedad_hoja);
                        }
                        
                        
                        
                        if(typeof data.responseJSON.error.lote !== 'undefined'){
                            $(".lote_mensaje").removeClass("hidden");
                            $(".lote_mensaje strong").text(data.responseJSON.error.lote);
                        }
                        if(typeof data.responseJSON.error.chacra !== 'undefined'){
                            $(".chacra_mensaje").removeClass("hidden");
                            $(".chacra_mensaje strong").text(data.responseJSON.error.chacra);
                        }
                        
                        
                        if(typeof data.responseJSON.error.buque !== 'undefined'){
                            $(".buque_mensaje").removeClass("hidden");
                            $(".buque_mensaje strong").text(data.responseJSON.error.buque);
                        }
                        if(typeof data.responseJSON.error.reserva !== 'undefined'){
                            $(".reserva_mensaje").removeClass("hidden");
                            $(".reserva_mensaje strong").text(data.responseJSON.error.reserva);
                        }
                        if(typeof data.responseJSON.error.codigo_aduana !== 'undefined'){
                            $(".codigo_aduana_mensaje").removeClass("hidden");
                            $(".codigo_aduana_mensaje strong").text(data.responseJSON.error.codigo_aduana);
                        }
                        if(typeof data.responseJSON.error.nro_certificado !== 'undefined'){
                            $(".nro_certificado_mensaje").removeClass("hidden");
                            $(".nro_certificado_mensaje strong").text(data.responseJSON.error.nro_certificado);
                        }
                        if(typeof data.responseJSON.error.nro_contenedor !== 'undefined'){
                            $(".nro_contenedor_mensaje").removeClass("hidden");
                            $(".nro_contenedor_mensaje strong").text(data.responseJSON.error.nro_contenedor);
                        }
                        if(typeof data.responseJSON.error.permiso_embarque !== 'undefined'){
                            $(".permiso_embarque_mensaje").removeClass("hidden");
                            $(".permiso_embarque_mensaje strong").text(data.responseJSON.error.permiso_embarque);
                        }
                        
                        
                        if(typeof data.responseJSON.error.derivado_a !== 'undefined'){
                            $(".derivado_a_mensaje").removeClass("hidden");
                            $(".derivado_a_mensaje strong").text(data.responseJSON.error.derivado_a);
                        }
                    }
                    
                    if(typeof data.responseJSON.pesaje_completo !== 'undefined'){
                        swal("Advertencia!", data.responseJSON.pesaje_completo, "error");
                    }
                    
                }
            });
        });
        
    
    });
</script>

@stop



@endsection
