@extends('layouts.backend')

@section('content')
<!--<div class="container" >-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2><strong>Dashboard</strong> Zafra 2016 - 2017 <strong>VS</strong> Zafra 2017 - 2018 </h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Inicio</a>
            </li>
            <li class="active">
                <strong>Resumen</strong> <span style="color: red;">(Actualizado al: {{ $fecha_actualizacion_format }})</span>
            </li>
        </ol>
    </div>
</div>
<div class="row m-t-md">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Ingreso Hoja Verde. </h5> &nbsp; (Acumulado al día de la fecha de actualización)
            </div>
            <div class="ibox-content">

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th><span style="color: red;">Actualizando tabla...</span></th>
                            <th style="text-align: center;" colspan="4">ZAFRA 2016/2017</th>
                            <th style="text-align: center;" colspan="5">ZAFRA 2017/2018</th>
                        </tr>
                        <tr style="font-size: 12px;">
                            <th>#</th>
                            <th style="text-align: center;">Origen</th>
                            <th style="text-align: center;">Sub Total</th>
                            <th style="text-align: center;">Deriv. a Terceros</th>
                            <th style="text-align: center;">Deriv. Internas</th>
                            <th style="text-align: center;">TOTAL ELABORADO</th>
                            <th style="text-align: center;">Sub Total</th>
                            <th style="text-align: center;">Deriv. a Terceros</th>
                            <th style="text-align: center;">Deriv. Internas</th>
                            <th style="text-align: center;">TOTAL ELABORADO</th>
                            <th style="text-align: center;">%</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Proveedores Certificado</td>
                        <td style="text-align: center;">{{ $hv_certi_proveedores_last_year }}  Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">{{ $hv_certi_proveedores }} Kg.</td>
                        <td style="text-align: center;">0 Kg.</td>
                        <td style="text-align: center;">0 Kg.</td>
                        <td style="text-align: center;">0 Kg.</td>
                        <td><div class="font-bold" @if($porc_cert_prov > 0) style="color: #090;" @else style="color: red;" @endif>{{ $porc_cert_prov }}% @if($porc_cert_prov > 0) <i class="fa fa-level-up"></i> @else <i class="fa fa-level-down"></i> @endif</div> </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Proveedores No Certificado</td>
                        <td style="text-align: center;">{{ $hv_no_certi_proveedores_last_year }} Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">{{ $hv_no_certi_proveedores }} Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td><div class=" font-bold" @if($porc_no_cert_prov > 0) style="color: #090;" @else style="color: red;" @endif>{{ $porc_no_cert_prov }}% @if($porc_no_cert_prov > 0) <i class="fa fa-level-up"></i> @else <i class="fa fa-level-down"></i> @endif</div></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Propio Certificado</td>
                        <td style="text-align: center;">{{ $hv_certi_propio_last_year }} Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">{{ $hv_certi_propio }} Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td><div class=" font-bold" @if($porc_cert_propio > 0) style="color: #090;" @else style="color: red;" @endif>{{ $porc_cert_propio }}% @if($porc_cert_propio > 0) <i class="fa fa-level-up"></i> @else <i class="fa fa-level-down"></i> @endif</div></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Propio No Certificado</td>
                        <td style="text-align: center;">{{ $hv_no_certi_propio_last_year }} Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">{{ $hv_no_certi_propio }} Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td style="text-align: center;">0  Kg.</td>
                        <td><div class="font-bold" @if($porc_no_cert_propio > 0) style="color: #090;" @else style="color: red;" @endif>{{ $porc_no_cert_propio }}% @if($porc_no_cert_propio > 0) <i class="fa fa-level-up"></i> @else <i class="fa fa-level-down"></i> @endif</div></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    
    
</div>
<div class="row m-t-md">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Certificado vs No Certificado</h5>
            </div>
            <div class="ibox-content">

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Zafra 16/17</th>
                        <th>Zafra 17/18</th>
                        <th>%</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><strong>&Sigma;</strong> Certificado</td>
                        <td>{{ $total_cert_2016_2017 }} Kg.</td>
                        <td>{{ $total_cert_2017_2018 }} Kg.</td>
                        <td><div class="font-bold" @if($porc_total_cert > 0) style="color: #090;" @else style="color: red;" @endif>{{ $porc_total_cert }}% @if($porc_total_cert > 0) <i class="fa fa-level-up"></i> @else <i class="fa fa-level-down"></i> @endif</div> </td>
                    </tr>
                    <tr>
                        <td><strong>&Sigma;</strong> No Certificado</td>
                        <td>{{ $total_no_cert_2016_2017 }} Kg.</td>
                        <td>{{ $total_no_cert_2017_2018 }} Kg.</td>
                        <td><div class=" font-bold" @if($porc_total_no_cert > 0) style="color: #090;" @else style="color: red;" @endif>{{ $porc_total_no_cert }}% @if($porc_total_no_cert > 0) <i class="fa fa-level-up"></i> @else <i class="fa fa-level-down"></i> @endif</div></td>
                    </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th style="text-align: center;"><strong>TOTAL</strong></th>
                            <th><strong>{{ $total_2016_2017 }} Kg.</strong></th>
                            <th><strong>{{ $total_2017_2018 }} Kg.</strong></th>
                            <th><div class=" font-bold" @if($porc_total > 0) style="color: #090;" @else style="color: red;" @endif>{{ $porc_total }}% @if($porc_total > 0) <i class="fa fa-level-up"></i> @else <i class="fa fa-level-down"></i> @endif</div></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="ibox-content m-b-md" style="text-align: center;">
        <h2 style="margin: 0px; padding: 0px;"><strong>RENDIMIENTOS CHACRAS PROPIAS (ACUMULADO)</strong></h2>
        <DIV class="hr-line-dashed"></DIV>
         <DIV class="text-center">Ingreso de brotes de chacras en comparación con la zafra pasada a la misma fecha. Argente no se encuentra en el listado.</DIV>
    </div>
    <div class="col-md-6 table-responsive">
        <table class="table table-bordered" style="background-color: white;">
            <thead style="font-size: 12px;">
                <th class="text-center" colspan="2" style="background-color: white;">ZAFRA 2016 - 2017</th>
            </thead>
            <thead>
                <!--<th>PLANTA</th>-->
            <th class="text-center" style="background-color: white;">CHACRA</th>
                <th class="text-center" style="background-color: white;">KG ACUMULADOS</th>
            </thead>
            <tbody>
                @foreach($hv_x_chacra_tv02_last_zafra as $hv_lastz)
                <tr>
                    <td>{{ $hv_lastz->chacra->Descripcion }}</td>
                    <td>{{ number_format($hv_lastz->peso) }} Kg.</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-6 table-responsive">
        
        <table class="table table-bordered" style="background-color: white;">
            <thead style="font-size: 12px;">
                <th class="text-center" colspan="3" style="background-color: white;">ZAFRA 2017 - 2018</th>
            </thead>
            <thead style="background-color: white ;">
                <!--<th>PLANTA</th>-->
                <th class="text-center" style="background-color: white;">CHACRA</th>
                <th class="text-center" style="background-color: white;">KG ACUMULADOS</th>
                <th class="text-center" style="background-color: white;">RENDIMIENTO</th>
        </thead>
            <tbody>
                @foreach($rinde_chacras as $rch)
                <tr>
                    <td>{{ $rch['descripcion'] }}</td>
                    <td>{{ number_format($rch['peso']) }} Kg.  </td>
                    <td>@if($rch['incremento'] == 0)  <span class="label label-success">Nuevo</span> @else <span  class="font-bold" @if($rch['incremento'] > 0) style="color: #090;" @else style="color: red;" @endif>{{ $rch['incremento'] }}% <i class="fa @if($rch['incremento'] > 0) fa-level-up @else fa-level-down @endif"></i></span> @endif</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


<div class="row">
    <div class="ibox-content m-b-md" style="text-align: center;">
        <h2 style="margin: 0px; padding: 0px;"><strong>ELABORACIÓN SECADEROS (ACUMULADO)</strong></h2>
        <DIV class="hr-line-dashed"></DIV>
         <DIV class="text-center">Comparación de Elaboración de Te Seco, Incluye todos los productos menos deshecho offgrade.</DIV>
         <DIV class="text-center">% ELABORACIÓN = [<span style="color: blue;">(</span><span style="color: red;">(</span>TOTAL_SECO_2017_2018 - REPROCESADO<span style="color: red;">)</span> * 100<span style="color: blue;">)</span> / TOTAL_SECO_2016_2017] - 100 </DIV>
    </div>
    <div class="col-md-6 table-responsive">
        <table class="table table-bordered" style="background-color: white;">
            <thead style="font-size: 12px;">
                <th class="text-center" colspan="2" style="background-color: white;">ZAFRA 2016 - 2017</th>
            </thead>
            <thead style="font-size: 12px;">
                <!--<th>PLANTA</th>-->
                <th class="text-center" style="background-color: white;">PLANTA</th>
                <th class="text-center" style="background-color: white;">TOTAL TÉ SECO</th>
            </thead>
            <tbody>
                <?php $total2 = 0; ?>
                @foreach($prod_secaderos_last_year as $old_prod)
                <tr>
                    <td>{{ $old_prod->planta->Descripcion }}</td>
                    <td>{{ number_format($old_prod->cantidad) }} Kg.</td>
                </tr>
                <?php $total2 = $total2 +  str_replace(',','',$old_prod->cantidad); ?>
                @endforeach
                <tr>
                    <td><strong>TOTAL</strong></td>
                    <td><strong>{{ number_format($total2) }} Kg.</strong></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-6 table-responsive">
        
        <table class="table table-bordered" style="background-color: white;">
            <thead style="font-size: 12px;">
                <th class="text-center" colspan="4" style="background-color: white;">ZAFRA 2017 - 2018</th>
            </thead>
            <thead style="background-color: white ; font-size: 12px;">
                <!--<th>PLANTA</th>-->
                <th class="text-center" style="background-color: white;">PLANTA</th>
                <th class="text-center" style="background-color: white;">TOTAL TÉ SECO</th>
                <th class="text-center" style="background-color: white;">REPROCESADO</th>
                <th class="text-center" style="background-color: white;">ELABORACIÓN</th>
            </thead>
            <tbody>
                <?php $total = 0; $incremento = 0; $tot_reprocesado = 0; ?>
                @foreach($rinde_secaderos as $new_prod)
                <tr>
                    <td>{{ $new_prod['descripcion'] }}  </td>
                    <td>{{ $new_prod['cantidad'] }} Kg.  </td>
                    <td>{{ number_format($new_prod['reprocesado']) }} Kg.  </td>
                    <td> <span  class="font-bold" @if($new_prod['incremento'] > 0) style="color: #090;" @else style="color: red;" @endif>{{ $new_prod['incremento'] }}% <i class="fa @if($new_prod['incremento'] > 0) fa-level-up @else fa-level-down @endif"></i></span> </td>
                </tr>
                <?php $tot_reprocesado = $tot_reprocesado + $new_prod['reprocesado']; $total = $total +  str_replace(',','',$new_prod['cantidad']); $incremento = $incremento + $new_prod['incremento'];?>
                @endforeach
                <tr>
                    <td><strong>TOTAL</strong></td>
                    <td><strong>{{ number_format($total) }} Kg.</strong></td>
                    <td><strong>{{ number_format($tot_reprocesado) }} Kg.</strong></td>
                    <?php $porc = ((($total - $tot_reprocesado) * 100)/$total2) -100; $porc = number_format($porc,2);  
                          
                    ?>
                    <td><strong><span  class="font-bold" @if($porc > 0) style="color: #090;" @else style="color: red;" @endif>{{ $porc }}% <i class="fa @if($porc > 0) fa-level-up @else fa-level-down @endif"></i></span> </strong></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="ibox-content m-b-md" style="text-align: center;">
        <h2 style="margin: 0px; padding: 0px;"><strong>DERIVACIONES</strong> ZAFRA 16/17 <strong>VS</strong> ZAFRA 17/18</h2>
        <DIV class="hr-line-dashed"></DIV>
        <DIV class="text-center">Se compara entre zafras distintas, pero al mismo Dia/Mes. (Dado por la fecha de actualización).</DIV>
    </div>
    <div class="col-md-6 table-responsive">
        <table class="table table-bordered" style="background-color: white;">
            <thead style="font-size: 12px;">
                <th class="text-center" colspan="3" style="background-color: white;">ZAFRA 2016 - 2017</th>
            </thead>
            <thead style="font-size: 12px;">
                <!--<th>PLANTA</th>-->
                <th class="text-center" style="background-color: white;">ORIGEN</th>
                <th class="text-center" style="background-color: white;">DESTINO</th>
                <th class="text-center" style="background-color: white;">TOTAL</th>
            </thead>
            <tbody>
                <?php $total_old = 0; ?>
                @foreach($derivado_1617 as $a)
                <tr style="font-size: 12px;">
                    <td>{{ $a->ORIGEN }}</td>
                    <td>{{ $a->DESTINO }}</td>
                    <td>{{ number_format($a->TOTAL) }} Kg.</td>
                </tr>
                <?php $total_old = $total_old + $a->TOTAL; ?>
                @endforeach
            </tbody>
            <tfoot>
                <th></th>
                <th style="text-align: center;"><strong>TOTAL</strong></th>
                <th><strong>{{ number_format($total_old) }} Kg.</strong></th>
            </tfoot>
        </table>
    </div>
    <div class="col-md-6 table-responsive">
        <table class="table table-bordered" style="background-color: white;">
            <thead style="font-size: 12px;">
                <th class="text-center" colspan="3" style="background-color: white;">ZAFRA 2017 - 2018</th>
            </thead>
            <thead style="font-size: 12px;">
                <!--<th>PLANTA</th>-->
                <th class="text-center" style="background-color: white;">ORIGEN</th>
                <th class="text-center" style="background-color: white;">DESTINO</th>
                <th class="text-center" style="background-color: white;">TOTAL</th>
            </thead>
            <tbody>
                <?php $total_new = 0; ?>
                @foreach($derivado_1718 as $a)
                <tr style="font-size: 12px;">
                    <td>{{ $a->ORIGEN }}</td>
                    <td>{{ $a->DESTINO }}</td>
                    <td>{{ number_format($a->TOTAL) }} Kg.</td>
                </tr>
                <?php $total_new = $total_new + $a->TOTAL; ?>
                @endforeach
            </tbody>
            <?php $porc_derivado = (($total_new * 100) / $total_old) - 100; $porc_derivado = number_format($porc_derivado);?>
            <tfoot>
                <th></th>
                <th style="text-align: center;"><strong>TOTAL</strong></th>
                <th><strong>{{ number_format($total_new) }} Kg. &nbsp; (<span  class="font-bold" @if($porc_derivado > 0) style="color: #090;" @else style="color: red;" @endif>{{ $porc_derivado }}% <i class="fa @if($porc_derivado > 0) fa-level-up @else fa-level-down @endif"></i></span>)</strong></th>
            </tfoot>
        </table>
    </div>
</div>



@endsection

@section('javascript')
 <!-- ChartJS-->
    <script src="{{ asset('backend/js/plugins/chartJs/Chart.min.js') }}"></script>
    <script src="{{ asset('backend/js/demo/chartjs-demo.js') }}"></script>
    
@stop
