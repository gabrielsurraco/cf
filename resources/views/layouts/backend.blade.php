<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Balanza Web') }}</title>

    <link href="{{ asset('backend/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Toastr style -->
    <link href="{{ asset('backend/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css">
    
     <!--<link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">-->

    <!-- Gritter -->
    <link href="{{ asset('backend/js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('backend/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/style.css') }}" rel="stylesheet">
    
    
   
    
    
    
    @yield('css')
    
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>

<body class="pace-done mini-navbar">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu" >
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                                <a href="/home"><img alt="image" class="img-circle" src="{{ asset('backend/img/perfil_cf.gif') }}" /></a>
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Auth::user()->name }}</strong>
                             </span> <span class="text-muted text-xs block">Bienvenido! <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="#">Profile</a></li>
                                <li><a href="#">Contacts</a></li>
                                <li><a href="#">Mailbox</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            <a href="/home">CF</a>
                        </div>
                    </li>
<!--                    <li class="@if(Route::is('secaderos.*')) active @endif">
                        <a href="index.html"><i class="fa fa-building"></i> <span class="nav-label">Secaderos</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li   class="@if(Route::is('secaderos.tv_chip')) active @endif" ><a href="/secaderos/tv_chip">TV y CHIP</a></li>
                            <li   class="@if(Route::is('secaderos.control_hv')) active @endif" ><a href="/secaderos/control_hv">Control Hoja Verde</a></li>
                        </ul>
                    </li>-->
<!--                    <li class="@if(Route::is('balanza')) active @endif">
                        <a href="/balanza"><i class="fa fa-balance-scale"></i> <span class="nav-label">Báscula</span></a>
                    </li>-->
                    <li title="Reportes" class="@if(Route::is('reportes.*')) active @endif">
                        <a href="index.html"><i class="fa fa-th-large"></i> <span class="nav-label">Informes</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <!--<li   class="@if(Route::is('reportes.secaderos')) active @endif" ><a href="/reportes/secaderos">Producción Secaderos</a></li>-->
                            <li   class="@if(Route::is('reportes.brotes')) active @endif" ><a href="/reportes/brotes">Ingreso de Brotes</a></li>
                            <li   class="@if(Route::is('reportes.stock')) active @endif" ><a href="/reportes/stock">Stock Semi Elaborados</a></li>
                            <!--<li   class="@if(Route::is('reportes.grafico.brotes')) active @endif" ><a href="/reportes/grafico-brotes">Gráficos de Brotes</a></li>-->
                            <!--<li   class="@if(Route::is('reportes.grafico.produccion')) active @endif" ><a href="/reportes/grafico-produccion">Gráficos de Producción</a></li>-->
                        </ul>
                    </li>
                    @if(Auth::user()->hasRole('ADMINISTRADOR'))
                    <li title="Usuarios" class="@if(Route::is('register') || Route::is('usuarios') || Route::is('roles.y.permisos')) active @endif">
                        <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Usuarios</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li   class="@if(Route::is('register')) active @endif" ><a href="/register">Alta Usuario</a></li>
                            <li   class="@if(Route::is('usuarios')) active @endif" ><a href="/usuarios">Usuarios y Accesos</a></li>
                            <li   class="@if(Route::is('roles.y.permisos')) active @endif" ><a href="/roles-y-permisos">Roles y Permisos</a></li>
                        </ul>
                    </li>
                    @endif
<!--                    <li class="@if(Route::is('stock')) active @endif">
                        <a href="/stock"><i class="fa fa-cube"></i> <span class="nav-label">Stock</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li   class="@if(Route::is('products')) active @endif" ><a href="/register">Products</a></li>
                            <li   class="@if(Route::is('latest-movements')) active @endif" ><a href="/latest-movements">Latest movements</a></li>
                        </ul>
                    </li>-->
<!--                    <li class="@if(Route::is('/tools/format-file')) active @endif">
                        <a href="/tools"><i class="fa fa-building"></i> <span class="nav-label">Tools</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="@if(Route::is('/tools/format-file')) active @endif" ><a href="/tools/format-file">Format File</a></li>
                        </ul>
                    </li>-->
                    <li class="@if(Route::is('sincronizar')) active @endif">
                        <a href="/sincronizar" title="Sincronizar"><i class="fa fa-server"></i> <span class="nav-label">Actualizaciones</span></a>
                    </li>
                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class=" dashbard-1" style="background-color: #f4f5f7">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">

                @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <!--<li><a href="{{ url('/register') }}">Register</a></li>-->
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
            </ul>

        </nav>
        </div>
            @yield('content')
        </div>
        
        
    </div>

    <!-- Mainly scripts -->
    <script src="{{ asset('backend/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <!-- Flot -->
    <script src="{{ asset('backend/js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/flot/jquery.flot.pie.js') }}"></script>

    <!-- Peity -->
    <script src="{{ asset('backend/js/plugins/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('backend/js/demo/peity-demo.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('backend/js/inspinia.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/pace/pace.min.js') }}"></script>

    <!-- jQuery UI -->
    <script src="{{ asset('backend/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

    <!-- GITTER -->
    <script src="{{ asset('backend/js/plugins/gritter/jquery.gritter.min.js') }}"></script>

    <!-- Sparkline -->
    <script src="{{ asset('backend/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>

    <!-- Sparkline demo data  -->
    <script src="{{ asset('backend/js/demo/sparkline-demo.js') }}"></script>

    <!-- ChartJS-->
    <script src="{{ asset('backend/js/plugins/chartJs/Chart.min.js') }}"></script>

    <!-- Toastr -->
    <script src="{{ asset('backend/js/plugins/toastr/toastr.min.js') }}"></script>
    
    <!--<script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>-->
    
    
    {!! Toastr::message() !!}
    @yield('javascript')
    
    
</body>
</html>
