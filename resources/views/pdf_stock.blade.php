<style>
    #sub-left {
        float: left;
        width: 27%;
        height: 14%;
    }
    #sub-right {
       float: right;
       width: 70%;
       height: 14%;
    }
    .clear-both {
       clear: both;
    }
    
    table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
<div id="sub-title" style="position: fixed;">
    <div id="sub-left">
        <img src="./backend/img/logo_cf_finlays.jpg" height="85" and width="180"/>
    </div>
    <div id="sub-right">
        <u><i><h2 style="margin-top: 30px; margin-left: 40px;">STOCK SEMI-ELABORADOS</h2></i></u>
        <p style="font-size: 13px;"><u><strong>FILTROS APLICADOS:</strong></u> <br/>
            <strong>Plantas Unificadas: </strong> @if(isset($request['aca'])) ACA- @endif
            @if(isset($request['cg'])) -CG- @endif 
            @if(isset($request['cv'])) -CV- @endif 
            @if(isset($request['obso'])) -OBSO- @endif 
            @if(isset($request['ta'])) -TA- @endif
            @if(isset($request['dcg'])) -DCG- @endif
            @if(isset($request['cvm'])) -CVM- @endif
            @if(isset($request['db'])) -DB- @endif
            @if(isset($request['dch'])) -DCH- @endif
            @if(isset($request['dcr'])) -DCR- @endif
            @if(isset($request['obmy'])) -OBMY- @endif
            @if(isset($request['obit'])) -OBIT- @endif
            @if(isset($request['cv2'])) -CV2- @endif
            @if(isset($request['obto'])) -OBTO- @endif</p>
    </div>
    
</div>
<div class="clear-both">
    <table>

        <tr>
          <th>Código</th>
          <th>Descripción</th>
          <th>Cantidad</th>
        </tr>
        <?php $total = 0; ?>
        @foreach($resultado as $r)
        <tr>
          <td>{{ $r->CODIGO }}</td>
          <td>{{ $r->DESCRIPCION }}</td>
          <td>{{ number_format($r->TOTAL) }} Kg.</td>
        </tr>
        <?php $total = $total + $r->TOTAL; ?>
        @endforeach
        <tr>
            <td></td>
            <td><strong>TOTAL: </strong></td>
            <td><strong>{{ number_format($total) }}</strong> Kg.</td>
        </tr>
      </table>
</div>
<script type="text/php">
    if ( isset($pdf) ) {
//        $font = Font_Metrics::get_font("helvetica", "bold");
        $pdf->page_text(500, 18, "Página: {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0,0,0));
    }
</script>