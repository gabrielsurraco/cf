@extends('layouts.backend')

@section('css')
    <link href="{{ asset('backend/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    
    <!-- Data Tables -->
    <link href="{{ asset('backend/css/plugins/dataTables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/plugins/dataTables/dataTables.responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/plugins/dataTables/dataTables.tableTools.min.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="row wrapper border-bottom white-bg ">
    <div class="col-lg-12">
        <h2>Ingreso de Brotes</h2> <p>Tener en cuenta que no se descuenta el brote derivado.</p>
        <hr/>
        
        {!! Form::open(['method' => 'post','id' => 'brotes']) !!}
        <div class="row">
            <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">Bases de Datos</label>

                <div class="col-sm-10">
                    <label class="checkbox-inline"> 
                    <input type="checkbox" id="inlineCheckbox1" name="aca"  checked=""> ACA </label> 
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox2" name="cg"  checked=""> CG </label> 
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="cv"  checked=""> CV </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="obso"  checked=""> OBSO </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="ta"  checked=""> TA </label>
                </div>
            </div>
            </div>
            <div class="hr-line-dashed"></div>
            
            <div class="form-group col-md-3" id="data_1">
                {{ Form::label('fecha_desde', 'Fecha Desde') }}
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" id="fecha_desde" name="fecha_desde" value="<?php echo date('d/m/Y'); ?>" class="form-control">
                </div>
            </div>
            <div class="form-group col-md-3" id="data_1">
                {{ Form::label('fecha_hasta', 'Fecha Hasta') }}
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" id="fecha_hasta" name="fecha_hasta" value="<?php echo date('d/m/Y'); ?>" class="form-control">
                </div>
            </div>
            <div class="form-group col-md-3">
                {{ Form::label('id_origen', 'Origen') }}
                {{ Form::select("id_origen", array('chacra' => 'Chacras (propio)','proveedor' => 'Proveedores'), null, array("class"=>"form-control")) }}
            </div>
            <div class="form-group col-md-3">
                {{ Form::label('id_certificado', 'Certificado') }}
                {{ Form::select("id_certificado", array('default' => 'Seleccione...') + array('default' => 'Seleccione...','certificado' => 'Certificado','no_certificado' => 'No Certificado'), null, array("class"=>"form-control")) }}
            </div>
            <div class="form-group col-md-3">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <label class="checkbox-inline"> 
                            <input type="checkbox" name="agrupado_x_lote" > Agrupar por Lotes </label> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <button class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Buscar</button>
                <!--<a class="btn btn-sm btn-default" id="clear"><i class="fa fa-eraser"></i> Limpiar</a>-->
                <!--<a class="btn btn-sm btn-danger" href="/reportes/brotes/pdf" target="_blank"><i class="fa fa-file-pdf-o"></i> Exportar a PDF</a>-->
                <a class="btn btn-sm btn-danger" id="pp"><i class="fa fa-file-pdf-o"></i> Exportar a PDF</a>
            </div>
        </div>
        {!! Form::close() !!}
        
    </div>
    <br/>
    <br/>
    
    <div class="col-md-12 m-t-md m-b-md">
        <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Descripción</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody id="body">
                    
                    
                    </tbody>
                    <tfoot>
                    <tr>
                        <th style="text-align: right">Sub Total</th>
                        <th id="total_general"> 0 Kg.</th>
                    </tr>
                    <tr>
                        <th style="text-align: right">Derivado a otros secaderos Propios</th>
                        <th id="deriv_propio">0 Kg.</th>
                    </tr>
                    <tr>
                        <th style="text-align: right">Derivado a Terceros</th>
                        <th id="deriv_seca">0 Kg.</th>
                    </tr>
                    <tr>
                        <th style="text-align: right">TOTAL ELABORADO</th>
                        <th id="tot_elaborado">0 Kg.</th>
                    </tr>
                    </tfoot>
                    </table>
                    
    </div>
</div>
@section('javascript')
<!-- Data picker -->
<script src="{{ asset('backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<!-- Data Tables -->
    <script src="{{ asset('backend/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/dataTables/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/dataTables/dataTables.tableTools.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        
        $('#data_1 .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: 'dd/mm/yyyy',
                
        });
        
        $('.dataTables-example').dataTable({
            responsive: true,
            
//            "dom": 'T<"clear">lfrtip',
//            "tableTools": {
//                "sSwfPath": "{{ asset('backend/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf') }}"
//            }
        });
        
        $("#pp").click(function(){
            var url = "/reportes/brotes/pdf?" + $("#brotes").serialize();
            $(location).attr('href',url);
        });
        
//        $("#getReport").click(function(e){
//            e.preventDefault();
//            alert("asd"),
//        });
            
            
        $("#brotes").submit(function(e) {

            var url = "brotes"; // the script where you handle the form input.
            var t = $('.dataTables-example').DataTable();
                        t.clear();
                        t.draw();
                        
            $.ajax({
                   type: "POST",
                   url: url,
                   data: $("#brotes").serialize(), // serializes the form's elements.
                   success: function(data)
                   {
                        
                        var t = $('.dataTables-example').DataTable();
                        t.clear();
                        t.draw();
                        var suma = 0;
                        var total = 0;
                        
                        var a = JSON.parse(data);
                        var deriv_seca = 0;
                        var deriv_propio = 0;
                        var tot_elaborado = 0;
                        
                        for (var key in a) {
                            var LOTE; 

                            if(key != 'DERIVADOS'){
                                if(typeof a[key]['LOTE'] != 'undefined')
                                    LOTE = ' -- LOTE: ' + a[key]['LOTE'];
                                else
                                    LOTE = '';

                                t.row.add( [
                                    a[key]['DESCRIPCION'] + ' ' + LOTE,
                                    (new Intl.NumberFormat().format(a[key]['TOTAL'])) + ' Kg.',
                                ] ).draw( false );

                                total = parseInt(total) + parseInt(a[key]['TOTAL']);
                            }else{
//                                deriv_seca = a[key]['TOT_DERIV_SECADEROS'];
                                if(typeof a[key]["propio"]["TOT_DERIV_PROPIO"] != 'undefined' && a[key]["propio"]["TOT_DERIV_PROPIO"] != null)
                                    deriv_propio = a[key]["propio"]["TOT_DERIV_PROPIO"];
                                if(typeof a[key]["terceros"]["TOT_DERIV_SECADEROS"] != 'undefined' && a[key]["terceros"]["TOT_DERIV_SECADEROS"] != null)
                                    deriv_seca = a[key]["terceros"]["TOT_DERIV_SECADEROS"];
                                
                            }
                            
                        }

                        $('#total_general').html((new Intl.NumberFormat().format(total)) +"<strong> Kg. </strong>");
                        $('#deriv_seca').html((new Intl.NumberFormat().format(deriv_seca)) + " Kg.");
                        $('#deriv_propio').html((new Intl.NumberFormat().format(deriv_propio)) + " Kg.");
                        $('#tot_elaborado').html((new Intl.NumberFormat().format(total - deriv_seca - deriv_propio)) + " Kg.");
                   },
                   error: function(msg){
                       alert(msg);
                       $('#total_general').html("<strong>TOTAL: </strong>0");
                   }
                   
            });

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });
        
    });
</script>

@stop
            
        
        
@endsection
