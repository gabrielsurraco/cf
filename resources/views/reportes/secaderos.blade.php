@extends('layouts.backend')

@section('css')
    <link href="{{ asset('backend/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    
    <!-- Data Tables -->
    <link href="{{ asset('backend/css/plugins/dataTables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/plugins/dataTables/dataTables.responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/plugins/dataTables/dataTables.tableTools.min.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="row wrapper border-bottom white-bg ">
    <div class="col-lg-12">
        <h2>Producción de Secaderos</h2>
        <hr/>
        {!! Form::open(['method' => 'post','id' => 'seca']) !!}
        <div class="row">
            <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">Bases de Datos</label>

                <div class="col-sm-10">
                    <label class="checkbox-inline"> 
                    <input type="checkbox" id="inlineCheckbox1" name="aca"  checked=""> ACA </label> 
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox2" name="cg"  checked=""> CG </label> 
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="cv"  checked=""> CV </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="obso"  checked=""> OBSO </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="ta"  checked=""> TA </label>
                </div>
            </div>
            </div>
            <div class="hr-line-dashed"></div>
            
            <div class="form-group col-md-3" id="data_1">
                {{ Form::label('fecha_desde', 'Fecha Desde') }}
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" id="fecha_desde" name="fecha_desde" value="<?php echo date('d/m/Y'); ?>" class="form-control">
                </div>
            </div>
            <div class="form-group col-md-3" id="data_1">
                {{ Form::label('fecha_hasta', 'Fecha Hasta') }}
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" id="fecha_hasta" name="fecha_hasta" value="<?php echo date('d/m/Y'); ?>" class="form-control">
                </div>
            </div>
<!--            <div class="form-group col-md-3">
                {{ Form::label('id_producto', 'Producto') }}
                {{ Form::select("id_producto", array('default' => 'Seleccionar...') + $p, null, array("class"=>"form-control")) }}
            </div>-->
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <button class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Buscar</button>
                <a class="btn btn-sm btn-default" id="clear"><i class="fa fa-eraser"></i> Limpiar</a>
            </div>
        </div>
        {!! Form::close() !!}
        
    </div>
    <br/>
    <br/>
    
    <div class="col-lg-12 m-t-md m-b-md">
        <table class="table table-striped table-bordered table-hover dataTables-example " >
                    <thead>
                    <tr>
                        <th>Secadero</th>
                        <th>Producto</th>
                        <th id="total_general">Cantidad (Kg)</th>
                        <th >Total</th>
                        <!--<th>% Por Secadero</th>-->
                        <th>Brote (excluye derivados</th>
                        <th>Rendimiento</th>
                    </tr>
                    </thead>
                    
                    <tbody id="body">
                    
                    
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Secadero</th>
                        <th>Producto</th>
                        <th id="total_general">Cantidad (Kg)</th>
                        <th >Total</th>
                        <!--<th>% Por Secadero</th>-->
                        <th>Brote (excluye derivados</th>
                        <th>Rendimiento</th>
                    </tr>
                    </tfoot>
                    </table>
                    
    </div>
</div>
@section('javascript')
<!-- Data picker -->
<script src="{{ asset('backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<!-- Data Tables -->
    <script src="{{ asset('backend/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/dataTables/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/dataTables/dataTables.tableTools.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#data_1 .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: 'dd/mm/yyyy',
                lang: 'es'
        });
        
        $('.dataTables-example').dataTable({
                responsive: true,
                
                
                
//                rowsGroup: [
//                    'first:name',
//                  ],
//                "dom": 'T<"clear">lfrtip',
//                "tableTools": {
//                    "sSwfPath": "{{ asset('backend/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf') }}"
//                }
            });
            
            
        $("#seca").submit(function(e) {

            var url = "seca"; // the script where you handle the form input.

            $.ajax({
                   type: "POST",
                   url: url,
                   data: $("#seca").serialize(), // serializes the form's elements.
                   success: function(data)
                   {
                        //var tr = "<tr></tr>"
                        var a = JSON.parse(data)
                        var t = $('.dataTables-example').DataTable();
                        t.clear();
                        t.draw();
                        for ( var i = 0; i < a.length; i++) {
                            if(i < a.length -1){
                                if(a[i]['base'] == 'ACA'){
                                    t.row.add( [
                                        a[i]['base'],
                                        a[i]['producto'] + ' ('+a[i]['representa']+' %)',
                                        a[i]['suma'],
                                        a[a.length-3]['aca'],
//                                       a[i]['representa'] + ' %',
                                        a[a.length - 2]['brote_aca'],
                                        a[a.length - 1]['rendimiento_aca']+ ' ',
                                    ] ).draw( false );
                                }
//                                if(a[i]['base'] == 'CV'){
//                                    t.row.add( [
//                                        a[i]['base'],
//                                        a[i]['producto'],
//                                        a[i]['suma'],
//                                        a[a.length -3 ]['cv'],
////                                        a[i]['representa'] + ' %',
//                                        a[a.length -2 ]['brote_cv'],
//                                        a[a.length - 1]['rendimiento_cv'] + ' ',
//                                    ] ).draw( false );
//                                }
//                                if(a[i]['base'] == 'CG'){
//                                    t.row.add( [
//                                        a[i]['base'],
//                                        a[i]['producto'],
//                                        a[i]['suma'],
//                                        a[a.length - 3]['cg'],
////                                        a[i]['representa'] + ' %',
//                                        a[a.length - 2]['brote_cg'],
//                                        a[a.length - 1]['rendimiento_cg'] + ' ',
//                                    ] ).draw( false );
//                                }
//                                if(a[i]['base'] == 'TA'){
//                                    t.row.add( [
//                                        a[i]['base'],
//                                        a[i]['producto'],
//                                        a[i]['suma'],
//                                        a[a.length - 3]['ta'],
////                                        a[i]['representa'] + ' %',
//                                        a[a.length - 2]['brote_ta'],
//                                        a[a.length - 1]['rendimiento_ta'] + ' ',
//                                    ] ).draw( false );
//                                }
//                                if(a[i]['base'] == 'OBSO'){
//                                    t.row.add( [
//                                        a[i]['base'],
//                                        a[i]['producto'],
//                                        a[i]['suma'],
//                                        a[a.length - 3]['obso'],
////                                        a[i]['representa'] + ' %',
//                                        a[a.length - 2]['brote_obso'],
//                                        a[a.length - 1]['rendimiento_obso'] + ' ',
//                                    ] ).draw( false );
//                                }
                            }
                        }
                        
//                        $('#total_general').html(a[a.length -4]['total'] + ' KG');
                        
                   },
                   error: function(msg){
                       alert(msg);
                   }
                   
                 });

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });
        
    });
</script>
@stop
            
        
        
@endsection
