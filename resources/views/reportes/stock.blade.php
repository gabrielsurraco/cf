@extends('layouts.backend')

@section('css')
    <link href="{{ asset('backend/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    
    <!-- Data Tables -->
    <link href="{{ asset('backend/css/plugins/dataTables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/plugins/dataTables/dataTables.responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/plugins/dataTables/dataTables.tableTools.min.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="row wrapper border-bottom white-bg ">
    <div class="col-lg-12">
        <h2>Stock Semi-Elaborados Unificado</h2> <p>Los productos son agrupados por Código y Descripción.<strong> Para ver la fecha de actualización de cada Planta, dirigirse a <a href="/sincronizar">ACTUALIZACIONES</a></strong></p>
        <hr/>
        
        {!! Form::open(['method' => 'post','id' => 'brotes']) !!}
        <div class="row">
            <div class="form-horizontal">
                
            <div class="form-group">
                <label class="col-sm-2 control-label">Bases de Datos</label>
                <div class="col-sm-10">
                    <label class="checkbox-inline"> 
                    <input type="checkbox" id="inlineCheckbox1" name="aca"  checked=""> ACA </label> 
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox2" name="cg"  checked=""> CG </label> 
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="dcg"  checked=""> DCG </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="cv"  checked=""> CV </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="cvm"  checked=""> CVM </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="obto"  checked=""> OBTO </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="obit"  checked=""> OBIT </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="obmy"  checked=""> OBMY </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="cv2"  checked=""> CV2 </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="dcr"  checked=""> DCR </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="dch"  checked=""> DCH </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="db"  checked=""> DBeltrame </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="obso"  checked=""> OBSO </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="ta"  checked=""> TA </label>
                </div>
                
                
            </div>
            </div>
            <div class="hr-line-dashed"></div>
            
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <button class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Buscar</button>
                <a class="btn btn-sm btn-danger" id="pp"><i class="fa fa-file-pdf-o"></i> Exportar a PDF</a>
            </div>
        </div>
        {!! Form::close() !!}
        
    </div>
    <br/>
    <br/>
    
    <div class="col-md-12 m-t-md m-b-md">
        <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Descripción</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody id="body">
                    
                    
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Código</th>
                        <th>Descripción</th>
                        <th id="total_general">Total</th>
                    </tr>
                    </tfoot>
                    </table>
                    
    </div>
</div>
@section('javascript')
<!-- Data picker -->
<script src="{{ asset('backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<!-- Data Tables -->
    <script src="{{ asset('backend/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/dataTables/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/dataTables/dataTables.tableTools.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        
//        $('.check').click(function(){
//            $('input:checkbox').attr('checked','checked');
//            $(this).val('uncheck all');
//        },function(){
//            $('input:checkbox').removeAttr('checked');
//            $(this).val('check all');        
//        });
        
        $('#data_1 .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: 'dd/mm/yyyy',
                
        });
        
        $('.dataTables-example').dataTable({
            responsive: true,
            
//            "dom": 'T<"clear">lfrtip',
//            "tableTools": {
//                "sSwfPath": "{{ asset('backend/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf') }}"
//            }
        });
        
        $("#pp").click(function(){
            var url = "/reportes/stock/pdf?" + $("#brotes").serialize();
            $(location).attr('href',url);
        });
        
            
            
        $("#brotes").submit(function(e) {

            var url = "stock"; // the script where you handle the form input.
            var t = $('.dataTables-example').DataTable();
                        t.clear();
                        t.draw();
                        
            $.ajax({
                   type: "POST",
                   url: url,
                   data: $("#brotes").serialize(), // serializes the form's elements.
                   success: function(data)
                   {
                        
                        var t = $('.dataTables-example').DataTable();
                        t.clear();
                        t.draw();
                        var suma = 0;
                        var total = 0;
                        
                        var a = JSON.parse(data);

                        for (var key in a) {

                            t.row.add( [
                                a[key]['CODIGO'],
                                a[key]['DESCRIPCION'],
                                (new Intl.NumberFormat().format(a[key]['TOTAL'])) + ' Kg.',
                            ] ).draw( false );

                            total = parseInt(total) + parseInt(a[key]['TOTAL']);
                        }

                        $('#total_general').html("<strong>TOTAL: </strong>"+(new Intl.NumberFormat().format(total)));
                        
                   },
                   error: function(msg){
                       alert(msg);
                       $('#total_general').html("<strong>TOTAL: </strong>0");
                   }
                   
            });

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });
        
    });
</script>

@stop
            
        
        
@endsection
