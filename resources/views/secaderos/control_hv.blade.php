@extends('layouts.backend')

@section('css')
    <link href="{{ asset('backend/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    
    <!-- Data Tables -->
    <link href="{{ asset('backend/css/plugins/dataTables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/plugins/dataTables/dataTables.responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/plugins/dataTables/dataTables.tableTools.min.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="row wrapper border-bottom white-bg ">
    <div class="col-lg-12">
        <h2>Control de Hoja Verde</h2>
        <hr/>
        
        {!! Form::open(['method' => 'post','id' => 'submit']) !!}
        <div class="row">
                
                <!--<label class="col-sm-2 control-label">Bases de Datos</label>-->
                
                <div class="form-group col-md-12">
                    {{ Form::label('base_datos', 'Base de Datos') }}<br/>
                    <label class="checkbox-inline"> 
                        <input type="checkbox" name="aca"  checked=""> ACA 
                    </label> 
                    <label class="checkbox-inline">
                        <input type="checkbox" name="cg"  checked=""> CG 
                    </label> 
                    <label class="checkbox-inline">
                        <input type="checkbox" name="cv"  checked=""> CV 
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="obso"  checked=""> OBSO 
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="ta"  checked=""> TA 
                    </label>
                </div>
                
        </div>
        <div class="row">
            
            
            <div class="form-group col-md-3" >
                {{ Form::label('fecha_desde', 'Fecha Desde') }}
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" id="fecha_desde" name="fecha_desde" value="<?php echo date('d/m/Y'); ?>" class="form-control">
                </div>
                @if ($errors->has('fecha_desde'))
                    <span class="help-block text-warning">
                        <strong>{{ $errors->first('fecha_desde') }}</strong>
                    </span>
                @endif
            </div>
            
            <div class="form-group col-md-3" >
                {{ Form::label('fecha_hasta', 'Fecha Hasta') }}
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" id="fecha_hasta" name="fecha_hasta" value="<?php echo date('d/m/Y'); ?>" class="form-control">
                </div>
                @if ($errors->has('fecha_hasta'))
                    <span class="help-block text-warning">
                        <strong>{{ $errors->first('fecha_hasta') }}</strong>
                    </span>
                @endif
            </div>
            
            <div class="form-group col-md-3">
                {{ Form::label('agrupado_por', 'Arupado Por') }}
                {{ Form::select("agrupado_por", array('semana' => 'Semana','mes' => 'Mes', 'anio' => 'Año'),null, array("class"=>"form-control")) }}
            </div>
            <div class="form-group col-md-3">
                <label class="checkbox-inline">
                    <input type="checkbox" name="derivaciones"  checked=""> <strong>Excluir Derivaciones</strong> 
                </label>
                
            </div>
<!--            <div class="form-group col-md-3">
                {{ Form::label('id_certificado', 'Certificado') }}
                {{ Form::select("id_certificado", array('default' => 'Seleccione...') + array('default' => 'Seleccione...','certificado' => 'Certificado','no_certificado' => 'No Certificado'), null, array("class"=>"form-control")) }}
            </div>-->
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <p class="text-center">(Las características que se obtienen del brote son: TAMAÑO, MALEZAS, HUMEDAD, CALIDAD, ACAROS, TEMPERATURA)</p>
            </div>
            
        </div>
        
        <div class="row">
            <div class="form-group col-md-4">
                <button  type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Buscar</button>
            </div>
        </div>
        {!! Form::close() !!}
        
    </div>
    <br/>
    <br/>
    
    <div class="col-md-12 m-t-md m-b-md">
        <table class="table table-striped table-bordered table-hover dataTables-example " >
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody id="body">
                    
                    
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Total</th>
                    </tr>
                    </tfoot>
                    </table>
                    
    </div>
</div>
@section('javascript')
<!-- Data picker -->
<script src="{{ asset('backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<!-- Data Tables -->
    <script src="{{ asset('backend/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/dataTables/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/dataTables/dataTables.tableTools.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){
//         $('.fixed-table-body').slimScroll({});
        $('.input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: 'dd/mm/yyyy',
                
        });
        
        $('.dataTables-example').dataTable({
                responsive: true,
//                "dom": 'T<"clear">lfrtip',
//                "tableTools": {
//                    "sSwfPath": "{{ asset('backend/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf') }}"
//                }
            });
            
            
//        $("#submit").submit(function(e) {
//
//            var url = "/secaderos/buscar"; // the script where you handle the form input.
//            var t = $('.dataTables-example').DataTable();
//                        t.clear();
//                        t.draw();
//                        
//            $.ajax({
//                   type: "POST",
//                   url: url,
//                   data: $("#submit").serialize(), // serializes the form's elements.
//                   success: function(data)
//                   {
//                        var a = JSON.parse(data);
//                        var t = $('.dataTables-example').DataTable();
//                        t.clear();
//                        t.draw();
//                        var suma = 0;
//                        for (var key in a) {
//                            if (key === 'length' || !a.hasOwnProperty(key)) continue;
//                            var value = a[key];
//                            suma = suma + parseInt(value);
//                            t.row.add( [
//                                key,
//                                value + ' Kg.',
//                            ] ).draw( false );
//                        }
//                        
//                        $('#total_general').html(suma + ' KG');
//                        
//                   },
//                   error: function(msg){
//                       alert(msg);
//                   }
//                   
//                 });
//
//            e.preventDefault(); // avoid to execute the actual submit of the form.
//        });
        
    });
</script>

@stop
            
        
        
@endsection
