@extends('layouts.backend')

@section('css')
    <link href="{{ asset('backend/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    
    <!-- Data Tables -->
    <link href="{{ asset('backend/css/plugins/dataTables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/plugins/dataTables/dataTables.responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/plugins/dataTables/dataTables.tableTools.min.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="row wrapper border-bottom white-bg ">
    <div class="col-lg-12">
        <h2>Recepción de TV y Consumo de CHIP</h2>
        <hr/>
        
        {!! Form::open(['method' => 'post','id' => 'brotes']) !!}
        <div class="row">
            
            
            <div class="form-group col-md-3" id="data_1">
                {{ Form::label('fecha_reporte', 'Fecha de Reporte') }}
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" id="fecha_reporte" name="fecha_reporte" value="<?php echo date('d/m/Y'); ?>" class="form-control">
                </div>
            </div>
            
            <div class="form-group col-md-3">
                {{ Form::label('tv01', 'TV01') }}
                {{ Form::number("tv01", 0, array("class"=>"form-control")) }}
            </div>
            <div class="form-group col-md-3">
                {{ Form::label('tv02', 'TV02') }}
                {{ Form::number("tv02", 0, array("class"=>"form-control")) }}
            </div>
<!--            <div class="form-group col-md-3">
                {{ Form::label('id_certificado', 'Certificado') }}
                {{ Form::select("id_certificado", array('default' => 'Seleccione...') + array('default' => 'Seleccione...','certificado' => 'Certificado','no_certificado' => 'No Certificado'), null, array("class"=>"form-control")) }}
            </div>-->
        </div>
        <div class="row">
            <div class="form-group col-md-3">
                {{ Form::label('chip', 'CHIP') }}
                {{ Form::number("chip", 0, array("class"=>"form-control")) }}
            </div>
            <div class="form-group col-md-6">
                {{ Form::label('observaciones', 'Observaciones') }}
                {{ Form::text("observaciones", null, array("class"=>"form-control", "placeholder"=>"Ingrese alguna observación...")) }}
            </div>
        </div>
        
        <div class="row">
            <div class="form-group col-md-4">
                <button class="btn btn-sm btn-primary"><i class="fa fa-upload"></i> Cargar</button>
            </div>
        </div>
        {!! Form::close() !!}
        
    </div>
    <br/>
    <br/>
    
    <div class="col-md-12 m-t-md m-b-md">
        <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Descripción</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody id="body">
                    
                    
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Descripción</th>
                        <th id="total_general">Total</th>
                    </tr>
                    </tfoot>
                    </table>
                    
    </div>
</div>
@section('javascript')
<!-- Data picker -->
<script src="{{ asset('backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<!-- Data Tables -->
    <script src="{{ asset('backend/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/dataTables/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/dataTables/dataTables.tableTools.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#data_1 .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: 'dd/mm/yyyy',
                
        });
        
        $('.dataTables-example').dataTable({
                responsive: true,
//                "dom": 'T<"clear">lfrtip',
//                "tableTools": {
//                    "sSwfPath": "{{ asset('backend/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf') }}"
//                }
            });
            
            
        $("#brotes").submit(function(e) {

            var url = "brotes"; // the script where you handle the form input.
            var t = $('.dataTables-example').DataTable();
                        t.clear();
                        t.draw();
                        
            $.ajax({
                   type: "POST",
                   url: url,
                   data: $("#brotes").serialize(), // serializes the form's elements.
                   success: function(data)
                   {
                        var a = JSON.parse(data);
                        var t = $('.dataTables-example').DataTable();
                        t.clear();
                        t.draw();
                        var suma = 0;
                        for (var key in a) {
                            if (key === 'length' || !a.hasOwnProperty(key)) continue;
                            var value = a[key];
                            suma = suma + parseInt(value);
                            t.row.add( [
                                key,
                                value + ' Kg.',
                            ] ).draw( false );
                        }
                        
                        $('#total_general').html(suma + ' KG');
                        
                   },
                   error: function(msg){
                       alert(msg);
                   }
                   
                 });

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });
        
    });
</script>

@stop
            
        
        
@endsection
