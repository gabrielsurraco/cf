@extends('layouts.backend')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Synchronization</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li class="active">
                <strong>BD Synchronization</strong>
            </li>
        </ol>
    </div>
</div>

            <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-8">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Available Data Bases</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
<!--                            <a class="btn btn-sm btn-success pull-right" id="sincronizar-todo"><i class="fa fa-refresh"></i> Sync Up All</a>
                            <div id="loadfull" class="sk-spinner sk-spinner-wave" hidden="">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                            </div>-->
                            <table class="table">
                                <thead>
                                <tr >
                                    <th>#</th>
                                    <th>Nombre BD</th>
                                    <th>Usuario</th>
                                    <th>Fecha de Actualización</th>
                                    @if(Auth::user()->hasRole('ADMINISTRADOR'))<th>Acción</th>@endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($bases_access as $key => $value)
                                    <tr>
                                        <td>{{ $value->ID }}</td>
                                        <td>{{ $value->Descripcion }}</td>
                                        <td>{{ $value->Usuario_Sincronizacion }}</td>
                                        <td>{{ $value->Fecha_Sincronizacion }}</td>
                                        @if(Auth::user()->hasRole('ADMINISTRADOR'))
                                        <td id="{{ $value->ID }}">
                                            <div class="sk-spinner sk-spinner-wave" hidden="">
                                                    <div class="sk-rect1"></div>
                                                    <div class="sk-rect2"></div>
                                                    <div class="sk-rect3"></div>
                                                    <div class="sk-rect4"></div>
                                                    <div class="sk-rect5"></div>
                                            </div>
                                            <a class="btn btn-default btn-xs sincronizar" ><i class="fa fa-refresh"></i> Sincronizar</a>
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
	</div>
        @section('javascript')
            <script type="text/javascript">
               $(document).ready(function(){
                //  var x = document.cookie;
                //  alert(x);

                   var token = $("meta[name='csrf-token']").attr("content");

                   $(".sincronizar").click(function(e){
                        e.preventDefault();
                        e.stopPropagation();
                        var id_a_sincronizar = $(this).parent().attr("id");
                        var td = $(this).parent();
                        var boton = $(this);

                       $.ajax({
                           type: "POST",
                           url: "/sincronizar/base",
                           async: true,
                           data: {_token: token, id_bd: id_a_sincronizar},
                           success: function(data){
                               alert(data.msg);
                               $("#"+id_a_sincronizar).parent().find("td:eq(2)").text(data.actualizacion.usuario);
                               $("#"+id_a_sincronizar).parent().find("td:eq(3)").text(data.actualizacion.fecha);
                           },
                           error: function(data){
                               alert('Error:', data.msg);
                           },
                           beforeSend: function(data){
                                td.find(".sk-spinner").removeAttr('hidden');
                                boton.css({ 'display': "none" });
                           },
                           complete: function(data) {
                                td.find(".sk-spinner").css({ 'display': "none" });
                                boton.removeAttr("style");
                            }
                       });
                   });

//                    $('#ACA').click(function(){
//                        var _token = $('input[name="_token"]').val();
//
//                        $.ajax({
//                            type: "POST",
//                            url: 'sincronizar/aca',
//                            async: true,
//                            data: {_token : _token},
//                            success: function( msg ) {
//                                alert(msg);
//                            },
//                            error: function (data) {
//                                alert('Error:', data);
//                            },
//                            beforeSend: function(a) {
//                                $('#loadaca').removeAttr('hidden');
//                                $("#ACA").css({ 'display': "none" });
//
//                            },
//                            complete: function(){
//                                $('#loadaca').css({ 'display': "none" });
//                                $("#ACA").removeAttr("style");
//                            }
//
//                        });
//                    });
//
//                    $('#CG').click(function(){
//                        var _token = $('input[name="_token"]').val();
//
//                        $.ajax({
//                            type: "POST",
//                            url: 'sincronizar/cg',
//                            async: true,
//                            data: {_token : _token},
//                            success: function( msg ) {
//                                alert(msg);
//                            },
//                            error: function (data) {
//                                alert('Error:', data);
//                            },
//                            beforeSend: function(a) {
//                                $('#loadcg').removeAttr('hidden');
//                                $("#CG").css({ 'display': "none" });
//                            },
//                            complete: function(){
//                                $('#loadcg').css({ 'display': "none" });
//                                $("#CG").removeAttr("style");
//                            }
//
//                        });
//                    });
//
//                    $('#CV').click(function(){
//                        var _token = $('input[name="_token"]').val();
//
//                        $.ajax({
//                            type: "POST",
//                            url: 'sincronizar/cv',
//                            async: true,
//                            data: {_token : _token},
//                            success: function( msg ) {
//                                alert(msg);
//                                console.log(JSON.parse(msg));
//                            },
//                            error: function (data) {
//                                alert('Error:', data);
//                            },
//                            beforeSend: function(a) {
//                                $('#loadcv').removeAttr('hidden');
//                                $("#CV").css({ 'display': "none" });
//
//                            },
//                            complete: function(data){
//                                $('#loadcv').css({ 'display': "none" });
//                                $("#CV").removeAttr("style");
//
//                            }
//
//                        });
//                    });
//
//                    $('#OBSO').click(function(){
//                        var _token = $('input[name="_token"]').val();
//
//                        $.ajax({
//                            type: "POST",
//                            url: 'sincronizar/obso',
//                            async: true,
//                            data: {_token : _token},
//                            success: function( msg ) {
//                                alert(msg);
//                            },
//                            error: function (data) {
//                                alert('Error:', data);
//                            },
//                            beforeSend: function(a) {
//                                $('#loadobso').removeAttr('hidden');
//                                $("#OBSO").css({ 'display': "none" });
//
//                            },
//                            complete: function(){
//                                $('#loadobso').css({ 'display': "none" });
//                                $("#OBSO").removeAttr("style");
//                            }
//
//                        });
//                    });
//
//                    $('#TA').click(function(){
//                        var _token = $('input[name="_token"]').val();
//
//                        $.ajax({
//                            type: "POST",
//                            url: 'sincronizar/ta',
//                            async: true,
//                            data: {_token : _token},
//                            success: function( msg ) {
//                                alert(msg);
//                            },
//                            error: function (data) {
//                                alert('Error:', data);
//                            },
//                            beforeSend: function(a) {
//                                $('#loadta').removeAttr('hidden');
//                                $("#TA").css({ 'display': "none" });
//
//                            },
//                            complete: function(){
//                                $('#loadta').css({ 'display': "none" });
//                                $("#TA").removeAttr("style");
//                            }
//
//                        });
//                    });

                    $('#sincronizar-todo').click(function(){
                        var _token = $('input[name="_token"]').val();

                        $.ajax({
                            type: "POST",
                            url: 'sincronizar/full',
                            async: false,
                            data: {_token : _token},
                            success: function( msg ) {
                                alert(msg);
                            },
                            error: function (data) {
                                alert('Error:', data);
                            },
                            beforeSend: function(a) {
                                $('#loadfull').removeAttr('hidden');
                                $("#sincronizar-todo").css({ 'display': "none" });

                            },
                            complete: function(){
                                $('#loadfull').css({ 'display': "none" });
                                $("#sincronizar-todo").removeAttr("style");
                            }

                        });
                    });


               });
            </script>
        @stop

@endsection
