@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        
        <div class="col-lg-8 m-t-lg">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Formatear Archivo</h5>
                    <div ibox-tools=""></div>
                </div>
                <div class="ibox-content">
                    {!! Form::open(['url' => '/tools/formatear','method' => 'post','id' => 'brotes']) !!}
                        {!! Form::file('image') !!}
                        <div class="m-t-md"></div>
                        <a><button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-rub"></i> Procesar</button></a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>



@endsection

@section('javascript')
<script type="text/javascript">
    var barData = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
            {
                label: "asd",
                fillColor: "rgba(220,220,220,0.5)",
                strokeColor: "rgba(220,220,220,0.8)",
                highlightFill: "rgba(220,220,220,0.75)",
                highlightStroke: "rgba(220,220,220,1)",
                data: [65, 59, 80, 81, 56, 55, 40],
            },
            {
                label: "dd",
                fillColor: "rgba(26,179,148,0.5)",
                strokeColor: "rgba(26,179,148,0.8)",
                highlightFill: "rgba(26,179,148,0.75)",
                highlightStroke: "rgba(26,179,148,1)",
                data: [28, 48, 40, 19, 86, 27, 90],
            }
        ]
    };

    var barOptions = {
        scaleBeginAtZero: true,
        scaleShowGridLines: true,
        scaleGridLineColor: "rgba(0,0,0,.05)",
        scaleGridLineWidth: 1,
        barShowStroke: true,
        barStrokeWidth: 2,
        barValueSpacing: 5,
        barDatasetSpacing: 1,
        responsive: true,
        label: 'prueba'
    }


    var ctx = document.getElementById("barChart").getContext("2d");
    var myNewChart = new Chart(ctx).Bar(barData, barOptions);
    

</script>
@stop
