@extends('layouts.backend')
@section('css')
<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('backend/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('backend/css/plugins/select2/select2-bootstrap.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/plugins/footable/footable.core.css') }}" rel="stylesheet">

<style>
    .select2-close-mask{
        z-index: 2099;
    }
    .select2-dropdown{
        z-index: 3051;
    }
</style>
@stop

@section('content')

@if (Session::has('msg'))
    <div class="row ">
        <div class="col-md-12 m-t-md">
            <div class="alert alert-info ">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!! Session::get('msg') !!}
            </div>
        </div>
    </div>
@endif


<div class="row">
    <div class="col-md-12">
        <div class="panel panel-success m-b-lg m-t-md">
            <div class="panel-heading">Usuarios</div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn btn-sm btn-success" href="/register"><i class="fa fa-plus-square"></i> Nuevo Usuario</a>
                    </div>
                </div>
                <div class="row m-t-md">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <input type="text" class="form-control input-sm m-b-xs " id="filter2" placeholder="Buscar en tabla...">
                            <table class="footable6 table table-stripped" data-page-size="8" data-filter=#filter2>
                                <thead>
                                    <tr>
                                        <th>Nombre y Apellido</th>
                                        <th>Email</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody class="body-permisos">
                                    @foreach($usuarios as $usuario => $value)
                                        <tr class="gradeX tr-permiso">
                                            <td style="width: 40%;">{{ $value->name }}</td>
                                            <td style="width: 40%;">{{ $value->email }}</td>
                                            <td style="width: 20%;" id_usuario='{{ $value->id }}'>
                                                <a class="btn btn-xs btn-primary" href="/usuarios/{{ $value->id }}/editar"><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-xs btn-danger eliminar_usuario"><i class="fa fa-trash"></i></a>
                                                <a class="btn btn-xs btn-default roles" data-toggle="modal" data-target="#ver_roles"><i class="fa fa-eye-slash"></i> Roles</a>
                                                <a class="btn btn-xs btn-default permisos" data-toggle="modal" data-target="#ver_permisos"><i class="fa fa-eye"></i> Permisos</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="5" style="text-align: center;">
                                            <ul class="pagination"></ul>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="ver_permisos" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Permisos</h4>
            </div>
            
            <div class="modal-body">
                
                <div class="input-group rol_id_aca">
                    <select name="permiso" class="form-control permiso" style="width: 100%; height: 10px !important;">
                        <option value=""></option>
                        @foreach($permisos as $permiso => $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                    </select>
                    <span class="input-group-btn "><a class="btn btn-primary add_permiso" id_usuario=""> Agregar Permiso</a></span>
                </div>
                
                    <input type="text" class="form-control input-sm m-b-xs m-t-md" id="filter4"
                       placeholder="Buscar en tabla...">

                <table class="footable8 table table-stripped" data-page-size="8" data-filter=#filter4>
                    <thead>
                        <tr>
                            <th>Descripción del Permiso</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody class="body_permisos">
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5" style="text-align: center;">
                                <ul class="pagination"></ul>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="ver_roles" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Roles</h4>
            </div>
            
            <div class="modal-body">
                <div class="input-group ">
                    <select name="permiso" class="form-control rol" style="width: 100%; height: 10px !important;">
                        <option value=""></option>
                        @foreach($roles as $rol => $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                    </select>
                    <span class="input-group-btn "><a class="btn btn-primary add_rol" id_usuario=""> Agregar Rol</a></span>
                </div>
                
                    <input type="text" class="form-control input-sm m-b-xs m-t-md" id="filter4"
                       placeholder="Buscar en tabla...">

                <table class="footable7 table table-stripped" data-page-size="8" data-filter=#filter4>
                    <thead>
                        <tr>
                            <th>Descripción del Rol</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody class="body_rol">
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5" style="text-align: center;">
                                <ul class="pagination"></ul>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<!-- FooTable -->
<script src="{{ asset('/backend/js/plugins/footable/footable.all.min.js') }}"></script>

<!-- Sweet alert -->
<script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('backend/js/plugins/select2/select2.min.js') }} "></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.footable6').footable();
        $('.footable7').footable();
        $('.footable8').footable();
        
        var token = $("meta[name='csrf-token']").attr("content");
    
        $(".permiso").select2({
            theme: "bootstrap",
            placeholder: "Seleccione el Permiso...",
            allowClear: true,
            dropdownParent: $("#ver_permisos")
        });
        
        $(".rol").select2({
            theme: "bootstrap",
            placeholder: "Seleccione el Rol...",
            allowClear: true,
            dropdownParent: $("#ver_roles")
        });
        
        //ver roles del usuario
        $(".roles").click(function(){
            id_usuario = $(this).parent().attr("id_usuario");
            $(".add_rol").attr("id_usuario", id_usuario);
            
            $.ajax({
                type: "POST",
                url: "/usuarios/get-roles",
                data: {_token: token, id_usuario: id_usuario},
                success: function(data){
                    $.each(data, function(key, value){
                        boton = $("<a title='Eliminar Rol'><i class='fa fa-trash-o'></i></a>",true);
                        boton.addClass("btn btn-xs btn-danger eliminar_rol");
                        tr = $("<tr class='gradeX'><td style='width:70%;'>"+value.name+"</td><td style='width: 30%' id_rol='"+value.id+"' id_usuario='"+value.pivot.user_id+"'></td></tr>");
                        tr.find("td:eq(1)").append(boton);
                        $(".body_rol").append(tr);
                    });
                    
                    $('.footable7').trigger('footable_initialize');
                },
                beforeSend: function(){
                    var body = $(".body_rol");
                    body.empty();
                },
                error: function(data){
                    alert(data);
                }
            });
            
        });
        
        //ver permisos del usuario
        $(".permisos").click(function(){
            id_usuario = $(this).parent().attr("id_usuario");
            $(".add_permiso").attr("id_usuario", id_usuario);
            
            $.ajax({
                type: "POST",
                url: "/usuarios/get-permisos",
                data: {_token: token, id_usuario: id_usuario},
                success: function(data){
                    $.each(data, function(key, value){
                        boton = $("<a title='Eliminar Rol'><i class='fa fa-trash-o'></i></a>",true);
                        boton.addClass("btn btn-xs btn-danger eliminar_permiso");
                        tr = $("<tr class='gradeX'><td style='width:70%;'>"+value.name+"</td><td style='width: 30%' id_permiso='"+value.id+"' id_usuario='"+value.pivot.user_id+"'></td></tr>");
                        tr.find("td:eq(1)").append(boton);
                        $(".body_permisos").append(tr);
                    });
                    
                    $('.footable8').trigger('footable_initialize');
                },
                beforeSend: function(){
                    var body = $(".body_permisos");
                    body.empty();
                },
                error: function(data){
                    alert(data);
                }
            });
        });
        
        $(".eliminar_usuario").click(function(){
            id_usuario = $(this).parent().attr("id_usuario");
            var tr = $(this).parent().parent();
            swal({  title: "Está seguro?",
                    //text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirmar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: true,
                    closeOnCancel: true },
                function (isConfirm) {
                    if (isConfirm) {
                        var token = $("meta[name='csrf-token']").attr("content");
                        
                        $.ajax({
                            type: "POST",
                            url: '/usuarios/eliminar',
                            data: {_token: token , id_usuario: id_usuario},
                            success: function(data){
                                tr.empty();
                                $('.footable6').trigger('footable_initialize');
                                swal("Eliminado!", data, "success");
                            },
                            error: function(data){
                                swal("Error", data, "error");
                            }
                        });
                    }
            });
        });
        
        $('.body_rol').on("click","a.eliminar_rol",function(){
            id_rol = $(this).parent().attr("id_rol");
            id_usuario = $(this).parent().attr("id_usuario");
            var tr = $(this).parent();
            swal({  title: "Está seguro?",
                    //text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirmar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: true,
                    closeOnCancel: true },
                function (isConfirm) {
                    if (isConfirm) {
                        var token = $("meta[name='csrf-token']").attr("content");
                        
                        $.ajax({
                            type: "POST",
                            url: '/usuarios/eliminar-rol',
                            data: {_token: token , id_rol: id_rol, id_usuario: id_usuario},
                            success: function(data){
                                tr.parent().empty();
                                $('.footable7').trigger('footable_initialize');
                                swal("Eliminado!", data.msg, "success");
                            },
                            error: function(data){
                                swal("Error", data.msg, "error");
                            }
                        });
                    }
                });
            
        });
        
        $('.body_permisos').on("click", "a.eliminar_permiso" , function(){
            id_permiso = $(this).parent().attr("id_permiso");
            id_usuario = $(this).parent().attr("id_usuario");
            var tr = $(this).parent();
            swal({  title: "Está seguro?",
                    //text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirmar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: true,
                    closeOnCancel: true },
                function (isConfirm) {
                    if (isConfirm) {
                        var token = $("meta[name='csrf-token']").attr("content");
                        
                        $.ajax({
                            type: "POST",
                            url: '/usuarios/eliminar-permiso',
                            data: {_token: token , id_permiso: id_permiso, id_usuario: id_usuario},
                            success: function(data){
                                tr.parent().empty();
                                $('.footable8').trigger('footable_initialize');
                                swal("Eliminado!", data.msg, "success");
                            },
                            error: function(data){
                                swal("Error", data.msg, "error");
                            }
                        });
                    }
                });
        });
        
        $(".add_permiso").click(function(){
            
            id_usuario = $(this).attr("id_usuario");
            id_permiso = $(".permiso").val();
            id_permiso_texto = $(".permiso option:selected").text();
            
            $.ajax({
                type: "POST",
                url: "/usuarios/agregar-permiso",
                data: {_token: token, id_permiso: id_permiso, id_usuario: id_usuario},
                success: function(data){
                    tr = $("<tr><td style='width:70%;'>"+id_permiso_texto+"</td><td style='width:30%;' id_permiso='"+id_permiso+"' id_usuario='"+id_usuario+"'><a title='Eliminar Permiso' class='btn btn-xs btn-danger eliminar_permiso'><i class='fa fa-trash-o'></i></a></td></tr>")
                    $('.body_permisos').append(tr);
                    $('.footable8').trigger('footable_initialize');
                    swal("Éxito!", data.msg, "success");
                },
                error: function(data){
                    swal("Error!", data.responseJSON.msg, "error");
                }
            });
            
        });
        
        $(".add_rol").click(function(){
            
            id_usuario = $(this).attr("id_usuario");
            id_rol = $(".rol").val();
            id_rol_texto = $(".rol option:selected").text();
            
            $.ajax({
                type: "POST",
                url: "/usuarios/agregar-rol",
                data: {_token: token, id_rol: id_rol, id_usuario: id_usuario},
                success: function(data){
                    tr = $("<tr><td style='width:70%;'>"+id_rol_texto+"</td><td style='width:30%;' id_rol='"+id_rol+"' id_usuario='"+id_usuario+"'><a title='Eliminar Rol' class='btn btn-xs btn-danger eliminar_rol'><i class='fa fa-trash-o'></i></a></td></tr>")
                    $('.body_rol').append(tr);
                    $('.footable7').trigger('footable_initialize')
                    swal("Éxito!", data.msg, "success");
                },
                error: function(data){
                    swal("Error!", data.responseJSON.msg, "error");
                }
            });
            
        });
        
    });
</script>
@stop