@extends('layouts.backend')
@section('css')

<!-- FooTable -->
<link href="{{ asset('/backend/css/plugins/footable/footable.core.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('backend/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('backend/css/plugins/select2/select2-bootstrap.css') }}" rel="stylesheet">

<style>
    .select2-close-mask{
        z-index: 2099;
    }
    .select2-dropdown{
        z-index: 3051;
    }
</style>
@stop

@section('content')

@if (Session::has('msg'))
<div class="container">
    <div class="row ">
        <div class="col-md-12 ">
            <div class="alert alert-info ">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!! Session::get('msg') !!}
            </div>
        </div>
    </div>
</div>
@endif

<!--<div class="container">-->

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="panel panel-success m-b-lg m-t-md">
                <div class="panel-heading">Roles y Permisos</div>
                <div class="panel-body">

                    <div class="row ">

                        <div class="col-md-6">
                            <div class="table-responsive">
                                <h3 style="border-bottom: 1px solid #ddd; padding-bottom: 5px;">Roles</h3>
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm m-b-xs " id="filter"
                                       placeholder="Buscar en tabla...">
                                    <span class="input-group-btn">
                                        <a class="btn btn-sm btn-success nuevo_rol_btn" data-toggle="modal" data-target="#nuevoRol"><i class="fa fa-plus"> Nuevo</i></a>
                                   </span>
                                </div>




                                <table class="footable5 table table-stripped" data-page-size="8" data-filter=#filter>
                                    <thead>
                                        <tr>
                                            <th>Descripción del Rol</th>
                                            <th>Fecha de Creación</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody class="body-roles">
                                        <tr class="gradeX tr-role hidden">
                                            <td style="width: 40%;">false</td>
                                            <td style="width: 30%;">false</td>
                                            <td style="width: 30%;" id_rol="1">
                                                <a class="btn btn-xs btn-danger eliminar_rol" title="Eliminar Rol"><i class="fa fa-trash-o"></i></a>
                                                <a data-toggle="modal" data-target="#nuevoRol" class="btn btn-xs btn-primary editar_rol_btn" title="Editar Rol"><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-xs btn-default ver_permisos" data-toggle="modal" data-target="#ver_permisos" title="Ver Permisos"><i class="fa fa-eye"></i></a>
                                            </td>
                                        </tr>
                                        @foreach($roles as $rol => $value)
                                            <tr class="gradeX" >
                                                <td style="width: 40%;">{{ $value->name }}</td>
                                                <td style="width: 30%;">{{ $value->created_at }}</td>
                                                <td style="width: 30%;" id_rol="{{ $value->id }}">
                                                    <a class="btn btn-xs btn-danger eliminar_rol" title="Eliminar Rol"><i class="fa fa-trash-o"></i></a>
                                                    <a data-toggle="modal" data-target="#nuevoRol" class="btn btn-xs btn-primary editar_rol_btn" title="Editar Rol"><i class="fa fa-edit"></i></a>
                                                    <a class="btn btn-xs btn-default ver_permisos" data-toggle="modal" data-target="#ver_permisos" title="Ver Permisos"><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="5" style="text-align: center;">
                                                <ul class="pagination"></ul>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>


                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="table-responsive">
                                <h3 style="border-bottom: 1px solid #ddd; padding-bottom: 5px;">Permisos</h3>


                                <div class="input-group">
                                    <input type="text" class="form-control input-sm m-b-xs " id="filter2"
                                       placeholder="Buscar en tabla...">
                                    <span class="input-group-btn">
                                        <a class="btn btn-sm btn-success nuevo_permiso_btn" data-toggle="modal" data-target="#nuevoPermiso"><i class="fa fa-plus"> Nuevo</i></a>
                                    </span>
                                </div>
                                <table class="footable2 table table-stripped" data-page-size="8" data-filter=#filter2>
                                    <thead>
                                        <tr>
                                            <th>Descripción del Permiso</th>
                                            <th>Fecha de Creación</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody class="body-permisos">
                                        <tr class="gradeX tr-permiso hidden" id_permiso="">
                                            <td style="width: 40%;">false</td>
                                            <td style="width: 30%;">false</td>
                                            <td style="width: 30%;" id_permiso="" >
                                                <a class="btn btn-xs btn-danger eliminar_permiso" title="Eliminar Permiso"><i class="fa fa-trash-o"></i></a>
                                                <a data-toggle="modal" data-target="#nuevoPermiso" class="btn btn-xs btn-primary editar_permiso_btn" title="Editar Permiso"><i class="fa fa-edit"></i></a>
                                            </td>
                                        </tr>
                                        @foreach($permisos as $permiso => $value)
                                            <tr class="gradeX" id_permiso="{{ $value->id }}">
                                                <td style="width: 40%;">{{ $value->name }}</td>
                                                <td style="width: 30%;">{{ $value->created_at }}</td>
                                                <td style="width: 30%;" id_permiso="{{ $value->id }}" >
                                                    <a class="btn btn-xs btn-danger eliminar_permiso" title="Eliminar Permiso"><i class="fa fa-trash-o"></i></a>
                                                    <a data-toggle="modal" data-target="#nuevoPermiso" class="btn btn-xs btn-primary editar_permiso_btn" title="Editar Permiso"><i class="fa fa-edit"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="5" style="text-align: center;">
                                                <ul class="pagination"></ul>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--</div>-->

<div class="modal inmodal fade" id="nuevoRol" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Nuevo Rol</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form-rol">
                    {{ Form::label('nombre','Nombre del Rol') }}
                    {{ Form::text('nombre',null,["class" => "form-control nombre-rol","placeholder" => "Descripción..."]) }}
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                <button type="button" accion="crear" ruta="" id_a_modificar="" class="btn btn-primary guardar-rol">Guardar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal fade" id="nuevoPermiso" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Nuevo Permiso</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form-permiso">
                    {{ Form::label('nombre','Nombre del Permiso') }}
                    {{ Form::text('nombre',null,["class" => "form-control nombre-permiso","placeholder" => "Descripción..."]) }}
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                <button type="button" accion="crear" ruta="" id_a_modificar="" class="btn btn-primary guardar-permiso">Guardar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="ver_permisos" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Permisos del Rol</h4>
            </div>

            <div class="modal-body">

                <div class="input-group rol_id_aca">
                    <select name="permiso" class="form-control permiso" style="width: 100%; height: 10px !important;">
                        <option value=""></option>
                        @foreach($permisos as $permiso => $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                    </select>
                    <span class="input-group-btn "><a class="btn btn-primary add_permission_to_rol"> Agregar Permiso</a></span>
                </div>

                    <input type="text" class="form-control input-sm m-b-xs m-t-md" id="filter4"
                       placeholder="Buscar en tabla...">

                <table class="footable4 table table-stripped" data-page-size="8" data-filter=#filter4>
                    <thead>
                        <tr>
                            <th>Descripción del Permiso</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody class="body-permisos-derol">
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5" style="text-align: center;">
                                <ul class="pagination"></ul>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<!-- FooTable -->
<script src="{{ asset('/backend/js/plugins/footable/footable.all.min.js') }}"></script>

<!-- Sweet alert -->
<script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('backend/js/plugins/select2/select2.min.js') }} "></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.footable4').footable();
        $('.footable2').footable();
        $('.footable5').footable();
        var token = $("meta[name='csrf-token']").attr("content");

        $(".add_permission_to_rol").click(function(){
            var val_select = $(".permiso");
            var id_rol = $(this).parent().parent().attr("id_rol");
            if(val_select.val() === ""){
                alert("Seleccione un permiso!");
            }else{
                $.ajax({
                    type: "POST",
                    url: '/roles-y-permisos/assign-permission-to-role',
                    data: {_token: token, id_permiso: val_select.val(), id_rol: id_rol },
                    success: function(data){
                        console.log(data);
                        tr = $("<tr><td style='width:70%;'>"+val_select.find("option:selected").text()+"</td><td style='width:30%;' id_permiso='"+val_select.val()+"' id_rol='"+id_rol+"'><a title='Eliminar Permiso' class='btn btn-xs btn-danger eliminar_permiso_derol'><i class='fa fa-trash-o'></i></a></td></tr>")
                        $('.body-permisos-derol').append(tr);
                        $('.footable4').trigger('footable_initialize');
                    },
                    error: function(data){
                       alert(data.responseJSON.msg);
                    }
                });
            }
        });

        $(".nuevo_permiso_btn").click(function(){
            $("#nuevoPermiso .modal-title").html("Nuevo Permiso");
            $("#nuevoPermiso input").val("");
            $("#nuevoPermiso .guardar-permiso").attr("accion","crear");
            $("#nuevoPermiso .guardar-permiso").attr("ruta","/roles-y-permisos/crear-permiso");
        });

        $(".nuevo_rol_btn").click(function(){
            $("#nuevoRol .modal-title").html("Nuevo Rol");
            $("#nuevoRol input").val("");
            $("#nuevoRol .guardar-rol").attr("accion","crear");
            $("#nuevoRol .guardar-rol").attr("ruta","/roles-y-permisos/crear-rol");
        });

        $(".editar_permiso_btn").click(function(){
            $("#nuevoPermiso .modal-title").html("Editar Permiso");
            permiso_name = $(this).parent().parent().find("td:eq(0)").text();
            id_permiso = $(this).parent().parent().find("td:eq(2)").attr("id_permiso");
            $("#nuevoPermiso input").val(permiso_name);
            $("#nuevoPermiso .guardar-permiso").attr("accion","editar");
            $("#nuevoPermiso .guardar-permiso").attr("ruta","/roles-y-permisos/editar-permiso");
            $("#nuevoPermiso .guardar-permiso").attr("id_a_modificar",id_permiso);
        });

        $(".editar_rol_btn").click(function(){
            $("#nuevoRol .modal-title").html("Editar Rol");
            rol_name = $(this).parent().parent().find("td:eq(0)").text();
            id_rol = $(this).parent().parent().find("td:eq(2)").attr("id_rol");
            $("#nuevoRol input").val(rol_name);
            $("#nuevoRol .guardar-rol").attr("accion","editar");
            $("#nuevoRol .guardar-rol").attr("ruta","/roles-y-permisos/editar-rol");
            $("#nuevoRol .guardar-rol").attr("id_a_modificar", id_rol);

        });

        $('.body-permisos-derol').on("click","a.eliminar_permiso_derol",function(){
            var a_borrar = $(this).parent().parent();
            id_permiso = $(this).parent().attr("id_permiso");
            id_rol = $(this).parent().attr("id_rol");
            $.ajax({
                 type: "POST",
                 url: '/roles-y-permisos/delete-permissions-of-role',
                 data: {_token: token, id_rol: id_rol,id_permiso: id_permiso},
                 success: function(data){
                     $('#ver_permisos').modal('toggle');
                     a_borrar.empty();
                     swal("Permiso Desafectado!", null, "success");
                 },
                 error: function(data){
                     swal("Error al eliminar el permiso!", null, "error");
                 }
             });
        });

        $(".ver_permisos").click(function(){
            id_rol = $(this).parent().parent().find("td:eq(2)").attr("id_rol");
            $(".rol_id_aca").attr("id_rol",id_rol)


            $.ajax({
                type: "POST",
                url: '/roles-y-permisos/get-permissions-of-role',
                data: {_token: token, id_rol: id_rol},
                success: function(data){
                    var body = $(".body-permisos-derol");

                    $.each(data,function(key, value){
                        boton = $("<a title='Eliminar Permiso'><i class='fa fa-trash-o'></i></a>",true);
                        boton.addClass("btn btn-xs btn-danger eliminar_permiso_derol");
                        tr = $("<tr class='gradeX tr-permiso-derol'><td style='width:70%;'>"+value.name+"</td><td style='width: 30%' id_permiso='"+value.id+"' id_rol='"+id_rol+"'></td></tr>");
                        tr.find("td:eq(1)").append(boton);
                        body.append(tr);
                    })

                    $('.footable4').trigger('footable_initialize');

                },
                beforeSend: function(){
                    var body = $(".body-permisos-derol");
                    body.empty();
                },
                error: function(data){
                    console.log(data);
                }
            });
        });

        $('.guardar-rol').click(function(){

            var nombre_rol = $('.nombre-rol').val();

            if(typeof nombre_rol === "undefined" || nombre_rol === ""){
                alert("Ingrese valor");
                return false;
            }

            var accion = $("#nuevoRol .guardar-rol").attr("accion");
            var url = $("#nuevoRol .guardar-rol").attr("ruta");
            var id_rol = $("#nuevoRol .guardar-rol").attr("id_a_modificar");

            if(accion === "crear"){
                datos = { _token: token, name: nombre_rol};
            }else if(accion === "editar"){
                datos = { _token: token, name: nombre_rol, id_rol: id_rol};
            }


            function create_rol_row (item) {
                var row = $('.tr-role').clone(true);
                row.removeClass("hidden");
                row.removeClass("tr-role");
                row.find("td:eq(0)").html(item.name);
                row.find("td:eq(1)").html(item.created_at);
                row.find("td:eq(2)").attr("id_rol", item.id);
                return row;
            }

            $.ajax({
                type: "POST",
                url:  url,
                data: datos,
                success: function(data){
                    $('#nuevoRol').modal('toggle');
                    $('.nombre-rol').val("");

                    if(accion === "crear"){
                        var row = create_rol_row(data);
                        $('.body-roles').append(row);
                        $('.footable5').trigger('footable_initialize');
                        swal("Rol Creado!", null, "success");
                    }else if(accion === "editar"){
                        $("td[id_rol='"+id_rol+"']").parent().find("td:eq(0)").text(nombre_rol);
                        $('.footable5').trigger('footable_initialize');
                        swal("Rol Modificado!", null, "success");
                    }


                },
                error: function(data){
                    $('#nuevoRol').modal('toggle');
                    swal("Error!", data, "error");
                }
            });

        });

        $('.guardar-permiso').click(function(){

            var nombre_permiso = $('.nombre-permiso').val();

            if(typeof nombre_permiso === "undefined" || nombre_permiso === ""){
                alert("Ingrese valor");
                return false;
            }

            var accion = $("#nuevoPermiso .guardar-permiso").attr("accion");
            var url = $("#nuevoPermiso .guardar-permiso").attr("ruta");
            var id_permiso = $("#nuevoPermiso .guardar-permiso").attr("id_a_modificar");

            if(accion === "crear"){
                datos = { _token: token, name: nombre_permiso};
            }else if(accion === "editar"){
                datos = { _token: token, name: nombre_permiso, id_permiso: id_permiso};
            }

            function create_permiso_row (item) {
                var row = $('.tr-permiso').clone(true);
                row.removeClass("hidden");
                row.removeClass("tr-permiso");
                row.find("td:eq(0)").html(item.name);
                row.find("td:eq(1)").html(item.created_at);
                row.find("td:eq(2)").attr("id_permiso", item.id);
                return row;
            }

            $.ajax({
                type: "POST",
                url:  url,
                data: datos,
                success: function(data){

                    $('#nuevoPermiso').modal('toggle');
                    $('.nombre-permiso').val("");

                    if(accion === "crear"){
                        var row = create_permiso_row(data);
                        $('.body-permisos').append(row);
                        $('.footable2').trigger('footable_initialize');
                        swal("Permiso Creado!", null, "success");
                    }else if(accion === "editar"){
                        $("td[id_permiso='"+id_permiso+"']").parent().find("td:eq(0)").text(nombre_permiso);
                        $('.footable2').trigger('footable_initialize');
                        swal("Permiso Modificado!", null, "success");
                    }

                },
                error: function(data){
                    $('#nuevoPermiso').modal('toggle');
                    console.log(data);
                    swal("Error!", data, "error");
                }
            });

        });

        $('.eliminar_rol').click(function () {
            var rol_id = $(this).parent().attr("id_rol");
            var tr = $(this).parent();
            swal({  title: "Está seguro?",
                    //text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirmar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: true,
                    closeOnCancel: true },
                function (isConfirm) {
                    if (isConfirm) {
                        var token = $("meta[name='csrf-token']").attr("content");

                        $.ajax({
                            type: "POST",
                            url: '/roles-y-permisos/eliminar-rol',
                            data: {_token: token , rol_id: rol_id},
                            success: function(data){
                                tr.parent().empty();
                                $('.footable').trigger('footable_initialize');
                                swal("Eliminado!", data, "success");
                            },
                            error: function(data){
                                swal("Error", data, "error");
                            }
                        });
                    }
                });
        });

        $('.eliminar_permiso').click(function () {
            var id_permiso = $(this).parent().attr("id_permiso");
            var tr = $(this).parent();
            swal({  title: "Está seguro?",
                    //text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirmar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: true,
                    closeOnCancel: true },
                function (isConfirm) {
                    if (isConfirm) {
                        var token = $("meta[name='csrf-token']").attr("content");

                        $.ajax({
                            type: "POST",
                            url: '/roles-y-permisos/eliminar-permiso',
                            data: {_token: token , id_permiso: id_permiso},
                            success: function(data){
                                tr.parent().empty();
                                $('.footable2').trigger('footable_initialize');
                                swal("Eliminado!", data, "success");
                            },
                            error: function(data){
                                swal("Error", data, "error");
                            }
                        });
                    }
            });
        });

        $(".permiso").select2({
            theme: "bootstrap",
            placeholder: "Seleccione el Permiso...",
            allowClear: true,
            dropdownParent: $("#ver_permisos")
        });


    });
</script>
@stop
