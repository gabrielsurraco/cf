<?php

use Illuminate\Http\Request;
use App\Model\ACA\ACA_Rodado;
use App\Model\ACA\ACA_Producto;
use App\Planta;
use App\Model\ACA\ACA_Chacra;
use App\Model\ACA\ACA_Proveedor;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/get-camion/', function (Request  $request) {
    
    if ($request->ajax()){
        
        try {
             $page = Input::get('page');
            $resultCount = 25;

            $offset = ($page - 1) * $resultCount;

            $breeds =   ACA_Rodado::select('id',DB::raw("codigo+' - '+descripcion AS text"))
                        ->where('id_bd', $request->planta)
                        ->where('codigo', 'LIKE',  '%' . Input::get("q"). '%')
                        ->orderBy('codigo')
                        ->skip($offset)
                        ->take($resultCount)
                        ->get();

            $count = ACA_Rodado::where('id_bd', $request->planta)
                        ->where('codigo', 'LIKE',  '%' . Input::get("q"). '%')->count();

            $endCount = $offset + $resultCount;

            if($count < 25)
                $morePages = false;
            else
                $morePages = $endCount < $count;

            $results = array(
              "results" => $breeds,
              "pagination" => array(
                "more" => $morePages
              )
            );
        } catch (Exception $ex) {
            return response()->json($ex->getMessage(),500);
        }
           
            
        return response()->json($results,200);
    }
});

Route::get('/get-producto/', function (Request  $request) {
    
    if ($request->ajax()){
        
        try {
            $page = Input::get('page');
            $resultCount = 25;
            $term = Input::get("q");
            $id_bd = $request->planta;

            $offset = ($page - 1) * $resultCount;

            $breeds =   DB::connection('EXPOTEC_STOCK')
            ->table('ARTICULOS')
            ->select('ID as id',DB::raw("Codigo+' - '+Descripcion AS text"))
            ->where('Activo', true)
            ->whereIn('Aplica_a',[0,2])
            ->where(function($q) use ($term) {
                $q->where(function($query) use ($term){
                    $query->where('Descripcion','like',$term.'%');
                })
                ->orWhere(function($query) use ($term) {
                    $query->where('Codigo','like',$term.'%');
                });
            })
            ->orderBy('Codigo')
            ->skip($offset)
            ->take($resultCount)
            ->get();
                        
            $count =   DB::connection('EXPOTEC_STOCK')
            ->table('ARTICULOS')
            ->where('Activo', true)
            ->whereIn('Aplica_a',[0,2])
            ->where(function($q) use ($term) {
                $q->where(function($query) use ($term){
                    $query->where('Descripcion','like',$term.'%');
                })
                ->orWhere(function($query) use ($term) {
                    $query->where('Codigo','like',$term.'%');
                });
            })
            ->count();
            
            $endCount = $offset + $resultCount;

            if($count < 25)
                $morePages = false;
            else
                $morePages = $endCount < $count;

            $results = array(
              "results"     => $breeds,
              "pagination"  => array(
                "more" => $morePages
              )
            );
        } catch (Exception $ex) {
            return response()->json($ex->getMessage(),500);
        }
           
            
        return response()->json($results,200);
    }
});

Route::get('/get-planta/', function (Request  $request) {
    $term = $request->q ?: '';
    $planta = $request->planta;
    $plantas = Planta::select("id","descripcion")
            ->when($term, function($q) use($term){
                return $q->where('descripcion','like',$term.'%')->orWhere('nomenclatura','like',$term.'%');
            })
            ->get();
    $total_count = count($plantas);
    
    if($total_count == 0)
        $incomplete_results = true;
    else
        $incomplete_results = false;
    
    $items = [];
    foreach ($plantas as $id => $p) {
        $items[] = ['id' => $p["id"], 'text' => $p["descripcion"]];
    }
    $respuesta = array();
    $respuesta["total_count"] = $total_count;
    $respuesta["incomplete_results"] = $incomplete_results;
    $respuesta["items"] = $items;
   
    return \Response::json($respuesta);
});

Route::get('/get-chacra/', function (Request  $request) {
    $term = $request->q ?: '';
    $planta = $request->planta;
    $chacras =  ACA_Chacra::select("id","codigo","descripcion")
                ->where('id_bd', $planta)
                ->when($term, function($q) use($term){
                    return $q->where('descripcion','like',$term.'%')->orWhere('codigo','like',$term.'%');
                })
                ->get();
    $total_count = count($chacras);
    
    if($total_count == 0)
        $incomplete_results = true;
    else
        $incomplete_results = false;
    
    $items = [];
    foreach ($chacras as $id => $p) {
        $items[] = ['id' => $p["id"], 'text' => $p["codigo"] ." - ". $p["descripcion"]];
    }
    $respuesta = array();
    $respuesta["total_count"] = $total_count;
    $respuesta["incomplete_results"] = $incomplete_results;
    $respuesta["items"] = $items;
   
    return \Response::json($respuesta);
});

Route::get('/get-proveedor/', function (Request  $request) {
    
    if ($request->ajax()){
        
        try {
            $page = Input::get('page');
            $resultCount = 25;
            $term = Input::get("q");
            $id_bd = $request->planta;

            $offset = ($page - 1) * $resultCount;

            $breeds =   DB::connection('EXPOTEC_COMPRAS')
            ->table('PROVEEDORES')
            ->select('ID as id',DB::raw("Codigo+' - '+Razon_Social AS text"))
            ->where('Activo', true)
            ->whereIn('Aplica_a',[0,2])
            ->where(function($q) use ($term) {
                $q->where(function($query) use ($term){
                    $query->where('Razon_Social','like',$term.'%');
                })
                ->orWhere(function($query) use ($term) {
                    $query->where('Codigo','like',$term.'%');
                });
            })
            ->orderBy('Razon_Social')
            ->skip($offset)
            ->take($resultCount)
            ->get();
                        
            $count =   DB::connection('EXPOTEC_COMPRAS')
            ->table('PROVEEDORES')
            ->where('Activo', true)
            ->whereIn('Aplica_a',[0,2])
            ->where(function($q) use ($term) {
                $q->where(function($query) use ($term){
                    $query->where('Razon_Social','like',$term.'%');
                })
                ->orWhere(function($query) use ($term) {
                    $query->where('Codigo','like',$term.'%');
                });
            })
            ->count();

            $endCount = $offset + $resultCount;

            if($count < 25)
                $morePages = false;
            else
                $morePages = $endCount < $count;

            $results = array(
              "results" => $breeds,
              "pagination" => array(
                "more" => $morePages
              )
            );
        } catch (Exception $ex) {
            return response()->json($ex->getMessage(),500);
        }
           
            
        return response()->json($results,200);
    }
});

