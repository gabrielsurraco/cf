<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

//Route::get('/balanza', function() {
//    return view('balanza.index_balanza');
//});

//AJAX route
//Route::post('/balanza', 'BalanzaController@index');

Route::get('/home', 'HomeController@index');

Route::get('/actualizando', 'HomeController@actualizando');

Route::post('/sincronizar/base', ['uses' => 'SincronizarController@sincronizarBase']);

Route::post('sincronizar/aca', [
    'as' => 'sincronizar.aca', 'uses' => 'SincronizarController@sincronizaraca'
]);

Route::post('sincronizar/cg', [
    'as' => 'sincronizar.cg', 'uses' => 'SincronizarController@sincronizarcg'
]);

Route::post('sincronizar/cv', [
    'as' => 'sincronizar.cv', 'uses' => 'SincronizarController@sincronizarcv'
]);

Route::post('sincronizar/obso', [
    'as' => 'sincronizar.obso', 'uses' => 'SincronizarController@sincronizarobso'
]);

Route::post('sincronizar/ta', [
    'as' => 'sincronizar.ta', 'uses' => 'SincronizarController@sincronizarta'
]);

Route::post('sincronizar/full', [
    'as' => 'sincronizar.full', 'uses' => 'SincronizarController@sincronizarFull'
]);

Route::get('/sincronizar', ['as' => 'sincronizar','uses' => 'SincronizarController@index']);

Route::get('/secaderos/tv_chip', ['as' => 'secaderos.tv_chip','uses' => 'Tv_chipController@index']);

Route::post('/secaderos/buscar', ['as' => 'secaderos.buscar','uses' => 'Control_hvController@buscar']);

Route::get('/secaderos/control_hv', ['as' => 'secaderos.control_hv','uses' => 'Control_hvController@index']);

Route::get('/tools/format-file', ['as' => 'tools.format-file','uses' => 'ToolsController@index']);

Route::post('/secaderos/control_hv', ['as' => 'secaderos.control_hv','uses' => 'Control_hvController@buscar']);


Route::group(['prefix' => 'reportes'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/secaderos', ['as' => 'reportes.secaderos', 'uses' => 'SecaderosController@index']);
        Route::get('/brotes', ['as' => 'reportes.brotes', 'uses' => 'BrotesController@index']);
        Route::post('/seca', 'SecaderosController@buscar');
        Route::post('/brotes', 'BrotesController@brotes');
        Route::get('/brotes/pdf', 'BrotesController@exportarpdf');
        Route::get('/stock', 'BrotesController@stock');
        Route::post('/stock', 'BrotesController@getStock');
        Route::get('/stock/pdf', 'BrotesController@stockApdf');
        
    });
});

Route::group(['middleware' => 'auth'], function () {
    /* Usuarios */
    Route::get('/usuarios', ['as' => 'usuarios', 'uses' => 'UsuariosController@indexUsuarios']);
    Route::post('/usuarios/alta', ['as' => 'usuarios', 'uses' => 'UsuariosController@crearUsuario']);
    Route::post('/usuarios/eliminar', ['as' => 'usuarios', 'uses' => 'UsuariosController@eliminarUsuario']);
    Route::get('/usuarios/{id}/editar', ['as' => 'usuarios.editar', 'uses' => 'UsuariosController@editarUsuario']);
    Route::post('/usuarios/guardar', ['as' => 'usuarios.guardar', 'uses' => 'UsuariosController@guardarUsuario']);
    Route::post('/usuarios/get-roles', ['as' => 'usuarios.getroles', 'uses' => 'UsuariosController@getRolesUsuario']);
    Route::post('/usuarios/get-permisos', ['as' => 'usuarios.getpermisos', 'uses' => 'UsuariosController@getPermisosUsuario']);
    Route::post('/usuarios/eliminar-permiso', [ 'uses' => 'UsuariosController@eliminarPermisoUsuario']);
    Route::post('/usuarios/eliminar-rol', [ 'uses' => 'UsuariosController@eliminarRolUsuario']);
    Route::post('/usuarios/agregar-permiso', [ 'uses' => 'UsuariosController@agregarPermisoUsuario']);
    Route::post('/usuarios/agregar-rol', [ 'uses' => 'UsuariosController@agregarRolUsuario']);
    Route::get('/roles-y-permisos', ['as' => 'roles.y.permisos', 'uses' => 'UsuariosController@indexRolesPermisos']);
    Route::post('/roles-y-permisos/eliminar-rol', [ 'uses' => 'UsuariosController@eliminarRol']);
    Route::post('/roles-y-permisos/eliminar-permiso', [ 'uses' => 'UsuariosController@eliminarPermiso']);
    Route::post('/roles-y-permisos/crear-rol', [ 'uses' => 'UsuariosController@crearRol']);
    Route::post('/roles-y-permisos/editar-rol', [ 'uses' => 'UsuariosController@editarRol']);
    Route::post('/roles-y-permisos/crear-permiso', [ 'uses' => 'UsuariosController@crearPermiso']);
    Route::post('/roles-y-permisos/editar-permiso', [ 'uses' => 'UsuariosController@editarPermiso']);
    Route::post('/roles-y-permisos/get-permissions-of-role', [ 'uses' => 'UsuariosController@getPermissionsOfRole']);
    Route::post('/roles-y-permisos/delete-permissions-of-role', [ 'uses' => 'UsuariosController@deletePermissionsOfRole']);
    Route::post('/roles-y-permisos/assign-permission-to-role', [ 'uses' => 'UsuariosController@assignPermissionToRole']);


    /* Balanza */
    Route::get('/balanza', ['as' => 'balanza', 'uses' => 'BalanzaController@index']);
    Route::post('/balanza/getpeso', ['as' => 'getpeso', 'uses' => 'BalanzaController@getPeso']);
    Route::post('/balanza/getembalaje', ['as' => 'getembalaje', 'uses' => 'BalanzaController@getEmbalaje']);
    Route::post('/balanza/gethumedad', ['as' => 'gethumedad', 'uses' => 'BalanzaController@getHumedad']);
    Route::get('/balanza/nuevo-pesaje', ['as' => 'balanza.add', 'uses' => 'BalanzaController@nuevoPesaje']);
    Route::post('/balanza/guardar-pesaje',['uses' => 'BalanzaController@storePesaje']);
    
    
    Route::post('/home/datosgraficos',['uses' => 'HomeController@datosGraficos']);
    
});
